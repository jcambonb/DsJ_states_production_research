# $D_{sJ}$ states production research at LHC$b$ in $pp$ collisions at $\sqrt{s}=13$ TeV

## Introduction

In the early 2000s, experiments such as BaBar, CLEO and Belle discovered excited states of the $D_{s}^+$ meson $[c\bar{s}]$ through radiative electromagnetic and strong decays. Particullary, the $`D_{s0}^*(2317)^+`$ and $D_{s1}(2460)^+$ states drew attention due to several qualities. On the one hand, their decays $`D_{s0}^*(2317)^+ \to D_s^+ \pi^0`$ and $`D_{s1}(2460)^+ \to D_s^{*+} \pi^0`$ do not respect isospin symmetry. Moreover, the direct de-excitations $`D_{s0}^*(2317)^+ \to D_s^+ \gamma`$ and $`D_{s1}(2460)^+ \to D_s^{*+} \gamma`$ are set to be forbidden according to the experimental results. And last but not least, their small widths and lower (but near) masses than the $D^0K^+$ and $D^*K^+$ threshold, make these hadrons as strong candidates to be exotic tetraquark-like particles.

This fact attracts great interest from a theoretical point of view and there are several models that try to explain the true nature of these particles. Some of them conceive these hadrons as partner $DK$ molecule states due to the fact that $m(D_{s1}^+)-m(D_{s0}^+)\simeq m(D^*)-m(D^0)$, while others describe them as compact tetraquark states. The key is that those natures can be tested by studying the production (number of events produced) of the particles as a function of various experimental observables, in particular the charged multiplicity, which is the number of charged particles produced in one event (collision).

Thus, this project will consist of experimentally studying the production of these hadrons as a function of these observables with the data obtained by CERN's LHCb experiment. We will use data from Run 2 (2016-2018) in proton-proton collisions at a centre-of-mass energy of 13 TeV, thus being the first study of this type carried out for $`D_{s0}^*(2317)^+`$ and $`D_{s1}(2460)^+`$. This repository includes all the scripts and codes designed to develop this analysis.


## Analysis strategy

The goal of the analysis is to obtain the production cross section of $D_{s0}^*(2317)^+$ and $D_{s1}(2460)^+$ through one of their decays as a function of some multiplicity proxy, which currently is set to be the nVeloTracks. In order to reduce systematic uncertanties and cancel efficiencies, instead of using directly the production cross section, we will obtain experimentally, as function of nVeloTracks, the ratio between the production of the particle under study and the production of some known particle which decays to the same final state. So, calling $X$ to the particle under study and $Y$ to the reference particle, the ratio can be expresed like:

```math
\frac{\sigma_{X}\mathcal{B}_{X}}{\sigma_{Y}\mathcal{B}_{Y}}=\frac{N_{X}f_{\mathrm{prompt}}^{X}}{N_{Y}f_{\mathrm{prompt}}^{Y}}\frac{\varepsilon^{\mathrm{sel}}_{Y}}{\varepsilon^{\mathrm{sel}}_{X}}\frac{\varepsilon^{\mathrm{reco}}_{Y}}{\varepsilon^{\mathrm{reco}}_{X}}\frac{\varepsilon^{\mathrm{acc}}_{Y}}{\varepsilon^{\mathrm{acc}}_{X}}
```

Where $`N`$ are the yields, $`\varepsilon`$ the efficiencies and $`f`$ the prompt fractions. So, the strategy is to compute correctly all the terms one by one and merged them to calculate the ratio for each bin of nVeloTracks. This will result in a experimental curve which can be compared to the resulting theoretical curves for each model. To calculate the ratio we choose:

* For the $D_{s0}^*(2317)^+$:
    * As $X$ we choose the $D_{s0}^*(2317)^+ \to D_s^+(\to K^+ K^- \pi^+) \pi^0(\to \gamma \gamma)$ decay
    * As $Y$ we choose the $D_{s}^{*+} \to D_s^+(\to K^+ K^- \pi^+) \pi^0(\to \gamma \gamma)$ decay

* For the $D_{s1}(2460)^+$:
    * As $X$ we choose the $D_{s1}(2460)^+ \to D_s^+(\to K^+ K^- \pi^+) \gamma$ decay
    * As $Y$ we choose the $D_{s}^{*+} \to D_s^+(\to K^+ K^- \pi^+) \gamma$ decay

Finally, the $D_{s1}(2460)^+ \to D_s^{*+} (\to D_s^+(\to K^+ K^- \pi^+) \gamma) \pi^0(\to \gamma \gamma)$ will appear as a typical peaking background in both studies. Due to the photon measurement properties in LHCb, the presence of these background will be one of them most challenger and important tasks of the analysis.

## Requirements

In order to run the codes a installation of python with ROOT is required. For LHC$`b`$ members, the lb-conda environment has all the needed packages for the analysis. Most of the codes of the project are .ipynb, so is needed an editor which allows to modify them (jupyter notebook or VSCode), and a python environment which can be used as kernel. To run the codes, the following packages are required:

1. ROOT
2. numpy
3. matplotlib.pyplot
4. pandas
5. uncertanties

Moreover, in order to improve our work with PyROOT, several python modules with functions and classes were created and are used in the analysis. They can be found in the **Python Modules** repository and the download is mandatory to not change all the scripts. 

The data used are the CHARM nDST stream of Run2. Moreover, at the day of commiting, we have a little official LHCb MonteCarlo sample for prompt $`D_{s0}^*(2317)^+ \to D_s^+(\to K^+ K^- \pi^+) \pi^0(\to \gamma \gamma)`$, $`D_{s1}(2460)^+ \to D_s^{*+} (\to D_s^+(\to K^+ K^- \pi^+) \gamma) \pi^0(\to \gamma \gamma)`$ and $`D_{s}^{*+} \to D_s^+(\to K^+ K^- \pi^+) \gamma`$ decays. In order to create the corresponding nTuples for the analysis, the **Ganga and DaVinci Scripts** repository have the DaVinci and Ganga scripts designed for that.



