import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
# data reading
#--------------------------------------------------------
ti = datetime.now()

head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy"
mc_path = f"{head_path}/Reduced/MC/TightCut/raw"

decays = ["Ds1DsGamma", "DsstDsGamma", "DsstDsPi0", "Ds0DsPi0", "Ds1DsstPi0"]
files = [f"{dec}*.root" for dec in decays]

Ds1Dsg_files     = "Ds1DsGamma*.root"
DsstDsg_files    = "DsstDsGamma*.root"
DsstDspiz_files  = "DsstDsPi0*.root"
Ds0Dspiz_files   = "Ds0DsPi0*.root"
Ds1Dsstpiz_files = "Ds1DsstPi0*.root"

dtt = "DsGammaTuple"

derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

tdf_MC = {dec: ROOT.RDataFrame(f"DecayTree", f"{mc_path}/{f}") for dec, f in zip(decays, files)}

tdf_aux = []
for dec, tdf in tdf_MC.items():
    for feat, form in derivated_features.items():
        tdf = tdf.Define(feat, form) 
    tdf_aux.append(tdf)

tdf_MC = {dec: tdf for dec, tdf in zip(decays, tdf_aux)}
#--------------------------------------------------------

# evt selection + cand selection + trigger conditions
#--------------------------------------------------------
evt_sel_file = open("../Selection/param_files/evt_selection.txt", "r")
evt_sel = evt_sel_file.read()
evt_sel_file.close()

cand_sel_file = open("../Selection/param_files/cand_selection.txt", "r")
cand_sel = cand_sel_file.read()
cand_sel_file.close()

trig_cond_file = open("../Selection/param_files/trig_selection.txt", "r")
trig_cond = trig_cond_file.read()
trig_cond_file.close()

sel = f"{evt_sel} && {cand_sel} && {trig_cond}"
#-------------------------------------------------------

# Combinatorial background
#--------------------------------------------------------
Ds1DsGamma_bkg_ID = "abs(gamma_MC_MOTHER_ID) != 20433"
DsstDsGamma_bkg_ID = "abs(gamma_MC_MOTHER_ID) != 433"
Ds0DsPi0_bkg_ID   = "abs(gamma_MC_GD_MOTHER_ID) != 10431"
Ds1DsstPi0_bkg_ID = "abs(gamma_MC_GD_MOTHER_ID) != 20433"
DsstDsPi0_bkg_ID = "abs(gamma_MC_GD_MOTHER_ID) != 433"

bkg_IDs = [Ds1DsGamma_bkg_ID, DsstDsGamma_bkg_ID, DsstDsPi0_bkg_ID, 
           Ds0DsPi0_bkg_ID, Ds1DsstPi0_bkg_ID]
    
tdf_bkg = {dec: tdf.Filter(ID) for ((dec, tdf), ID) in zip(tdf_MC.items(), bkg_IDs)}
tdf_bkg_sel = {dec: tdf.Filter(sel) for dec, tdf in tdf_bkg.items()}

hmc_Dsg_mass_bkg = {dec: tdf.Histo1D(("","",100,2000,3000), "DsgM") 
                    for dec, tdf in tdf_bkg.items()}

hmc_Dsg_mass_bkg_sel = {dec: tdf.Histo1D(("","",100,2000,3000), "DsgM") 
                        for dec, tdf in tdf_bkg_sel.items()}

colors = [4, 2, ROOT.kYellow+3, 1, 3]
Dsg_mass_label = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
ylabel = "A.U / (7 MeV/c^{2})"

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.3, 0.9, 0.6)
hs = []
for dec, color in zip(decays, colors):
    h = hmc_Dsg_mass_bkg[dec]
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, ylabel=ylabel, norm=True)
    hs.append(h)
rsh.legend_plot(lgd, [h.GetPtr() for h in hs], decays, ["l" for h in hs])
c1.Draw()
c1.SaveAs("./plots/MC_Dsg_mass_bkg.pdf")

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.3, 0.9, 0.6)
hs = []
for dec, color in zip(decays, colors):
    h = hmc_Dsg_mass_bkg_sel[dec]
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, ylabel=ylabel, norm=True)
    hs.append(h)
rsh.legend_plot(lgd, [h.GetPtr() for h in hs], decays, ["l" for h in hs])
c1.Draw()
c1.SaveAs("./plots/MC_Dsg_mass_bkg_sel.pdf")
#--------------------------------------------------------

# Peaking backgrounds
#--------------------------------------------------------
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

tdf_matched = {dec: tdf.Filter(TRUEID_dtt[dec]) for dec, tdf in tdf_MC.items()}
tdf_sel = {dec: tdf.Filter(sel) for dec, tdf in tdf_matched.items()}

hmc_Dsg_mass_raw = {dec: tdf.Histo1D(("","",100,2000,2700), "DsgM") 
                    for dec, tdf in tdf_matched.items()}
hmc_Dsg_mass_sel = {dec: tdf.Histo1D(("","",100,2000,2700), "DsgM") 
                    for dec, tdf in tdf_sel.items()}

## Dsst -> Dspiz decay study
tdf_Dsst_prb = tdf_matched["DsstDsPi0"]; tdf_Dsst_prb_sel = tdf_Dsst_prb.Filter(sel)

hmc_Dsg_mass_Dsst_prb = tdf_Dsst_prb.Histo1D(("","",100,2000,2700), "DsgM")
hmc_Dsg_mass_Dsst_prb_sel = tdf_Dsst_prb_sel.Histo1D(("","",100,2000,2700), "DsgM")

eff_sel = hmc_Dsg_mass_Dsst_prb_sel.Integral() / hmc_Dsg_mass_Dsst_prb.Integral()
ueff_sel = np.sqrt((eff_sel*(1-eff_sel))/hmc_Dsg_mass_Dsst_prb.Integral())

print(f"Offline selection efficiency for Dsst -> Ds piz: {eff_sel} +/- {ueff_sel}")

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.5, 0.6, 0.9, 0.9)
rsh.TH1D_plot(hmc_Dsg_mass_Dsst_prb, color=5, xlabel=Dsg_mass_label)
rsh.TH1D_plot(hmc_Dsg_mass_Dsst_prb_sel, color=ROOT.kYellow+3, xlabel=Dsg_mass_label)
rsh.legend_plot(lgd,
                [hmc_Dsg_mass_Dsst_prb.GetPtr(), hmc_Dsg_mass_Dsst_prb_sel.GetPtr()],
                ["Raw Dsst -> Ds piz", "sel Dsst -> Ds piz"],
                ["l", "l"])
c1.SaveAs("./plots/MC_Dsg_mass_Dsst_prb.pdf")

## Ds0 and Ds1 partially reconstructed backgrounds
gammas_from_Dsst = "abs(gamma_MC_MOTHER_ID) == 433"; gammas_from_piz = "abs(gamma_MC_MOTHER_ID) == 111"

tdf_Ds0_prb = tdf_matched["Ds0DsPi0"]; tdf_Ds0_prb_sel = tdf_Ds0_prb.Filter(sel)
tdf_Ds1_prb = tdf_matched["Ds1DsstPi0"]; tdf_Ds1_prb_sel = tdf_Ds1_prb.Filter(sel)

tdf_Ds1_prb_st = tdf_Ds1_prb.Filter(gammas_from_Dsst)
tdf_Ds1_prb_piz = tdf_Ds1_prb.Filter(gammas_from_piz)

tdf_Ds1_prb_st_sel  = tdf_Ds1_prb_st.Filter(sel)
tdf_Ds1_prb_piz_sel = tdf_Ds1_prb_piz.Filter(sel)

r_stpiz  = tdf_Ds1_prb_st.Count().GetValue() / tdf_Ds1_prb_piz.Count().GetValue()
ur_stpiz = np.sqrt((r_stpiz*(r_stpiz))/tdf_Ds1_prb_piz.Count().GetValue())

r_stpiz_sel = tdf_Ds1_prb_st_sel.Count().GetValue() / tdf_Ds1_prb_piz_sel.Count().GetValue()
ur_stpiz_sel = np.sqrt((r_stpiz_sel*(r_stpiz_sel))/tdf_Ds1_prb_piz_sel.Count().GetValue())

print(f"Ratio Dsst/piz contribution in Ds1 -> Dsst piz decay raw: {r_stpiz} +/- {ur_stpiz}")
print(f"Ratio Dsst/piz contribution in Ds1 -> Dsst piz decay sel: {r_stpiz_sel} +/- {ur_stpiz_sel}")

tdf_DsJ_to_piz = [tdf_Ds0_prb, tdf_Ds1_prb,
                  tdf_Ds1_prb_st, tdf_Ds1_prb_piz]

tdf_DsJ_to_piz_sel = [tdf_Ds0_prb_sel, tdf_Ds1_prb_sel,
                      tdf_Ds1_prb_st_sel, tdf_Ds1_prb_piz_sel]

hmc_Dsg_mass_DsJ_to_piz = [tdf.Histo1D(("","",100,2000,2700), "DsgM") for tdf in tdf_DsJ_to_piz]

hmc_Dsg_mass_DsJ_to_piz_sel = [tdf.Histo1D(("","",100,2000,2700), "DsgM") for tdf in tdf_DsJ_to_piz_sel]

colors_4 = [1, 3, ROOT.kOrange, ROOT.kMagenta]
names_4 = ["Ds0DsPi0", "Ds1DsstPi0", "Dsst gamma for Ds1DsstPi0", "pi0 gamma for Ds1DsstPi0"]

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
for h, color in zip(hmc_Dsg_mass_DsJ_to_piz, colors_4):
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, max_val=30e4, ylabel=ylabel)
rsh.legend_plot(lgd,
                [h.GetPtr() for h in hmc_Dsg_mass_DsJ_to_piz],
                names_4,
                ["l" for n in names_4])
c1.SaveAs("./plots/MC_Dsg_mass_DsJ_to_piz_raw.pdf")

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
for h, color in zip(hmc_Dsg_mass_DsJ_to_piz_sel, colors_4):
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.legend_plot(lgd,
                [h.GetPtr() for h in hmc_Dsg_mass_DsJ_to_piz_sel],
                names_4,
                ["l" for n in names_4])
c1.SaveAs("./plots/MC_Dsg_mass_DsJ_to_piz_sel.pdf")

## All relevant decays all together in the same plot

decays_4 = ["DsstDsGamma", "Ds1DsGamma", "Ds0DsPi0", "Ds1DsstPi0"]
colors_4 = [2, 4, 1, 3]

c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
hs = []
for dec, color in zip(decays_4, colors_4):
    h = hmc_Dsg_mass_raw[dec]
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, ylabel=ylabel, norm=True)
    hs.append(h)
rsh.legend_plot(lgd, [h.GetPtr() for h in hs], decays, ["l" for h in hs])
c1.Draw()
c1.SaveAs("./plots/MC_Dsg_mass_for_all_decay_raw.pdf")

c2 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
hs = []
for dec, color in zip(decays_4, colors_4):
    h = hmc_Dsg_mass_sel[dec]
    rsh.TH1D_plot(h, color=color, xlabel=Dsg_mass_label, ylabel=ylabel, norm=True)
    hs.append(h)
rsh.legend_plot(lgd, [h.GetPtr() for h in hs], decays, ["l" for h in hs])
c2.Draw()
c2.SaveAs("./plots/MC_Dsg_mass_for_all_decay_sel.pdf")

tf = datetime.now()

print(f"Execution time = {tf-ti}")
#--------------------------------------------------------