from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
pol  = options.polarity
year = options.year
run2 = options.run2

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced"
mc_path = f"{head_path}/MC/TightCut/sel_truth"

Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

if run2:
    tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/{DsstDsg_files}")
    tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/{Ds1Dsg_files}")
else:   
    tdf_Ds1_sig  = ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_{pol}{year}*.root")
    tdf_Dsst_sig = ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_{pol}{year}*.root")
        
#--------------------------------------------------------

# Selection
#--------------------------------------------------------
tdf_Ds1_sig  = tdf_Ds1_sig.Filter("totCandidates2 == 1")
tdf_Dsst_sig = tdf_Dsst_sig.Filter("totCandidates2 == 1")

#tdf_Dsst_sig = tdf_Dsst_sig
#--------------------------------------------------------

# Signal events after selection: Nf
#--------------------------------------------------------

## Integrated Nch
df_Dsst_sig = rsh.rdf_to_pdf(tdf_Dsst_sig, columns=["nVeloTracks"])
df_Ds1_sig  = rsh.rdf_to_pdf(tdf_Ds1_sig, columns=["nVeloTracks"])

Nf_Dsst = un.ufloat(len(df_Dsst_sig["nVeloTracks"]), np.sqrt(len(df_Dsst_sig["nVeloTracks"])))
Nf_Ds1  = un.ufloat(len(df_Ds1_sig["nVeloTracks"]), np.sqrt(len(df_Ds1_sig["nVeloTracks"])))

print(f"Final signal events for Dsst in {pol} {year} sample = {Nf_Dsst}")
print(f"Final signal events for Ds1  in {pol} {year} sample = {Nf_Ds1}")

## binning of nVeloTracks
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")
ninters = len(nVelo_intervals) - 1

Ds1_vals_file = f"../../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"

df_Ds1_fit_vals = pd.read_csv(Ds1_vals_file)

nVelo_means = df_Ds1_fit_vals["nVelo_mean"]
nVelo_up    = df_Ds1_fit_vals["nVelo_uup"]
nVelo_down  = df_Ds1_fit_vals["nVelo_udown"]

mNf_nVelo_Dsst, nVelo_bins = np.histogram(df_Dsst_sig["nVeloTracks"], bins=nVelo_intervals)
mNf_nVelo_Ds1, nVelo_bins  = np.histogram(df_Ds1_sig["nVeloTracks"], bins=nVelo_intervals)

uNf_nVelo_Dsst = np.sqrt(mNf_nVelo_Dsst); uNf_nVelo_Ds1 = np.sqrt(mNf_nVelo_Ds1)

Nf_nVelo_Dsst = np.array([un.ufloat(m, u) for m, u in zip(mNf_nVelo_Dsst, uNf_nVelo_Dsst)])
Nf_nVelo_Ds1  = np.array([un.ufloat(m, u) for m, u in zip(mNf_nVelo_Ds1,  uNf_nVelo_Ds1)])

plt.figure(1)
plt.errorbar(nVelo_means, mNf_nVelo_Dsst, uNf_nVelo_Dsst, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label=r"$D_s^{*+} \to D_s^+ \gamma$ MC")
plt.errorbar(nVelo_means, mNf_nVelo_Ds1, uNf_nVelo_Ds1, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label=r"$D_{s1}(2460)^+ \to D_s^+ \gamma$ MC")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Number of signal events")
plt.title("MC events after all selection procedure")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_Nf_vs_nVelo_{ninters}_bins.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

# Total efficiency computation
#--------------------------------------------------------

## Retation rate
df_bkk_Dsst = pd.read_csv("./param_files/mc_ret_rate_values_Dsst.csv")
df_bkk_Ds1  = pd.read_csv("./param_files/mc_ret_rate_values_Ds1.csv")

df_bkk_Dsst = df_bkk_Dsst.query(f'Sample == "{pol}{year}"')
df_bkk_Ds1  = df_bkk_Ds1.query(f'Sample == "{pol}{year}"')

Nbkk_Dsst = un.ufloat(float(df_bkk_Dsst["Nbkk"]), np.sqrt(float(df_bkk_Dsst["Nbkk"])))
Nbkk_Ds1  = un.ufloat(float(df_bkk_Ds1["Nbkk"]),  np.sqrt(float(df_bkk_Ds1["Nbkk"])))

Npfil_Dsst = un.ufloat(float(df_bkk_Dsst["Nprefilt"]), np.sqrt(float(df_bkk_Dsst["Nprefilt"])))
Npfil_Ds1  = un.ufloat(float(df_bkk_Ds1["Nprefilt"]),  np.sqrt(float(df_bkk_Ds1["Nprefilt"])))

ret_rate_Dsst = Nbkk_Dsst / Npfil_Dsst
ret_rate_Ds1  = Nbkk_Ds1 / Npfil_Ds1

print(f"ret rate for Dsst MC in {pol} {year} sample = {ret_rate_Dsst}")
print(f"ret rate for Ds1 MC in {pol} {year} sample  = {ret_rate_Ds1}")

## Generator level efficiency
mc_stats = pd.read_html(f"./html_files/IFT_Generation_Sim10-Beam6500GeV-{year}-{pol}-Nu1.6-25ns-Pythia8.html")

# mc_stats[7] corresponds to the particle-antiparticle statistics
particle_stats = mc_stats[7]

if "D*_s+" in (particle_stats[2])[0]: 
    i_Ds1 = 2; i_Dsst = 6
if "D_s1(2460)+" in (particle_stats[2])[0]:
    i_Ds1 = 6; i_Dsst = 2

gen_stats_Ds1 = mc_stats[i_Ds1]; gen_stats_Dsst = mc_stats[i_Dsst]

gen_cut_info_Dsst = np.array(gen_stats_Dsst[0])
gen_cut_info_Ds1  = np.array(gen_stats_Ds1[0])

meff_gen_Dsst = float(gen_cut_info_Dsst[1])
meff_gen_Ds1  = float(gen_cut_info_Ds1[1])

ueff_gen_Dsst = float(gen_cut_info_Dsst[2].replace('± ', ''))
ueff_gen_Ds1  = float(gen_cut_info_Ds1[2].replace('± ', ''))

eff_gen_Dsst = un.ufloat(meff_gen_Dsst, ueff_gen_Dsst)
eff_gen_Ds1  = un.ufloat(meff_gen_Ds1,  ueff_gen_Ds1)

print(f"Generator level cut eff for Dsst MC in {pol} {year} sample = {eff_gen_Dsst}")
print(f"Generator level cut eff for Ds1 MC in {pol} {year} sample  = {eff_gen_Ds1}")

## Generated events
Ngen_Dsst = Nbkk_Dsst / (ret_rate_Dsst * eff_gen_Dsst); 
Ngen_Ds1  = Nbkk_Ds1 / (ret_rate_Ds1 * eff_gen_Ds1); 

eff_tot_Dsst = Nf_Dsst / Ngen_Dsst
eff_tot_Ds1 = Nf_Ds1 / Ngen_Ds1

print(f"Total efficiency for Dsst in {pol} {year} sample = {eff_tot_Dsst}")
print(f"Total efficiency for Ds1  in {pol} {year} sample = {eff_tot_Ds1}")

eff_tot_nVelo_Dsst = Nf_nVelo_Dsst / np.array([Ngen_Dsst for n in Nf_nVelo_Dsst])
eff_tot_nVelo_Ds1  = Nf_nVelo_Ds1 / np.array([Ngen_Ds1 for n in Nf_nVelo_Dsst])

meff_tot_nVelo_Dsst = np.array([eff.nominal_value for eff in eff_tot_nVelo_Dsst])
meff_tot_nVelo_Ds1  = np.array([eff.nominal_value for eff in eff_tot_nVelo_Ds1])

ueff_tot_nVelo_Dsst = np.array([eff.std_dev for eff in eff_tot_nVelo_Dsst])
ueff_tot_nVelo_Ds1  = np.array([eff.std_dev for eff in eff_tot_nVelo_Ds1])

plt.figure(2)
plt.errorbar(nVelo_means, meff_tot_nVelo_Dsst, ueff_tot_nVelo_Dsst, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label=r"$D_s^{*+} \to D_s^+ \gamma$ MC")
plt.errorbar(nVelo_means, meff_tot_nVelo_Ds1, ueff_tot_nVelo_Ds1, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label=r"$D_{s1}(2460)^+ \to D_s^+ \gamma$ MC")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Number of signal events")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_tot_effs_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

# efficiency ratio computation
#--------------------------------------------------------
ratio_eff_tot  = eff_tot_Dsst / eff_tot_Ds1

print(f"Total efficiency ratio in {pol} {year} sample = {ratio_eff_tot}")

# Format the value with uncertainty

reff_tot = {"val": ratio_eff_tot.nominal_value,
            "unc": float(ratio_eff_tot.std_dev)}

with open(f"./param_files/Sim_{pol}{year}_eff_tot_ratio_fst_order.yaml", "w") as file:
    yaml.dump(reff_tot, file, default_flow_style=False)

ratio_eff_tot_nVelo  = eff_tot_nVelo_Dsst / eff_tot_nVelo_Ds1

mratio_eff_tot_nVelo = np.array([r.nominal_value for r in ratio_eff_tot_nVelo])
uratio_eff_tot_nVelo = np.array([r.std_dev for r in ratio_eff_tot_nVelo])

plt.figure(3)
plt.errorbar(nVelo_means, mratio_eff_tot_nVelo, uratio_eff_tot_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label=f"{pol}{year}")
plt.title(f"MC Total efficiency ratio {pol} {year}")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{T}(D_s^{*+})/\varepsilon_{T}(D_{s1}^+)$")
plt.title(f"MC Total efficiency {pol} {year}")
plt.savefig(f"./plots/Sim_Both_{pol}{year}_tot_effs_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(4)
plt.errorbar(nVelo_means, uratio_eff_tot_nVelo/mratio_eff_tot_nVelo, yerr=0, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label=f"{pol}{year}")
plt.title("MC Total efficiency ratio")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Relative uncertainty")
plt.title(f"MC Total efficiency {pol} {year}")
plt.savefig(f"./plots/Sim_Both_{pol}{year}_tot_effs_ratio_ru_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

np.savetxt(f"./param_files/Sim_{pol}{year}_eff_tot_ratio_vals_{ninters}_nVelo_fst_order.txt", mratio_eff_tot_nVelo)
np.savetxt(f"./param_files/Sim_{pol}{year}_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order.txt", uratio_eff_tot_nVelo)
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")