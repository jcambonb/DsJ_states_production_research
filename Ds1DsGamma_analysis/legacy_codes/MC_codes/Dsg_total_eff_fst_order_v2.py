from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# options for polarity: MagDown, MagUp, Both
pol  = options.polarity
year = options.year
run2 = options.run2

# data reading
#--------------------------------------------------------

head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced"

mc_raw_path = f"{head_path}/MC/TightCut/raw"
mc_sel_path = f"{head_path}/MC/TightCut/sel_truth"

dtt = "DsGammaTuple"

if run2:
    tdf_Dsst_mc_gen = ROOT.RDataFrame("MCDecayTree", f"{mc_raw_path}/DsstDsGamma*_gen.root")
    tdf_Dsst_mc_raw = ROOT.RDataFrame("DecayTree", f"{mc_raw_path}/DsstDsGamma*_raw.root")
    tdf_Dsst_mc_sel = ROOT.RDataFrame("DecayTree", f"{mc_sel_path}/DsstDsGamma*.root")
    
    tdf_Ds1_mc_gen = ROOT.RDataFrame("MCDecayTree", f"{mc_raw_path}/Ds1DsGamma*_gen.root")
    tdf_Ds1_mc_raw = ROOT.RDataFrame("DecayTree", f"{mc_raw_path}/Ds1DsGamma*_raw.root")
    tdf_Ds1_mc_sel = ROOT.RDataFrame("DecayTree", f"{mc_sel_path}/Ds1DsGamma*.root")
else:   
    tdf_Dsst_mc_gen = ROOT.RDataFrame("MCDecayTree", f"{mc_raw_path}/DsstDsGamma_{pol}{year}*_gen.root")
    tdf_Dsst_mc_raw = ROOT.RDataFrame("DecayTree", f"{mc_raw_path}/DsstDsGamma_{pol}{year}*_raw.root")
    tdf_Dsst_mc_sel = ROOT.RDataFrame("DecayTree", f"{mc_sel_path}/DsstDsGamma_{pol}{year}*.root")
    
    tdf_Ds1_mc_gen = ROOT.RDataFrame("MCDecayTree", f"{mc_raw_path}/Ds1DsGamma_{pol}{year}*_gen.root")
    tdf_Ds1_mc_raw = ROOT.RDataFrame("DecayTree", f"{mc_raw_path}/Ds1DsGamma_{pol}{year}*_raw.root")
    tdf_Ds1_mc_sel = ROOT.RDataFrame("DecayTree", f"{mc_sel_path}/Ds1DsGamma_{pol}{year}*.root")
    
## Truth matching
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

TRUEID_Dsst = TRUEID_dtt["DsstDsGamma"]
TRUEID_Ds1  = TRUEID_dtt["Ds1DsGamma"]

tdf_Dsst_mc_gen_sig = tdf_Dsst_mc_gen.Filter("abs(gamma_MC_MOTHER_ID) == 433 && abs(Ds_MC_MOTHER_ID) == 433")
tdf_Dsst_mc_raw_sig = tdf_Dsst_mc_raw.Filter(TRUEID_Dsst)
tdf_Dsst_mc_sel_sig = tdf_Dsst_mc_sel.Filter(TRUEID_Dsst)

tdf_Ds1_mc_gen_sig = tdf_Ds1_mc_gen.Filter("abs(gamma_MC_MOTHER_ID) == 20433 && abs(Ds_MC_MOTHER_ID) == 20433")
tdf_Ds1_mc_raw_sig = tdf_Ds1_mc_raw.Filter(TRUEID_Ds1)
tdf_Ds1_mc_sel_sig = tdf_Ds1_mc_sel.Filter(TRUEID_Ds1)

## DataFrame conversion
df_Dsst_mc_gen_sig = rsh.rdf_to_pdf(tdf_Dsst_mc_gen_sig, columns=["nVeloTracks"])
df_Dsst_mc_raw_sig = rsh.rdf_to_pdf(tdf_Dsst_mc_raw_sig, columns=["nVeloTracks"])
df_Dsst_mc_sel_sig = rsh.rdf_to_pdf(tdf_Dsst_mc_sel_sig, columns=["nVeloTracks"])

df_Ds1_mc_gen_sig = rsh.rdf_to_pdf(tdf_Ds1_mc_gen_sig, columns=["nVeloTracks"])
df_Ds1_mc_raw_sig = rsh.rdf_to_pdf(tdf_Ds1_mc_raw_sig, columns=["nVeloTracks"])
df_Ds1_mc_sel_sig = rsh.rdf_to_pdf(tdf_Ds1_mc_sel_sig, columns=["nVeloTracks"])
#--------------------------------------------------------

# Total efficiency for integrated Nch
#========================================================

## Generator level effiency
mc_stats = pd.read_html(f"./html_files/IFT_Generation_Sim10-Beam6500GeV-{year}-{pol}-Nu1.6-25ns-Pythia8.html")

# mc_stats[7] corresponds to the particle-antiparticle statistics
particle_stats = mc_stats[7]

if "D*_s+" in (particle_stats[2])[0]: 
    i_Ds1 = 2; i_Dsst = 6
if "D_s1(2460)+" in (particle_stats[2])[0]:
    i_Ds1 = 6; i_Dsst = 2

gen_stats_Ds1 = mc_stats[i_Ds1]; gen_stats_Dsst = mc_stats[i_Dsst]

gen_cut_info_Dsst = np.array(gen_stats_Dsst[0])
gen_cut_info_Ds1  = np.array(gen_stats_Ds1[0])

meff_gen_Dsst = float(gen_cut_info_Dsst[1])
meff_gen_Ds1  = float(gen_cut_info_Ds1[1])

ueff_gen_Dsst = float(gen_cut_info_Dsst[2].replace('± ', ''))
ueff_gen_Ds1  = float(gen_cut_info_Ds1[2].replace('± ', ''))

eff_gen_Dsst = un.ufloat(meff_gen_Dsst, ueff_gen_Dsst)
eff_gen_Ds1  = un.ufloat(meff_gen_Ds1,  ueff_gen_Ds1)

print(f"Generator level cut eff for Dsst MC in {pol} {year} sample = {eff_gen_Dsst}")
print(f"Generator level cut eff for Ds1 MC in {pol} {year} sample  = {eff_gen_Ds1}")

## Filtering algorithm efficiency
df_bkk_Dsst = pd.read_csv("./param_files/mc_ret_rate_values_Dsst.csv")
df_bkk_Ds1  = pd.read_csv("./param_files/mc_ret_rate_values_Ds1.csv")

df_bkk_Dsst = df_bkk_Dsst.query(f'Sample == "{pol}{year}"')
df_bkk_Ds1  = df_bkk_Ds1.query(f'Sample == "{pol}{year}"')

Nbkk_Dsst = un.ufloat(float(df_bkk_Dsst["Nbkk"]), np.sqrt(float(df_bkk_Dsst["Nbkk"])))
Nbkk_Ds1  = un.ufloat(float(df_bkk_Ds1["Nbkk"]),  np.sqrt(float(df_bkk_Ds1["Nbkk"])))

Npfil_Dsst = un.ufloat(float(df_bkk_Dsst["Nprefilt"]), np.sqrt(float(df_bkk_Dsst["Nprefilt"])))
Npfil_Ds1  = un.ufloat(float(df_bkk_Ds1["Nprefilt"]),  np.sqrt(float(df_bkk_Ds1["Nprefilt"])))

ret_rate_Dsst = Nbkk_Dsst / Npfil_Dsst
ret_rate_Ds1  = Nbkk_Ds1 / Npfil_Ds1

print(f"ret rate for Dsst MC in {pol} {year} sample = {ret_rate_Dsst}")
print(f"ret rate for Ds1 MC in {pol} {year} sample  = {ret_rate_Ds1}")

print(f"Events in bkk for Dsst MC in {pol} {year} sample = {Nbkk_Dsst}")
print(f"Events in bkk for Ds1 MC in {pol} {year} sample = {Nbkk_Ds1}")
print(f"ratio of bookkeeping events for MC {pol} {year} sample = {Nbkk_Ds1/Nbkk_Dsst}")

## Stripping and reconstruction efficiency
Nstrip_Dsst = un.ufloat(len(df_Dsst_mc_raw_sig.index), np.sqrt(len(df_Dsst_mc_raw_sig.index)))
Nstrip_Ds1  = un.ufloat(len(df_Ds1_mc_raw_sig.index), np.sqrt(len(df_Ds1_mc_raw_sig.index)))

eff_strip_Dsst = Nstrip_Dsst / Nbkk_Dsst
eff_strip_Ds1  = Nstrip_Ds1 / Nbkk_Ds1

print(f"Stripping efficiency for Dsst MC in {pol} {year} sample = {eff_strip_Dsst}")
print(f"Stripping efficiency for Ds1 MC in {pol} {year} sample  = {eff_strip_Ds1}")

## Offline selection efficiency
Noff_Dsst = un.ufloat(len(df_Dsst_mc_sel_sig.index), np.sqrt(len(df_Dsst_mc_sel_sig.index)))
Noff_Ds1  = un.ufloat(len(df_Ds1_mc_sel_sig.index), np.sqrt(len(df_Ds1_mc_sel_sig.index)))

eff_off_Dsst = Noff_Dsst / Nstrip_Dsst
eff_off_Ds1  = Noff_Ds1 / Nstrip_Ds1

print(f"Final selected events for Dsst MC in {pol} {year} sample = {Noff_Dsst}")
print(f"Final selected events for Ds1 MC in {pol} {year} sample = {Noff_Ds1}")
print(f"ratio of final selected events for MC {pol} {year} sample = {Noff_Ds1/Noff_Dsst}")

print(f"Offline selection efficiency for Dsst MC in {pol} {year} sample = {eff_off_Dsst}")
print(f"Offline selection efficiency for Ds1 MC in {pol} {year} sample  = {eff_off_Ds1}")
print(f"ratio of final selected events for MC {pol} {year} sample = {Noff_Ds1/Noff_Dsst}")

## Total efficiency

eff_tot_Dsst = eff_gen_Dsst * ret_rate_Dsst * eff_strip_Dsst * eff_off_Dsst
eff_tot_Ds1  = eff_gen_Ds1 * ret_rate_Ds1 * eff_strip_Ds1 * eff_off_Ds1

ratio_eff_tot = eff_tot_Dsst / eff_tot_Ds1

print(f"Total efficiency for Dsst MC in {pol} {year} sample = {eff_tot_Dsst}")
print(f"Total efficiency for Ds1 MC in {pol} {year} sample  = {eff_tot_Ds1}")
print(f"Total efficiency ratio in {pol} {year} sample = {ratio_eff_tot}")

#========================================================

# Total efficiency vs nVeloTracks
#========================================================

## binning of nVeloTracks
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")
ninters = len(nVelo_intervals) - 1

Ds1_vals_file = f"../../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"

df_Ds1_fit_vals = pd.read_csv(Ds1_vals_file)

nVelo_means = df_Ds1_fit_vals["nVelo_mean"]
nVelo_up    = df_Ds1_fit_vals["nVelo_uup"]
nVelo_down  = df_Ds1_fit_vals["nVelo_udown"]

## Stripping and reconstruction efficiency
mNbkk_nVelo_Dsst, nVelo_bins = np.histogram(df_Dsst_mc_gen_sig["nVeloTracks"], bins=nVelo_intervals)
mNbkk_nVelo_Ds1, nVelo_bins = np.histogram(df_Ds1_mc_gen_sig["nVeloTracks"], bins=nVelo_intervals)

uNbkk_nVelo_Dsst = np.sqrt(mNbkk_nVelo_Dsst); uNbkk_nVelo_Ds1 = np.sqrt(mNbkk_nVelo_Ds1)

Nbkk_nVelo_Dsst = np.array([un.ufloat(m, u) for m, u in zip(mNbkk_nVelo_Dsst, uNbkk_nVelo_Dsst)])
Nbkk_nVelo_Ds1  = np.array([un.ufloat(m, u) for m, u in zip(mNbkk_nVelo_Ds1,  uNbkk_nVelo_Ds1)])

mNstrip_nVelo_Dsst, nVelo_bins = np.histogram(df_Dsst_mc_raw_sig["nVeloTracks"], bins=nVelo_intervals)
mNstrip_nVelo_Ds1, nVelo_bins = np.histogram(df_Ds1_mc_raw_sig["nVeloTracks"], bins=nVelo_intervals)

uNstrip_nVelo_Dsst = np.sqrt(mNstrip_nVelo_Dsst); uNstrip_nVelo_Ds1 = np.sqrt(mNstrip_nVelo_Ds1)

Nstrip_nVelo_Dsst = np.array([un.ufloat(m, u) for m, u in zip(mNstrip_nVelo_Dsst, uNstrip_nVelo_Dsst)])
Nstrip_nVelo_Ds1  = np.array([un.ufloat(m, u) for m, u in zip(mNstrip_nVelo_Ds1,  uNstrip_nVelo_Ds1)])

eff_strip_nVelo_Dsst = Nstrip_nVelo_Dsst /  Nbkk_nVelo_Dsst
eff_strip_nVelo_Ds1  = Nstrip_nVelo_Ds1 /  Nbkk_nVelo_Ds1

meff_strip_nVelo_Dsst = np.array([eff.nominal_value for eff in eff_strip_nVelo_Dsst])
meff_strip_nVelo_Ds1  = np.array([eff.nominal_value for eff in eff_strip_nVelo_Ds1])

ueff_strip_nVelo_Dsst = np.array([eff.std_dev for eff in eff_strip_nVelo_Dsst])
ueff_strip_nVelo_Ds1  = np.array([eff.std_dev for eff in eff_strip_nVelo_Ds1])

ratio_eff_strip_nVelo = eff_strip_nVelo_Dsst / eff_strip_nVelo_Ds1

mratio_eff_strip_nVelo = np.array([r.nominal_value for r in ratio_eff_strip_nVelo])
uratio_eff_strip_nVelo = np.array([r.std_dev for r in ratio_eff_strip_nVelo])

plt.figure(1)
plt.errorbar(nVelo_means, mratio_eff_strip_nVelo, uratio_eff_strip_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label=f"{pol}{year} MC samples")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Stripping and reconstruction efficiency ratio")
plt.title(f"MC Total efficiency {pol} {year}")
plt.ylim(0.55, 0.7)
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_strip_effs_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(2)
plt.errorbar(nVelo_means, meff_strip_nVelo_Dsst, ueff_strip_nVelo_Dsst, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label=r"$D_s^{*+} \to D_s^+ \gamma$ MC")
plt.errorbar(nVelo_means, meff_strip_nVelo_Ds1, ueff_strip_nVelo_Ds1, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label=r"$D_{s1}(2460)^+ \to D_s^+ \gamma$ MC")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Stripping and reconstruction efficiency")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_strip_effs_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

## Offline selection efficiency
mNoff_nVelo_Dsst, nVelo_bins = np.histogram(df_Dsst_mc_sel_sig["nVeloTracks"], bins=nVelo_intervals)
mNoff_nVelo_Ds1, nVelo_bins  = np.histogram(df_Ds1_mc_sel_sig["nVeloTracks"], bins=nVelo_intervals)

uNoff_nVelo_Dsst = np.sqrt(mNoff_nVelo_Dsst); uNoff_nVelo_Ds1 = np.sqrt(mNoff_nVelo_Ds1)

Noff_nVelo_Dsst = np.array([un.ufloat(m, u) for m, u in zip(mNoff_nVelo_Dsst, uNoff_nVelo_Dsst)])
Noff_nVelo_Ds1  = np.array([un.ufloat(m, u) for m, u in zip(mNoff_nVelo_Ds1,  uNoff_nVelo_Ds1)])

eff_off_nVelo_Dsst = Noff_nVelo_Dsst /  Nstrip_nVelo_Dsst
eff_off_nVelo_Ds1  = Noff_nVelo_Ds1 /  Nstrip_nVelo_Ds1

meff_off_nVelo_Dsst = np.array([eff.nominal_value for eff in eff_off_nVelo_Dsst])
meff_off_nVelo_Ds1  = np.array([eff.nominal_value for eff in eff_off_nVelo_Ds1])

ueff_off_nVelo_Dsst = np.array([eff.std_dev for eff in eff_off_nVelo_Dsst])
ueff_off_nVelo_Ds1  = np.array([eff.std_dev for eff in eff_off_nVelo_Ds1])

ratio_eff_off_nVelo = eff_off_nVelo_Dsst / eff_off_nVelo_Ds1

mratio_eff_off_nVelo = np.array([r.nominal_value for r in ratio_eff_off_nVelo])
uratio_eff_off_nVelo = np.array([r.std_dev for r in ratio_eff_off_nVelo])

plt.figure(3)
plt.errorbar(nVelo_means, mratio_eff_off_nVelo, uratio_eff_off_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label=f"{pol}{year} MC samples")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Offline selection efficiency ratio")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_off_effs_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(4)
plt.errorbar(nVelo_means, meff_off_nVelo_Dsst, ueff_off_nVelo_Dsst, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label=r"$D_s^{*+} \to D_s^+ \gamma$ MC")
plt.errorbar(nVelo_means, meff_off_nVelo_Ds1, ueff_off_nVelo_Ds1, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label=r"$D_{s1}(2460)^+ \to D_s^+ \gamma$ MC")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Offline selection efficiency")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_off_effs_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

# Total efficiency
eff_tot_nVelo_Dsst = eff_gen_Dsst * ret_rate_Dsst * eff_strip_nVelo_Dsst * eff_off_nVelo_Dsst
eff_tot_nVelo_Ds1  = eff_gen_Ds1 * ret_rate_Ds1 * eff_strip_nVelo_Ds1 * eff_off_nVelo_Ds1

meff_tot_nVelo_Dsst = np.array([eff.nominal_value for eff in eff_tot_nVelo_Dsst])
meff_tot_nVelo_Ds1  = np.array([eff.nominal_value for eff in eff_tot_nVelo_Ds1])

ueff_tot_nVelo_Dsst = np.array([eff.std_dev for eff in eff_tot_nVelo_Dsst])
ueff_tot_nVelo_Ds1  = np.array([eff.std_dev for eff in eff_tot_nVelo_Ds1])

ratio_eff_tot_nVelo = eff_tot_nVelo_Dsst / eff_tot_nVelo_Ds1

mratio_eff_tot_nVelo = np.array([r.nominal_value for r in ratio_eff_tot_nVelo])
uratio_eff_tot_nVelo = np.array([r.std_dev for r in ratio_eff_tot_nVelo])

plt.figure(5)
plt.errorbar(nVelo_means, mratio_eff_tot_nVelo, uratio_eff_tot_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label=f"{pol}{year} MC samples")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Total selection efficiency ratio")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_tot_effs_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(6)
plt.errorbar(nVelo_means, meff_tot_nVelo_Dsst, ueff_tot_nVelo_Dsst, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label=r"$D_s^{*+} \to D_s^+ \gamma$ MC")
plt.errorbar(nVelo_means, meff_tot_nVelo_Ds1, ueff_tot_nVelo_Ds1, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label=r"$D_{s1}(2460)^+ \to D_s^+ \gamma$ MC")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Totals selection efficiency")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_{pol}{year}_tot_effs_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

# Partial ratios
ratio_bkk_nVelo = Nbkk_nVelo_Ds1 / Nbkk_nVelo_Dsst

mratio_bkk_nVelo = np.array([r.nominal_value for r in ratio_bkk_nVelo])
uratio_bkk_nVelo = np.array([r.std_dev for r in ratio_bkk_nVelo])

ratio_Noff_nVelo = Noff_nVelo_Dsst / Noff_nVelo_Ds1

mratio_Noff_nVelo = np.array([r.nominal_value for r in ratio_Noff_nVelo])
uratio_Noff_nVelo = np.array([r.std_dev for r in ratio_Noff_nVelo])

plt.figure(6)
plt.errorbar(nVelo_means, mratio_bkk_nVelo, uratio_bkk_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\mathrm{N_{bkk}}(D_{s1}(2460)^{+})/\mathrm{N_{bkk}}(D_s^{*+})$")
plt.title(f"Bookkeping events ratio MC {pol} {year}")
plt.ylim(0.75, 1.0)
plt.savefig(f"./plots/Sim_Both_{pol}{year}_bkk_events_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(7)
plt.errorbar(nVelo_means, mratio_Noff_nVelo, uratio_Noff_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\mathrm{N_{sel}}(D_s^{*+})/\mathrm{N_{sel}}(D_{s1}(2460)^{+})$")
plt.title(f"Final selected events ratio MC {pol} {year}")
plt.ylim(0, 0.2)
plt.savefig(f"./plots/Sim_Both_{pol}{year}_final_events_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')
#========================================================

# Saving the data
#--------------------------------------------------------
# Total efficiency ratio for integrated nVeloTracks 
eff_stats = {"gen_eff_Dsst_val"  : eff_gen_Dsst.nominal_value,
             "gen_eff_Dsst_unc"  : float(eff_gen_Dsst.std_dev),
             "gen_eff_Ds1_val"   : eff_gen_Ds1.nominal_value,
             "gen_eff_Ds1_unc"   : float(eff_gen_Ds1.std_dev),
             "filt_eff_Dsst_val" : ret_rate_Dsst.nominal_value,
             "filt_eff_Dsst_unc" : float(ret_rate_Dsst.std_dev),
             "filt_eff_Ds1_val"  : ret_rate_Ds1.nominal_value,
             "filt_eff_Ds1_unc"  : float(ret_rate_Ds1.std_dev),
             "strip_eff_Dsst_val": eff_strip_Dsst.nominal_value,
             "strip_eff_Dsst_unc": float(eff_strip_Dsst.std_dev),
             "strip_eff_Ds1_val" : eff_strip_Ds1.nominal_value,
             "strip_eff_Ds1_unc" : float(eff_strip_Ds1.std_dev),
             "off_eff_Dsst_val"  : eff_strip_Dsst.nominal_value,
             "off_eff_Dsst_unc"  : float(eff_strip_Dsst.std_dev),
             "off_eff_Ds1_val"   : eff_strip_Ds1.nominal_value,
             "off_eff_Ds1_unc"   : float(eff_strip_Ds1.std_dev),
             "tot_eff_Dsst_val"  : eff_tot_Dsst.nominal_value,
             "tot_eff_Dsst_unc"  : float(eff_tot_Dsst.std_dev),
             "tot_eff_Ds1_val"   : eff_tot_Ds1.nominal_value,
             "tot_eff_Ds1_unc"   : float(eff_tot_Ds1.std_dev),
             "ratio_eff_val"     : ratio_eff_tot.nominal_value,
             "ratio_eff_unc"     : float(ratio_eff_tot.std_dev),}

with open(f"./param_files/Sim_{pol}{year}_eff_stats_fst_order.yaml", "w") as file:
    yaml.dump(eff_stats, file, default_flow_style=False)
    
# Total efficiency ratio versus nVeloTracks
eff_stats_nVelo = {"strip_eff_Dsst_val": meff_strip_nVelo_Dsst,
                   "strip_eff_Dsst_unc": ueff_strip_nVelo_Dsst,
                   "strip_eff_Ds1_val" : meff_strip_nVelo_Ds1,
                   "strip_eff_Ds1_unc" : ueff_strip_nVelo_Ds1,
                   "off_eff_Dsst_val"  : meff_off_nVelo_Dsst,
                   "off_eff_Dsst_unc"  : ueff_off_nVelo_Dsst,
                   "off_eff_Ds1_val"   : meff_off_nVelo_Ds1,
                   "off_eff_Ds1_unc"   : ueff_off_nVelo_Ds1,
                   "tot_eff_Dsst_val"  : meff_tot_nVelo_Dsst,
                   "tot_eff_Dsst_unc"  : ueff_tot_nVelo_Dsst,
                   "tot_eff_Ds1_val"   : meff_tot_nVelo_Ds1,
                   "tot_eff_Ds1_unc"   : ueff_tot_nVelo_Ds1,
                   "ratio_eff_val"     : mratio_eff_tot_nVelo,
                   "ratio_eff_unc"     : uratio_eff_tot_nVelo} 

for key, values in eff_stats_nVelo.items():
    np.savetxt(f"./param_files/Sim_{pol}{year}_{key}_{ninters}_nVelo_fst_order.txt", values)

#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")