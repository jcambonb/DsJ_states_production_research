import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# Total efficiencies at first order (MC)
#--------------------------------------------------------
polarities = ["MagDown", "MagUp"]
years      = ["2016", "2017", "2018"]
samples    = [f"{pol}{year}" for pol in polarities for year in years]

short_pols = ["MD", "MU"]
short_year = ["16", "17", "18"]
short_samples = [f"{pol}{year}" for pol in short_pols for year in short_year]

path = "./total_eff_fst_order/param_files"

def mean_weigthed(vals, uncs):
    weights = 1 / uncs**2
    return np.sum(vals*weights)/ np.sum(weights)

def err_weigthed(uncs):
    weigths = 1 / uncs**2
    return 1/np.sqrt(np.sum(weigths))

## total eff ratio Nch integrated
ratio_eff_tot_aux = {}

for pol in polarities:
    for year in years:
        with open(f"{path}/Sim_{pol}{year}_eff_tot_ratio_fst_order.yaml", "r") as file:
            ratio_eff_tot_aux[f"{pol}{year}"] = yaml.safe_load(file)
    
mratio_eff_tot_v2 = np.array([(ratio_eff_tot_aux[s])["val"] for s in samples])
uratio_eff_tot_v2 = np.array([(ratio_eff_tot_aux[s])["unc"] for s in samples])

mratio_eff_tot_mean = mean_weigthed(mratio_eff_tot_v2, uratio_eff_tot_v2)
uratio_eff_tot_mean = err_weigthed(uratio_eff_tot_v2)

ratio_eff_tot = un.ufloat(mratio_eff_tot_mean, uratio_eff_tot_mean)

print(f"Weighted average of total efficiency: {ratio_eff_tot}")
print(f"Relative uncertainty of total efficiency: {uratio_eff_tot_mean/mratio_eff_tot_mean}")

reff_tot_mean = {"val": float(mratio_eff_tot_mean),
                 "unc": float(uratio_eff_tot_mean)}

with open(f"./param_files/Sim_avg_eff_tot_ratio_fst_order.yaml", "w") as file:
    yaml.dump(reff_tot_mean, file, default_flow_style=False)

xaux = np.array([i+1 for i in range(len(samples))])
plt.figure(1)
for x, s in zip(xaux, samples):
    plt.errorbar(x, (ratio_eff_tot_aux[s])["val"], (ratio_eff_tot_aux[s])["unc"],
                 fmt=".", color="black", ecolor="black", capsize=7)
plt.axhline(mratio_eff_tot_mean, color="red", label="Weighted average")
plt.axhline(mratio_eff_tot_mean+uratio_eff_tot_mean, color="red", linestyle="--")
plt.axhline(mratio_eff_tot_mean-uratio_eff_tot_mean, color="red", linestyle="--")
plt.ylabel(r"$\varepsilon_{T}(D_s^{*+})/\varepsilon_{T}(D_{s1}^+)$")
plt.title(f"MC Total efficiency first order")
plt.xticks(xaux, short_samples)
plt.legend()
plt.savefig(f"./plots/Sim_Both_tot_effs_ratio_fst_order.pdf", dpi=300, bbox_inches='tight')

## total eff ratio vs nVelo
inters_path = "../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")
ninters = len(nVelo_intervals) - 1

Ds1_vals_file = f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"

df_Ds1_fit_vals = pd.read_csv(Ds1_vals_file)

nVelo_means = df_Ds1_fit_vals["nVelo_mean"]
nVelo_up    = df_Ds1_fit_vals["nVelo_uup"]
nVelo_down  = df_Ds1_fit_vals["nVelo_udown"]

ninters = len(nVelo_means)

mratio_eff_tot_nVelo = {s: np.loadtxt(f"{path}/Sim_{s}_eff_tot_ratio_vals_{ninters}_nVelo_fst_order.txt") 
                        for s in samples}
uratio_eff_tot_nVelo = {s: np.loadtxt(f"{path}/Sim_{s}_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order.txt") 
                        for s in samples}

mreff_tot_nVelo_split = {f"{i+1} bin": np.array([(mratio_eff_tot_nVelo[s])[i] for s in samples])
                         for i in range(ninters)}
ureff_tot_nVelo_split = {f"{i+1} bin": np.array([(uratio_eff_tot_nVelo[s])[i] for s in samples])
                         for i in range(ninters)}
        
mratio_eff_tot_nVelo_mean = np.array([mean_weigthed(mreff_tot_nVelo_split[f"{i+1} bin"], ureff_tot_nVelo_split[f"{i+1} bin"])
                                     for i in range(ninters)])
uratio_eff_tot_nVelo_mean = np.array([err_weigthed(ureff_tot_nVelo_split[f"{i+1} bin"])
                                      for i in range(ninters)])

np.savetxt(f"./param_files/Sim_avg_eff_tot_ratio_vals_{ninters}_nVelo_fst_order.txt", mratio_eff_tot_nVelo_mean)
np.savetxt(f"./param_files/Sim_avg_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order.txt", uratio_eff_tot_nVelo_mean)

colors = ["red", "blue", "green", "gold", "cyan", "magenta"]

plt.figure(2)
for s, color in zip(samples, colors):
    plt.errorbar(nVelo_means, mratio_eff_tot_nVelo[s], uratio_eff_tot_nVelo[s], xerr=[nVelo_down, nVelo_up], 
                 fmt='.', color=color, ecolor=color, capsize=7, label=s)
plt.errorbar(nVelo_means, mratio_eff_tot_nVelo_mean, uratio_eff_tot_nVelo_mean, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label="Weighted mean")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{T}(D_s^{*+})/\varepsilon_{T}(D_{s1}^+)$")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_tot_effs_ratio_{ninters}_nVelo_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(3)
plt.errorbar(nVelo_means, mratio_eff_tot_nVelo_mean, uratio_eff_tot_nVelo_mean, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label="Weighted mean")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{T}(D_s^{*+})/\varepsilon_{T}(D_{s1}^+)$")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_tot_effs_ratio_{ninters}_nVelo_wmean_fst_order.pdf", dpi=300, bbox_inches='tight')

plt.figure(4)
plt.errorbar(nVelo_means, uratio_eff_tot_nVelo_mean/mratio_eff_tot_nVelo_mean, yerr=0, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label="Weighted mean")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{T}(D_s^{*+})/\varepsilon_{T}(D_{s1}^+)$")
plt.title(f"MC Total efficiency {pol} {year}")
plt.legend()
plt.savefig(f"./plots/Sim_Both_tot_effs_ratio_{ninters}_nVelo_wmean_ru_fst_order.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")