import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import os
import sys
from datetime import datetime
sys.path.append('/home3/ivan.cambon/Python_Modules/RooPyFit')
sys.path.append('/home3/ivan.cambon/Python_Modules/RooPyShort')
import RooPyFit as rpf
import RooPyShort as rsh

init = datetime.now()
print(init)

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

# Data reading

Data_path = '/scratch42/ivan.cambon/DsJ_Spectroscopy/Data/MagDown/2018/'
Data_files = "DsJ_Data_MagD18_264.root"
dtt = "DsGammaTuple"

files = [Data_path+"DsJ_Data_MagD18_{0}.root".format(i+400) for i in range(50)]

def MultipleCands(ttree, histo_mono, histo_multi):
    
    nentries = ttree.GetEntries()
    aux = []
    count_dup = 0
    count_sel = 0
    
    for i in range(nentries):
        ttree.GetEntry(i)               
        evtN = ttree.eventNumber
        runN = ttree.runNumber
        Ds_M = ttree.Ds_M
        g_PT = ttree.gamma_PT
        g_CL = ttree.gamma_CL
        nPVs = ttree.nPVs
        DsgM = ttree.Dsg_M - Ds_M + 1969
        
        if abs(Ds_M-1969) < 20 and g_PT > 1200 and g_CL > 0.7 and nPVs == 1 and DsgM < 2700:
            count_sel += 1
            pair = (runN, evtN)
            if pair not in aux:
                aux.append(pair)
                histo_mono.Fill(DsgM)
            else:
                count_dup += 1
                histo_multi.Fill(DsgM)
                
    return count_sel, count_dup, histo_mono, histo_multi

ndup = 0; nsel = 0

Dsg_mass_hist_monocand = ROOT.TH1D("", "", 100, 2000, 2700)
Dsg_mass_hist_multicand = ROOT.TH1D("", "", 100, 2000, 2700)

for x in files:
    file = ROOT.TFile(x, "READ")
    ttree = file.Get(dtt+"/DecayTree")

    count_sel, count_dup, Dsg_mass_hist_monocand, Dsg_mass_hist_multicand = MultipleCands(ttree, Dsg_mass_hist_monocand, Dsg_mass_hist_multicand)
    
    nsel += count_sel
    ndup += count_dup
    file.Close()

# Esto es totalmente compatible para una nTupla
print("Number of entries after selection: {0}".format(nsel))
print("Number of duplicates: {0}".format(ndup))
print("ratio duplicates/total: {0} %".format(100*ndup/nsel))

c1 = ROOT.TCanvas()
rsh.TH1D_plot(Dsg_mass_hist_monocand, color=1)
rsh.TH1D_plot(Dsg_mass_hist_multicand, color=2)
c1.Draw()

end = datetime.now()
print(end)

print("Execution time: {0}".format(end-init))