import ROOT
from datetime import datetime
import numpy as np
import sys 
import uncertainties as un
import pandas as pd
from particle import Particle
import matplotlib.pyplot as plt
import os
from datetime import datetime
ROOT.EnableImplicitMT()
sys.path.append('/home3/ivan.cambon/Python_Modules/RooPyFit')
sys.path.append('/home3/ivan.cambon/Python_Modules/RooPyShort')
import RooPyFit as rpf
import RooPyShort as rsh


init = datetime.now()
print(init)

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

# nTuples reading

Year = "2018"
Polarity = "MagUp"
Data_name = "MagU18"

Data_path = '/scratch42/ivan.cambon/DsJ_Spectroscopy/Data/{0}/{1}/'.format(Polarity, Year)

Data_files = "DsJ_Data_{0}_*.root".format(Data_name)
dtt = "DsGammaTuple"

tdf_data = ROOT.RDataFrame(dtt+"/DecayTree", Data_path+Data_files)
tdf_data = tdf_data.Define("DsgM", "Dsg_M-Ds_M+1969")
tdf_data = tdf_data.Define("dR_Dsg", "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)")

# Selection

L0_cuts = "(Ds_L0HadronDecision_TOS == 1 | Ds_L0Global_TIS == 1)"
HLT1_cuts = "(Ds_Hlt1TrackMVADecision_TOS == 1 | Ds_Hlt1TwoTrackMVADecision_TOS == 1)"
HLT2_cuts = "(Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS == 1) | (Ds_Hlt2Topo4BodyDecision_TOS == 1)"

Trigger_cuts = L0_cuts + " && " + HLT1_cuts + " &&  " + HLT2_cuts

cuts = "gamma_PT > 1200 && gamma_CL > 0.7 && abs(Ds_M-1969) < 20"
nPVs_req = "nPVs == 1"

sel = Trigger_cuts + "&&" + cuts + "&&" + nPVs_req

tdf_data_sel = tdf_data.Filter(sel)

# Multiple candidates counter algorithm

columns = ["DsgM", "eventNumber", "runNumber", "Ds_M",
           "gamma_PT", "gamma_CL", "nVeloTracks", "nPVs"]

tdf_data_sel = rsh.nCandidate2_def(tdf_data_sel, columns=columns)

total_entries = tdf_data_sel.Count().GetValue()
duplicated_evts = tdf_data_sel.Filter("totCandidates2 != 1").Count().GetValue()
monocand_evts = tdf_data_sel.Filter("totCandidates2 == 1").Count().GetValue()

print("Number of entries after selection: {0}".format(total_entries))
print("Number of events with duplicates: {0}".format(duplicated_evts))
print("Number of monocandidate eventes: {0}".format(monocand_evts))
print("ratio duplicates/total: {0} %".format(100*duplicated_evts/total_entries))
print("ratio monocandidates/total: {0} %".format(100*monocand_evts/total_entries))

tdf_data_sel_mono = tdf_data_sel.Filter("totCandidates2 == 1")

# path for new root files

new_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Data_reduced/Selected_mcand/{0}/{1}/".format(Polarity, Year)
new_tree = "DecayTree"

features = {"DsgM", "eventNumber", "runNumber", "Ds_M",
           "gamma_PT", "gamma_CL", "nVeloTracks", "nPVs", "nCandidate2", "totCandidates2"}

# Full spectrum Snapshot

new_file_full = "DsgL_Data_{0}_sel_mcand.root".format(Data_name)

tdf_data_sel_mono.Snapshot(new_tree, new_path+new_file_full, features)

# Dsst peak Snapshot

Dsst_window = "DsgM > 2050 && DsgM < 2250"
tdf_data_sel_Dsst = tdf_data_sel_mono.Filter(Dsst_window)

new_file_Dsst = "DsgL_Data_{0}_Dsst_sel_mcand.root".format(Data_name)

tdf_data_sel_Dsst.Snapshot(new_tree, new_path+new_file_Dsst, features)

# Ds1 peak Snapshot

Ds1_window = "DsgM > 2350 && DsgM < 2600"
tdf_data_sel_Ds1 = tdf_data_sel_mono.Filter(Ds1_window)

new_file_Ds1 = "DsgL_Data_{0}_Ds1_sel_mcand.root".format(Data_name)

tdf_data_sel_Ds1.Snapshot(new_tree, new_path+new_file_Ds1, features)

end = datetime.now()
print(end)

print("Execution time: {0}".format(end-init))