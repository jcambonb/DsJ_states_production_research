# Study of the $D_{s1}(2460)^+ \to D_s^+ \gamma$ production at LHC$b$ in $pp$ collisions at $\sqrt{s}=13$ TeV

## Introduction

In this folder we have the production analysis of the $D_{s1}(2460)^+$ hadron through $D_{s1}(2460)^+ \to D_s^+ \gamma$ decay. Although the $D_{s0}^*(2317)^+$ drew most of the attention, the $D_{s1}(2460)^+$ is also set to be an tetraquark candidate. In fact, for this hadron more decay channels have been measured, giving more chances of analysis. One of them is $D_{s1}(2460)^+ \to D_s^+ \gamma$ channel, which is a good option for a multiplicity analysis since is easier to work with one photon rather than two

## Analysis characteristics

As we said, for the $D_{s1}(2460)^+$ we are going to work with 

* $D_{s1}(2460)^+ \to D_s^+(\to K^+ K^- \pi^+) \gamma$ decay as signal
* $D_s^{*+}\to D_s^+(\to K^+ K^- \pi^+) \gamma$ decay as reference

The following trigger decisions are used:

- L0:   *Dsg_L0Global_TIS 
- HLT1: *Ds_Hlt1TrackMVADecision_TOS | Ds_Hlt1TwoTrackMVADecision_TOS*
- HLT2: *Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS*

And the stripping line is the following:

 - __DsGammaForExcitedDsSpectroscopyLine__ for $D_s^{*+}\to D_s^+(\to K^+ K^- \pi^+) \gamma$  decay descriptor

