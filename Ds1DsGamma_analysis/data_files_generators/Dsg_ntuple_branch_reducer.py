from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-f", "--folder", dest="folder", default="/scratch42/ivan.cambon/DsJ_Spectroscopy")
parser.add_option("-m", "--mc", action="store_true", dest="mc", default=False)
parser.add_option("-d", "--data", action="store_true", dest="data", default=False)
parser.add_option("-r", "--remove", action="store_true", dest="remove", default=False)

(options, args) = parser.parse_args()

import ROOT
import yaml
from datetime import datetime
import sys 
import os

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
import RooPyShort as rsh

print(datetime.now())

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

# Dsg features
Dsg_kinematics = ["Dsg_M", "Dsg_MM", "Dsg_PT", "Dsg_P", "Dsg_PE", "Dsg_ETA", "Dsg_PHI"]

Dsg_features = Dsg_kinematics 

# Ds features
Ds_kinematics  = ["Ds_M", "Ds_MM", "Ds_PT", "Ds_P", "Ds_PE", "Ds_ETA", "Ds_PHI"]
Ds_phase_space = ["Ds_M12", "Ds_M13", "Ds_Dalitz_Kminus_Kplus_M2", "Ds_Dalitz_Kminus_piplus_M2"]
Ds_track_reco  = ["Ds_ENDVERTEX_CHI2", "Ds_IP_OWNPV", "Ds_IPCHI2_OWNPV", "Ds_FD_OWNPV", 
                  "Ds_FDCHI2_OWNPV", "Ds_DIRA_OWNPV"]

Ds_features = Ds_kinematics + Ds_phase_space + Ds_track_reco

# charged hadrons features
ch_kinematics = ["Kpl_P", "Kmi_P", "pi_P", 
                 "Kpl_PT", "Kmi_PT", "pi_PT",
                 "Kpl_ETA", "Kmi_ETA", "pi_ETA"]

ch_features = ch_kinematics

# gamma features
gamma_kinematics = ["gamma_PT", "gamma_PE", "gamma_ETA", "gamma_PHI"]
gamma_properties = ["gamma_CL", "gamma_Prs", "gamma_Converted", "gamma_ShowerShape"]

gamma_features = gamma_kinematics + gamma_properties

# cone features
cone_sizes = ["0.40", "0.50", "0.60", "0.70", "0.80", "0.90", "1.00"]
cone_features = [f"Ds_{size}_nc_mult" for size in cone_sizes] + [f"Ds_{size}_nc_sPT" for size in cone_sizes]

# trigger conds
Dsg_trigger   = ["Dsg_L0Global_TIS", "Dsg_L0Global_TOS"]
Ds_trigger    = ["Ds_L0HadronDecision_TOS", "Ds_L0HadronDecision_TIS",
                 "Ds_Hlt1TrackMVADecision_TOS", "Ds_Hlt1TwoTrackMVADecision_TOS", 
                 "Ds_Hlt1TrackMVADecision_TIS", "Ds_Hlt1TwoTrackMVADecision_TIS",
                 "Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS", "Ds_Hlt2CharmHadDspToKmKpPipDecision_TIS",
                 "Ds_Hlt2Phys_TIS", "Ds_Hlt1Phys_TIS", "Ds_Hlt2Phys_TOS", "Ds_Hlt1Phys_TOS"]
gamma_trigger = ["gamma_L0PhotonDecision_TOS", "gamma_L0PhotonDecision_TIS"]

trigger_conds = Dsg_trigger + Ds_trigger + gamma_trigger

# event features
event_labels = ["runNumber", "eventNumber", "totCandidates", "nCandidate"]
event_pvs = ["nPVs", "PVZ"]
event_multiplicity = ["nTracks", "nLongTracks", "nDownstreamTracks", "nUpstreamTracks",
                      "nVeloTracks", "nTTracks", "nBackTracks", "nVeloClusters", "nSPDHits"]

event_features = event_labels + event_pvs + event_multiplicity

# mc truth
hierarchy = ["TRUEID", "MC_MOTHER_ID", "MC_GD_MOTHER_ID", "MC_GD_GD_MOTHER_ID"]

Ds_mc_herar    = [f"Ds_{hie}" for hie in hierarchy]
gamma_mc_herar = [f"gamma_{hie}" for hie in hierarchy]
kpl_mc_herar   = [f"Kpl_{hie}" for hie in hierarchy]
kmi_mc_herar   = [f"Kmi_{hie}" for hie in hierarchy]
pipl_mc_herar  = [f"pi_{hie}" for hie in hierarchy]

mc_truth = Ds_mc_herar + gamma_mc_herar + kpl_mc_herar + kmi_mc_herar + pipl_mc_herar

data_features = Dsg_features + Ds_features + gamma_features + cone_features + trigger_conds + event_features + ch_features
mc_features   = data_features + mc_truth

# mcdtt features 
hierarchy_mcdtt = ["MC_MOTHER_ID", "MC_GD_MOTHER_ID", "MC_GD_GD_MOTHER_ID"]

Ds_mc_herar_mcdtt      = [f"Ds_{hie}" for hie in hierarchy_mcdtt]
gamma_mc_herar_mcdtt   = [f"gamma_{hie}" for hie in hierarchy_mcdtt]
gamma_1_mc_herar_mcdtt = [f"gamma_1_{hie}" for hie in hierarchy_mcdtt]
gamma_2_mc_herar_mcdtt = [f"gamma_2_{hie}" for hie in hierarchy_mcdtt]
kpl_mc_herar_mcdtt     = [f"Kpl_{hie}" for hie in hierarchy_mcdtt]
kmi_mc_herar_mcdtt     = [f"Kmi_{hie}" for hie in hierarchy_mcdtt]
pipl_mc_herar_mcdtt    = [f"pi_{hie}" for hie in hierarchy_mcdtt]
piz_mc_herar_mcdtt     = [f"piz_{hie}" for hie in hierarchy_mcdtt]

with open("./txt_files/Dsg_data_stored_features.txt", 'w') as file:
    for feat in data_features:
        file.write(feat + '\n')
        
with open("./txt_files/Dsg_mc_stored_features.txt", 'w') as file:
    for feat in mc_features:
        file.write(feat + '\n')

print(f"Number of features for data: {len(data_features)}")
print(f"Number of features for MC: {len(mc_features)}")
# Common options
#--------------------------------------------------------

pol       = options.polarity
year      = options.year
data_path = options.folder
delete_files = options.remove

dtt = "DsGammaTuple"

# data files reductor
#-------------------------------------------------------- 

## Auxiliar functions
def data_ntuple_reductor(in_folder, out_folder, ttree_name, branches_to_keep):
    in_files = os.listdir(in_folder)
    in_path_files = [f"{in_folder}/{f}" for f in in_files]
    
    out_files = [f.replace(".root", "") + "_raw.root" for f in in_files]
    out_path_files = [f"{out_folder}/{f}" for f in out_files]
    
    for fold, fnew in zip(in_path_files, out_path_files):
        rsh.ntuple_branch_reductor(fold, ttree_name, fnew, branches_to_keep)
        
def file_deleter(path):
    for file in os.listdir(path):
        file_path = os.path.join(path, file)
        if os.path.isfile(file_path):
            os.remove(file_path)

if options.data:
    print("----------------------")
    print(f"Data ntuple branch reduction")
    print("----------------------")

    ti = datetime.now()

    data_in_folder  = f"{data_path}/Data"
    data_out_folder = f"{data_path}/Reduced/Data/raw"

    if not os.path.exists(f"{data_out_folder}/{pol}"):
        os.makedirs(f"{data_out_folder}/{pol}")

    if not os.path.exists(f"{data_out_folder}/{pol}/{year}"):
        os.makedirs(f"{data_out_folder}/{pol}/{year}")

    in_folder_pol_year  = f"{data_in_folder}/{pol}/{year}"
    out_folder_pol_year = f"{data_out_folder}/{pol}/{year}"

    if len(os.listdir(out_folder_pol_year)) == 0 or delete_files:
        if delete_files:
            print(f"Removing old ntuples")
            file_deleter(out_folder_pol_year)

        print(f"Creating reduced nTuples for data {pol} {year}")
        data_ntuple_reductor(in_folder_pol_year, out_folder_pol_year, f"{dtt}/DecayTree", data_features)          
    else:
        print(f"reduced nTuples for data {pol} {year} already exist")

    tf = datetime.now()

    print("----------------------")
    print(f"Execution time for data {pol} {year}: {tf-ti}")
    print("----------------------")
  
        
# MC files reductor
#--------------------------------------------------------
if options.mc:
    print("----------------------")
    print(f"MC ntuple branch reduction")
    print("----------------------")
    
    pfn_yaml_path = "/scratch42/ivan.cambon/Run2_DPA/Run2_datasamples_catalogs/MC/AP_ntuples"
    pfn_yaml_file = "DsJ_mc_ntuples_grid_locations_v2.yaml"
    
    with open(f"{pfn_yaml_path}/{pfn_yaml_file}", 'r') as file:
        pfn_files = yaml.safe_load(file)

    mc_in_folder  = f"{data_path}/MC/Official/TightCut"
    mc_out_folder = f"{data_path}/Reduced/MC/TightCut/raw"

    decays = ["Ds1DsGamma", "DsstDsGamma",
              "Ds1DsstPi0", "DsstDsPi0", "Ds0DsPi0"]

    for dec in decays:
        ti = datetime.now()  

        mc_files = pfn_files[dec]
        mc_files_year_pol = set(mc_files[f"{pol}{year}"])
        
        mc_out_file = f"{dec}_{pol+year}_TightCut_raw.root"
        mcdtt_out_file = f"{dec}_{pol+year}_TightCut_gen.root"
        
        if not mc_out_file in os.listdir(mc_out_folder) or delete_files:
            if delete_files:
                print(f"Removing nTuple for {dec} MC {year} {pol}")
                if os.path.isfile(f"{mc_out_folder}/{mc_out_file}"):
                    os.remove(f"{mc_out_folder}/{mc_out_file}")    
                    
            print(f"Creating reduced nTuple for {dec} MC {year} {pol}")
            
            rdf = ROOT.RDataFrame(f"{dtt}/DecayTree", mc_files_year_pol)
            rdf.Snapshot("DecayTree", f"{mc_out_folder}/{mc_out_file}", mc_features)
            
            if dec != "Ds1DsstPi0":
                rdf_mcdtt = ROOT.RDataFrame(f"{dec}MCTuple/MCDecayTree", mc_files_year_pol)
                
                if "Pi0" not in dec:
                    mc_truth_mcdtt = Ds_mc_herar_mcdtt + gamma_mc_herar_mcdtt + kpl_mc_herar_mcdtt + kmi_mc_herar_mcdtt + pipl_mc_herar_mcdtt
                    mcdtt_features = ["nPVs"] + event_labels + event_multiplicity + mc_truth_mcdtt     
                    
                    rdf_mcdtt.Snapshot("MCDecayTree", f"{mc_out_folder}/{mcdtt_out_file}", mcdtt_features)
                else:
                    mc_truth_mcdtt = Ds_mc_herar_mcdtt + gamma_1_mc_herar_mcdtt + gamma_2_mc_herar_mcdtt + piz_mc_herar_mcdtt + kpl_mc_herar_mcdtt + kmi_mc_herar_mcdtt + pipl_mc_herar_mcdtt
                    mcdtt_features = ["nPVs"] + event_labels + event_multiplicity + mc_truth_mcdtt     
                    
                    rdf_mcdtt.Snapshot("MCDecayTree", f"{mc_out_folder}/{mcdtt_out_file}", mcdtt_features)
            else:
                print("Ds1DsstPi0 decay has not mcdtt defined in the ntuples")
        else:
            print(f"reduced nTuples for {dec} MC {year} {pol} already exist")

        tf = datetime.now()

        print("----------------------")
        print(f"Execution time for {dec} MC: {tf-ti}")
        print("----------------------")