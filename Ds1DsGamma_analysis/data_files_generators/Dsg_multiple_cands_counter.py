from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-f", "--folder", dest="folder", default="/scratch42/ivan.cambon/DsJ_Spectroscopy")
parser.add_option("-m", "--mc", action="store_true", dest="mc", default=False)
parser.add_option("-d", "--data", action="store_true", dest="data", default=False)
parser.add_option("-r", "--remove", action="store_true", dest="remove", default=False)

(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

print(datetime.now())

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

# Dsg features
Dsg_kinematics = ["Dsg_M", "Dsg_MM", "Dsg_PT", "Dsg_P", "Dsg_PE", "Dsg_ETA", "Dsg_PHI"]

Dsg_features = Dsg_kinematics 

# Ds features
Ds_kinematics  = ["Ds_M", "Ds_MM", "Ds_PT", "Ds_P", "Ds_PE", "Ds_ETA", "Ds_PHI"]
Ds_track_reco  = ["Ds_ENDVERTEX_CHI2", "Ds_IP_OWNPV", "Ds_IPCHI2_OWNPV", "Ds_FD_OWNPV", 
                  "Ds_FDCHI2_OWNPV", "Ds_DIRA_OWNPV"]

Ds_features = Ds_kinematics + Ds_track_reco

# Charged hadrons features
ch_kinematics = ["Kpl_P", "Kmi_P", "pi_P", 
                 "Kpl_PT", "Kmi_PT", "pi_PT",
                 "Kpl_ETA", "Kmi_ETA", "pi_ETA"]

ch_features = ch_kinematics

# gamma features
gamma_kinematics = ["gamma_PT", "gamma_PE", "gamma_ETA", "gamma_PHI"]
gamma_properties = ["gamma_CL", "gamma_Prs", "gamma_Converted", "gamma_ShowerShape"]

gamma_features = gamma_kinematics + gamma_properties

# trigger conds
Dsg_trigger   = ["Dsg_L0Global_TIS", "Dsg_L0Global_TOS"]
Ds_trigger    = ["Ds_L0HadronDecision_TOS", "Ds_L0HadronDecision_TIS",
                 "Ds_Hlt1TrackMVADecision_TOS", "Ds_Hlt1TwoTrackMVADecision_TOS", 
                 "Ds_Hlt1TrackMVADecision_TIS", "Ds_Hlt1TwoTrackMVADecision_TIS",
                 "Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS", "Ds_Hlt2CharmHadDspToKmKpPipDecision_TIS",
                 "Ds_Hlt2Phys_TIS", "Ds_Hlt1Phys_TIS", "Ds_Hlt2Phys_TOS", "Ds_Hlt1Phys_TOS"]
gamma_trigger = ["gamma_L0PhotonDecision_TOS", "gamma_L0PhotonDecision_TIS"]

trigger_conds  = Dsg_trigger + Ds_trigger + gamma_trigger
new_trig_conds = [f"{trig}_int" for trig in trigger_conds]

# event features
event_labels = ["runNumber", "eventNumber", "totCandidates", "nCandidate"]
event_pvs = ["nPVs"]
event_multiplicity = ["nTracks", "nLongTracks", "nDownstreamTracks", "nUpstreamTracks",
                      "nVeloTracks", "nTTracks", "nBackTracks", "nVeloClusters", "nSPDHits"]

event_features = event_labels + event_pvs + event_multiplicity

# Features calculated at RDataFrame level
derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

def_feats = [feats[0] for feats in derivated_features.items()]

# mc truth
hierarchy = ["TRUEID", "MC_MOTHER_ID", "MC_GD_MOTHER_ID", "MC_GD_GD_MOTHER_ID"]

Ds_mc_herar    = [f"Ds_{hie}" for hie in hierarchy]
gamma_mc_herar = [f"gamma_{hie}" for hie in hierarchy]
kpl_mc_herar   = [f"Kpl_{hie}" for hie in hierarchy]
kmi_mc_herar   = [f"Kmi_{hie}" for hie in hierarchy]
pipl_mc_herar  = [f"pi_{hie}" for hie in hierarchy]

mc_truth = Ds_mc_herar + gamma_mc_herar + kpl_mc_herar + kmi_mc_herar + pipl_mc_herar

data_features = Dsg_features + Ds_features + gamma_features + new_trig_conds + event_features + def_feats + ch_features
mc_features   = data_features + mc_truth

print(f"Number of features for data: {len(data_features)}")
print(f"Number of features for MC: {len(mc_features)}")

# evt selection + cand selection + trigger conditions
#--------------------------------------------------------
evt_sel_file = open("../selection/param_files/evt_selection.txt", "r")
evt_sel = evt_sel_file.read()
evt_sel_file.close()

cand_sel_file = open("../selection/param_files/cand_selection.txt", "r")
cand_sel = cand_sel_file.read()
cand_sel_file.close()

trig_cond_file = open("../selection/param_files/trig_selection.txt", "r")
trig_cond = trig_cond_file.read()
trig_cond_file.close()

sel = f"{evt_sel} && {cand_sel} && {trig_cond}"
#-------------------------------------------------------

# Common options
#--------------------------------------------------------
pol       = options.polarity
year      = options.year
data_path = options.folder
delete_files = options.remove

dtt = "DsGammaTuple"
#--------------------------------------------------------

# Multiple candidates algorithm execution
#========================================================
def mult_cands_counter(rdf, output_file, branches_to_keep, print_info=False):
    rdf_new = rsh.nCandidate2_def(rdf, branches_to_keep, print_info)
    new_branches = rdf_new.GetColumnNames()
    rdf_new.Snapshot("DecayTree", output_file, new_branches)
            
# Data
#--------------------------------------------------------
if options.data:
    print("----------------------")
    print(f"Data multiple candidates counter")
    print("----------------------")

    ti = datetime.now()

    data_in_path  = f"{data_path}/Reduced/Data/raw/{pol}/{year}"
    data_out_path = f"{data_path}/Reduced/Data/sel"

    out_file = f"DsJ_Data_{pol}_{year}_sel.root"

    if out_file not in os.listdir(data_out_path) or delete_files:
        if delete_files:
            print(f"Removing {year} {pol} old ntuples")
            if os.path.isfile(f"{data_out_path}/{out_file}"):
                os.remove(f"{data_out_path}/{out_file}")

        print(f"Creating ntuples with multiple candidate counter for data {pol} {year}")
        rdf = ROOT.RDataFrame("DecayTree", f"{data_in_path}/DsJ_*.root")
        for keys, values in derivated_features.items():
            rdf = rdf.Define(keys, values)

        for trig in trigger_conds:
            rdf = rdf.Define(f"{trig}_int", f"static_cast<int>({trig})")

        rdf = rdf.Filter(sel)
        mult_cands_counter(rdf, f"{data_out_path}/{out_file}", data_features, print_info=True)
    else:
        print(f"ntuples with multiple candidate counter for data {pol} {year} already exist")

    tf = datetime.now()

    print(f"Execution time for data = {tf-ti}")
#--------------------------------------------------------

# MC
#--------------------------------------------------------
if options.mc:
    print("----------------------")
    print(f"MC multiple candidates counter")
    print("----------------------")
     
    mc_in_path    = f"{data_path}/Reduced/MC/TightCut/raw"
    mc_out_path   = f"{data_path}/Reduced/MC/TightCut/sel"
    mc_out_path_t = f"{data_path}/Reduced/MC/TightCut/sel_truth"

    decays = ["Ds1DsGamma", "DsstDsGamma",
              "DsstDsPi0", "Ds0DsPi0", "DsstDsPi0"]
    
    TRUEID = rsh.TRUEIDs()
    TRUEID_dtt = TRUEID[dtt]
    
    for dec in decays:
        ti = datetime.now()
        
        mc_in_file    = f"{dec}_{pol+year}_TightCut_raw.root"
        mc_out_file   = f"{dec}_{pol+year}_TightCut_sel.root"
        mc_out_file_t = f"{dec}_{pol+year}_TightCut_sel_t.root"
        
        if not mc_out_file in os.listdir(mc_out_path) or delete_files:
            print(f"Creating nTuples with multiple candidate counter for {dec} MC")

            if delete_files:
                print(f"Removing nTuple for {dec} MC {year} {pol}")
                if os.path.isfile(f"{mc_out_path}/{mc_out_file}"):
                    os.remove(f"{mc_out_path}/{mc_out_file}")    

            rdf_mc = ROOT.RDataFrame("DecayTree", f"{mc_in_path}/{mc_in_file}")
            
            for keys, values in derivated_features.items():
                rdf_mc = rdf_mc.Define(keys, values)

            for trig in trigger_conds:
                rdf_mc = rdf_mc.Define(f"{trig}_int", f"static_cast<int>({trig})")
                            
            rdf_mc  = rdf_mc.Filter(sel)
            
            mult_cands_counter(rdf_mc, f"{mc_out_path}/{mc_out_file}", mc_features)

        if not mc_out_file_t in os.listdir(mc_out_path_t) or delete_files:
            if delete_files:
                print(f"Removing truth nTuple for {dec} MC {year} {pol}")   
                if os.path.isfile(f"{mc_out_path_t}/{mc_out_file_t}"):
                    os.remove(f"{mc_out_path_t}/{mc_out_file_t}")   
            
            rdf_mc = ROOT.RDataFrame("DecayTree", f"{mc_in_path}/{mc_in_file}")
            
            truth = TRUEID_dtt[dec]

            rdf_sig = rdf_mc.Filter(truth)

            for keys, values in derivated_features.items():
                rdf_sig = rdf_sig.Define(keys, values)

            for trig in trigger_conds:
                rdf_sig = rdf_sig.Define(f"{trig}_int", f"static_cast<int>({trig})")
            
            rdf_sig = rdf_sig.Filter(sel)
            
            mult_cands_counter(rdf_sig, f"{mc_out_path_t}/{mc_out_file_t}", mc_features)
            
        tf = datetime.now()
        
        print("----------------------")
        print(f"Execution time for {dec} MC: {tf-ti}")
        print("----------------------")
#--------------------------------------------------------

#========================================================