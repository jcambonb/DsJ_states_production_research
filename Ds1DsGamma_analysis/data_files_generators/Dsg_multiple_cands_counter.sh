#!/bin/bash

data=false
mc=true
del=true
polarities=("MagDown" "MagUp")
years=("2018" "2017" "2016")

conda activate yvk_env

for p in "${polarities[@]}"
do
    for y in "${years[@]}"
    do  
        echo "Running the Dsg multiple cands counter for $p $y data sample"
        echo "--------------------------------"
        if $mc; then
            if $del; then
                python Dsg_multiple_cands_counter.py -p $p -y $y -m -r
            else
                python Dsg_multiple_cands_counter.py -p $p -y $ -m 
            fi
        fi
        if $data; then
            if $del; then
                python Dsg_multiple_cands_counter.py -p $p -y $y -d -r
            else 
                python Dsg_multiple_cands_counter.py -p $p -y $y -d
            fi
        fi
        echo "Iteration for  $p $y data sample finished" 
        echo "--------------------------------"
    done
done