from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

pol  = options.polarity
year = options.year
run2 = options.run2

def unc_ratio(a, b, ua, ub):
    ratio = a/b
    uratio = ratio * np.sqrt((ua/a)**2+(ub/b)**2)
    return uratio
    
    
# cross section ratio for integrated Nch
#--------------------------------------------------------
if run2:
    Dsst_total_fit_file = f"../mass_fits/param_files/Exp_Run2_Dsst_sig_mass_fit.csv"
    Ds1_total_fit_file  = f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit.csv"
else:
    Dsst_total_fit_file = f"../mass_fits/param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit.csv"
    Ds1_total_fit_file  = f"../mass_fits/param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit.csv"
    
    print(f"Only {pol} {year} data_sample_used")

df_Dsst_fit = pd.read_csv(Dsst_total_fit_file)
df_Ds1_fit  = pd.read_csv(Ds1_total_fit_file)

mNDsst = float((df_Dsst_fit.query('Parameters == "NDsst"'))["Values"]); uNDsst = float((df_Dsst_fit.query('Parameters == "NDsst"'))["Errors"])
mNDs1  = float((df_Ds1_fit.query('Parameters == "NDs1"'))["Values"]);   uNDs1  = float((df_Ds1_fit.query('Parameters == "NDs1"'))["Errors"])

NDsst = un.ufloat(mNDsst, uNDsst); NDs1 = un.ufloat(mNDs1, uNDs1)

yratio = NDs1 / NDsst

if run2:
    ratio_eff_tot_path = "../efficiencies_ratio/param_files"
    ratio_eff_tot_file = "Sim_avg_eff_tot_ratio_fst_order_v2.yaml"
else:
    ratio_eff_tot_path = "../efficiencies_ratio/total_efficiency_in_mc/param_files"
    ratio_eff_tot_file = f"Sim_{pol}{year}_eff_tot_ratio_fst_order.yaml"  

with open(f"{ratio_eff_tot_path}/{ratio_eff_tot_file}", "r") as file:
    ratio_eff_tot_data = yaml.safe_load(file)
    
mratio_eff_tot = ratio_eff_tot_data["val"]
uratio_eff_tot = ratio_eff_tot_data["unc"]

ratio_eff_tot = un.ufloat(mratio_eff_tot, uratio_eff_tot)

cr_sec_ratio       = yratio.n * ratio_eff_tot.n
ustat_cr_sec_ratio = cr_sec_ratio * (yratio.s/yratio.n)
usys_cr_sec_ratio  = cr_sec_ratio * (uratio_eff_tot/mratio_eff_tot)

if run2:
    print(f"Cross section ratio for full Run2 data: {cr_sec_ratio} +- {ustat_cr_sec_ratio} (stat) +- {usys_cr_sec_ratio} (sys)")
else:
    print(f"Cross section ratio {pol} {year} data sample: {cr_sec_ratio} +- {ustat_cr_sec_ratio} (stat) +- {usys_cr_sec_ratio} (sys)")
    
plt.figure(1)
plt.errorbar(1, cr_sec_ratio, ustat_cr_sec_ratio, 
             fmt='.', color="black", ecolor="black", capsize=7, label=r"$\mathrm{u_{stat}}$")
bx = 0.9
by = cr_sec_ratio - usys_cr_sec_ratio
rect = plt.Rectangle((bx, by), 0.2, 2 * usys_cr_sec_ratio,
                     edgecolor='black', facecolor='none', linestyle='-', label=r"$\mathrm{u_{sys}}$")
plt.gca().add_patch(rect)  
plt.ylabel(r"$\frac{\sigma_{D_{s1}(2460)^+}\mathcal{B}(D_{s1}(2460)^+ \to D_s^+ \gamma)}{\sigma_{D_s^{*+}}\mathcal{B}(D_s^{*+} \to D_s^+ \gamma)}$")
plt.title("Cross section ratio")
plt.legend(loc='lower right')
plt.xlim(0, 2)
#plt.xticks([i for i in range(5)], ["", "", r"pp $\sqrt{s} = 13 \mathrm{TeV}$", "", ""])
#r"pp $\sqrt{s} = 13 \mathrm{TeV}$"
if run2:
    plt.savefig(f"./plots/Exp_Run2_cross_section_ratio.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Exp_{pol}_{year}_cross_section_ratio.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

# cross sections vs Nch
#--------------------------------------------------------
ninters = 5

if run2:
    print("All Run2 data samples used")
    
    Dsst_vals_nVelo_file = f"../mass_fits/param_files/Exp_Run2_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv"
    Dsst_uncs_nVelo_file = f"../mass_fits/param_files/Exp_Run2_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv"

    Ds1_vals_nVelo_file = f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"
    Ds1_uncs_nVelo_file = f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv"
else:
    print(f"Only {pol} {year} data sample used")
    
    Dsst_vals_nVelo_file = f"../mass_fits/param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv"
    Dsst_uncs_nVelo_file = f"../mass_fits/param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv"

    Ds1_vals_nVelo_file = f"../mass_fits/param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"
    Ds1_uncs_nVelo_file = f"../mass_fits/param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv"

df_Dsst_vals_nVelo = pd.read_csv(Dsst_vals_nVelo_file) 
df_Dsst_uncs_nVelo = pd.read_csv(Dsst_uncs_nVelo_file) 

df_Ds1_vals_nVelo = pd.read_csv(Ds1_vals_nVelo_file)
df_Ds1_uncs_nVelo = pd.read_csv(Ds1_uncs_nVelo_file)

my_Dsst_nVelo = np.array(df_Dsst_vals_nVelo["NDsst"]); uy_Dsst_nVelo = np.array(df_Dsst_uncs_nVelo["NDsst"])
my_Ds1_nVelo  = np.array(df_Ds1_vals_nVelo["NDs1"]);   uy_Ds1_nVelo  = np.array(df_Ds1_uncs_nVelo["NDs1"])

yDsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(my_Dsst_nVelo, uy_Dsst_nVelo)])
yDs1_Velo   = np.array([un.ufloat(m, u) for m, u in zip(my_Ds1_nVelo, uy_Ds1_nVelo)])

yratio_nVelo = yDs1_Velo / yDsst_nVelo

myratio_nVelo = np.array([yr.n for yr in yratio_nVelo])
uyratio_nVelo = np.array([yr.s for yr in yratio_nVelo])

if run2:
    effs_path_nVelo  = "../efficiencies_ratio/param_files"
    effs_file_nVelo  = f"Sim_avg_eff_tot_ratio_vals_{ninters}_nVelo_fst_order_v2.txt"
    ueffs_file_nVelo = f"Sim_avg_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order_v2.txt"
else:
    effs_path_nVelo  = "../efficiencies_ratio/param_files"
    effs_file_nVelo  = f"Sim_{pol}_{year}_eff_tot_ratio_vals_{ninters}_nVelo_fst_order.txt"
    ueffs_file_nVelo = f"Sim_{pol}_{year}_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order.txt"

mratio_eff_tot_nVelo = np.loadtxt(f"{effs_path_nVelo}/{effs_file_nVelo}")
uratio_eff_tot_nVelo = np.loadtxt(f"{effs_path_nVelo}/{ueffs_file_nVelo}")

cr_sec_ratio_nVelo       = myratio_nVelo * mratio_eff_tot_nVelo
ustat_cr_sec_ratio_nVelo = cr_sec_ratio_nVelo * (uyratio_nVelo/myratio_nVelo)
usys_cr_sec_ratio_nVelo  = cr_sec_ratio_nVelo * (uratio_eff_tot_nVelo/mratio_eff_tot_nVelo)

colors = ["black", "red", "blue"]
        
plt.figure(2)
plt.text(5, 0.042, r"LHCb Unofficial", fontsize=25)
plt.text(5, 0.039, r"$pp$ $\sqrt{s}=13$ $\mathrm{TeV} \text{ } \int \text{ } \mathcal{L} = 5.4 \text{ } \mathrm{fb^{-1}}$", fontsize=25)
plt.text(5, 0.036, r"$p_{\mathrm{T}} > 3.5 \text{ } \mathrm{GeV/c}$", fontsize=25)
plt.errorbar(df_Ds1_vals_nVelo["nVelo_mean"], cr_sec_ratio_nVelo, ustat_cr_sec_ratio_nVelo, 
             xerr=[df_Ds1_vals_nVelo["nVelo_udown"], df_Ds1_vals_nVelo["nVelo_uup"]],
             fmt='.', color="black", ecolor="black", capsize=7, label=r"$\mathrm{u_{stat}}$")
i = 0
for x, y, uy in zip(df_Ds1_vals_nVelo["nVelo_mean"], cr_sec_ratio_nVelo, usys_cr_sec_ratio_nVelo):
    bx = x - 4
    by = y - uy
    if i == 0:
        rect = plt.Rectangle((bx, by), 8, 2 * uy,
                             edgecolor='black', facecolor='none', linestyle='-', label=r"$\mathrm{u_{sys}}$")
    else:
        rect = plt.Rectangle((bx, by), 8, 2 * uy,
                             edgecolor='black', facecolor='none', linestyle='-')
    plt.gca().add_patch(rect)  
    i += 1
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\frac{\sigma_{D_{s1}(2460)^+}\mathcal{B}(D_{s1}(2460)^+ \to D_s^+ \gamma)}{\sigma_{D_s^{*+}}\mathcal{B}(D_s^{*+} \to D_s^+ \gamma)}$")
plt.title("Cross section ratio vs multiplicity")
plt.ylim(0, 0.045)
plt.legend(loc='lower right')
if run2:
    plt.savefig(f"./plots/Exp_Run2_cross_section_ratio_vs_nVelo.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Exp_{pol}_{year}_cross_section_ratio_vs_nVelo.pdf", dpi=300, bbox_inches='tight')

plt.figure(3)
plt.errorbar(df_Ds1_vals_nVelo["nVelo_mean"], ustat_cr_sec_ratio_nVelo/cr_sec_ratio_nVelo, yerr=0,
             xerr=[df_Ds1_vals_nVelo["nVelo_udown"], df_Ds1_vals_nVelo["nVelo_uup"]], 
             fmt='.', color="black", ecolor="black", capsize=7, label="Run2")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Relative uncertainty (%)")
plt.title("Cross section ratio vs multiplicity uncertainty")
if run2:
    plt.savefig(f"./plots/Exp_Run2_cross_section_ratio_vs_nVelo_ru.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Exp_{pol}_{year}_cross_section_ratio_vs_nVelo_ru.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

# Tendency fit
#--------------------------------------------------------

#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")