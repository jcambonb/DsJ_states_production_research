polarities="Both"
years=("2018" "2017" "2016")

if [ "$polarities" != "Both" ]; then
    for p in "${polarities[@]}"
    do
        for y in "${years[@]}"
        do
            echo "Computing total eficiency at first order for $p $y data sample"
            echo "--------------------------------"

            python Dsg_total_eff_fst_order.py -p $p -y $y

            echo "Iteration for  $p $y  sample finished" 
            echo "--------------------------------"
        done
    done
else
    for y in "${years[@]}"
    do
        echo "Computing total eficiency at first order for $polarities $y data sample"
        echo "--------------------------------"

        python Dsg_total_eff_fst_order.py -p $polarities -y $y

        echo "Iteration for  $p $y  sample finished" 
        echo "--------------------------------"
    done
fi
