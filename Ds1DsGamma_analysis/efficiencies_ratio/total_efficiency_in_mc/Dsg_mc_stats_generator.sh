mc_tab=false
mc_ret=true

polarities=("MagDown" "MagUp")
years=("2018" "2017" "2016")

yaml_file="./yaml_files/DsJ_mc_prod_ids.yaml"

get_value() {
    python3 -c "
import yaml
with open('$yaml_file', 'r') as f:
    data = yaml.safe_load(f)
print(data['$1']['$2'])
"
}

source /cvmfs/lhcb.cern.ch/lhcbdirac/lhcbdirac

if $mc_ret; then   

    rm ./param_files/*ret_rate*.csv

    ret_out_file_Ds1="./param_files/mc_ret_rate_values_Ds1.csv"
    ret_out_file_Dsst="./param_files/mc_ret_rate_values_Dsst.csv"

    echo "Sample,Nbkk,Nprefilt,ret_rate" >> "$ret_out_file_Ds1"
    echo "Sample,Nbkk,Nprefilt,ret_rate" >> "$ret_out_file_Dsst"

fi

# Alternatively, read and print all values for each key in Ds1DsGamma
for p in "${polarities[@]}"
    do
        for y in "${years[@]}"
        do
            prodid_Ds1=$(get_value "Ds1DsGamma" $p$y)
            echo "$p$y for Ds1DsGamma: $prodid_Ds1"

            prodid_Dsst=$(get_value "DsstDsGamma" $p$y)
            echo "$p$y for DsstDsGamma: $prodid_Dsst"

            if $mc_tab; then
                cd MCStatTools

                python scripts/DownloadAndBuildStat.py $prodid_Ds1,$prodid_Dsst -w IFT

                cd ..

                mv MCStatTools/IFT_*.html html_files
            fi
            
            if $mc_ret; then

                temp_file_Ds1="./param_files/temp_file_Ds1.txt"
                temp_file_Dsst="./param_files/temp_file_Dsst.txt"

                # Execute the commands and capture the output
                prodid_Ds1=$((prodid_Ds1 + 2))
                prodid_Dsst=$((prodid_Dsst + 2))
                
                dirac-bookkeeping-rejection-stats -P $prodid_Ds1 > "$temp_file_Ds1" # With this you save the output
                dirac-bookkeeping-rejection-stats -P $prodid_Dsst > "$temp_file_Dsst"
                
                # Parse and save Retention values

                Nbkk_Ds1=$(grep "Event stat:" "$temp_file_Ds1" | awk '{print $3}')
                Nbkk_Dsst=$(grep "Event stat:" "$temp_file_Dsst" | awk '{print $3}')
                
                Nprefilt_Ds1=$(grep "EventInputStat:" "$temp_file_Ds1" | awk '{print $2}')
                Nprefilt_Dsst=$(grep "EventInputStat:" "$temp_file_Dsst" | awk '{print $2}')

                ret_Ds1=$(grep "Retention:" "$temp_file_Ds1" | awk '{print $2}')
                ret_Dsst=$(grep "Retention:" "$temp_file_Dsst" | awk '{print $2}')
    
                # Output file to store all results
                ret_out_file_Ds1="./param_files/mc_ret_rate_values_Ds1.csv"
                ret_out_file_Dsst="./param_files/mc_ret_rate_values_Dsst.csv"

                # Append the results with labels to the output file
                echo "$p$y,$Nbkk_Ds1,$Nprefilt_Ds1,$ret_Ds1" >> "$ret_out_file_Ds1"
                echo "$p$y,$Nbkk_Dsst,$Nprefilt_Dsst,$ret_Dsst" >> "$ret_out_file_Dsst"

                # Clean up temporary files
                rm "$temp_file_Ds1" "$temp_file_Dsst"
            fi
    done
done
