import yaml
import numpy as np
import pandas as pd
import os

folders = ["plots", "xml_files", "param_files", "yaml_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

req_ids = {"MagDown2018": "00211877",
           "MagDown2017": "00212520",
           "MagDown2016": "00211357",
           "MagUp2018"  : "00211865",
           "MagUp2017"  : "00212508",
           "MagUp2016"  : "00211345"}

prod_ids = {"Ds1DsGamma": {"MagDown2018": 211875,
                           "MagDown2017": 212518,
                           "MagDown2016": 211355,
                           "MagUp2018"  : 211863,
                           "MagUp2017"  : 212506,
                           "MagUp2016"  : 211343},
           
           "DsstDsGamma": {"MagDown2018": 211878,
                           "MagDown2017": 212521,
                           "MagDown2016": 211358,
                           "MagUp2018"  : 211866,
                           "MagUp2017"  : 212509,
                           "MagUp2016"  : 211346}}

bkk_evts = {"Ds1DsGamma": {"MagDown2018": 361792,
                           "MagDown2017": 363359,
                           "MagDown2016": 854137,
                           "MagUp2018"  : 360114,
                           "MagUp2017"  : 359745,
                           "MagUp2016"  : 916761},
           
           "DsstDsGamma": {"MagDown2018": 412508,
                           "MagDown2017": 366039,
                           "MagDown2016": 351374,
                           "MagUp2018"  : 387655,
                           "MagUp2017"  : 358361,
                           "MagUp2016"  : 365008,}}

yaml_files = ["DsJ_mc_prod_ids.yaml", "DsJ_mc_bkk_evts.yaml"]
dicts = [prod_ids, bkk_evts]

for f, d in zip(yaml_files, dicts):
    with open(f"./yaml_files/{f}", "w") as file:
        yaml.dump(d, file)
        
