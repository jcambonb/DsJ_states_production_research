from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
pol  = options.polarity
year = options.year
run2 = options.run2

ti = datetime.now()

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

units_CB_Ds1  = ["No units", "MeV/$c^2$", "MeV/$c^2$", "", "", ""]

if run2:
    samples = "run2"
else:
    samples = f"{pol}{year}"

# data reading
#--------------------------------------------------------
path = "./root_files"
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1

trig_conds = ["TOS", "TISTOS"]

## Nch integrated
if run2:
    files_Dsst_full = {trig: ROOT.TFile(f"{path}/DsJ_data_Dsst_mass_lz_{trig}_sample.root") 
                       for trig in trig_conds}
    files_Ds1_full  = {trig: ROOT.TFile(f"{path}/DsJ_data_Ds1_mass_lz_{trig}_sample.root") 
                       for trig in trig_conds}
else:
    files_Dsst_full = {trig: ROOT.TFile(f"{path}/DsJ_data_Dsst_mass_lz_{trig}_sample.root") 
                       for trig in trig_conds}
    files_Ds1_full  = {trig: ROOT.TFile(f"{path}/DsJ_data_Ds1_mass_lz_{trig}_sample.root") 
                       for trig in trig_conds}
    
hDsst_full = {trig: f.Get(f"Dsst_mass_{trig}") for trig, f in files_Dsst_full.items()}
hDs1_full  = {trig: f.Get(f"Ds1_mass_{trig}") for trig, f in files_Ds1_full.items()}

## vs nVelo
if run2:
    files_Dsst_nVelo = {trig: [ROOT.TFile(f"{path}/DsJ_data_Dsst_mass_lz_{trig}_nVelo_{i+1}_sample.root")
                               for i in range(ninters)] 
                        for trig in trig_conds}
    files_Ds1_nVelo  = {trig: [ROOT.TFile(f"{path}/DsJ_data_Ds1_mass_lz_{trig}_nVelo_{i+1}_sample.root")
                               for i in range(ninters)]  
                        for trig in trig_conds}
else:
    files_Dsst_nVelo = {trig: [ROOT.TFile(f"{path}/DsJ_data_Dsst_mass_lz_{trig}_nVelo_{i+1}_sample.root")
                               for i in range(ninters)] 
                        for trig in trig_conds}
    files_Ds1_nVelo  = {trig: [ROOT.TFile(f"{path}/DsJ_data_Ds1_mass_lz_{trig}_nVelo_{i+1}_sample.root")
                               for i in range(ninters)]  
                        for trig in trig_conds}

hDsst_nVelo = {trig: [f.Get(f"Dsst_mass_{trig}") for f in fs] for trig, fs in files_Dsst_nVelo.items()}
hDs1_nVelo  = {trig: [f.Get(f"Ds1_mass_{trig}") for f in fs] for trig, fs in files_Ds1_nVelo.items()}
#--------------------------------------------------------

# Fit models
#--------------------------------------------------------
Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

# Dsst model pdfs 
mDsst = ROOT.RooRealVar("DsgM", "DsgM", Dsst_mass[0], Dsst_mass[1])

mu_c = 2112; sigma_c = 5; alpha_c = 1; n_c = 4

mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", mu_c, mu_c-10, mu_c+10)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaR_Dsst = ROOT.RooRealVar("alphaR_Dsst", "alphaR_Dsst", alpha_c, -20*alpha_c, -0.01*alpha_c)
nR_Dsst = ROOT.RooRealVar("nR_Dsst", "nR_Dsst", 10)

CB_Dsst = ROOT.RooCrystalBall("CB_Dsst", "CB_Dsst", mDsst, 
                              mu_Dsst, sigma_Dsst, 
                              alphaR_Dsst, nR_Dsst)

units_CB_Dsst = ["", "", ""]

aDsst = ROOT.RooRealVar("aDsst", "aDsst", 0.1, -5, 5)
bDsst = ROOT.RooRealVar("bDsst", "bDsst", -0.1, -5, 5)
cDsst = ROOT.RooRealVar("cDsst", "cDsst", -0.1, -5, 5)

cheb_Dsst = ROOT.RooChebychev("cheb_Dsst", "cheb_Dsst", mDsst, ROOT.RooArgList(aDsst, bDsst, cDsst))

units_cheb_Dsst = ["", "", ""]

# Ds1 model pdfs
mDs1 = ROOT.RooRealVar("DsgM", "DsgM", 2350, 2600)

mu_c = 2460; sigma_c = 20; alpha_c = 1; n_c = 2

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-20, mu_c+20)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", sigma_c, 0.1*sigma_c, 2*sigma_c)

units_CB_Ds1 = ["", ""]

aDs1 = ROOT.RooRealVar("aDs1", "aDs1", -5, 5)
bDs1 = ROOT.RooRealVar("bDs1", "bDs1", -5, 5)
cDs1 = ROOT.RooRealVar("cDs1", "cDs1", -5, 5)

cheb_Ds1 = ROOT.RooChebychev("cheb_Ds1", "cheb_Ds1", mDs1, ROOT.RooArgList(aDs1, bDs1, cDs1))

units_cheb_Ds1 = ["", "", ""]

colors = [ROOT.kRed, ROOT.kGreen+2]

ylabel_Dsst = "N_{events} / (2 MeV/c^{2})"; ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

comps_Dsst = ["CB_Dsst", "cheb_Dsst"]; comps_Ds1 = ["CB_Ds1", "cheb_Ds1"]
#--------------------------------------------------------

# Fits for integrated Nch
#--------------------------------------------------------

print("--------------------------------------------------------")
print("Mass fits in MC for integrated Nch")

for trig in trig_conds:
    hDsst = hDsst_full[trig]; hDs1 = hDs1_full[trig]
    
    dh_Dsst = ROOT.RooDataHist("dh_Dsst", "dh_Dsst", ROOT.RooArgList(mDsst), hDsst)
    dh_Ds1  = ROOT.RooDataHist("dh_Ds1", "dh_Ds1", ROOT.RooArgList(mDs1), hDs1)
    
    ## Dsst fit
    Nentries_Dsst = hDsst.Integral()

    NDsst = ROOT.RooRealVar("NDsst", "NDsst", 0, Nentries_Dsst)
    Ncomb_Dsst = ROOT.RooRealVar("Ncomb_Dsst", "Ncomb_Dsst", 0, Nentries_Dsst)

    yields = [NDsst, Ncomb_Dsst]

    for y in yields:
        y.setError(np.sqrt(Nentries_Dsst))

    model_Dsst = ROOT.RooAddPdf("model_Dsst", "model_Dsst", 
                                ROOT.RooArgList(CB_Dsst, cheb_Dsst), 
                                ROOT.RooArgList(NDsst, Ncomb_Dsst))
    units_Dsst = units_CB_Dsst + units_cheb_Dsst + ["", ""]

    Dsst_fit = rpf.Fit(model_Dsst.fitTo(dh_Dsst, ROOT.RooFit.Save()))

    Dsst_fit.save_to_csv(file_name=f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_{trig}_sample.csv")
    Dsst_fit.save_to_latex(mDsst, dh_Dsst, model_Dsst, 
                           units=units_Dsst, fit_type="b", 
                           file_name=f"./latex_tables/Exp_{samples}_Dsst_sig_mass_fit_{trig}_sample.tex")
    rpf.plot(mDsst, dh_Dsst, model_Dsst, 
             comps=comps_Dsst, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Dsst,
             file_name=f"./plots/Exp_{samples}_Dsst_sig_mass_fit_{trig}_sample.pdf")
    
    # NDs1 fit
    mc_Ds1_fit = pd.read_csv(f"./param_files/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_sample.csv")
    
    mc_Ds1_fit_vals = np.array(mc_Ds1_fit[["Parameters", "Values"]])

    alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", mc_Ds1_fit_vals[0, 1])
    nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", mc_Ds1_fit_vals[3, 1])
    alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", mc_Ds1_fit_vals[1, 1])
    nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", mc_Ds1_fit_vals[4, 1])

    CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1",
                                  mDs1, mu_Ds1, sigma_Ds1,
                                  alphaL_Ds1, nL_Ds1,
                                  alphaR_Ds1, nR_Ds1)

    Nentries_Ds1 = hDs1.Integral()

    NDs1 = ROOT.RooRealVar("NDs1", "NDs1", 0, Nentries_Ds1)
    Ncomb_Ds1 = ROOT.RooRealVar("Ncomb_Ds1", "Ncomb_Ds1", 0, Nentries_Ds1)

    yields = [NDs1, Ncomb_Ds1]

    for y in yields:
        y.setError(np.sqrt(Nentries_Ds1))

    model_Ds1 = ROOT.RooAddPdf("model_Ds1", "model_Ds1", 
                               ROOT.RooArgList(CB_Ds1, cheb_Ds1), 
                               ROOT.RooArgList(NDs1, Ncomb_Ds1))
    units_Ds1 = units_CB_Ds1 + units_cheb_Ds1 + ["", ""]
    
    Ds1_fit = rpf.Fit(model_Ds1.fitTo(dh_Ds1, ROOT.RooFit.Save()))

    Ds1_fit.save_to_csv(file_name=f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_{trig}_sample.csv")
    Ds1_fit.save_to_latex(mDs1, dh_Ds1, model_Ds1, 
                          units=units_Ds1, fit_type="b", 
                          file_name=f"./latex_tables/Exp_{samples}_Ds1_sig_mass_fit_{trig}_sample.tex")
    rpf.plot(mDs1, dh_Ds1, model_Ds1, 
             comps=comps_Ds1, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Ds1,
             file_name=f"./plots/Exp_{samples}_Ds1_sig_mass_fit_{trig}_sample.pdf") 

print("--------------------------------------------------------")
#--------------------------------------------------------

# Fits vs nVeloTracks
#--------------------------------------------------------
print("--------------------------------------------------------")
print("Mass fits in MC vs nVeloTracks")

for trig in trig_conds:
    hsDsst = hDsst_nVelo[trig]; hsDs1 = hDs1_nVelo[trig]
    
    Dsst_fits_vals = []; Dsst_fits_uncs = [] 
    Ds1_fits_vals  = []; Ds1_fits_uncs = []

    i = 0

    ## Dsst fit
    for hDsst, hDs1 in zip(hsDsst, hsDs1):
        
        dh_Dsst = ROOT.RooDataHist("dh_Dsst", "dh_Dsst", ROOT.RooArgList(mDsst), hDsst)
        dh_Ds1  = ROOT.RooDataHist("dh_Ds1", "dh_Ds1", ROOT.RooArgList(mDs1), hDs1)
        
        i += 1
        Nentries_Dsst = hDsst.Integral()

        NDsst = ROOT.RooRealVar("NDsst", "NDsst", 0, Nentries_Dsst)
        Ncomb_Dsst = ROOT.RooRealVar("Ncomb_Dsst", "Ncomb_Dsst", 0, Nentries_Dsst)

        yields = [NDsst, Ncomb_Dsst]

        for y in yields:
            y.setError(np.sqrt(Nentries_Dsst))

        model_Dsst = ROOT.RooAddPdf("model_Dsst", "model_Dsst", ROOT.RooArgList(CB_Dsst, cheb_Dsst), ROOT.RooArgList(NDsst, Ncomb_Dsst))
        units_Dsst = units_CB_Dsst + units_cheb_Dsst + ["", ""]

        Dsst_fit = rpf.Fit(model_Dsst.fitTo(dh_Dsst, ROOT.RooFit.Save()))

        Dsst_fit.save_to_latex(mDsst, dh_Dsst, model_Dsst, 
                               units=units_Dsst, fit_type="b", 
                               file_name=f"./latex_tables/Exp_{samples}_Dsst_sig_mass_fit_{trig}_nVelo_{i}_sample.tex")
        rpf.plot(mDsst, dh_Dsst, model_Dsst, 
                 comps=comps_Dsst, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Dsst,
                 file_name=f"./plots/Exp_{samples}_Dsst_sig_mass_fit_{trig}_nVelo_{i}_sample.pdf") 
        
        # NDs1 fit
        mc_Ds1_fit = pd.read_csv(f"./param_files/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_nVelo_{i}_sample.csv")

        mc_Ds1_fit_vals = np.array(mc_Ds1_fit[["Parameters", "Values"]])

        alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", mc_Ds1_fit_vals[0, 1])
        nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", mc_Ds1_fit_vals[3, 1])
        alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", mc_Ds1_fit_vals[1, 1])
        nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", mc_Ds1_fit_vals[4, 1])

        CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1",
                                      mDs1, mu_Ds1, sigma_Ds1,
                                      alphaL_Ds1, nL_Ds1,
                                      alphaR_Ds1, nR_Ds1)

        Nentries_Ds1 = hDs1.Integral()

        NDs1 = ROOT.RooRealVar("NDs1", "NDs1", 0, Nentries_Ds1)
        Ncomb_Ds1 = ROOT.RooRealVar("Ncomb_Ds1", "Ncomb_Ds1", 0, Nentries_Ds1)

        yields = [NDs1, Ncomb_Ds1]

        for y in yields:
            y.setError(np.sqrt(Nentries_Ds1))

        model_Ds1 = ROOT.RooAddPdf("model_Ds1", "model_Ds1", ROOT.RooArgList(CB_Ds1, cheb_Ds1), ROOT.RooArgList(NDs1, Ncomb_Ds1))
        units_Ds1 = units_CB_Ds1 + units_cheb_Ds1 + ["", ""]

        Ds1_fit = rpf.Fit(model_Ds1.fitTo(dh_Ds1, ROOT.RooFit.Save()))

        Ds1_fit.save_to_latex(mDs1, dh_Ds1, model_Ds1, 
                              units=units_Ds1, fit_type="b", 
                              file_name=f"./latex_tables/Exp_{samples}_Ds1_sig_mass_fit_{trig}_nVelo_{i}_sample.tex")
        rpf.plot(mDs1, dh_Ds1, model_Ds1, 
                 comps=comps_Ds1, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Ds1,
                 file_name=f"./plots/Exp_{samples}_Ds1_sig_mass_fit_{trig}_nVelo_{i}_sample.pdf") 

        Dsst_vals = Dsst_fit.par_values(); Dsst_uncs = Dsst_fit.par_errors(); Dsst_pars = Dsst_fit.par_names()
        Ds1_vals = Ds1_fit.par_values(); Ds1_uncs = Ds1_fit.par_errors(); Ds1_pars = Ds1_fit.par_names()

        Dsst_fits_vals.append(Dsst_vals); Dsst_fits_uncs.append(Dsst_uncs)
        Ds1_fits_vals.append(Ds1_vals); Ds1_fits_uncs.append(Ds1_uncs)
        
        df_Dsst_fits_vals = (pd.DataFrame(np.array(Dsst_fits_vals))).set_axis(Dsst_pars, axis='columns')
        df_Dsst_fits_uncs = (pd.DataFrame(np.array(Dsst_fits_uncs))).set_axis(Dsst_pars, axis='columns')

        df_Ds1_fits_vals = (pd.DataFrame(np.array(Ds1_fits_vals))).set_axis(Ds1_pars, axis='columns')
        df_Ds1_fits_uncs = (pd.DataFrame(np.array(Ds1_fits_uncs))).set_axis(Ds1_pars, axis='columns')

        df_Dsst_fits_uncs.to_csv(f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo_{trig}_sample.csv", index=False)
        df_Dsst_fits_vals.to_csv(f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_vals_{ninters}_nVelo_{trig}_sample.csv", index=False)
        
        df_Ds1_fits_vals.to_csv(f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_vals_{ninters}_nVelo_{trig}_sample.csv", index=False)
        df_Ds1_fits_uncs.to_csv(f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo_{trig}_sample.csv", index=False)
            
print("--------------------------------------------------------")
#--------------------------------------------------------
