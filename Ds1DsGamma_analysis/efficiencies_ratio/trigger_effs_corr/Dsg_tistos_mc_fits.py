from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
pol  = options.polarity
year = options.year
run2 = options.run2

ti = datetime.now()

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

units_CB_Ds1  = ["No units", "MeV/$c^2$", "MeV/$c^2$", "", "", ""]

if run2:
    samples = "run2"
else:
    samples = f"{pol}{year}"

# data reading
#--------------------------------------------------------
path = "./root_files"
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1

trig_conds = ["TIS", "TOS", "TISTOS"]

if run2:
    tdf_Ds1_full = {trig: ROOT.RDataFrame("DecayTree", f"{path}/Ds1DsGamma_MC_sig_*_{trig}*.root") 
                    for trig in trig_conds}
    
    tdf_Ds1_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{path}/Ds1DsGamma_MC_sig_*_{trig}*_nVelo_{i+1}*.root")
                            for i in range(ninters)] for trig in trig_conds}
    
else:
    tdf_Ds1_full = {trig: ROOT.RDataFrame("DecayTree", f"{path}/Ds1DsGamma_MC_{pol}{year}_*_{trig}*.root") 
                    for trig in trig_conds}

    tdf_Ds1_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{path}/Ds1DsGamma_MC_{pol}{year}_*_{trig}*_nVelo_{i+1}*.root")
                            for i in range(ninters)] for trig in trig_conds}
#--------------------------------------------------------

# Signal fit model
#--------------------------------------------------------
Ds1_mass = [2350, 2600]

mDs1 = ROOT.RooRealVar("DsgM", "DsgM", Ds1_mass[0], Ds1_mass[1])

mu_c = 2460; sigma_c = 15; alpha_c = 1; n_c = 4

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-10, mu_c+10)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", 10, 0, 100)
nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", 10, 0, 100)

CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1", mDs1, 
                             mu_Ds1, sigma_Ds1, 
                             alphaL_Ds1, nL_Ds1, 
                             alphaR_Ds1, nR_Ds1)

units_CB_Ds1 = ["", "", "", "", "", ""]
#--------------------------------------------------------

# Fits for integrated Nch
#--------------------------------------------------------

print("--------------------------------------------------------")
print("Ds1DsGamma mass fits in MC for integrated Nch")

for trig in trig_conds:
    print(f"L0 {trig} category")
    
    tdf = tdf_Ds1_full[trig]
    
    np_data = tdf.AsNumpy(columns=["DsgM"])
    
    data = ROOT.RooDataSet.from_numpy(np_data, [mDs1])
    
    fit = rpf.Fit(CB_Ds1.fitTo(data, ROOT.RooFit.Save()))
    
    fit.print()
    fit.save_to_csv(file_name=f"./param_files/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_sample.csv")
    fit.save_to_latex(mDs1, data, CB_Ds1, units_CB_Ds1,
                      fit_type="u", file_name=f"./latex_tables/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_sample.tex")
    rpf.plot(mDs1, data, CB_Ds1,
             file_name=f"./plots/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_sample.pdf", xlabel=label_Dsg_mass, ylabel=ylabel_Ds1)
    
print("--------------------------------------------------------")
#--------------------------------------------------------

# Fits vs nVeloTracks
#--------------------------------------------------------
print("--------------------------------------------------------")
print("Ds1DsGamma mass fits in MC vs nVeloTracks")

for trig in trig_conds:
    print(f"L0 {trig} category")
    
    tdfs = tdf_Ds1_nVelo[trig]
      
    i = 0
    for tdf in tdfs:
        i += 1
        np_data = tdf.AsNumpy(columns=["DsgM"])

        data = ROOT.RooDataSet.from_numpy(np_data, [mDs1])
        
        fit = rpf.Fit(CB_Ds1.fitTo(data, ROOT.RooFit.Save()))

        fit.print()
        
        fit.save_to_csv(file_name=f"./param_files/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_nVelo_{i}_sample.csv")
        fit.save_to_latex(mDs1, data, CB_Ds1, units_CB_Ds1,
                          fit_type="u", file_name=f"./latex_tables/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_nVelo_{i}_sample.tex")
        rpf.plot(mDs1, data, CB_Ds1,
                 file_name=f"./plots/Sim_Ds1_sig_mass_fit_CB_{samples}_{trig}_nVelo_{i}_sample.pdf", xlabel=label_Dsg_mass, ylabel=ylabel_Ds1)
print("--------------------------------------------------------")
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")