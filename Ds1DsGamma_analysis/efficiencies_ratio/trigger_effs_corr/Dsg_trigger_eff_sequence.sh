run2=false
polarities=("MagDown" "MagUp")
years=("2018" "2017" "2016")
gen_samples=false
merged_pol=true

conda activate yvk_env

if $run2; then
    echo "Running the Dsg trigger effs for all Run2 samples"
    echo "--------------------------------"
    python Dsg_trigger_eff_corr.py -t
else
    for y in "${years[@]}"
    do
        if $merged_pol; then
            if $gen_samples; then
                echo "Generating trigger samples for $y data"
                echo "--------------------------------"
                python Dsg_tistos_samples_gen.py -p "merged" -y $y
                echo "--------------------------------"
            fi
            echo "Doing tistos fitting sequence"
            echo "--------------------------------"
            python Dsg_tistos_mc_fits.py -p "merged" -y $y
            python Dsg_tistos_data_fits.py -p "merged" -y $y
            echo "Running the Dsg trigger effs for $y data sample"
            echo "--------------------------------"
            python Dsg_trigger_eff_corr.py -p "merged" -y $y
            echo "Iteration for $y data sample finished" 
            echo "--------------------------------"
        else
            for p in "${polarities[@]}"
                do  
                    if $gen_samples; then
                        echo "Generating trigger samples for $p $y data"
                        echo "--------------------------------"
                        python Dsg_tistos_samples_gen.py -p $p -y $y
                    fi
                    echo "Doing tistos fitting sequence"
                    echo "--------------------------------"
                    python Dsg_tistos_mc_fits.py -p $p -y $y
                    python Dsg_tistos_data_fits.py -p $p -y $y
                    echo "--------------------------------"
                    echo "Running the Dsg trigger effs for $p $y data sample"
                    echo "--------------------------------"
                    python Dsg_trigger_eff_corr.py -p $p -y $y
                    echo "Iteration for $p $y data sample finished" 
                    echo "--------------------------------"
                    fi
            done
        fi
    done
fi