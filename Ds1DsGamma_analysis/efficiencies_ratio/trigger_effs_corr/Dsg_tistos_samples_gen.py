from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
parser.add_option("-m", "--mc", action="store_true", dest="mc", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "param_files", "root_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

pol  = options.polarity
year = options.year
run2 = options.run2

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/"
mc_path = f"{head_path}/MC/TightCut/raw"

dtt = "DsGammaTuple"
    
if run2:
    # data
    years = ["2016", "2017", "2018"]
    pols = ["MagDown", "MagUp"]
    
    data_paths = {f"{pol}{year}": f"{head_path}/Data/raw/{pol}/{year}" for pol in pols for year in years}
    data_files = {keys: os.listdir(values) for keys, values in data_paths.items()}
    data_files = [[f"{values1}/{file}" for file in values2] 
                  for ((keys1, values1), (keys2, values2)) in zip(data_paths.items(), data_files.items())]
    
    data_files = set(sum(data_files, []))
    tdf_data = ROOT.RDataFrame("DecayTree", data_files)
    
    # MC
    if options.mc:
        tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma*.root")
        tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma*.root")
    
else:
    if pol != "merged":
        # data
        data_path = f"{head_path}/Data/raw/{pol}/{year}"
        tdf_data = ROOT.RDataFrame("DecayTree", f"{data_path}/*.root")

        # MC
        if options.mc:
            tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_{pol}{year}*.root")
            tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_{pol}{year}*.root")
            
    if pol == "merged":
        pols = ["MagDown", "MagUp"]
        
        data_paths = {f"{pol}": f"{head_path}/Data/raw/{pol}/{year}" for pol in pols}
        data_files = {keys: os.listdir(values) for keys, values in data_paths.items()}
        data_files = [[f"{values1}/{file}" for file in values2] 
                      for ((keys1, values1), (keys2, values2)) in zip(data_paths.items(), data_files.items())]

        data_files = set(sum(data_files, []))
        tdf_data = ROOT.RDataFrame("DecayTree", data_files)

        # MC
        if options.mc:
            tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_*{year}*.root")
            tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_*{year}*.root")
        

derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

for keys, values in derivated_features.items():
    tdf_data = tdf_data.Define(keys, values)
    
    if options.mc:
        tdf_Ds1_MC = tdf_Ds1_MC.Define(keys, values)
        tdf_Dsst_MC = tdf_Dsst_MC.Define(keys, values)
    
# data mass windows
Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

tdf_data_Dsst = tdf_data.Filter(f"DsgM > {Dsst_mass[0]} & DsgM < {Dsst_mass[1]}")
tdf_data_Ds1 = tdf_data.Filter(f"DsgM > {Ds1_mass[0]} & DsgM < {Ds1_mass[1]}")

# mctruth matching 
if options.mc:
    TRUEID = rsh.TRUEIDs()
    TRUEID_dtt = TRUEID[dtt]

    TRUEID_Dsst_sig = TRUEID_dtt["DsstDsGamma"]
    TRUEID_Ds1_sig  = TRUEID_dtt["Ds1DsGamma"]

    tdf_Dsst_sig = tdf_Dsst_MC.Filter(TRUEID_Dsst_sig)
    tdf_Ds1_sig  = tdf_Ds1_MC.Filter(TRUEID_Ds1_sig)
    
    tdf_Dsst_sig = tdf_Dsst_sig.Filter(f"DsgM > {Dsst_mass[0]} & DsgM < {Dsst_mass[1]}")
    tdf_Ds1_sig  = tdf_Ds1_sig.Filter(f"DsgM > {Ds1_mass[0]} & DsgM < {Ds1_mass[1]}")

#--------------------------------------------------------

# evt selection + cand selection
#--------------------------------------------------------
sel_path = "../../selection/"

evt_sel_file = open(f"{sel_path}/param_files/evt_selection.txt", "r")
evt_sel = evt_sel_file.read()
evt_sel_file.close()

cand_sel_file = open(f"{sel_path}/param_files/cand_selection.txt", "r")
cand_sel = cand_sel_file.read()
cand_sel_file.close()

sel = f"{evt_sel} && {cand_sel}"

tdf_data_Dsst_sel = tdf_data_Dsst.Filter(sel)
tdf_data_Ds1_sel  = tdf_data_Ds1.Filter(sel)

if options.mc:
    tdf_Ds1_sig_sel = tdf_Ds1_sig.Filter(sel)
    tdf_Dsst_sig_sel = tdf_Dsst_sig.Filter(sel)

## plotting configs
Dsg_mass_label = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
ylabel = "A.U / (6 MeV/c^{2})"

## nVeloTracks bins
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"
nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1
nVelo_bins = [[nVelo_intervals[i-1], nVelo_intervals[i]] for i in range(1, len(nVelo_intervals))]
#-------------------------------------------------------

# L0 trigger
#--------------------------------------------------------
lz_tis = "Dsg_L0Global_TIS == 1"
lz_tos = "Dsg_L0Global_TOS == 1"

lz_conds = {"TIS":    lz_tis, 
            "TOS":    lz_tos, 
            "TISTOS": f"{lz_tis} & {lz_tos}"}

columns = ["DsgM", "nVeloTracks"]

## MC: ntuples generation
if options.mc:
    
    ### Nch integrated
    tdf_Dsst_sig_sel_lz_trig = {k: tdf_Dsst_sig_sel.Filter(cut) for k, cut in lz_conds.items()}
    tdf_Ds1_sig_sel_lz_trig  = {k: tdf_Ds1_sig_sel.Filter(cut) for k, cut in lz_conds.items()}

    ### nVelo bins
    tdf_Dsst_sig_sel_lz_trig_nVelo = {k: [tdf.Filter(f"nVeloTracks > {bins[0]} & nVeloTracks <= {bins[1]}") for bins in nVelo_bins]
                                      for k, tdf in tdf_Dsst_sig_sel_lz_trig.items()}
    tdf_Ds1_sig_sel_lz_trig_nVelo = {k: [tdf.Filter(f"nVeloTracks > {bins[0]} & nVeloTracks <= {bins[1]}") for bins in nVelo_bins]
                                      for k, tdf in tdf_Ds1_sig_sel_lz_trig.items()}
    
    for cond in lz_conds:
        if run2:
            Dsst_file = f"DsstDsGamma_MC_sig_sel_lz_{cond}_sample.root"
            Ds1_file  = f"Ds1DsGamma_MC_sig_sel_lz_{cond}_sample.root"
        else:
            Dsst_file = f"DsstDsGamma_MC_{pol}{year}_sig_sel_lz_{cond}_sample.root"
            Ds1_file  = f"Ds1DsGamma_MC_{pol}{year}_sig_sel_lz_{cond}_sample.root"
        
        if Dsst_file not in os.listdir("./root_files"):
            tdf_Dsst_sig_sel_lz_trig[cond].Snapshot("DecayTree", f"./root_files/{Dsst_file}", columns)
        else:
            print("DsstDsGamma for integrated Nch already exists")
            
        if Ds1_file not in os.listdir("./root_files"):
            tdf_Ds1_sig_sel_lz_trig[cond].Snapshot("DecayTree", f"./root_files/{Ds1_file}", columns)
        else:
            print("Ds1DsGamma for integrated Nch already exists")
            
        tdfs_Dsst = tdf_Dsst_sig_sel_lz_trig_nVelo[cond]
        tdfs_Ds1  = tdf_Ds1_sig_sel_lz_trig_nVelo[cond]

        i = 0
        for tdf_Dsst, tdf_Ds1 in zip(tdfs_Dsst, tdfs_Ds1):
            
            i += 1
            if run2:
                Dsst_file = f"DsstDsGamma_MC_sig_sel_lz_{cond}_nVelo_{i}_sample.root"
                Ds1_file  = f"Ds1DsGamma_MC_sig_sel_lz_{cond}_nVelo_{i}_sample.root"
            else:
                Dsst_file = f"DsstDsGamma_MC_{pol}{year}_sig_sel_lz_{cond}_nVelo_{i}_sample.root"
                Ds1_file  = f"Ds1DsGamma_MC_{pol}{year}_sig_sel_lz_{cond}_nVelo_{i}_sample.root"
                
            if Dsst_file not in os.listdir("./root_files"):
                tdf_Dsst.Snapshot("DecayTree", f"./root_files/{Dsst_file}", columns)
            else: 
                print(f"DsstDsGamma for {i}º nVeloTracks bin already exists")

            if Ds1_file not in os.listdir("./root_files"):
                tdf_Ds1.Snapshot("DecayTree", f"./root_files/{Ds1_file}", columns)
            else: 
                print(f"Ds1DsGamma for {i}º nVeloTracks bin already exists")
                
## Data: histogram saving

### Nch integrated
tdf_data_Dsst_sel_lz_trig = {k: tdf_data_Dsst_sel.Filter(cut) for k, cut in lz_conds.items()}
tdf_data_Ds1_sel_lz_trig  = {k: tdf_data_Ds1_sel.Filter(cut) for k, cut in lz_conds.items()}

hdata_mass_Dsst_sel_lz_trig = {k: tdf.Histo1D(("","",100,Dsst_mass[0], Dsst_mass[1]), "DsgM")  
                               for k, tdf in tdf_data_Dsst_sel_lz_trig.items()}
hdata_mass_Ds1_sel_lz_trig = {k: tdf.Histo1D(("","",100,Ds1_mass[0], Ds1_mass[1]), "DsgM")  
                              for k, tdf in tdf_data_Ds1_sel_lz_trig.items()}
### nVelo bins
tdf_data_Dsst_sel_lz_trig_nVelo = {k: [tdf.Filter(f"nVeloTracks > {bins[0]} & nVeloTracks <= {bins[1]}") for bins in nVelo_bins]
                                   for k, tdf in tdf_data_Dsst_sel_lz_trig.items()}
tdf_data_Ds1_sel_lz_trig_nVelo  = {k: [tdf.Filter(f"nVeloTracks > {bins[0]} & nVeloTracks <= {bins[1]}") for bins in nVelo_bins]
                                   for k, tdf in tdf_data_Ds1_sel_lz_trig.items()}

hdata_mass_Dsst_sel_lz_trig_nVelo = {k: [tdf.Histo1D(("","",100,Dsst_mass[0], Dsst_mass[1]), "DsgM") for tdf in tdfs]
                                   for k, tdfs in tdf_data_Dsst_sel_lz_trig_nVelo.items()}
hdata_mass_Ds1_sel_lz_trig_nVelo  = {k: [tdf.Histo1D(("","",100,Ds1_mass[0], Ds1_mass[1]), "DsgM") for tdf in tdfs]
                                   for k, tdfs in tdf_data_Ds1_sel_lz_trig_nVelo.items()}

for cond in lz_conds: 
    if run2:
        Dsst_file = f"DsJ_data_Dsst_mass_lz_{cond}_sample.root"
        Ds1_file  = f"DsJ_data_Ds1_mass_lz_{cond}_sample.root"
    else:
        Dsst_file = f"DsJ_data_{pol}{year}_Dsst_mass_lz_{cond}_sample.root"
        Ds1_file  = f"DsJ_data_{pol}{year}_Ds1_mass_lz_{cond}_sample.root"
    
    
    if Dsst_file not in os.listdir("./root_files"):
        Dsst_file = ROOT.TFile.Open(f"./root_files/{Dsst_file}", "RECREATE")  
        hdata_mass_Dsst_sel_lz_trig[cond].Write(f"Dsst_mass_{cond}")
        Dsst_file.Close()
    else:
        print("Dsst mass histo for integrated Nch already exists")
        
    if Ds1_file not in os.listdir("./root_files"):
        Ds1_file = ROOT.TFile.Open(f"./root_files/{Ds1_file}", "RECREATE")  
        hdata_mass_Ds1_sel_lz_trig[cond].Write(f"Ds1_mass_{cond}")
        Ds1_file.Close()
    else:
        print("Ds1 mass histo for integrated Nch already exists")
        
    hs_Dsst = hdata_mass_Dsst_sel_lz_trig_nVelo[cond]
    hs_Ds1  = hdata_mass_Ds1_sel_lz_trig_nVelo[cond]
    
    i = 0
    for h_Dsst, h_Ds1 in zip(hs_Dsst, hs_Ds1):
        i += 1
        if run2:
            Dsst_file = f"DsJ_data_Dsst_mass_lz_{cond}_nVelo_{i}_sample.root"
            Ds1_file  = f"DsJ_data_Ds1_mass_lz_{cond}_nVelo_{i}_sample.root"
        else:
            Dsst_file = f"DsJ_data_{pol}{year}_Dsst_mass_lz_{cond}_nVelo_{i}_sample.root"
            Ds1_file  = f"DsJ_data_{pol}{year}_Ds1_mass_lz_{cond}_nVelo_{i}_sample.root"

        if Dsst_file not in os.listdir("./root_files"):
            Dsst_file = ROOT.TFile.Open(f"./root_files/{Dsst_file}", "RECREATE")  
            h_Dsst.Write(f"Dsst_mass_{cond}")
            Dsst_file.Close()
        else: 
            print(f"Dsst mass histo for {i}º nVeloTracks bin already exists")
        
        if Ds1_file not in os.listdir("./root_files"):
            Ds1_file = ROOT.TFile.Open(f"./root_files/{Ds1_file}", "RECREATE")  
            h_Ds1.Write(f"Ds1_mass_{cond}")
            Ds1_file.Close()
        else: 
            print(f"Ds1 mass histo for {i}º nVeloTracks bin already exists")

#--------------------------------------------------------


tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")