from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

# 3 options: polarity + year, year or Run 2
# polarity: MagUp, MagDown, Both
# year: 2018, 2017, 2016

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import uncertainties as un
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "param_files", "root_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

pol  = options.polarity
year = options.year
run2 = options.run2

Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

# trigger eff ratio integrated Nch
#--------------------------------------------------------

## MC ratio
mc_path = "./root_files/"
trig_conds = ["TOS", "TISTOS"]

if run2:
    samples = "run2"
else: 
    samples = f"{pol} {year}"

if run2:
    tdf_Dsst_full = {trig: ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_MC_sig_*_{trig}*.root") 
                     for trig in trig_conds}
    
    tdf_Ds1_full = {trig: ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_MC_sig_*_{trig}*.root") 
                    for trig in trig_conds}
else:
    tdf_Dsst_full = {trig: ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_MC_{pol}{year}_*_{trig}*.root") 
                     for trig in trig_conds}

    tdf_Ds1_full = {trig: ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_MC_{pol}{year}_*_{trig}*.root") 
                    for trig in trig_conds}

hmc_mass_Dsst_full = {trig: tdf.Histo1D(("", "", 100, Dsst_mass[0], Dsst_mass[1]), "DsgM") 
                      for trig, tdf in tdf_Dsst_full.items()}

hmc_mass_Ds1_full = {trig: tdf.Histo1D(("", "", 100, Ds1_mass[0], Ds1_mass[1]), "DsgM") 
                     for trig, tdf in tdf_Ds1_full.items()}

mNtos_mc_Dsst    = hmc_mass_Dsst_full["TOS"].Integral();    uNtos_mc_Dsst = np.sqrt(mNtos_mc_Dsst)
mNtistos_mc_Dsst = hmc_mass_Dsst_full["TISTOS"].Integral(); uNtistos_mc_Dsst = np.sqrt(mNtistos_mc_Dsst)

Ntos_mc_Dsst    = un.ufloat(mNtos_mc_Dsst, uNtos_mc_Dsst)
Ntistos_mc_Dsst = un.ufloat(mNtistos_mc_Dsst, uNtistos_mc_Dsst)

mNtos_mc_Ds1    = hmc_mass_Ds1_full["TOS"].Integral();    uNtos_mc_Ds1 = np.sqrt(mNtos_mc_Ds1)
mNtistos_mc_Ds1 = hmc_mass_Ds1_full["TISTOS"].Integral(); uNtistos_mc_Ds1 = np.sqrt(mNtistos_mc_Ds1)

Ntos_mc_Ds1    = un.ufloat(mNtos_mc_Ds1, uNtos_mc_Ds1)
Ntistos_mc_Ds1 = un.ufloat(mNtistos_mc_Ds1, uNtistos_mc_Ds1)

eff_tis_mc_Dsst = Ntistos_mc_Dsst / Ntos_mc_Dsst
eff_tis_mc_Ds1  = Ntistos_mc_Ds1 / Ntos_mc_Ds1

ratio_eff_tis_mc = eff_tis_mc_Dsst / eff_tis_mc_Ds1
    
print(f"MC {samples} L0 TIS efficiency for Dsst = {eff_tis_mc_Dsst}")
print(f"MC {samples} L0 TIS efficiency for Ds1  = {eff_tis_mc_Ds1}")
print(f"MC {samples} L0 TIS efficiency ratio = {ratio_eff_tis_mc}")

## datio ratio
Dsst_tos_fit_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_TOS_sample.csv"
Ds1_tos_fit_file  = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_TOS_sample.csv"
    
Dsst_tistos_fit_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_TISTOS_sample.csv"
Ds1_tistos_fit_file  = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_TISTOS_sample.csv"

print(f"data {samples} sample used")

df_Dsst_tos_fit = pd.read_csv(Dsst_tos_fit_file)
df_Ds1_tos_fit  = pd.read_csv(Ds1_tos_fit_file)

df_Dsst_tistos_fit = pd.read_csv(Dsst_tistos_fit_file)
df_Ds1_tistos_fit  = pd.read_csv(Ds1_tistos_fit_file)

mNDsst_data_tos  = float((df_Dsst_tos_fit.query('Parameters == "NDsst"'))["Values"])
uNDsst_data_tos = float((df_Dsst_tos_fit.query('Parameters == "NDsst"'))["Errors"])

mNDsst_data_tistos = float((df_Dsst_tistos_fit.query('Parameters == "NDsst"'))["Values"])
uNDsst_data_tistos = float((df_Dsst_tistos_fit.query('Parameters == "NDsst"'))["Errors"])

NDsst_data_tos = un.ufloat(mNDsst_data_tos, uNDsst_data_tos)
NDsst_data_tistos = un.ufloat(mNDsst_data_tistos, uNDsst_data_tistos)

mNDs1_data_tos  = float((df_Ds1_tos_fit.query('Parameters == "NDs1"'))["Values"])
uNDs1_data_tos = float((df_Ds1_tos_fit.query('Parameters == "NDs1"'))["Errors"])

mNDs1_data_tistos  = float((df_Ds1_tistos_fit.query('Parameters == "NDs1"'))["Values"])
uNDs1_data_tistos = float((df_Ds1_tistos_fit.query('Parameters == "NDs1"'))["Errors"])

NDs1_data_tos = un.ufloat(mNDs1_data_tos, uNDs1_data_tos)
NDs1_data_tistos = un.ufloat(mNDs1_data_tistos, uNDs1_data_tistos)

eff_tis_data_Dsst = NDsst_data_tistos / NDsst_data_tos
eff_tis_data_Ds1  = NDs1_data_tistos / NDs1_data_tos

ratio_eff_tis_data  = eff_tis_data_Dsst / eff_tis_data_Ds1

print(f"data {samples} L0 TIS efficiency for Dsst = {eff_tis_data_Dsst}")
print(f"data {samples} L0 TIS efficiency for Ds1  = {eff_tis_data_Ds1}")
print(f"data {samples} L0 TIS efficiency ratio = {ratio_eff_tis_data}")

## weights
w_eff_tis_ratio  = ratio_eff_tis_data / ratio_eff_tis_mc

print(f"data and MC {samples} L0 TIS efficiency difference = {w_eff_tis_ratio}")
#--------------------------------------------------------

# trigger eff ratio vs nVeloTracks
#--------------------------------------------------------

## MC ratio
inters_path = "../../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1

Ds1_vals_file = f"../../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"

df_Ds1_fit_vals = pd.read_csv(Ds1_vals_file)

nVelo_means = df_Ds1_fit_vals["nVelo_mean"]
nVelo_up    = df_Ds1_fit_vals["nVelo_uup"]
nVelo_down  = df_Ds1_fit_vals["nVelo_udown"]

if run2:
    tdf_Dsst_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_MC_sig_*_{trig}*_nVelo_{i+1}*.root")
                        for i in range(ninters)] for trig in trig_conds}
    tdf_Ds1_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_MC_sig_*_{trig}*_nVelo_{i+1}*.root")
                            for i in range(ninters)] for trig in trig_conds}
else:
    tdf_Dsst_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{mc_path}/DsstDsGamma_MC_{pol}{year}_*_{trig}*_nVelo_{i+1}*.root")
                        for i in range(ninters)] for trig in trig_conds}
    tdf_Ds1_nVelo = {trig: [ROOT.RDataFrame("DecayTree", f"{mc_path}/Ds1DsGamma_MC_{pol}{year}_*_{trig}*_nVelo_{i+1}*.root")
                            for i in range(ninters)] for trig in trig_conds}

hmc_mass_Dsst_nVelo = {trig: [tdf.Histo1D(("","",100, Dsst_mass[0], Dsst_mass[1]), "DsgM") for tdf in tdfs]
                       for trig, tdfs in tdf_Dsst_nVelo.items()}

hmc_mass_Ds1_nVelo = {trig: [tdf.Histo1D(("","",100, Ds1_mass[0], Ds1_mass[1]), "DsgM") for tdf in tdfs]
                      for trig, tdfs in tdf_Ds1_nVelo.items()}

mNtos_mc_Dsst_nVelo = np.array([h.Integral() for h in hmc_mass_Dsst_nVelo["TOS"]]); uNtos_mc_Dsst_nVelo = np.sqrt(mNtos_mc_Dsst_nVelo)
mNtistos_mc_Dsst_nVelo = np.array([h.Integral() for h in hmc_mass_Dsst_nVelo["TISTOS"]]); uNtistos_mc_Dsst_nVelo = np.sqrt(mNtistos_mc_Dsst_nVelo)

Ntos_mc_Dsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNtos_mc_Dsst_nVelo, uNtos_mc_Dsst_nVelo)])
Ntistos_mc_Dsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNtistos_mc_Dsst_nVelo, uNtistos_mc_Dsst_nVelo)])

mNtos_mc_Ds1_nVelo = np.array([h.Integral() for h in hmc_mass_Ds1_nVelo["TOS"]]); uNtos_mc_Ds1_nVelo = np.sqrt(mNtos_mc_Ds1_nVelo)
mNtistos_mc_Ds1_nVelo = np.array([h.Integral() for h in hmc_mass_Ds1_nVelo["TISTOS"]]); uNtistos_mc_Ds1_nVelo = np.sqrt(mNtistos_mc_Ds1_nVelo)

Ntos_mc_Ds1_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNtos_mc_Ds1_nVelo, uNtos_mc_Ds1_nVelo)])
Ntistos_mc_Ds1_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNtistos_mc_Ds1_nVelo, uNtistos_mc_Ds1_nVelo)])

eff_tis_mc_Dsst_nVelo = Ntistos_mc_Dsst_nVelo / Ntos_mc_Dsst_nVelo
meff_tis_mc_Dsst_nVelo = np.array([eff.nominal_value for eff in eff_tis_mc_Dsst_nVelo])
ueff_tis_mc_Dsst_nVelo = np.array([eff.std_dev for eff in eff_tis_mc_Dsst_nVelo])

eff_tis_mc_Ds1_nVelo = Ntistos_mc_Ds1_nVelo / Ntos_mc_Ds1_nVelo
meff_tis_mc_Ds1_nVelo = np.array([eff.nominal_value for eff in eff_tis_mc_Ds1_nVelo])
ueff_tis_mc_Ds1_nVelo = np.array([eff.std_dev for eff in eff_tis_mc_Ds1_nVelo])

ratio_eff_tis_mc_nVelo = eff_tis_mc_Dsst_nVelo / eff_tis_mc_Ds1_nVelo
mratio_eff_tis_mc_nVelo = np.array([r.nominal_value for r in ratio_eff_tis_mc_nVelo])
uratio_eff_tis_mc_nVelo = np.array([r.std_dev for r in ratio_eff_tis_mc_nVelo])

## datio ratio
Dsst_tos_nVelo_vals_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_vals_{ninters}_nVelo_TOS_sample.csv"
Dsst_tos_nVelo_uncs_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo_TOS_sample.csv"

Dsst_tistos_nVelo_vals_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_vals_{ninters}_nVelo_TISTOS_sample.csv"
Dsst_tistos_nVelo_uncs_file = f"./param_files/Exp_{samples}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo_TISTOS_sample.csv"
    
Ds1_tos_nVelo_vals_file = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_vals_{ninters}_nVelo_TOS_sample.csv"
Ds1_tos_nVelo_uncs_file = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo_TOS_sample.csv"

Ds1_tistos_nVelo_vals_file = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_vals_{ninters}_nVelo_TISTOS_sample.csv"
Ds1_tistos_nVelo_uncs_file = f"./param_files/Exp_{samples}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo_TISTOS_sample.csv"

df_Dsst_tos_nVelo_vals = pd.read_csv(Dsst_tos_nVelo_vals_file)
df_Dsst_tos_nVelo_uncs = pd.read_csv(Dsst_tos_nVelo_uncs_file)

df_Dsst_tistos_nVelo_vals = pd.read_csv(Dsst_tistos_nVelo_vals_file)
df_Dsst_tistos_nVelo_uncs = pd.read_csv(Dsst_tistos_nVelo_uncs_file)

df_Ds1_tos_nVelo_vals = pd.read_csv(Ds1_tos_nVelo_vals_file)
df_Ds1_tos_nVelo_uncs = pd.read_csv(Ds1_tos_nVelo_uncs_file)

df_Ds1_tistos_nVelo_vals = pd.read_csv(Ds1_tistos_nVelo_vals_file)
df_Ds1_tistos_nVelo_uncs = pd.read_csv(Ds1_tistos_nVelo_uncs_file)

mNDsst_data_tos_nVelo = np.array(df_Dsst_tos_nVelo_vals["NDsst"])
uNDsst_data_tos_nVelo = np.array(df_Dsst_tos_nVelo_uncs["NDsst"])

mNDsst_data_tistos_nVelo = np.array(df_Dsst_tistos_nVelo_vals["NDsst"])
uNDsst_data_tistos_nVelo = np.array(df_Dsst_tistos_nVelo_uncs["NDsst"])

Ntos_data_Dsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNDsst_data_tos_nVelo, uNDsst_data_tos_nVelo)])
Ntistos_data_Dsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNDsst_data_tistos_nVelo, uNDsst_data_tistos_nVelo)])

mNDs1_data_tos_nVelo = np.array(df_Ds1_tos_nVelo_vals["NDs1"])
uNDs1_data_tos_nVelo = np.array(df_Ds1_tos_nVelo_uncs["NDs1"])

mNDs1_data_tistos_nVelo = np.array(df_Ds1_tistos_nVelo_vals["NDs1"])
uNDs1_data_tistos_nVelo = np.array(df_Ds1_tistos_nVelo_uncs["NDs1"])

Ntos_data_Ds1_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNDs1_data_tos_nVelo, uNDs1_data_tos_nVelo)])
Ntistos_data_Ds1_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNDs1_data_tistos_nVelo, uNDs1_data_tistos_nVelo)])

eff_tis_data_Dsst_nVelo = Ntistos_data_Dsst_nVelo / Ntos_data_Dsst_nVelo
meff_tis_data_Dsst_nVelo = np.array([eff.nominal_value for eff in eff_tis_data_Dsst_nVelo])
ueff_tis_data_Dsst_nVelo = np.array([eff.std_dev for eff in eff_tis_data_Dsst_nVelo])

eff_tis_data_Ds1_nVelo = Ntistos_data_Ds1_nVelo / Ntos_data_Ds1_nVelo
meff_tis_data_Ds1_nVelo = np.array([eff.nominal_value for eff in eff_tis_data_Ds1_nVelo])
ueff_tis_data_Ds1_nVelo = np.array([eff.std_dev for eff in eff_tis_data_Ds1_nVelo])

ratio_eff_tis_data_nVelo = eff_tis_data_Dsst_nVelo / eff_tis_data_Ds1_nVelo
mratio_eff_tis_data_nVelo = np.array([r.nominal_value for r in ratio_eff_tis_data_nVelo])
uratio_eff_tis_data_nVelo = np.array([r.std_dev for r in ratio_eff_tis_data_nVelo])

plt.figure(1)
plt.subplot(121)
plt.errorbar(nVelo_means, meff_tis_data_Dsst_nVelo, ueff_tis_data_Dsst_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="red", ecolor="red", capsize=7, label="Data")
plt.errorbar(nVelo_means, meff_tis_mc_Dsst_nVelo, ueff_tis_mc_Dsst_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="magenta", ecolor="magenta", capsize=7, label="MC")
plt.ylim(0, 1)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{L0,TIS}$")
plt.title(f"L0 TIS eff {samples} Dsst")
plt.legend()
plt.subplot(122)
plt.errorbar(nVelo_means, meff_tis_data_Ds1_nVelo, ueff_tis_data_Ds1_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="blue", ecolor="blue", capsize=7, label="Data")
plt.errorbar(nVelo_means, meff_tis_mc_Ds1_nVelo, ueff_tis_mc_Ds1_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="violet", ecolor="violet", capsize=7, label="MC")
plt.ylim(0, 1)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{L0,TIS}$")
plt.title(f"L0 TIS eff {samples} Ds1")
plt.legend()
plt.savefig(f"./plots/Comp_lz_tis_effs_{ninters}_nVelo_data_mc.pdf", dpi=300, bbox_inches='tight')

plt.figure(2)
plt.errorbar(nVelo_means, mratio_eff_tis_data_nVelo, uratio_eff_tis_data_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7, label="Data")
plt.errorbar(nVelo_means, mratio_eff_tis_mc_nVelo, uratio_eff_tis_mc_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="green", ecolor="green", capsize=7, label="MC")
plt.ylim(0.5, 2)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{L0,TIS}(D_s^{*+})/\varepsilon_{L0,TIS}(D_{s1}^+)$")
plt.legend()
plt.savefig(f"./plots/Comp_lz_tis_effs_ratio_{ninters}_nVelo_data_mc.pdf", dpi=300, bbox_inches='tight')

## weights
w_eff_tis_ratio_nVelo  = ratio_eff_tis_data_nVelo / ratio_eff_tis_mc_nVelo
mw_eff_tis_ratio_nVelo = np.array([w.nominal_value for w in w_eff_tis_ratio_nVelo])
uw_eff_tis_ratio_nVelo = np.array([w.std_dev for w in w_eff_tis_ratio_nVelo])

plt.figure(3)
plt.errorbar(nVelo_means, mw_eff_tis_ratio_nVelo, uw_eff_tis_ratio_nVelo, xerr=[nVelo_down, nVelo_up], 
             fmt='.', color="black", ecolor="black", capsize=7)
plt.ylim(0.5, 2)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$\varepsilon_{L0,TIS}(D_s^{*+})/\varepsilon_{L0,TIS}(D_{s1}^+)$ correction")
plt.savefig(f"./plots/Comp_lz_tis_effs_ratio_weight_{ninters}_nVelo_data_mc.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")