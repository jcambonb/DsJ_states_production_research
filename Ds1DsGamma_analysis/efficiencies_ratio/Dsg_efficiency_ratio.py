from optparse import OptionParser

parser = OptionParser()
parser.add_option("-w", "--yprops", action="store_true", dest="yprops", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

years = ["2016", "2017", "2018"]
tot_eff_path = "./total_efficiency_in_mc/param_files"
props_path = "../mass_fits/param_files"

wy = options.yprops

# Integrated nVeloTracks
#--------------------------------------------------------
# Total efficiencies efficiencies per year
eff_tot_Dsst_year = {}
eff_tot_Ds1_year  = {}

for y in years:
    with open(f"{tot_eff_path}/Sim_Both{y}_eff_stats_fst_order.yaml", "r") as file:
        eff_stats = yaml.safe_load(file)
        
        eff_tot_Dsst_year[y] = un.ufloat(eff_stats["tot_eff_Dsst_val"], eff_stats["tot_eff_Dsst_unc"])
        eff_tot_Ds1_year[y]  = un.ufloat(eff_stats["tot_eff_Ds1_val"], eff_stats["tot_eff_Ds1_unc"])

# data proportions
with open(f"{props_path}/Exp_Run2_Dsst_yield_props_vals.yaml", "r") as file:
    year_props_Dsst = yaml.safe_load(file)

with open(f"{props_path}/Exp_Run2_Ds1_yield_props_vals.yaml", "r") as file:
    year_props_Ds1 = yaml.safe_load(file)

meff_tot_Dsst_year = np.array([eff_tot_Dsst_year[y].n for y in years])
ueff_tot_Dsst_year = np.array([eff_tot_Dsst_year[y].s for y in years])

meff_tot_Ds1_year = np.array([eff_tot_Ds1_year[y].n for y in years])
ueff_tot_Ds1_year = np.array([eff_tot_Ds1_year[y].s for y in years])

weff_tot_Dsst_year = np.array([year_props_Dsst[y] for y in years])
weff_tot_Ds1_year  = np.array([year_props_Ds1[y] for y in years])

iueff_tot_Dsst_year = 1 / (ueff_tot_Dsst_year ** 2)
iueff_tot_Ds1_year  = 1 / (ueff_tot_Ds1_year ** 2)

if wy:
    print("Doing weighted average with weights calculated as the data yield proportions per year")
    weff_tot_Dsst_year = np.array([year_props_Dsst[y] for y in years])
    weff_tot_Ds1_year  = np.array([year_props_Ds1[y] for y in years])
else:
    print("Doing weighted average with weighted calculated as the inverse of the uncertainty")
    weff_tot_Dsst_year = iueff_tot_Dsst_year
    weff_tot_Ds1_year  = iueff_tot_Ds1_year
    
# nominal value
meff_tot_Dsst = np.sum(weff_tot_Dsst_year*meff_tot_Dsst_year) / np.sum(weff_tot_Dsst_year)
meff_tot_Ds1  = np.sum(weff_tot_Ds1_year*meff_tot_Ds1_year) / np.sum(weff_tot_Ds1_year)

ueff_tot_Dsst = 1 /np.sqrt(np.sum(iueff_tot_Dsst_year))
ueff_tot_Ds1  = 1 /np.sqrt(np.sum(iueff_tot_Ds1_year))

eff_tot_Dsst = un.ufloat(meff_tot_Dsst, ueff_tot_Dsst)
eff_tot_Ds1  = un.ufloat(meff_tot_Ds1, ueff_tot_Ds1)

print(f"Average efficiency for Dsst = {eff_tot_Dsst}")
print(f"Average efficiency for Ds1  = {eff_tot_Ds1}")

x = [i for i in range(1, 4)]
plt.figure(1)
plt.subplot(1, 2, 1)
plt.errorbar(x, meff_tot_Dsst_year, ueff_tot_Dsst_year, color="red", fmt=".", label="Dsst MC", capsize=7)
plt.axhline(meff_tot_Dsst, color="red", label="Average", linestyle="dashed")
plt.xticks(x, years)
plt.title("Dsst")
plt.ylabel("Total efficiency in Simulation")
plt.legend()
plt.subplot(1, 2, 2)
plt.errorbar(x, meff_tot_Ds1_year, ueff_tot_Ds1_year, color="blue", fmt=".", label="Ds1 MC", capsize=7)
plt.axhline(meff_tot_Ds1, color="blue", label="Average", linestyle="dashed")
plt.xticks(x, years)
plt.title("Ds1")
plt.legend()
plt.savefig("./plots/Sim_Both_tot_eff_per_year.pdf", dpi=300, bbox_inches="tight")

# efficiency ratio
ratio_eff_tot = eff_tot_Dsst / eff_tot_Ds1

print(f"Total efficiency ration in MC = {ratio_eff_tot}")

reff_tot_mean = {"val": float(ratio_eff_tot.n),
                 "unc": float(ratio_eff_tot.s)}

with open(f"./param_files/Sim_avg_eff_tot_ratio_fst_order_v2.yaml", "w") as file:
    yaml.dump(reff_tot_mean, file, default_flow_style=False)
#--------------------------------------------------------

# nVeloTracks binning
#--------------------------------------------------------
## total eff ratio vs nVelo
inters_path = "../mass_fits/param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")
ninters = len(nVelo_intervals) - 1

Ds1_vals_file = f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv"

df_Ds1_fit_vals = pd.read_csv(Ds1_vals_file)

nVelo_means = df_Ds1_fit_vals["nVelo_mean"]
nVelo_up    = df_Ds1_fit_vals["nVelo_uup"]
nVelo_down  = df_Ds1_fit_vals["nVelo_udown"]

ninters = len(nVelo_means)

# Total efficiencies efficiencies per year
eff_stats_nVelo_year = {y: pd.read_csv(f"{tot_eff_path}/Sim_Both{y}_eff_stats_nVelo_fst_order.csv") 
                        for y in years}

meff_tot_Dsst_nVelo_year = pd.DataFrame({y: np.array(df["tot_eff_Dsst_val"]) for y, df in eff_stats_nVelo_year.items()})
ueff_tot_Dsst_nVelo_year = pd.DataFrame({y: np.array(df["tot_eff_Dsst_unc"]) for y, df in eff_stats_nVelo_year.items()})

meff_tot_Ds1_nVelo_year = pd.DataFrame({y: np.array(df["tot_eff_Ds1_val"]) for y, df in eff_stats_nVelo_year.items()})
ueff_tot_Ds1_nVelo_year = pd.DataFrame({y: np.array(df["tot_eff_Ds1_unc"]) for y, df in eff_stats_nVelo_year.items()})

iueff_tot_Dsst_nVelo_year = pd.DataFrame({y: 1 /(ueff_tot_Dsst_nVelo_year[y] ** 2) for y in years})
iueff_tot_Ds1_nVelo_year  = pd.DataFrame({y: 1 /(ueff_tot_Ds1_nVelo_year[y] ** 2) for y in years})

# data_proportions
year_props_Dsst_nVelo = pd.read_csv(f"{props_path}/Exp_Run2_Dsst_yield_props_vals_5_nVelo.csv")
year_props_Ds1_nVelo  = pd.read_csv(f"{props_path}/Exp_Run2_Ds1_yield_props_vals_5_nVelo.csv")

# nominal value
if wy:
    weff_tot_Dsst_nVelo = pd.DataFrame({y: year_props_Dsst_nVelo[y] for y in years}) # dropping some nasty empty column
    weff_tot_Ds1_nVelo  = pd.DataFrame({y: year_props_Ds1_nVelo[y] for y in years})
else:
    weff_tot_Dsst_nVelo = iueff_tot_Dsst_nVelo_year
    weff_tot_Ds1_nVelo  = iueff_tot_Ds1_nVelo_year

wmeff_tot_Dsst_nVelo = pd.DataFrame({ }); wmeff_tot_Ds1_nVelo = pd.DataFrame({ })

for y in years:
    wmeff_tot_Dsst_nVelo[y] = year_props_Dsst_nVelo[y] * meff_tot_Dsst_nVelo_year[y]
    wmeff_tot_Ds1_nVelo[y]  = year_props_Ds1_nVelo[y] * meff_tot_Ds1_nVelo_year[y]

def row_sum(row):
    return row['2016'] + row['2017'] + row['2018']

meff_tot_Dsst_nVelo = np.array(wmeff_tot_Dsst_nVelo.apply(row_sum, axis=1)) / weff_tot_Dsst_nVelo.apply(row_sum, axis=1)
meff_tot_Ds1_nVelo  = np.array(wmeff_tot_Ds1_nVelo.apply(row_sum, axis=1)) / weff_tot_Ds1_nVelo.apply(row_sum, axis=1)

# uncertianties
ueff_tot_Dsst_nVelo = np.array(1/np.sqrt(iueff_tot_Dsst_nVelo_year.apply(row_sum, axis=1)))
ueff_tot_Ds1_nVelo  = np.array(1/np.sqrt(iueff_tot_Ds1_nVelo_year.apply(row_sum, axis=1)))

plt.figure(2)
plt.subplot(1, 2, 1)
for y in years:
    plt.errorbar(nVelo_means, meff_tot_Dsst_nVelo_year[y], ueff_tot_Dsst_nVelo_year[y], 
                 xerr=[nVelo_down, nVelo_up], fmt='.', capsize=7, label=y)
plt.errorbar(nVelo_means, meff_tot_Dsst_nVelo, ueff_tot_Dsst_nVelo, 
             xerr=[nVelo_down, nVelo_up], fmt='.', color="black", capsize=7, label="Average")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Total efficiency in Simulation")
plt.title("Dsst")
plt.legend()
plt.subplot(1, 2, 2)
for y in years:
    plt.errorbar(nVelo_means, meff_tot_Ds1_nVelo_year[y], ueff_tot_Ds1_nVelo_year[y], 
                 xerr=[nVelo_down, nVelo_up], fmt='.', capsize=7, label=y)
plt.errorbar(nVelo_means, meff_tot_Ds1_nVelo, ueff_tot_Ds1_nVelo, 
             xerr=[nVelo_down, nVelo_up], fmt='.', color="black", capsize=7, label="Average")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.title("Ds1")
plt.legend()
plt.savefig(f"./plots/Sim_Both_tot_eff_per_year_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

# efficiency ratio
eff_tot_Dsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(meff_tot_Dsst_nVelo, ueff_tot_Dsst_nVelo)])
eff_tot_Ds1_nVelo  = np.array([un.ufloat(m, u) for m, u in zip(meff_tot_Ds1_nVelo, ueff_tot_Ds1_nVelo)])

ratio_eff_tot_nVelo = eff_tot_Dsst_nVelo / eff_tot_Ds1_nVelo

mratio_eff_tot_nVelo = np.array([r.n for r in ratio_eff_tot_nVelo])
uratio_eff_tot_nVelo = np.array([r.s for r in ratio_eff_tot_nVelo])

plt.figure(3)
plt.errorbar(nVelo_means, mratio_eff_tot_nVelo, uratio_eff_tot_nVelo, 
             xerr=[nVelo_down, nVelo_up], fmt='.', capsize=7, color="black")
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("Total efficiency ratio")
plt.title("Efficiency ratio with averaged efficiencies")
plt.ylim(0, 0.2)
plt.savefig(f"./plots/Sim_Both_tot_effs_ratio_{ninters}_nVelo.pdf")

np.savetxt(f"./param_files/Sim_avg_eff_tot_ratio_vals_{ninters}_nVelo_fst_order_v2.txt", mratio_eff_tot_nVelo)
np.savetxt(f"./param_files/Sim_avg_eff_tot_ratio_uncs_{ninters}_nVelo_fst_order_v2.txt", uratio_eff_tot_nVelo)
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")
