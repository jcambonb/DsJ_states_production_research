import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy"
mc_path = f"{head_path}/Reduced/MC/TightCut/sel"

Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

tdf_Dsst_MC = ROOT.RDataFrame(f"DecayTree", f"{mc_path}/{DsstDsg_files}")
tdf_Ds1_MC  = ROOT.RDataFrame(f"DecayTree", f"{mc_path}/{Ds1Dsg_files}")
    
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

TRUEID_Ds1_sig  = TRUEID_dtt["Ds1DsGamma"]
TRUEID_Dsst_sig = TRUEID_dtt["DsstDsGamma"]

tdf_Ds1_sig  = tdf_Ds1_MC.Filter(TRUEID_Ds1_sig)
tdf_Dsst_sig = tdf_Dsst_MC.Filter(TRUEID_Dsst_sig)
#--------------------------------------------------------

# multiple candidates removals
#--------------------------------------------------------
tdf_Ds1_sig  = tdf_Ds1_sig.Filter("totCandidates2 == 1")
tdf_Dsst_sig = tdf_Dsst_sig.Filter("totCandidates2 == 1")
#--------------------------------------------------------

# nVeloTracks interval samples
#--------------------------------------------------------
Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

inters_path = "./param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1

nVelo_cuts = [f"nVeloTracks > {nVelo_intervals[i-1]} && nVeloTracks <= {nVelo_intervals[i]}" for i in range(1, len(nVelo_intervals))]

Dsst_wind = f"DsgM > {Dsst_mass[0]} && DsgM < {Dsst_mass[1]}"
Ds1_wind  = f"DsgM > {Ds1_mass[0]} && DsgM < {Ds1_mass[1]}"

tdf_Dsst_sig_velo_cuts = [tdf_Dsst_sig.Filter(f"{Dsst_wind} && {inter}") for inter in nVelo_cuts] 
tdf_Ds1_sig_velo_cuts  = [tdf_Ds1_sig.Filter(f"{Ds1_wind} && {inter}") for inter in nVelo_cuts]

np_Dsst_sig_velo_cuts = [tdf.AsNumpy(["DsgM"]) for tdf in tdf_Dsst_sig_velo_cuts] 
np_Ds1_sig_velo_cuts  = [tdf.AsNumpy(["DsgM"]) for tdf in tdf_Ds1_sig_velo_cuts]

hmc_Dsg_mass_Dsst_sig_velo_cuts = [tdf.Histo1D(("","",100,Dsst_mass[0],Dsst_mass[1]), "DsgM") for tdf in tdf_Dsst_sig_velo_cuts] 
hmc_Dsg_mass_Ds1_sig_velo_cuts = [tdf.Histo1D(("","",100,Ds1_mass[0],Ds1_mass[1]), "DsgM") for tdf in tdf_Ds1_sig_velo_cuts] 

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

i = 0; j = 0

c1 = ROOT.TCanvas("", "", 1200, 800)
lgd1 = ROOT.TLegend(0.57, 0.44, 0.92, 0.87)
lgd2 = ROOT.TLegend(0.3, 0.22, 0.65, 0.65)
c1.Divide(2, 1)
c1.cd(1)
for h in hmc_Dsg_mass_Dsst_sig_velo_cuts:
    i += 1
    rsh.TH1D_plot(h, color=i, norm=True, xlabel=label_Dsg_mass)
rsh.legend_plot(lgd1, 
                [h.GetPtr() for h in hmc_Dsg_mass_Dsst_sig_velo_cuts], 
                ["{0}º nVeloTracks interval".format(i+1) for i in range(ninters)],
                ["l" for i in range(ninters)])
c1.cd(2)    
for h in hmc_Dsg_mass_Ds1_sig_velo_cuts:
    j += 1
    rsh.TH1D_plot(h, color=j, norm=True, xlabel=label_Dsg_mass)
rsh.legend_plot(lgd2, 
                [h.GetPtr() for h in hmc_Dsg_mass_Ds1_sig_velo_cuts], 
                ["{0}º nVeloTracks interval".format(i+1) for i in range(ninters)],
                ["l" for i in range(ninters)])
c1.Draw()
c1.SaveAs(f"./plots/Sim_Both_Dsg_mass_vs_{ninters}_nVelo.pdf")
#--------------------------------------------------------

# Fit models
#--------------------------------------------------------
# Dsst model 
mDsst = ROOT.RooRealVar("DsgM", "DsgM", Dsst_mass[0], Dsst_mass[1])

mu_c = 2112; sigma_c = 5; alpha_c = 1; n_c = 4

mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", mu_c, mu_c-10, mu_c+10)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaR_Dsst = ROOT.RooRealVar("alphaR_Dsst", "alphaR_Dsst", alpha_c, -20*alpha_c, -0.01*alpha_c)
nR_Dsst = ROOT.RooRealVar("nR_Dsst", "nR_Dsst", 10)

CB_Dsst = ROOT.RooCrystalBall("CB_Dsst", "CB_Dsst", mDsst, 
                              mu_Dsst, sigma_Dsst, 
                              alphaR_Dsst, nR_Dsst)

units_CB_Dsst = ["", "", ""]

# Ds1 model 
mDs1 = ROOT.RooRealVar("DsgM", "DsgM", Ds1_mass[0], Ds1_mass[1])

mu_c = 2460; sigma_c = 15; alpha_c = 1; n_c = 4

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-10, mu_c+10)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", 10, 0, 100)
nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", 10, 0, 100)

CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1", mDs1, 
                             mu_Ds1, sigma_Ds1, 
                             alphaL_Ds1, nL_Ds1, 
                             alphaR_Ds1, nR_Ds1)

units_CB_Ds1 = ["", "", "", "", "", ""]
#--------------------------------------------------------

# Fit for each nVeloTrack sample
#--------------------------------------------------------
Dsst_fits_vals = []; Dsst_fits_uncs = [] 
Ds1_fits_vals  = []; Ds1_fits_uncs  = []

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

ylabel_Dsst = "N_{events} / (2 MeV/c^{2})"
ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

i = 0

print("Starting the UELF for Dsst and Ds1 masses vs nVeloTracks")

for (x,y) in zip(np_Dsst_sig_velo_cuts, np_Ds1_sig_velo_cuts):
    
    i += 1
    
    dh_Dsst = ROOT.RooDataSet.from_numpy(x, [mDsst])
    dh_Ds1 = ROOT.RooDataSet.from_numpy(y, [mDs1])

    Dsst_fit = rpf.Fit(CB_Dsst.fitTo(dh_Dsst, ROOT.RooFit.Save()))
    Ds1_fit = rpf.Fit(CB_Ds1.fitTo(dh_Ds1, ROOT.RooFit.Save()))

    Dsst_fit.print()
    Dsst_fit.save_to_latex(mDsst, dh_Dsst, CB_Dsst, units_CB_Dsst, 
                           fit_type="u",
                           file_name=f"./latex_tables/Sim_Dsst_sig_mass_fit_CB_{ninters}_nVelo_{i}.tex")
    rpf.plot(mDsst, dh_Dsst, CB_Dsst, 
             file_name=f"./plots/Sim_Dsst_sig_mass_fit_CB_{ninters}_nVelo_{i}.pdf",
             xlabel=label_Dsg_mass,
             ylabel=ylabel_Dsst)

    Ds1_fit.print()
    Ds1_fit.save_to_latex(mDs1, dh_Ds1, CB_Ds1, units_CB_Ds1, 
                          fit_type="u",
                          file_name=f"./latex_tables/Sim_Ds1_sig_mass_fit_CB_{ninters}_nVelo_{i}.tex")
    rpf.plot(mDs1, dh_Ds1, CB_Ds1, 
             file_name=f"./plots/Sim_Ds1_sig_mass_fit_CB_{ninters}_nVelo_{i}.pdf",
             xlabel=label_Dsg_mass,
             ylabel=ylabel_Ds1)
    
    Dsst_vals = Dsst_fit.par_values(); Dsst_uncs = Dsst_fit.par_errors(); Dsst_pars = Dsst_fit.par_names()
    Ds1_vals = Ds1_fit.par_values();  Ds1_uncs = Ds1_fit.par_errors(); Ds1_pars = Ds1_fit.par_names()
    
    Dsst_fits_vals.append(Dsst_vals); Dsst_fits_uncs.append(Dsst_uncs)
    Ds1_fits_vals.append(Ds1_vals); Ds1_fits_uncs.append(Ds1_uncs)
#--------------------------------------------------------

# Parameter saving
#--------------------------------------------------------
print("Parameter saving process")

df_Dsst_fits_vals = (pd.DataFrame(np.array(Dsst_fits_vals))).set_axis(Dsst_pars, axis='columns')
df_Dsst_fits_uncs = (pd.DataFrame(np.array(Dsst_fits_uncs))).set_axis(Dsst_pars, axis='columns')

df_Ds1_fits_vals = (pd.DataFrame(np.array(Ds1_fits_vals))).set_axis(Ds1_pars, axis='columns')
df_Ds1_fits_uncs = (pd.DataFrame(np.array(Ds1_fits_uncs))).set_axis(Ds1_pars, axis='columns')

nVelo_inter_avg = np.array([tdf.Mean("nVeloTracks").GetValue() for tdf in tdf_Ds1_sig_velo_cuts])
nVelo_inter_min = np.array([tdf.Min("nVeloTracks").GetValue() for tdf in tdf_Ds1_sig_velo_cuts])
nVelo_inter_max = np.array([tdf.Max("nVeloTracks").GetValue() for tdf in tdf_Ds1_sig_velo_cuts])

nVelo_inter_udown = nVelo_inter_avg - nVelo_inter_min
nVelo_inter_uup   = nVelo_inter_max - nVelo_inter_avg

df_Dsst_fits_vals.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Dsst_fits_vals.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Dsst_fits_vals.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Dsst_fits_uncs.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Dsst_fits_uncs.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Dsst_fits_uncs.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Ds1_fits_vals.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Ds1_fits_vals.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Ds1_fits_vals.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Ds1_fits_uncs.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Ds1_fits_uncs.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Ds1_fits_uncs.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Dsst_fits_vals.to_csv(f"./param_files/Sim_Dsst_sig_mass_fit_CB_vals_{ninters}_nVelo.csv", index=False)
df_Dsst_fits_uncs.to_csv(f"./param_files/Sim_Dsst_sig_mass_fit_CB_uncs_{ninters}_nVelo.csv", index=False)

df_Ds1_fits_vals.to_csv(f"./param_files/Sim_Ds1_sig_mass_fit_CB_vals_{ninters}_nVelo.csv", index=False)
df_Ds1_fits_uncs.to_csv(f"./param_files/Sim_Ds1_sig_mass_fit_CB_uncs_{ninters}_nVelo.csv", index=False)
#--------------------------------------------------------

# Yields plots for MC
#--------------------------------------------------------
def uratio(a, b, ua, ub):
    ratio = a/b
    uratio = ratio * np.sqrt((ua/a)**2+(ub/b)**2)
    return uratio

yield_Dsg_mass_Dsst_sig_velo_cuts = np.array([h.Integral() for h in hmc_Dsg_mass_Dsst_sig_velo_cuts]) 
yield_Dsg_mass_Ds1_sig_velo_cuts  = np.array([h.Integral() for h in hmc_Dsg_mass_Ds1_sig_velo_cuts]) 

yield_ratio = yield_Dsg_mass_Ds1_sig_velo_cuts / yield_Dsg_mass_Dsst_sig_velo_cuts
uyield_ratio = uratio(yield_Dsg_mass_Ds1_sig_velo_cuts, yield_Dsg_mass_Dsst_sig_velo_cuts,
                     np.sqrt(yield_Dsg_mass_Ds1_sig_velo_cuts), np.sqrt(yield_Dsg_mass_Dsst_sig_velo_cuts))

plt.figure(1)
plt.errorbar(nVelo_inter_avg, yield_ratio, yerr=uyield_ratio,
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="k", ecolor="k", capsize=7)
plt.ylim(5, 25)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$N_S(D_{s1}^+)/N_S(D_s^{*+})$")
plt.title("Yield ratio vs multiplicity")
plt.savefig(f"./plots/Sim_DsgL_yield_ratio_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

plt.figure(2)
plt.errorbar(nVelo_inter_avg, uyield_ratio / yield_ratio, yerr=0,
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="k", ecolor="k", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Relative uncertainty")
plt.title("Yield ratio vs multiplicity")
plt.savefig(f"./plots/Sim_DsgL_yield_ratio_ru_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

plt.figure(3)
plt.subplot(121)
plt.errorbar(nVelo_inter_avg, yield_Dsg_mass_Dsst_sig_velo_cuts, np.sqrt(yield_Dsg_mass_Dsst_sig_velo_cuts),
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="r", ecolor="r", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Signal Yield")
plt.title("Yield ratio vs multiplicity")
plt.subplot(122)
plt.errorbar(nVelo_inter_avg, yield_Dsg_mass_Ds1_sig_velo_cuts, yerr=np.sqrt(yield_Dsg_mass_Ds1_sig_velo_cuts),
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="b", ecolor="b", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.title("Yield ratio vs multiplicity")
plt.savefig(f"./plots/Sim_Both_yields_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")