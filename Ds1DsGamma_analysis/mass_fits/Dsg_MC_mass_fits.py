import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy"
mc_path = f"{head_path}/Reduced/MC/TightCut/sel"

Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

tdf_Dsst_MC = ROOT.RDataFrame(f"DecayTree", f"{mc_path}/{DsstDsg_files}")
tdf_Ds1_MC  = ROOT.RDataFrame(f"DecayTree", f"{mc_path}/{Ds1Dsg_files}")
    
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

TRUEID_Dsst_sig = TRUEID_dtt["DsstDsGamma"]
TRUEID_Ds1_sig  = TRUEID_dtt["Ds1DsGamma"]

tdf_Dsst_sig = tdf_Dsst_MC.Filter(TRUEID_Dsst_sig)
tdf_Ds1_sig  = tdf_Ds1_MC.Filter(TRUEID_Ds1_sig)
#--------------------------------------------------------

# multiple candidates removals
#--------------------------------------------------------
tdf_Dsst_sig = tdf_Dsst_sig.Filter("totCandidates2 == 1")
tdf_Ds1_sig  = tdf_Ds1_sig.Filter("totCandidates2 == 1")
#--------------------------------------------------------

# Dsst fit: Right handed CB 
#-------------------------------------------------------
Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

# unbinned data definition
np_Dsst_sig = tdf_Dsst_sig.AsNumpy(columns = ["DsgM"])

print(f"Number of MCmatched Dsst signal candidates: {tdf_Dsst_sig.Count().GetValue()}")
print(f'Number of MCmatched and selected Ds1 signal candidates: {len(np_Dsst_sig["DsgM"])}')

mDsst = ROOT.RooRealVar("DsgM", "DsgM", Dsst_mass[0], Dsst_mass[1])

data_Dsst = ROOT.RooDataSet.from_numpy(np_Dsst_sig, [mDsst])

# parameters and models
mu_c = 2112; sigma_c = 5; alpha_c = 1; n_c = 4

mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", mu_c, mu_c-10, mu_c+10)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaR_Dsst = ROOT.RooRealVar("alphaR_Dsst", "alphaR_Dsst", alpha_c, -20*alpha_c, -0.01*alpha_c)
nR_Dsst = ROOT.RooRealVar("nR_Dsst", "nR_Dsst", 10)

CB_Dsst = ROOT.RooCrystalBall("CB_Dsst", "CB_Dsst", mDsst, 
                              mu_Dsst, sigma_Dsst, 
                              alphaR_Dsst, nR_Dsst)
# Fit
print("Preparing UELF for Dsst mass")

Dsst_fit = rpf.Fit(CB_Dsst.fitTo(data_Dsst, ROOT.RooFit.Save()))
#-------------------------------------------------------

# Ds1 fit: double CB
#-------------------------------------------------------
# unbinned data definition
np_Ds1_sig = tdf_Ds1_sig.AsNumpy(columns = ["DsgM"])

print(f"Number of MCmatched Ds1 signal candidates: {tdf_Ds1_sig.Count().GetValue()}")
print('Number of MCmatched and selected Ds1 signal candidates: {0}'.format({len(np_Ds1_sig["DsgM"])}))

mDs1 = ROOT.RooRealVar("DsgM", "DsgM", Ds1_mass[0], Ds1_mass[1])

data_Ds1 = ROOT.RooDataSet.from_numpy(np_Ds1_sig, [mDs1])

# parameters and models
mu_c = 2460; sigma_c = 15; alpha_c = 1; n_c = 4

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-10, mu_c+10)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", alpha_c, 0.01*alpha_c, 20*alpha_c)
nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", 10, 0, 100)
nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", 10, 0, 100)

CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1", mDs1, 
                             mu_Ds1, sigma_Ds1, 
                             alphaL_Ds1, nL_Ds1, 
                             alphaR_Ds1, nR_Ds1)
# Fit
print("Preparing UELF for Ds1 mass")

Ds1_fit = rpf.Fit(CB_Ds1.fitTo(data_Ds1, ROOT.RooFit.Save()))
#-------------------------------------------------------

# plots and data saving
#-------------------------------------------------------
label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

ylabel_Dsst = "N_{events} / (2 MeV/c^{2})"
ylabel_Ds1  = "N_{events} / (2.5 MeV/c^{2})"

units_CB_Dsst = ["No units", "MeV/$c^2$", "MeV/$c^2$"]
units_CB_Ds1  = ["No units", "MeV/$c^2$", "MeV/$c^2$", "", "", ""]

Dsst_fit.print()
Dsst_fit.save_to_csv(file_name="./param_files/Sim_Dsst_sig_mass_fit_CB.csv")
Dsst_fit.save_to_latex(mDsst, data_Dsst, CB_Dsst, units_CB_Dsst, 
                       fit_type="u", file_name="./latex_tables/Sim_Dsst_sig_mass_fit_CB.tex")

rpf.plot(mDsst, data_Dsst, CB_Dsst, 
         file_name="./plots/Sim_Dsst_sig_mass_fit_CB.pdf", xlabel=label_Dsg_mass, ylabel=ylabel_Dsst)

Ds1_fit.print()
Ds1_fit.save_to_csv(file_name="./param_files/Sim_Ds1_sig_mass_fit_CB.csv")
Ds1_fit.save_to_latex(mDs1, data_Ds1, CB_Ds1, units_CB_Ds1, 
                      fit_type="u", file_name="./latex_tables/Sim_Ds1_sig_mass_fit_CB.tex")

rpf.plot(mDs1, data_Ds1, CB_Ds1, 
         file_name="./plots/Sim_Ds1_sig_mass_fit_CB.pdf", xlabel=label_Dsg_mass, ylabel=ylabel_Ds1)
#-------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")