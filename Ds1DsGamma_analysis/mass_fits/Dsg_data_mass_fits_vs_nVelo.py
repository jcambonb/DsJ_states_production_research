from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
pol  = options.polarity
year = options.year
run2 = options.run2

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/Data/sel"

years = ["2018", "2017", "2016"]
polarities = ["MagDown", "MagUp"]

if run2:
    print("Merging Run2 data samples")
    tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_*_sel.root")
else: 
    if pol == "Both":
        print(f"Only {year} data sample")
        tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_*_{year}_sel.root")
    else:
        print(f"Only {pol} {year} data sample")
        tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_{pol}_{year}_sel.root")
#--------------------------------------------------------

# multiple candidates removal and variable definition
#--------------------------------------------------------
tdf_data_sel = tdf_data.Filter("totCandidates2 == 1")

tdf_data_sel = tdf_data_sel.Define("Ds_log10_IPCHI2", "log10(Ds_IPCHI2_OWNPV)")
#--------------------------------------------------------

# nVeloTracks interval samples
#--------------------------------------------------------
inters_path = "./param_files"
inters_file = "Exp_Run2_nVeloTracks_binning_Ds1.txt"

Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

nVelo_intervals = np.loadtxt(f"{inters_path}/{inters_file}")

ninters = len(nVelo_intervals) - 1

nVelo_cuts = [f"nVeloTracks > {nVelo_intervals[i-1]} && nVeloTracks <= {nVelo_intervals[i]}" for i in range(1, len(nVelo_intervals))]

Dsst_wind = f"DsgM > {Dsst_mass[0]} && DsgM < {Dsst_mass[1]}"
Ds1_wind  = f"DsgM > {Ds1_mass[0]} && DsgM < {Ds1_mass[1]}"

tdf_data_Dsst_velo_cuts = [tdf_data_sel.Filter(f"{Dsst_wind} && {inter}") for inter in nVelo_cuts] 
tdf_data_Ds1_velo_cuts  = [tdf_data_sel.Filter(f"{Ds1_wind} && {inter}") for inter in nVelo_cuts]

np_data_Dsst_velo_cuts = [tdf.AsNumpy(["DsgM"]) for tdf in tdf_data_Dsst_velo_cuts] 
np_data_Ds1_velo_cuts  = [tdf.AsNumpy(["DsgM"]) for tdf in tdf_data_Ds1_velo_cuts]

hdata_Dsg_mass_Dsst_velo_cuts = [tdf.Histo1D(("","",100,Dsst_mass[0],Dsst_mass[1]), "DsgM") for tdf in tdf_data_Dsst_velo_cuts] 
hdata_Dsg_mass_Ds1_velo_cuts  = [tdf.Histo1D(("","",100,Ds1_mass[0],Ds1_mass[1]), "DsgM") for tdf in tdf_data_Ds1_velo_cuts] 

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"

i = 0; j = 0

c1 = ROOT.TCanvas("", "", 1200, 800)
lgd1 = ROOT.TLegend(0.57, 0.44, 0.92, 0.87)
lgd2 = ROOT.TLegend(0.3, 0.22, 0.65, 0.65)
c1.Divide(2, 1)
c1.cd(1)
for h in hdata_Dsg_mass_Dsst_velo_cuts:
    i += 1
    rsh.TH1D_plot(h, color=i, norm=True, xlabel=label_Dsg_mass)
rsh.legend_plot(lgd1, 
                [h.GetPtr() for h in hdata_Dsg_mass_Dsst_velo_cuts], 
                ["{0}º nVeloTracks interval".format(i+1) for i in range(ninters)],
                ["l" for i in range(ninters)])
c1.cd(2)    
for h in hdata_Dsg_mass_Ds1_velo_cuts:
    j += 1
    rsh.TH1D_plot(h, color=j, norm=True, xlabel=label_Dsg_mass)
rsh.legend_plot(lgd2, 
                [h.GetPtr() for h in hdata_Dsg_mass_Ds1_velo_cuts], 
                ["{0}º nVeloTracks interval".format(i+1) for i in range(ninters)],
                ["l" for i in range(ninters)])
c1.Draw()
if run2:
    c1.SaveAs(f"./plots/Exp_Run_Dsg_mass_vs_{ninters}_nVelo.pdf")
else:
    c1.SaveAs(f"./plots/Exp_{pol}_{year}_Dsg_mass_vs_{ninters}_nVelo.pdf")
#--------------------------------------------------------

# Fit models
#--------------------------------------------------------
# Dsst model pdfs 
mDsst = ROOT.RooRealVar("DsgM", "DsgM", Dsst_mass[0], Dsst_mass[1])

mu_c = 2112; sigma_c = 5; alpha_c = 1; n_c = 4

mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", mu_c, mu_c-10, mu_c+10)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", 0.5*sigma_c, 0.2*sigma_c, 2*sigma_c)
alphaR_Dsst = ROOT.RooRealVar("alphaR_Dsst", "alphaR_Dsst", alpha_c, -20*alpha_c, -0.01*alpha_c)
nR_Dsst = ROOT.RooRealVar("nR_Dsst", "nR_Dsst", 10)

CB_Dsst = ROOT.RooCrystalBall("CB_Dsst", "CB_Dsst", mDsst, 
                              mu_Dsst, sigma_Dsst, 
                              alphaR_Dsst, nR_Dsst)

units_CB_Dsst = ["", "", ""]

aDsst = ROOT.RooRealVar("aDsst", "aDsst", 0.1, -5, 5)
bDsst = ROOT.RooRealVar("bDsst", "bDsst", -0.1, -5, 5)
cDsst = ROOT.RooRealVar("cDsst", "cDsst", -0.1, -5, 5)

cheb_Dsst = ROOT.RooChebychev("cheb_Dsst", "cheb_Dsst", mDsst, ROOT.RooArgList(aDsst, bDsst, cDsst))

units_cheb_Dsst = ["", "", ""]

# Ds1 model pdfs
mDs1 = ROOT.RooRealVar("DsgM", "DsgM", 2350, 2600)

mu_c = 2460; sigma_c = 20; alpha_c = 1; n_c = 2

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-20, mu_c+20)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", sigma_c, 0.1*sigma_c, 2*sigma_c)

units_CB_Ds1 = ["", ""]

aDs1 = ROOT.RooRealVar("aDs1", "aDs1", -5, 5)
bDs1 = ROOT.RooRealVar("bDs1", "bDs1", -5, 5)
cDs1 = ROOT.RooRealVar("cDs1", "cDs1", -5, 5)

cheb_Ds1 = ROOT.RooChebychev("cheb_Ds1", "cheb_Ds1", mDs1, ROOT.RooArgList(aDs1, bDs1, cDs1))

units_cheb_Ds1 = ["", "", ""]
#--------------------------------------------------------

# Fit for each nVeloTrack sample
#--------------------------------------------------------
mc_Ds1_fits = pd.read_csv(f"./param_files/Sim_Ds1_sig_mass_fit_CB_vals_{ninters}_nVelo.csv")

units_Dsst = units_CB_Dsst + units_cheb_Dsst + ["", ""]
units_Ds1 = units_CB_Ds1 + units_cheb_Ds1 + ["", ""]
colors = [ROOT.kRed, ROOT.kGreen+2]

ylabel_Dsst = "N_{events} / (2 MeV/c^{2})"; ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

comps_Dsst = ["CB_Dsst", "cheb_Dsst"]; comps_Ds1 = ["CB_Ds1", "cheb_Ds1"]

Dsst_fits_vals = []; Dsst_fits_uncs = [] 
Ds1_fits_vals = []; Ds1_fits_uncs = []

i = 0

for (x,y) in zip(np_data_Dsst_velo_cuts, np_data_Ds1_velo_cuts):
    
    Nentries_Dsst = len(x["DsgM"]); Nentries_Ds1 = len(y["DsgM"])

    NDsst = ROOT.RooRealVar("NDsst", "NDsst", 0, Nentries_Dsst)
    Ncomb_Dsst = ROOT.RooRealVar("Ncomb_Dsst", "Ncomb_Dsst", 0, Nentries_Dsst)

    NDs1 = ROOT.RooRealVar("NDs1", "NDs1", 0.001*Nentries_Ds1, 0, Nentries_Ds1)
    Ncomb_Ds1 = ROOT.RooRealVar("Ncomb_Ds1", "Ncomb_Ds1", 0.999*Nentries_Ds1, 0, Nentries_Ds1)

    NDsst.setError(np.sqrt(Nentries_Dsst)); Ncomb_Dsst.setError(np.sqrt(Nentries_Dsst))
    NDs1.setError(np.sqrt(Nentries_Ds1)); Ncomb_Ds1.setError(np.sqrt(Nentries_Ds1))
    
    model_Dsst = ROOT.RooAddPdf("model_Dsst", "model_Dsst", ROOT.RooArgList(CB_Dsst, cheb_Dsst), ROOT.RooArgList(NDsst, Ncomb_Dsst))
    
    # Ds1 model 
    alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", (np.array(mc_Ds1_fits["alphaL_Ds1"]))[i])
    nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", (np.array(mc_Ds1_fits["nL_Ds1"]))[i])
    alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", (mc_Ds1_fits["alphaR_Ds1"])[i])
    nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", (mc_Ds1_fits["nR_Ds1"])[i])
    
    CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1",
                                 mDs1, mu_Ds1, sigma_Ds1, 
                                 alphaL_Ds1, nL_Ds1, alphaR_Ds1, nR_Ds1)

    model_Ds1 = ROOT.RooAddPdf("model_Ds1", "model_Ds1", ROOT.RooArgList(CB_Ds1, cheb_Ds1), ROOT.RooArgList(NDs1, Ncomb_Ds1))

    dh_Dsst = ROOT.RooDataSet.from_numpy(x, [mDsst])
    dh_Ds1 = ROOT.RooDataSet.from_numpy(y, [mDs1])

    Dsst_fit = rpf.Fit(model_Dsst.fitTo(dh_Dsst, ROOT.RooFit.Save()))
    Ds1_fit = rpf.Fit(model_Ds1.fitTo(dh_Ds1, ROOT.RooFit.Save()))
    
    if run2:
        Dsst_fit.print()
        Dsst_fit.save_to_latex(mDsst, dh_Dsst, model_Dsst, units_Dsst, 
                               fit_type="u",
                               file_name=f"./latex_tables/Exp_Run2_Dsst_sig_mass_fit_{ninters}_nVelo_{i+1}.tex")
        rpf.plot(mDsst, dh_Dsst, model_Dsst, 
                 xlabel=label_Dsg_mass, comps=comps_Dsst, comps_color=colors, 
                 file_name=f"./plots/Exp_Run2_Dsst_sig_mass_fit_{ninters}_nVelo_{i+1}.pdf")

        Ds1_fit.print()
        Ds1_fit.save_to_latex(mDs1, dh_Ds1, model_Ds1, units_Ds1, 
                              fit_type="u",
                              file_name=f"./latex_tables/Exp_Run2_Ds1_sig_mass_fit_{ninters}_nVelo_{i+1}.tex")
        rpf.plot(mDs1, dh_Ds1, model_Ds1, 
                 xlabel=label_Dsg_mass, comps=comps_Ds1, comps_color=colors, 
                 file_name=f"./plots/Exp_Run2_Ds1_sig_mass_fit_{ninters}_nVelo_{i+1}.pdf")  
    else:   
        Dsst_fit.print()
        Dsst_fit.save_to_latex(mDsst, dh_Dsst, model_Dsst, units_Dsst, 
                               fit_type="u",
                               file_name=f"./latex_tables/Exp_{pol}_{year}_Dsst_sig_mass_fit_{ninters}_nVelo_{i+1}.tex")
        rpf.plot(mDsst, dh_Dsst, model_Dsst, 
                 xlabel=label_Dsg_mass, comps=comps_Dsst, comps_color=colors, 
                 file_name=f"./plots/Exp_{pol}_{year}_Dsst_sig_mass_fit_{ninters}_nVelo_{i+1}.pdf")

        Ds1_fit.print()
        Ds1_fit.save_to_latex(mDs1, dh_Ds1, model_Ds1, units_Ds1, 
                              fit_type="u",
                              file_name=f"./latex_tables/Exp_{pol}_{year}_Ds1_sig_mass_fit_{ninters}_nVelo_{i+1}.tex")
        rpf.plot(mDs1, dh_Ds1, model_Ds1, 
                 xlabel=label_Dsg_mass, comps=comps_Ds1, comps_color=colors, 
                 file_name=f"./plots/Exp_{pol}_{year}_Ds1_sig_mass_fit_{ninters}_nVelo_{i+1}.pdf") 
    
    Dsst_vals = Dsst_fit.par_values(); Dsst_uncs = Dsst_fit.par_errors(); Dsst_pars = Dsst_fit.par_names()
    Ds1_vals = Ds1_fit.par_values(); Ds1_uncs = Ds1_fit.par_errors(); Ds1_pars = Ds1_fit.par_names()
    
    Dsst_fits_vals.append(Dsst_vals); Dsst_fits_uncs.append(Dsst_uncs)
    Ds1_fits_vals.append(Ds1_vals); Ds1_fits_uncs.append(Ds1_uncs)
    
    i += 1
#--------------------------------------------------------

# Parameter saving
#--------------------------------------------------------
print("Parameter saving process")

df_Dsst_fits_vals = (pd.DataFrame(np.array(Dsst_fits_vals))).set_axis(Dsst_pars, axis='columns')
df_Dsst_fits_uncs = (pd.DataFrame(np.array(Dsst_fits_uncs))).set_axis(Dsst_pars, axis='columns')

df_Ds1_fits_vals = (pd.DataFrame(np.array(Ds1_fits_vals))).set_axis(Ds1_pars, axis='columns')
df_Ds1_fits_uncs = (pd.DataFrame(np.array(Ds1_fits_uncs))).set_axis(Ds1_pars, axis='columns')

nVelo_inter_avg = np.array([tdf.Mean("nVeloTracks").GetValue() for tdf in tdf_data_Ds1_velo_cuts])
nVelo_inter_min = np.array([tdf.Min("nVeloTracks").GetValue() for tdf in tdf_data_Ds1_velo_cuts])
nVelo_inter_max = np.array([tdf.Max("nVeloTracks").GetValue() for tdf in tdf_data_Ds1_velo_cuts])

nVelo_inter_udown = nVelo_inter_avg - nVelo_inter_min
nVelo_inter_uup   = nVelo_inter_max - nVelo_inter_avg

df_Dsst_fits_vals.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Dsst_fits_vals.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Dsst_fits_vals.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Dsst_fits_uncs.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Dsst_fits_uncs.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Dsst_fits_uncs.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Ds1_fits_vals.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Ds1_fits_vals.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Ds1_fits_vals.insert(2, "nVelo_uup", nVelo_inter_uup)

df_Ds1_fits_uncs.insert(2, "nVelo_mean", nVelo_inter_avg)
df_Ds1_fits_uncs.insert(2, "nVelo_udown", nVelo_inter_udown)
df_Ds1_fits_uncs.insert(2, "nVelo_uup", nVelo_inter_uup)

if run2:
    df_Dsst_fits_uncs.to_csv(f"./param_files/Exp_Run2_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv", index=False)
    df_Dsst_fits_vals.to_csv(f"./param_files/Exp_Run2_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv", index=False)

    df_Ds1_fits_vals.to_csv(f"./param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv", index=False)
    df_Ds1_fits_uncs.to_csv(f"./param_files/Exp_Run2_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv", index=False)
else:
    df_Dsst_fits_uncs.to_csv(f"./param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv", index=False)
    df_Dsst_fits_vals.to_csv(f"./param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv", index=False)

    df_Ds1_fits_vals.to_csv(f"./param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv", index=False)
    df_Ds1_fits_uncs.to_csv(f"./param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv", index=False)
    
#--------------------------------------------------------

# Yields plots for data
#--------------------------------------------------------
def uratio(a, b, ua, ub):
    ratio = a/b
    uratio = ratio * np.sqrt((ua/a)**2+(ub/b)**2)
    return uratio

yield_ratio = df_Ds1_fits_vals["NDs1"] / df_Dsst_fits_vals["NDsst"]
uyield_ratio = uratio(df_Ds1_fits_vals["NDs1"], df_Dsst_fits_vals["NDsst"],
                      df_Ds1_fits_uncs["NDs1"], df_Dsst_fits_uncs["NDsst"])

plt.figure(1)
plt.errorbar(nVelo_inter_avg, yield_ratio, yerr=uyield_ratio,
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="k", ecolor="k", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$N_S(D_{s1}^+)/N_S(D_s^{*+})$")
plt.title("Yield ratio vs multiplicity")
plt.ylim(0.2, 0.6)
if run2:
    plt.savefig(f"./plots/Exp_Run2_yield_ratio_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')
else:    
    plt.savefig(f"./plots/Exp_{pol}_{year}_yield_ratio_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

plt.figure(2)
plt.errorbar(nVelo_inter_avg, uyield_ratio / yield_ratio, yerr=0,
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="k", ecolor="k", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Relative uncertainty")
plt.title("Yield ratio vs multiplicity")
if run2:
    plt.savefig(f"./plots/Exp_Run2_yield_ratio_ru_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Exp_{pol}_{year}_yield_ratio_ru_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

plt.figure(3)
plt.subplot(121)
plt.errorbar(nVelo_inter_avg, df_Dsst_fits_vals["NDsst"],  df_Dsst_fits_uncs["NDsst"],
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="r", ecolor="r", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"Signal Yield")
plt.title("Yield ratio vs multiplicity")
plt.subplot(122)
plt.errorbar(nVelo_inter_avg, df_Ds1_fits_vals["NDs1"],  df_Ds1_fits_uncs["NDs1"],
             xerr=[df_Ds1_fits_vals["nVelo_udown"], df_Ds1_fits_vals["nVelo_uup"]], 
             fmt='.', color="b", ecolor="b", capsize=7)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.title("Yield ratio vs multiplicity")
if run2:
    plt.savefig(f"./plots/Exp_Run2_yields_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Exp_{pol}_{year}_yields_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")