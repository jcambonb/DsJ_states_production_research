#!/bin/bash
# Polarity values: "MagDown", "MagUp", "Both"

run2=false
polarity="Both"
years=("2016" "2017" "2018")

conda activate yvk_env

echo "Running mass fits in MC. Nch is integrated"
echo "--------------------------------"
python Dsg_MC_mass_fits.py
echo "--------------------------------"

if $run2; then
    echo "Running mass fits in data for all Run2 data samples. Nch is integrated"
    python Dsg_data_mass_fits.py -t
else
    for y in "${years[@]}"
    do
        echo "Running mass fit in data for $polarity $y data sample. Nch is integrated"
        python Dsg_data_mass_fits.py -p $polarity -y $y
    done
fi 
echo "--------------------------------"

echo "Running mass fits in MC for Nch bins"
echo "--------------------------------"
python Dsg_MC_mass_fits_vs_nVelo.py
echo "--------------------------------"

if $run2; then
    echo "Running mass fits for Nch bins in data for all Run2 data samples"
    python Dsg_data_mass_fits_vs_nVelo.py -t
else
    for y in "${years[@]}"
    do
        echo "Running mass fits for Nch bins in data for $polarity $y data sample"
        python Dsg_data_mass_fits_vs_nVelo.py -p $polarity -y $y
    done
fi 
echo "--------------------------------"