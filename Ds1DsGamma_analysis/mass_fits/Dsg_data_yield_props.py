from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep
import yaml

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
years = ["2016", "2017", "2018"]

# Integrated nVeloTracks
#--------------------------------------------------------
df_Dsst_fit = pd.read_csv(f"./param_files/Exp_Run2_Dsst_sig_mass_fit.csv")
df_Ds1_fit  = pd.read_csv(f"./param_files/Exp_Run2_Ds1_sig_mass_fit.csv")

df_Dsst_year_fits = {y: pd.read_csv(f"./param_files/Exp_Both_{y}_Dsst_sig_mass_fit.csv") for y in years}
df_Ds1_year_fits  = {y: pd.read_csv(f"./param_files/Exp_Both_{y}_Ds1_sig_mass_fit.csv") for y in years}

mNDsst = float((df_Dsst_fit.query('Parameters == "NDsst"'))["Values"]); uNDsst = float((df_Dsst_fit.query('Parameters == "NDsst"'))["Errors"])
mNDs1  = float((df_Ds1_fit.query('Parameters == "NDs1"'))["Values"]);   uNDs1  = float((df_Ds1_fit.query('Parameters == "NDs1"'))["Errors"])

NDsst = un.ufloat(mNDsst, uNDsst); NDs1 = un.ufloat(mNDs1, uNDs1)

NDsst_per_year = {}; NDs1_per_year = {}

for y in years:
    df_Dsst_fit_y = df_Dsst_year_fits[y]; df_Ds1_fit_y = df_Ds1_year_fits[y]
    
    mNDsst_y = float((df_Dsst_fit_y.query('Parameters == "NDsst"'))["Values"]); uNDsst_y = float((df_Dsst_fit_y.query('Parameters == "NDsst"'))["Errors"])
    mNDs1_y  = float((df_Ds1_fit_y.query('Parameters == "NDs1"'))["Values"]);   uNDs1_y  = float((df_Ds1_fit_y.query('Parameters == "NDs1"'))["Errors"])
    
    NDsst_y = un.ufloat(mNDsst_y, uNDsst_y); NDs1_y = un.ufloat(mNDs1_y, uNDs1_y)
    
    NDsst_per_year[y] = NDsst_y; NDs1_per_year[y] = NDs1_y
    
year_props_Dsst = {y: N/NDsst for y, N in NDsst_per_year.items()}
year_props_Ds1  = {y: N/NDs1 for y, N in NDs1_per_year.items()}

for y in years:
    print(f"Dsst yield proportion for {y} data sample = {year_props_Dsst[y]}")
    print(f"Ds1 yield proportion for {y} data sample = {year_props_Ds1[y]}")
    
myear_props_Dsst = {y: prop.nominal_value for y, prop in year_props_Dsst.items()}
uyear_props_Dsst = {y: float(prop.std_dev) for y, prop in year_props_Dsst.items()}

myear_props_Ds1 = {y: prop.nominal_value for y, prop  in year_props_Ds1.items()}
uyear_props_Ds1 = {y: float(prop.std_dev) for y, prop in year_props_Ds1.items()}

with open(f"./param_files/Exp_Run2_Dsst_yield_props_vals.yaml", "w") as file:
    yaml.dump(myear_props_Dsst, file, default_flow_style=False)

with open(f"./param_files/Exp_Run2_Dsst_yield_props_uncs.yaml", "w") as file:
    yaml.dump(uyear_props_Dsst, file, default_flow_style=False)
    
with open(f"./param_files/Exp_Run2_Ds1_yield_props_vals.yaml", "w") as file:
    yaml.dump(myear_props_Ds1, file, default_flow_style=False)

with open(f"./param_files/Exp_Run2_Ds1_yield_props_uncs.yaml", "w") as file:
    yaml.dump(uyear_props_Ds1, file, default_flow_style=False)
#--------------------------------------------------------

# nVeloTracks binning
#--------------------------------------------------------
ninters = 5

df_Dsst_vals_nVelo = pd.read_csv(f"../mass_fits/param_files/Exp_Run2_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv") 
df_Dsst_uncs_nVelo = pd.read_csv(f"../mass_fits/param_files/Exp_Run2_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv") 

df_Ds1_vals_nVelo = pd.read_csv(f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv")
df_Ds1_uncs_nVelo = pd.read_csv(f"../mass_fits/param_files/Exp_Run2_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv")

df_Dsst_vals_nVelo_per_year = {y: pd.read_csv(f"../mass_fits/param_files/Exp_Both_{y}_Dsst_sig_mass_fit_vals_{ninters}_nVelo.csv") 
                               for y in years}
df_Dsst_uncs_nVelo_per_year = {y: pd.read_csv(f"../mass_fits/param_files/Exp_Both_{y}_Dsst_sig_mass_fit_uncs_{ninters}_nVelo.csv") 
                               for y in years}

df_Ds1_vals_nVelo_per_year = {y: pd.read_csv(f"../mass_fits/param_files/Exp_Both_{y}_Ds1_sig_mass_fit_vals_{ninters}_nVelo.csv") 
                              for y in years}
df_Ds1_uncs_nVelo_per_year = {y: pd.read_csv(f"../mass_fits/param_files/Exp_Both_{y}_Ds1_sig_mass_fit_uncs_{ninters}_nVelo.csv") 
                              for y in years}

mNDsst_nVelo = df_Dsst_vals_nVelo["NDsst"]; uNDsst_nVelo = df_Dsst_uncs_nVelo["NDsst"]
mNDs1_nVelo  = df_Ds1_vals_nVelo["NDs1"];   uNDs1_nVelo  = df_Ds1_uncs_nVelo["NDs1"]

NDsst_nVelo = np.array([un.ufloat(m, u) for m, u in zip(mNDsst_nVelo, uNDsst_nVelo)])
NDs1_nVelo  = np.array([un.ufloat(m, u) for m, u in zip(mNDs1_nVelo, uNDs1_nVelo)])

NDsst_nVelo_per_year = {}; NDs1_nVelo_per_year = {}

for y in years:
    df_Dsst_vals_nVelo_y = df_Dsst_vals_nVelo_per_year[y]; df_Ds1_vals_nVelo_y = df_Ds1_vals_nVelo_per_year[y]
    df_Dsst_uncs_nVelo_y = df_Dsst_uncs_nVelo_per_year[y]; df_Ds1_uncs_nVelo_y = df_Ds1_uncs_nVelo_per_year[y]
    
    mNDsst_nVelo_y = df_Dsst_vals_nVelo_y["NDsst"]; uNDsst_nVelo_y = df_Dsst_uncs_nVelo_y["NDsst"]
    mNDs1_nVelo_y  = df_Ds1_vals_nVelo_y["NDs1"];   uNDs1_nVelo_y  = df_Ds1_uncs_nVelo_y["NDs1"]
    
    NDsst_nVelo_per_year[y] = np.array([un.ufloat(m, u) for m, u in zip(mNDsst_nVelo_y, uNDsst_nVelo_y)])
    NDs1_nVelo_per_year[y]  = np.array([un.ufloat(m, u) for m, u in zip(mNDs1_nVelo_y, uNDs1_nVelo_y)])

year_props_nVelo_Dsst = {y: N_nVelo/NDsst_nVelo for y, N_nVelo in NDsst_nVelo_per_year.items()}
year_props_nVelo_Ds1  = {y: N_nVelo/NDs1_nVelo for y, N_nVelo  in NDs1_nVelo_per_year.items()}

myear_props_nVelo_Dsst = pd.DataFrame({y: np.array([p.nominal_value for p in prop]) 
                                       for y, prop in year_props_nVelo_Dsst.items()})
uyear_props_nVelo_Dsst = pd.DataFrame({y: np.array([float(p.std_dev) for p in prop]) 
                                       for y, prop in year_props_nVelo_Dsst.items()})

myear_props_nVelo_Ds1 = pd.DataFrame({y: np.array([p.nominal_value for p in prop]) 
                                      for y, prop in year_props_nVelo_Ds1.items()})
uyear_props_nVelo_Ds1 = pd.DataFrame({y: np.array([float(p.std_dev) for p in prop]) 
                                      for y, prop in year_props_nVelo_Ds1.items()})

plt.figure(1)
plt.subplot(1, 2, 1)
for y in years:
    plt.errorbar(df_Ds1_vals_nVelo["nVelo_mean"], myear_props_nVelo_Dsst[y], uyear_props_nVelo_Dsst[y],
                 xerr=[df_Ds1_vals_nVelo["nVelo_udown"], df_Ds1_vals_nVelo["nVelo_uup"]],
                 fmt='.', capsize=7, label=y)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$N_{year}/\sum_i N_i$")
plt.title("Dsst yield proportion vs nVeloTracks")
plt.legend()
plt.subplot(1, 2, 2)
for y in years:
    plt.errorbar(df_Ds1_vals_nVelo["nVelo_mean"], myear_props_nVelo_Ds1[y], uyear_props_nVelo_Ds1[y],
                 xerr=[df_Ds1_vals_nVelo["nVelo_udown"], df_Ds1_vals_nVelo["nVelo_uup"]],
                 fmt='.', capsize=7, label=y)
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel(r"$N_{year}/\sum_i N_i$")
plt.title("Ds1 yield proportion")
plt.legend()
plt.savefig(f"./plots/Exp_Run2_yield_proportion_per_year_{ninters}_nVelo.pdf", dpi=300, bbox_inches='tight')

myear_props_nVelo_Dsst.to_csv(f"./param_files/Exp_Run2_Dsst_yield_props_vals_{ninters}_nVelo.csv")
uyear_props_nVelo_Dsst.to_csv(f"./param_files/Exp_Run2_Dsst_yield_props_uncs_{ninters}_nVelo.csv")

myear_props_nVelo_Ds1.to_csv(f"./param_files/Exp_Run2_Ds1_yield_props_vals_{ninters}_nVelo.csv")
uyear_props_nVelo_Ds1.to_csv(f"./param_files/Exp_Run2_Ds1_yield_props_uncs_{ninters}_nVelo.csv")
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")