from optparse import OptionParser

parser = OptionParser()
parser.add_option("-y", "--year", dest="year", default="2018")
parser.add_option("-p", "--polarity", dest="polarity", default="MagDown")
parser.add_option("-t", "--total", action="store_true", dest="run2", default=False)
(options, args) = parser.parse_args()

# 3 options: polarity + year, year or Run 2
# polarity: MagUp, MagDown, Both
# year: 2018, 2017, 2016

import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

pol  = options.polarity
year = options.year
run2 = options.run2

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/Data/sel"

if run2:
    print("Merging Run2 data samples")
    tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_*_sel.root")
else: 
    if pol == "Both":
        print(f"Only {year} data sample")
        tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_*_{year}_sel.root")
    else:
        print(f"Only {pol} {year} data sample")
        tdf_data = ROOT.RDataFrame("DecayTree", f"{head_path}/DsJ_Data_{pol}_{year}_sel.root")
#--------------------------------------------------------

# multiple candidates removal and variable definition
#--------------------------------------------------------
tdf_data_sel = tdf_data.Filter("totCandidates2 == 1")

tdf_data_sel = tdf_data_sel.Define("Ds_log10_IPCHI2", "log10(Ds_IPCHI2_OWNPV)")
#--------------------------------------------------------

# Common parametes
#--------------------------------------------------------
Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

features = ["DsgM", "nVeloTracks", "Ds_log10_IPCHI2", "Ds_IP_OWNPV", 
            "Ds_PT", "Ds_ETA", "Dsg_PT", "Dsg_ETA"]

label_Dsg_mass = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
label_nVeloTracks = r"$\mathrm{N_{Tracks}^{VELO}}$"
#--------------------------------------------------------

# Dsst fit: right handed CB + Chebyshev
#--------------------------------------------------------
# unbinned data definition
Dsst_wind = f"DsgM > {Dsst_mass[0]}  && DsgM < {Dsst_mass[1]}"

tdf_data_Dsst = tdf_data_sel.Filter(Dsst_wind)

np_data_Dsst = tdf_data_Dsst.AsNumpy(columns=features)

mDsst_data = ROOT.RooRealVar("DsgM", "DsgM", Dsst_mass[0], Dsst_mass[1])
nVeloTracks_Dsst_data = ROOT.RooRealVar("nVeloTracks", "nVeloTracks", 6, 327)
Ds_log10_IPCHI2_Dsst_data = ROOT.RooRealVar("Ds_log10_IPCHI2", "Ds_log10_IPCHI2", -5, 1)
Ds_IP_Dsst_data  = ROOT.RooRealVar("Ds_IP_OWNPV", "Ds_IP_OWNPV", 0, 0.1)
Ds_PT_Dsst_data  = ROOT.RooRealVar("Ds_PT", "Ds_PT", 1000, 30000)
Ds_ETA_Dsst_data = ROOT.RooRealVar("Ds_ETA", "Ds_ETA", 1.5, 5.0)
Dsg_PT_Dsst_data  = ROOT.RooRealVar("Dsg_PT", "Dsg_PT", 1000, 40000)
Dsg_ETA_Dsst_data = ROOT.RooRealVar("Dsg_ETA", "Dsg_ETA", 1.5, 5.0)

data_Dsst = ROOT.RooDataSet.from_numpy(np_data_Dsst, [mDsst_data, nVeloTracks_Dsst_data, 
                                                      Ds_log10_IPCHI2_Dsst_data, Ds_IP_Dsst_data,
                                                      Ds_PT_Dsst_data, Ds_ETA_Dsst_data,
                                                      Dsg_PT_Dsst_data, Dsg_ETA_Dsst_data])

c1 = ROOT.TCanvas()
fr = mDsst_data.frame()
fr.GetXaxis().SetTitle(label_Dsg_mass)
data_Dsst.plotOn(fr)
fr.Draw()
c1.Draw()
if run2:
    c1.SaveAs(f"./plots/Exp_Run2_mass_Dsst_peak.pdf")
else:
    c1.SaveAs(f"./plots/Exp_{pol}_{year}_mass_Dsst_peak.pdf")


# parameters and models
mu_c = 2112; sigma_c = 10; alpha_c = 1; n_c = 4

## CB
mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", mu_c, mu_c-20, mu_c+20)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", sigma_c, 0.1*sigma_c, 2*sigma_c)
alphaR_Dsst = ROOT.RooRealVar("alphaR_Dsst", "alphaR_Dsst", alpha_c, -10*alpha_c, -0.01*alpha_c)
nR_Dsst = ROOT.RooRealVar("nR_Dsst", "nR_Dsst", 10)

CB_Dsst = ROOT.RooCrystalBall("CB_Dsst", "CB_Dsst",
                              mDsst_data, mu_Dsst, sigma_Dsst,
                              alphaR_Dsst, nR_Dsst)

units_CB_Dsst = ["No units", "MeV/$c^2$", "MeV/$c^2$"]

## Chebyshev
aDsst = ROOT.RooRealVar("aDsst", "aDsst", 0.1, -5, 5)
bDsst = ROOT.RooRealVar("bDsst", "bDsst", -0.1, -5, 5)
cDsst = ROOT.RooRealVar("cDsst", "cDsst", -0.1, -5, 5)

cheb_Dsst = ROOT.RooChebychev("cheb_Dsst", "cheb_Dsst", mDsst_data, ROOT.RooArgList(aDsst, bDsst, cDsst))

units_cheb_Dsst = ["", "", ""]

## Yields
Nentries_Dsst = len(np_data_Dsst["DsgM"])

NDsst = ROOT.RooRealVar("NDsst", "NDsst", 0, Nentries_Dsst)
Ncomb_Dsst = ROOT.RooRealVar("Ncomb_Dsst", "Ncomb_Dsst", 0, Nentries_Dsst)

yields = [NDsst, Ncomb_Dsst]

for y in yields:
    y.setError(np.sqrt(Nentries_Dsst))
    
model_Dsst = ROOT.RooAddPdf("model_Dsst", "model_Dsst", ROOT.RooArgList(CB_Dsst, cheb_Dsst), ROOT.RooArgList(NDsst, Ncomb_Dsst))
units_Dsst = units_CB_Dsst + units_cheb_Dsst + ["", ""]

# Fit
print("Preparing UELF for Dsst mass")

Dsst_fit = rpf.Fit(model_Dsst.fitTo(data_Dsst, ROOT.RooFit.Save()))
#--------------------------------------------------------

# Ds1 fit: double CB + Chebyshev
#--------------------------------------------------------
# unbinned data definition
Ds1_wind = f"DsgM > {Ds1_mass[0]}  && DsgM < {Ds1_mass[1]}"

tdf_data_Ds1 = tdf_data_sel.Filter(Ds1_wind)

np_data_Ds1 = tdf_data_Ds1.AsNumpy(columns=features)

mDs1_data = ROOT.RooRealVar("DsgM", "DsgM", Ds1_mass[0], Ds1_mass[1])
nVeloTracks_Ds1_data = ROOT.RooRealVar("nVeloTracks", "nVeloTracks", 6, 327)
Ds_log10_IPCHI2_Ds1_data = ROOT.RooRealVar("Ds_log10_IPCHI2", "Ds_log10_IPCHI2", -5, 1)
Ds_IP_Ds1_data  = ROOT.RooRealVar("Ds_IP_OWNPV", "Ds_IP_OWNPV", 0, 0.1)
Ds_PT_Ds1_data  = ROOT.RooRealVar("Ds_PT", "Ds_PT", 1000, 30000)
Ds_ETA_Ds1_data = ROOT.RooRealVar("Ds_ETA", "Ds_ETA", 1.5, 5.0)
Dsg_PT_Ds1_data  = ROOT.RooRealVar("Dsg_PT", "Dsg_PT", 1000, 40000)
Dsg_ETA_Ds1_data = ROOT.RooRealVar("Dsg_ETA", "Dsg_ETA", 1.5, 5.0)

data_Ds1 = ROOT.RooDataSet.from_numpy(np_data_Ds1, [mDs1_data, nVeloTracks_Ds1_data, 
                                                    Ds_log10_IPCHI2_Ds1_data, Ds_IP_Ds1_data,
                                                    Ds_PT_Ds1_data, Ds_ETA_Ds1_data,
                                                    Dsg_PT_Ds1_data, Dsg_ETA_Ds1_data])

c1 = ROOT.TCanvas()
fr = mDs1_data.frame()
fr.GetXaxis().SetTitle(label_Dsg_mass)
data_Ds1.plotOn(fr)
fr.Draw()
c1.Draw()
if run2:
    c1.SaveAs(f"./plots/Exp_Run2_mass_Ds1_peak.pdf")
else:
    c1.SaveAs(f"./plots/Exp_{pol}_{year}_mass_Ds1_peak.pdf")
    
# parameters and models
mu_c = 2460; sigma_c = 15; alpha_c = 1; n_c = 4

## CB
mc_Ds1_fit = pd.read_csv("./param_files/Sim_Ds1_sig_mass_fit_CB.csv")
mc_Ds1_fit_vals = np.array(mc_Ds1_fit[["Parameters", "Values"]])

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", mu_c, mu_c-10, mu_c+10)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", sigma_c, 0.2*sigma_c, 5*sigma_c)
alphaL_Ds1 = ROOT.RooRealVar("alphaL_Ds1", "alphaL_Ds1", mc_Ds1_fit_vals[0, 1])
nL_Ds1 = ROOT.RooRealVar("nL_Ds1", "nL_Ds1", mc_Ds1_fit_vals[3, 1])
alphaR_Ds1 = ROOT.RooRealVar("alphaR_Ds1", "alphaR_Ds1", mc_Ds1_fit_vals[1, 1])
nR_Ds1 = ROOT.RooRealVar("nR_Ds1", "nR_Ds1", mc_Ds1_fit_vals[4, 1])

CB_Ds1 = ROOT.RooCrystalBall("CB_Ds1", "CB_Ds1",
                              mDs1_data, mu_Ds1, sigma_Ds1,
                              alphaL_Ds1, nL_Ds1,
                              alphaR_Ds1, nR_Ds1)

units_CB_Ds1 = ["No units", "MeV/$c^2$"]

## Chebyshev
aDs1 = ROOT.RooRealVar("aDs1", "aDs1", 0.1, -5, 5)
bDs1 = ROOT.RooRealVar("bDs1", "bDs1", -0.1, -5, 5)
cDs1 = ROOT.RooRealVar("cDs1", "cDs1", -0.1, -5, 5)

cheb_Ds1 = ROOT.RooChebychev("cheb_Ds1", "cheb_Ds1", mDs1_data, ROOT.RooArgList(aDs1, bDs1, cDs1))

units_cheb_Ds1 = ["", "", ""]

## Yields
Nentries_Ds1 = len(np_data_Ds1["DsgM"])

NDs1 = ROOT.RooRealVar("NDs1", "NDs1", 0, Nentries_Ds1)
Ncomb_Ds1 = ROOT.RooRealVar("Ncomb_Ds1", "Ncomb_Ds1", 0, Nentries_Ds1)

yields = [NDs1, Ncomb_Ds1]

for y in yields:
    y.setError(np.sqrt(Nentries_Ds1))
    
model_Ds1 = ROOT.RooAddPdf("model_Ds1", "model_Ds1", ROOT.RooArgList(CB_Ds1, cheb_Ds1), ROOT.RooArgList(NDs1, Ncomb_Ds1))
units_Ds1 = units_CB_Ds1 + units_cheb_Ds1 + ["", ""]

# Fit
print("Preparing UELF for Ds1 mass")

Ds1_fit = rpf.Fit(model_Ds1.fitTo(data_Ds1, ROOT.RooFit.Save()))
#--------------------------------------------------------

# Fit results and plots
#--------------------------------------------------------
colors = [ROOT.kRed, ROOT.kGreen+2]

ylabel_Dsst = "N_{events} / (2 MeV/c^{2})"; ylabel_Ds1 = "N_{events} / (2.5 MeV/c^{2})"

comps_Dsst = ["CB_Dsst", "cheb_Dsst"]; comps_Ds1 = ["CB_Ds1", "cheb_Ds1"]

Dsst_fit.print()
if run2:
    Dsst_fit.save_to_csv(file_name=f"./param_files/Exp_Run2_Dsst_sig_mass_fit.csv")
    Dsst_fit.save_to_latex(mDsst_data, data_Dsst, model_Dsst, 
                           units=units_Dsst, fit_type="u", 
                           file_name=f"./latex_tables/Exp_Run2_Dsst_sig_mass_fit.tex")
    rpf.plot(mDsst_data, data_Dsst, model_Dsst, 
         comps=comps_Dsst, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Dsst,
         file_name=f"./plots/Exp_Run2_Dsst_sig_mass_fit.pdf") 

else:
    Dsst_fit.save_to_csv(file_name=f"./param_files/Exp_{pol}_{year}_Dsst_sig_mass_fit.csv")
    Dsst_fit.save_to_latex(mDsst_data, data_Dsst, model_Dsst, 
                           units=units_Dsst, fit_type="u", 
                           file_name=f"./latex_tables/Exp_{pol}_{year}_Dsst_sig_mass_fit.tex")
    rpf.plot(mDsst_data, data_Dsst, model_Dsst, 
         comps=comps_Dsst, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Dsst,
         file_name=f"./plots/Exp_{pol}_{year}_Dsst_sig_mass_fit.pdf") 


Ds1_fit.print()
if run2:
    Ds1_fit.save_to_csv(file_name=f"./param_files/Exp_Run2_Ds1_sig_mass_fit.csv")
    Ds1_fit.save_to_latex(mDs1_data, data_Ds1, model_Ds1, 
                           units=units_Ds1, fit_type="u", 
                           file_name=f"./latex_tables/Exp_Run2_Ds1_sig_mass_fit.tex")
    rpf.plot(mDs1_data, data_Ds1, model_Ds1, 
             comps=comps_Ds1, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Ds1,
             file_name=f"./plots/Exp_Run2_Ds1_sig_mass_fit.pdf") 
else:
    Ds1_fit.save_to_csv(file_name=f"./param_files/Exp_{pol}_{year}_Ds1_sig_mass_fit.csv")
    Ds1_fit.save_to_latex(mDs1_data, data_Ds1, model_Ds1, 
                           units=units_Ds1, fit_type="u", 
                           file_name=f"./latex_tables/Exp_{pol}_{year}_Ds1_sig_mass_fit.tex")
    rpf.plot(mDs1_data, data_Ds1, model_Ds1, 
             comps=comps_Ds1, comps_color=colors, xlabel=label_Dsg_mass, ylabel=ylabel_Ds1,
             file_name=f"./plots/Exp_{pol}_{year}_Ds1_sig_mass_fit.pdf") 
    
# Yield ratio 
#--------------------------------------------------------
yDs1 = NDs1.getVal(); uyDs1 = NDs1.getError()
yDsst = NDsst.getVal(); uyDsst = NDsst.getError()

yr = yDs1 / yDsst
uyr = yr * np.sqrt((uyDs1/yDs1)**2 + (uyDsst/yDsst)**2)

if run2:
    print(f"Yield ratio for whole run2 data set = {yr} +/- {uyr}")
    print(f"Relative statistical uncertainty = {uyr/yr}")
else:
    print(f"Yield ratio for {pol} {year} data set = {yr} +/- {uyr}")
    print(f"Relative statistical uncertainty = {uyr/yr}")
#--------------------------------------------------------

# nVeloTracks interval inference
#--------------------------------------------------------

# sPlot and sWeigths
sData_Dsst = ROOT.RooStats.SPlot('splot', "sPlot", data_Dsst, model_Dsst, ROOT.RooArgList(NDsst, Ncomb_Dsst))
data_Dsst.Print()

sData_Ds1 = ROOT.RooStats.SPlot('splot', "sPlot", data_Ds1, model_Ds1, ROOT.RooArgList(NDs1, Ncomb_Ds1))
data_Ds1.Print()

df_data_Dsst_sw = pd.DataFrame(data_Dsst.to_numpy())
df_data_Ds1_sw = pd.DataFrame(data_Ds1.to_numpy())

# sWeigthed nVeloTracks distrubutions
nVelo_min_Dsst = int(tdf_data_Dsst.Min("nVeloTracks").GetValue())
nVelo_max_Dsst = int(tdf_data_Dsst.Max("nVeloTracks").GetValue())

nVelo_min_Ds1  = int(tdf_data_Ds1.Min("nVeloTracks").GetValue())
nVelo_max_Ds1 = int(tdf_data_Ds1.Max("nVeloTracks").GetValue())

nVelo_bins_Dsst = nVelo_max_Dsst - nVelo_min_Dsst
nVelo_bins_Ds1 = nVelo_max_Ds1 - nVelo_min_Ds1

nVelo_hist_Dsst, nVelo_bins_Dsst = np.histogram(df_data_Dsst_sw["nVeloTracks"], 
                                                bins=nVelo_bins_Dsst, 
                                                density=True, 
                                                weights=df_data_Dsst_sw["NDsst_sw"])

nVelo_hist_Ds1, nVelo_bins_Ds1 = np.histogram(df_data_Ds1_sw["nVeloTracks"], 
                                              bins=nVelo_bins_Ds1, 
                                              density=True, 
                                              weights=df_data_Ds1_sw["NDs1_sw"])

nVelo_cdf_Dsst = np.cumsum(nVelo_hist_Dsst * np.diff(nVelo_bins_Dsst))
nVelo_cdf_Ds1 = np.cumsum(nVelo_hist_Ds1 * np.diff(nVelo_bins_Ds1))

plt.figure(1)
plt.subplot(121)
plt.hist(df_data_Ds1_sw["nVeloTracks"], bins=nVelo_bins_Ds1, density=True, alpha=0.7, weights=df_data_Ds1_sw["NDs1_sw"], color="blue")
plt.title('Density Histogram')
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("A.U")
plt.subplot(122)
plt.plot(nVelo_bins_Ds1[1:], nVelo_cdf_Ds1, color="blue")
plt.title('Cumulative Distribution Function')
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
if run2:
    plt.savefig("./plots/Comp_DsgL_Run2_nVeloTracks_sw_dist_Ds1.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Comp_DsgL_{pol}_{year}_nVeloTracks_sw_dist_Ds1.pdf", dpi=300, bbox_inches='tight')


plt.figure(2)
plt.subplot(121)
plt.hist(df_data_Dsst_sw["nVeloTracks"], bins=nVelo_bins_Dsst, density=True, alpha=0.7, weights=df_data_Dsst_sw["NDsst_sw"], color="red")
plt.title('Density Histogram')
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
plt.ylabel("A.U")
plt.subplot(122)
plt.plot(nVelo_bins_Dsst[1:], nVelo_cdf_Dsst, color="red")
plt.title('Cumulative Distribution Function')
plt.xlabel(r"$\mathrm{N}^{\mathrm{VELO}}_{\mathrm{Tracks}}$")
if run2:
    plt.savefig("./plots/Comp_DsgL_Run2_nVeloTracks_sw_dist_Dsst.pdf", dpi=300, bbox_inches='tight')
else:
    plt.savefig(f"./plots/Comp_DsgL_{pol}_{year}_nVeloTracks_sw_dist_Dsst.pdf", dpi=300, bbox_inches='tight')

# interval inference
print("nVeloTracks classes from Dsst sWeighted cdf")
if run2:
    plt.figure(3)
    nVelo_classes_Dsst = rsh.var_interval_inference(df_data_Dsst_sw, "nVeloTracks", 5, 
                                                    xlabel=label_nVeloTracks, fmt="r-",
                                                    bins=nVelo_bins_Dsst, weights="NDsst_sw",
                                                    fig_name=f"./plots/Exp_Run2_nVeloTracks_binning_Dsst.pdf",
                                                    file_name=f"./param_files/Exp_Run2_nVeloTracks_binning_Dsst.txt")
else:
    plt.figure(3)
    nVelo_classes_Dsst = rsh.var_interval_inference(df_data_Dsst_sw, "nVeloTracks", 5, 
                                                    xlabel=label_nVeloTracks, fmt="r-",
                                                    bins=nVelo_bins_Dsst, weights="NDsst_sw",
                                                    fig_name=f"./plots/Exp_{pol}_{year}_nVeloTracks_binning_Dsst.pdf",
                                                    file_name=f"./param_files/Exp_{pol}_{year}_nVeloTracks_binning_Dsst.txt")

print("nVeloTracks classes from Ds1 sWeighted cdf")
plt.figure(4)
if run2:
    nVelo_classes_Ds1 = rsh.var_interval_inference(df_data_Ds1_sw, "nVeloTracks", 5, 
                                                   xlabel=label_nVeloTracks,
                                                   bins=nVelo_bins_Ds1, weights="NDs1_sw",
                                                   fig_name=f"./plots/Exp_Run2_nVeloTracks_binning_Ds1.pdf",
                                                   file_name=f"./param_files/Exp_Run2_nVeloTracks_binning_Ds1.txt")
else:
    nVelo_classes_Ds1 = rsh.var_interval_inference(df_data_Ds1_sw, "nVeloTracks", 5, 
                                                   xlabel=label_nVeloTracks,
                                                   bins=nVelo_bins_Ds1, weights="NDs1_sw",
                                                   fig_name=f"./plots/Exp_{pol}_{year}_nVeloTracks_binning_Ds1.pdf",
                                                   file_name=f"./param_files/Exp_{pol}_{year}_nVeloTracks_binning_Ds1.txt")
#-------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")