import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

ti = datetime.now()

# File reading
#--------------------------------------------------------
folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/"
data_path = f"{head_path}/Data/raw/MagDown/2018"
mc_path   = f"{head_path}/MC/TightCut/raw"

data_files    = set([f"{data_path}/DsJ_Data_MagD18_{i}_raw.root" for i in range(100, 700)])
Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

tdf_data = ROOT.RDataFrame("DecayTree", data_files)

derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

for keys, values in derivated_features.items():
    tdf_data = tdf_data.Define(keys, values)
#--------------------------------------------------------

# Nch distributions vs nPVs
#--------------------------------------------------------

## All nPVs merged
hdata_nVeloTracks_raw = tdf_data.Histo1D(("","",360,0,360), "nVeloTracks")
hdata_pvz_raw = tdf_data.Histo1D(("","", 100, -250, 250), "PVZ")

## Separate sample for number of PVs
tdf_data_pvs = [tdf_data.Filter(f"nPVs == {i}") for i in range(1, 7)]

hdata_nVeloTracks_pvs = [tdf.Histo1D(("","",360,0,360), "nVeloTracks") for tdf in tdf_data_pvs]
hdata_pvz_pvs = [tdf.Histo1D(("","", 100, -250, 250), "PVZ") for tdf in tdf_data_pvs]

i1 = 1; i2 = 1

nVelo_label = "N_{Tracks}^{VELO}"
npvs_label  = "nPVs"
pvz_label   = "z_{PV} [mm]"

c1 = ROOT.TCanvas("", "", 1200, 800)
lgd1 = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
lgd2 = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
c1.Divide(2, 1)
c1.cd(1)
for h in hdata_nVeloTracks_pvs:
    i1 += 1
    rsh.TH1D_plot(h, color=i1, norm=True, xlabel=nVelo_label)
rsh.TH1D_plot(hdata_nVeloTracks_raw, norm=True, xlabel=nVelo_label)
rsh.legend_plot(lgd1, 
                [h.GetPtr() for h in hdata_nVeloTracks_pvs],
                [f"nPVs = {i}" for i in range(1, 7)],
                ["l" for i in range(1, 7)])
c1.cd(2)
for h in hdata_pvz_pvs:
    i2 += 1
    rsh.TH1D_plot(h, color=i2, norm=True, xlabel=pvz_label)
rsh.TH1D_plot(hdata_pvz_raw, norm=True, xlabel=pvz_label)
rsh.legend_plot(lgd2, 
                [h.GetPtr() for h in hdata_pvz_pvs],
                [f"nPVs = {i}" for i in range(1, 7)],
                ["l" for i in range(1, 7)])
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_nVeloTracks_PVZs_vs_nPVs.pdf")
#--------------------------------------------------------

## zPV range values for nVeloTracks
#--------------------------------------------------------

### 1 PV samples
tdf_data_1PV = tdf_data_pvs[0]

### pvz ranges
pvz_ranges = [140 - 20 * i for i in range(15)]
tdf_data_1PV_pvzs = [tdf_data_1PV.Filter(f"PVZ_1PV < {pvz_ranges[i-1]} && PVZ_1PV > {pvz_ranges[i]}") 
                     for i in range(1, len(pvz_ranges))]

hdata_nVeloTracks_1PV_pvzs = [tdf.Filter("nVeloTracks < 250").Histo1D(("","",250,0,250), "nVeloTracks") 
                              for tdf in tdf_data_1PV_pvzs]

i1 = 1
c2 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.7, 0.2, 0.95, 0.95)
for h in hdata_nVeloTracks_1PV_pvzs:
    i1 += 1
    rsh.TH1D_plot(h, color=i1, xlabel=nVelo_label, norm=True)
rsh.legend_plot(lgd, 
                [h.GetPtr() for h in hdata_nVeloTracks_1PV_pvzs],
                [f"{pvz_ranges[i-1]} > PVZ > {pvz_ranges[i]}" for i in range(1, len(pvz_ranges))],
                ["l" for h in hdata_nVeloTracks_1PV_pvzs])
c2.Draw()
c2.SaveAs("./plots/Exp_MagD18_nVeloTracks_for_PVZ_ranges.pdf")
#--------------------------------------------------------

# Saving Selection in a file
#--------------------------------------------------------

nPV_cut   = "nPVs == 1"
nVelo_cut = "nVeloTracks <= 250"
PVZ_range = "PVZ_1PV < 140 && PVZ_1PV > -40"

evt_sel = f"{nPV_cut} && {nVelo_cut} && {PVZ_range}"

text_file = open("./param_files/evt_selection.txt", "w")
text_file.write(evt_sel)
text_file.close()
#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")