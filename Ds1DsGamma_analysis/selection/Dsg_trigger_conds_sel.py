import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# data reading
#--------------------------------------------------------
head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/"
data_path = f"{head_path}/Data/raw/MagDown/2018"
mc_path = f"{head_path}/MC/TightCut/raw"

data_files = set([f"{data_path}/DsJ_Data_MagD18_{i}_raw.root" for i in range(100, 700)])
Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

tdf_data    = ROOT.RDataFrame("DecayTree", data_files)
tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/{Ds1Dsg_files}")
tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/{DsstDsg_files}")

derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

for keys, values in derivated_features.items():
    tdf_data = tdf_data.Define(keys, values)
    tdf_Ds1_MC = tdf_Ds1_MC.Define(keys, values)
    tdf_Dsst_MC = tdf_Dsst_MC.Define(keys, values)
    
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

TRUEID_Ds1_sig  = TRUEID_dtt["Ds1DsGamma"]
TRUEID_Dsst_sig = TRUEID_dtt["DsstDsGamma"]

tdf_Ds1_sig  = tdf_Ds1_MC.Filter(TRUEID_Ds1_sig)
tdf_Dsst_sig = tdf_Dsst_MC.Filter(TRUEID_Dsst_sig)
#--------------------------------------------------------

# evt selection + cand selection
#--------------------------------------------------------
evt_sel_file = open("./param_files/evt_selection.txt", "r")
evt_sel = evt_sel_file.read()
evt_sel_file.close()

cand_sel_file = open("./param_files/cand_selection.txt", "r")
cand_sel = cand_sel_file.read()
cand_sel_file.close()

sel = f"{evt_sel} && {cand_sel}"

tdf_data_sel = tdf_data.Filter(sel)

## plotting configs
Dsg_mass_label = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
ylabel = "A.U / (7 MeV/c^{2})"
#-------------------------------------------------------

# L0 trigger lines
#-------------------------------------------------------
lz_tis = "(Dsg_L0Global_TIS == 1)"
lz_tos = "(Ds_L0HadronDecision_TOS == 1)"
lz_tistos = f"{lz_tis} && {lz_tos}"
#-------------------------------------------------------

# HLT1 trigger lines
#-------------------------------------------------------
hlt1_tis = "(Ds_Hlt1TrackMVADecision_TIS == 1 | Ds_Hlt1TwoTrackMVADecision_TIS == 1)"
hlt1_tos = "(Ds_Hlt1TrackMVADecision_TOS == 1 | Ds_Hlt1TwoTrackMVADecision_TOS == 1)"
hlt1_tistos = f"{hlt1_tis} && {hlt1_tos}"
#-------------------------------------------------------

# HLT2 trigger lines
#-------------------------------------------------------
hlt2_tis = "(Ds_Hlt2CharmHadDspToKmKpPipDecision_TIS == 1)"
hlt2_tos = "(Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS == 1)"
hlt2_tistos = f"{hlt2_tis} && {hlt2_tos}"
#-------------------------------------------------------

# Total trigger strategy
#-------------------------------------------------------
trig_lines = f"{lz_tis} && {hlt1_tos} && {hlt2_tos}"
#-------------------------------------------------------

# mass plot after all trigger selections
#-------------------------------------------------------

## No trigger mass histograms
hdata_Dsg_mass_raw = tdf_data.Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel = tdf_data_sel.Histo1D(("","",100,2000,2700), "DsgM")

## L0 trigger mass histograms
hdata_Dsg_mass_sel_lz_tis = tdf_data_sel.Filter(lz_tis).Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_lz_tos = tdf_data_sel.Filter(lz_tos).Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_lz_tistos = tdf_data_sel.Filter(lz_tistos).Histo1D(("","",100,2000,2700), "DsgM")

## HLT1 trigger mass histograms
hdata_Dsg_mass_sel_hlt1_tis = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tis}").Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_hlt1_tos = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tos}").Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_hlt1_tistos = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tistos}").Histo1D(("","",100,2000,2700), "DsgM")

## HLT2 trigger mass histograms
hdata_Dsg_mass_sel_hlt2_tis = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tos} && {hlt2_tis}").Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_hlt2_tos = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tos} && {hlt2_tos}").Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel_hlt2_tistos = tdf_data_sel.Filter(f"{lz_tis} && {hlt1_tos} && {hlt2_tistos}").Histo1D(("","",100,2000,2700), "DsgM")

## full trigger mass histograms
hdata_Dsg_mass_sel_trig = tdf_data.Filter(f"{evt_sel} && {cand_sel} && {trig_lines}").Histo1D(("","",100,2000,2700), "DsgM")

## L0 plot
c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
rsh.TH1D_plot(hdata_Dsg_mass_sel, norm=True, opt="E1 same", color=ROOT.kGreen,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_lz_tis, norm=True, opt="E1 same", color=ROOT.kRed,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_lz_tos, norm=True, opt="E1 same", color=ROOT.kBlue,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_lz_tistos, norm=True, opt="E1 same", color=ROOT.kOrange,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.legend_plot(lgd, 
                [hdata_Dsg_mass_sel.GetPtr(), hdata_Dsg_mass_sel_lz_tis.GetPtr(),
                 hdata_Dsg_mass_sel_lz_tos.GetPtr(), hdata_Dsg_mass_sel_lz_tistos.GetPtr()],
                ["sel", "L0 TIS", "L0 TOS", "L0 TIS&TOS"],
                ["lp", "lp", "lp", "lp"])
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel_lz.pdf")

## HLT1 plot
c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
rsh.TH1D_plot(hdata_Dsg_mass_sel_lz_tis, norm=True, opt="E1 same", color=ROOT.kGreen,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt1_tis, norm=True, opt="E1 same", color=ROOT.kRed,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt1_tos, norm=True, opt="E1 same", color=ROOT.kBlue,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt1_tistos, norm=True, opt="E1 same", color=ROOT.kOrange,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.legend_plot(lgd, 
                [hdata_Dsg_mass_sel_lz_tis.GetPtr(), hdata_Dsg_mass_sel_hlt1_tis.GetPtr(),
                 hdata_Dsg_mass_sel_hlt1_tos.GetPtr(), hdata_Dsg_mass_sel_hlt1_tistos.GetPtr()],
                ["sel & L0", "HLT1 TIS", "HLT1 TOS", "HLT1 TIS&TOS"],
                ["lp", "lp", "lp", "lp"])
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel_hlt1.pdf")

## HLT2 plot
c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt1_tos, norm=True, opt="E1 same", color=ROOT.kGreen,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt2_tis, norm=True, opt="E1 same", color=ROOT.kRed,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt2_tos, norm=True, opt="E1 same", color=ROOT.kBlue,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_hlt2_tistos, norm=True, opt="E1 same", color=ROOT.kOrange,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.legend_plot(lgd, 
                [hdata_Dsg_mass_sel_hlt1_tis.GetPtr(), hdata_Dsg_mass_sel_hlt2_tis.GetPtr(),
                 hdata_Dsg_mass_sel_hlt2_tos.GetPtr(), hdata_Dsg_mass_sel_hlt2_tistos.GetPtr()],
                ["sel & L0 & HLT1", "HLT2 TIS", "HLT2 TOS", "HLT2 TIS&TOS"],
                ["lp", "lp", "lp", "lp"])
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel_hlt2.pdf")

## Full trigger plot
c1 = ROOT.TCanvas()
lgd = ROOT.TLegend(0.6, 0.6, 0.9, 0.9)
rsh.TH1D_plot(hdata_Dsg_mass_raw, norm=True, opt="E1 same",
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel, norm=True, opt="E1 same", color=ROOT.kGreen,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.TH1D_plot(hdata_Dsg_mass_sel_trig, norm=True, opt="E1 same", color=ROOT.kRed,
              xlabel=Dsg_mass_label, ylabel=ylabel)
rsh.legend_plot(lgd, 
                [hdata_Dsg_mass_raw.GetPtr(), hdata_Dsg_mass_sel.GetPtr(),
                 hdata_Dsg_mass_sel_trig.GetPtr()],
                ["raw", "sel", "sel & trig"],
                ["lp", "lp", "lp"])
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel_trig.pdf")
#-------------------------------------------------------

# Selection save
#--------------------------------------------------------
text_file = open("./param_files/trig_selection.txt", "w")
text_file.write(trig_lines)
text_file.close()
#-------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")