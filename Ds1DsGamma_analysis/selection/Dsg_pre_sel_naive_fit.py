import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")
        
# Reading histograms for naive fit
#--------------------------------------------------------
files_for_fit_folder = f"{os.getcwd()}/root_files"  

Dsst_mass = [2050, 2250]; Ds1_mass = [2350, 2600]

pre_sel = "abs(Ds_M - 1969) < 20 && gamma_PT > 700 && gamma_CL > 0.7"

text_file = open("./param_files/pre_selection_FoM.txt", "w")
text_file.write(pre_sel)
text_file.close()

if len(os.listdir(files_for_fit_folder)) == 0:
    # Data reading
    derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                          "PVZ_1PV": "PVZ[0]",
                          "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                          "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                          "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                          }

    head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/"
    data_path = f"{head_path}/Data/raw/MagDown/2018"

    data_files = set([f"{data_path}/DsJ_Data_MagD18_{i}_raw.root" for i in range(100, 700)])
    dtt = "DsGammaTuple"

    tdf_data = ROOT.RDataFrame("DecayTree", data_files)

    for keys, values in derivated_features.items():
        tdf_data = tdf_data.Define(keys, values)

    # Event selection
    evt_sel_file = open("./param_files/evt_selection.txt", "r")
    evt_sel = evt_sel_file.read()
    evt_sel_file.close()
    
    tdf_data_evt_sel = tdf_data.Filter(f"{evt_sel} && {pre_sel}") 

    # Mass range for peaks
    tdf_data_Dsst = tdf_data_evt_sel.Filter(f"DsgM > {Dsst_mass[0]} && DsgM < {Dsst_mass[1]}")
    tdf_data_Ds1 = tdf_data_evt_sel.Filter(f"DsgM > {Ds1_mass[0]} && DsgM < {Ds1_mass[1]}")

    # Histogram creation or reading
    hdata_Dsst_mass = tdf_data_Dsst.Histo1D(("","",100,Dsst_mass[0],Dsst_mass[1]), "DsgM")
    hdata_Ds1_mass = tdf_data_Ds1.Histo1D(("","",100,Ds1_mass[0],Ds1_mass[1]), "DsgM")
    
    Dsst_mass_file = ROOT.TFile.Open(f"./root_files/hdata_Dsst_mass.root", "RECREATE")  
    hdata_Dsst_mass.Write("hdata_Dsst_mass")
    Dsst_mass_file.Close()
    
    Ds1_mass_file = ROOT.TFile.Open(f"./root_files/hdata_Ds1_mass.root", "RECREATE")  
    hdata_Ds1_mass.Write("hdata_Ds1_mass")
    Ds1_mass_file.Close()

Dsst_mass_file  = ROOT.TFile(f"{os.getcwd()}/root_files/hdata_Dsst_mass.root", "READ") 
hdata_Dsst_mass = Dsst_mass_file.Get("hdata_Dsst_mass")

Ds1_mass_file  = ROOT.TFile(f"{os.getcwd()}/root_files/hdata_Ds1_mass.root", "READ") 
hdata_Ds1_mass = Ds1_mass_file.Get("hdata_Ds1_mass")
#--------------------------------------------------------

# Fit model: Gaussian + chebyshev
#--------------------------------------------------------
units_G = ["", ""]
units_cheb = ["", "", ""]
units_model = units_G + units_cheb + ["", ""]

## Dsst fit
#========================================================
mDsst = ROOT.RooRealVar("mDsst", "mDsst", Dsst_mass[0], Dsst_mass[1])
dh_Dsst = ROOT.RooDataHist("dh_Dsst", "dh_Dsst", mDsst, hdata_Dsst_mass)

mu_Dsst = ROOT.RooRealVar("mu_Dsst", "mu_Dsst", 2090, 2130)
sigma_Dsst = ROOT.RooRealVar("sigma_Dsst", "sigma_Dsst", 2, 20)
G_Dsst = ROOT.RooGaussian("G_Dsst", "G_Dsst", mDsst, mu_Dsst, sigma_Dsst)

aDsst = ROOT.RooRealVar("aDsst", "aDsst", 0.1, -5, 5)
bDsst = ROOT.RooRealVar("bDsst", "bDsst", -0.1, -5, 5)
cDsst = ROOT.RooRealVar("cDsst", "cDsst", -0.1, -5, 5)
cheb_Dsst = ROOT.RooChebychev("cheb_Dsst", "cheb_Dsst", mDsst, ROOT.RooArgList(aDsst, bDsst, cDsst))

Nentries_Dsst = hdata_Dsst_mass.Integral()

NDsst = ROOT.RooRealVar("NDsst", "NDsst", 0, Nentries_Dsst)
Ncomb_Dsst = ROOT.RooRealVar("Ncomb_Dsst", "Ncomb_Dsst", 0, Nentries_Dsst)

yDsst = [NDsst, Ncomb_Dsst]

for y in yDsst:
    y.setError(np.sqrt(Nentries_Dsst))

model_Dsst = ROOT.RooAddPdf("model_Dsst", "model_Dsst", 
                            ROOT.RooArgList(G_Dsst, cheb_Dsst), 
                            ROOT.RooArgList(NDsst, Ncomb_Dsst))

Dsst_fit = rpf.Fit(model_Dsst.fitTo(dh_Dsst, ROOT.RooFit.Save()))

Dsst_fit.save_to_csv(file_name="./param_files/Exp_Dsst_mass_fit_evt_sel.csv")
Dsst_fit.print()

rpf.plot(mDsst, dh_Dsst, model_Dsst, 
         comps=["G_Dsst", "cheb_Dsst"], comps_color=[ROOT.kRed, ROOT.kGreen],
         xlabel="#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]", ylabel="N_{candidates} / (2 MeV/c^{2})",
         file_name="./plots/Exp_Dsst_mass_fit_evt_sel.pdf")
#========================================================

# Ds1 fit
#========================================================
mDs1 = ROOT.RooRealVar("mDs1", "mDs1", Ds1_mass[0], Ds1_mass[1])
dh_Ds1 = ROOT.RooDataHist("dh_Ds1", "dh_Ds1", mDs1, hdata_Ds1_mass)

mu_Ds1 = ROOT.RooRealVar("mu_Ds1", "mu_Ds1", 2430, 2490)
sigma_Ds1 = ROOT.RooRealVar("sigma_Ds1", "sigma_Ds1", 2, 30)
G_Ds1 = ROOT.RooGaussian("G_Ds1", "G_Ds1", mDs1, mu_Ds1, sigma_Ds1)

aDs1 = ROOT.RooRealVar("aDs1", "aDs1", 0.1, -5, 5)
bDs1 = ROOT.RooRealVar("bDs1", "bDs1", -0.1, -5, 5)
cDs1 = ROOT.RooRealVar("cDs1", "cDs1", -0.1, -5, 5)
cheb_Ds1 = ROOT.RooChebychev("cheb_Ds1", "cheb_Ds1", mDs1, ROOT.RooArgList(aDs1, bDs1, cDs1))

Nentries_Ds1 = hdata_Ds1_mass.Integral()

NDs1 = ROOT.RooRealVar("NDs1", "NDs1", 0, Nentries_Ds1)
Ncomb_Ds1 = ROOT.RooRealVar("Ncomb_Ds1", "Ncomb_Ds1", 0, Nentries_Ds1)

yDs1 = [NDs1, Ncomb_Ds1]

for y in yDs1:
    y.setError(np.sqrt(Nentries_Ds1))

model_Ds1 = ROOT.RooAddPdf("model_Ds1", "model_Ds1", 
                            ROOT.RooArgList(G_Ds1, cheb_Ds1), 
                            ROOT.RooArgList(NDs1, Ncomb_Ds1))

Ds1_fit = rpf.Fit(model_Ds1.fitTo(dh_Ds1, ROOT.RooFit.Save()))

Ds1_fit.print()
Ds1_fit.save_to_csv(file_name="./param_files/Exp_Ds1_mass_fit_evt_sel.csv")

rpf.plot(mDs1, dh_Ds1, model_Ds1, 
         comps=["G_Ds1", "cheb_Ds1"], comps_color=[ROOT.kRed, ROOT.kGreen],
         xlabel="#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]", ylabel="N_{candidates} / (2.5 MeV/c^{2})",
         file_name="./plots/Exp_Ds1_mass_fit_evt_sel.pdf")
#========================================================

#--------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")