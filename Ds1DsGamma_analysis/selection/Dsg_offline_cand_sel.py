import ROOT
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy as sci
import uncertainties as un
from particle import Particle
from datetime import datetime
import sys 
import os
import mplhep

python_modules_path = "/home3/ivan.cambon/Python_Modules"
sys.path.append(f'{python_modules_path}/RooPyShort')
sys.path.append(f'{python_modules_path}/RooPyFit')
import RooPyFit as rpf
import RooPyShort as rsh

ROOT.gROOT.ProcessLine(".L lhcbStyle.C")
ROOT.lhcbStyle()
mplhep.styles.use(mplhep.styles.LHCb2)

ti = datetime.now()

folders = ["plots", "latex_tables", "fit_results", "root_files", "param_files"]

for f in folders:
    if not os.path.exists(f"./{f}"):
        os.makedirs(f"./{f}")

# data reading
#--------------------------------------------------------

head_path = "/scratch42/ivan.cambon/DsJ_Spectroscopy/Reduced/"
data_path = f"{head_path}/Data/raw/MagDown/2018"
mc_path = f"{head_path}/MC/TightCut/raw"

data_files = set([f"{data_path}/DsJ_Data_MagD18_{i}_raw.root" for i in range(100, 700)])
Ds1Dsg_files  = "Ds1DsGamma*.root"
DsstDsg_files = "DsstDsGamma*.root"

dtt = "DsGammaTuple"

tdf_data    = ROOT.RDataFrame("DecayTree", data_files)
tdf_Ds1_MC  = ROOT.RDataFrame("DecayTree", f"{mc_path}/{Ds1Dsg_files}")
tdf_Dsst_MC = ROOT.RDataFrame("DecayTree", f"{mc_path}/{DsstDsg_files}")

derivated_features = {"DsgM": "Dsg_M-Ds_M+1969",
                      "PVZ_1PV": "PVZ[0]",
                      "dR_Dsg": "deltaR(Ds_ETA, Ds_PHI, gamma_ETA, gamma_PHI)",
                      "aPT_Dsg": "asym_PT(Ds_PT, gamma_PT)",
                      "aETA_Dsg": "asym_ETA(Ds_ETA, gamma_ETA)"
                      }

for keys, values in derivated_features.items():
    tdf_data = tdf_data.Define(keys, values)
    tdf_Ds1_MC = tdf_Ds1_MC.Define(keys, values)
    tdf_Dsst_MC = tdf_Dsst_MC.Define(keys, values)
    
TRUEID = rsh.TRUEIDs()
TRUEID_dtt = TRUEID[dtt]

TRUEID_Ds1_sig  = TRUEID_dtt["Ds1DsGamma"]
TRUEID_Dsst_sig = TRUEID_dtt["DsstDsGamma"]

tdf_Ds1_sig  = tdf_Ds1_MC.Filter(TRUEID_Ds1_sig)
tdf_Dsst_sig = tdf_Dsst_MC.Filter(TRUEID_Dsst_sig)
#--------------------------------------------------------

# evt selection + pre selection
#--------------------------------------------------------

evt_sel_file = open("./param_files/evt_selection.txt", "r")
evt_sel = evt_sel_file.read()
evt_sel_file.close()

pre_sel_file = open("./param_files/pre_selection_FoM.txt", "r")
pre_sel = pre_sel_file.read()
pre_sel_file.close()

tdf_data_pre = tdf_data.Filter(f"{evt_sel} && {pre_sel}")

tdf_bkg_pre = tdf_data_pre.Filter("DsgM > 2600")
tdf_Ds1_sig_pre = tdf_Ds1_sig.Filter(f"{evt_sel} && {pre_sel}")
tdf_Dsst_sig_pre = tdf_Ds1_sig.Filter(f"{evt_sel} && {pre_sel}")

#--------------------------------------------------------

# FoM 1D plot for pT(gamma) optimization cut
#--------------------------------------------------------

path_pre_sel_fit = "./param_files"

Dsst_fit = pd.read_csv(f"{path_pre_sel_fit}/Exp_Dsst_mass_fit_evt_sel.csv")
Ds1_fit  = pd.read_csv(f"{path_pre_sel_fit}/Exp_Ds1_mass_fit_evt_sel.csv")

NDsst = np.array((Dsst_fit.query('Parameters == "NDsst"'))["Values"])[0]
Ncomb_Dsst = np.array((Dsst_fit.query('Parameters == "Ncomb_Dsst"'))["Values"])[0]

NDs1 = np.array((Ds1_fit.query('Parameters == "NDs1"'))["Values"])[0]
Ncomb_Ds1 = np.array((Ds1_fit.query('Parameters == "Ncomb_Ds1"'))["Values"])[0]

print("Calculating the FoM plot")
plt.figure(1)
opt_pt_cut = rsh.FoM1D_plot(tdf_Ds1_sig_pre, tdf_bkg_pre, "gamma_PT", 3000,
                            NDs1, Ncomb_Ds1, xlabel=r"$p_{\mathrm{T}}(\gamma)$ [MeV/c]", 
                            fmt="b.", label_plot=r"$D_{s1}(2460)^+$ MC")
plt.legend()
plt.savefig("./plots/Comp_FoM_1D_gamma_PT.pdf", dpi=300, bbox_inches="tight")
plt.show()

#--------------------------------------------------------

# Selection save
#--------------------------------------------------------
cand_sel = f"{opt_pt_cut} && gamma_CL > 0.7 && abs(Ds_M - 1969) < 20"

text_file = open("./param_files/cand_selection.txt", "w")
text_file.write(cand_sel)
text_file.close()
#--------------------------------------------------------

# Mass plot before and after
#--------------------------------------------------------
hdata_Dsg_mass_raw = tdf_data.Histo1D(("","",100,2000,2700), "DsgM")
hdata_Dsg_mass_sel = tdf_data.Filter(f"{evt_sel} && {cand_sel}").Histo1D(("","",100,2000,2700), "DsgM")

Dsg_mass_label = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
c1 = ROOT.TCanvas()
rsh.TH1D_plot(hdata_Dsg_mass_raw, norm=True, opt="E1 same",
              xlabel=Dsg_mass_label, ylabel="A.U / (7 MeV/c^{2})")
rsh.TH1D_plot(hdata_Dsg_mass_sel, norm=True, opt="E1 same", color=ROOT.kGreen,
              xlabel=Dsg_mass_label, ylabel="A.U / (7 MeV/c^{2})")
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel.pdf")

Dsg_mass_label = "#it{M(D_{s}^{+}#gamma)} [MeV/c^{2}]"
c1 = ROOT.TCanvas()
rsh.TH1D_plot(hdata_Dsg_mass_sel, norm=True, opt="E1 same", color=ROOT.kBlack,
              xlabel=Dsg_mass_label, ylabel="A.U / (7 MeV/c^{2})")
c1.Draw()
c1.SaveAs("./plots/Exp_MagD18_Dsg_mass_cand_evt_sel_v2.pdf")

#-------------------------------------------------------

tf = datetime.now()

print("----------------------")
print(f"Execution time: {tf-ti}")
print("----------------------")