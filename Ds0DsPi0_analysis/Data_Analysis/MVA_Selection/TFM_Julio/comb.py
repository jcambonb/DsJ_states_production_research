# -*- coding: utf-8 -*-
"""

@author: novoa
"""

import root_pandas
from ROOT import *
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from xgboost import XGBClassifier
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
ROOT.EnableImplicitMT()

pd.set_option('display.max_columns', None)

title = "TV: g1_PT, g2_PT, g1_CL, g2_CL, R_g1_g2, pi0_PTAsym, pi0_etaAsym"

def plot_comparison(var, mc_df, bkg_df, a, b):
    fig, ax = plt.subplots()
    _, bins, _ = plt.hist(mc_df[var], bins=100, 
                          histtype='step', label='MC', density=1)
    _, bins, _ = plt.hist(bkg_df[var], bins=bins, 
                          histtype='step', label='Background', density=1)
    plt.xlabel(var)
    plt.xlim(a, b)
    plt.legend(loc='best')
    plt.title(title)

def plot_mass(df, **kwargs):
    counts, bins, _ = plt.hist(df["Ds0stM"], bins=100, 
                               range=[2250, 2400], histtype='step', **kwargs)
    # You can also use LaTeX in the axis label
    plt.xlabel('$Ds0*$ mass [MeV]')
    plt.xlim(bins[0], bins[-1])
    plt.title(title)

    
def plot_roc(bdt, training_data, training_columns, label=None):
    y_score = bdt.predict_proba(training_data[training_columns])[:,1]
    fpr, tpr, thresholds = roc_curve(training_data['category'], y_score)
    area = auc(fpr, tpr)

    plt.plot([0, 1], [0, 1], color='grey', linestyle='--')
    if label:
        plt.plot(fpr, tpr, label=f'{label} (area = {area:.3f})')
    else:
        plt.plot(fpr, tpr, label=f'ROC curve (area = {area:.3f})')
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='lower right')
    # We can make the plot look nicer by forcing the grid to be square
    plt.gca().set_aspect('equal', adjustable='box')
    plt.title(title)

def plot_significance(bdt, training_data, training_columns, a, b, label):
    y_score = bdt.predict_proba(training_data[training_columns])[:,1]
    fpr, tpr, thresholds = roc_curve(training_data['category'], y_score)
    n_sig = 1200
    n_bkg = 23000
    S = n_sig*tpr
    B = n_bkg*fpr
    metric_nan = S/np.sqrt(S+B)
    metric = np.nan_to_num(metric_nan, nan=0.0)

    plt.plot(thresholds, metric, label=label)
    plt.xlabel('BDT cut value')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(a, b)
    optimal_cut = thresholds[np.argmax(metric)]
    plt.axvline(x=optimal_cut, color='black', linestyle='--', label=f"Optimal cut = {optimal_cut:.3f}")
    plt.legend(loc='upper right')
    plt.title(title)
    return optimal_cut

# paths for data and MC    
path = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/Data/Exclusive/MagDown/2018/'
pathmc = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/MC/Exclusive/'

# loads the experimental data and defines Pi0 and Dso* mass
df = ROOT.RDataFrame("Ds2KKPiTuple/DecayTree", path+"4068638*.root")
 
df = df.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                       - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")
    
df = df.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")

df = df.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       +(gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")    

df = df.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")     
        
df = df.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       +(Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")

df = df.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")    
    
df = df.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")  

df = df.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
df = df.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

df = df.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
df = df.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

df = df.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 

df = df.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")
    
df = df.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
df = df.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")
    
df = df.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

df = df.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")  
    
df = df.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
    
# selects the columns we want to work with
cols = ["Ds0stM", 
        "Ds_eta",
        "pi0_eta",
        "R_Ds_Pi0",
        "R_g1_g2",
        "R_Ds_g1",
        "R_Ds_g2",
        "Ds_phi",
        "pi0_phi",
        "pi_PT", 
        "Kpl_PT", 
        "Kmi_PT", 
        'pi0M',
        "gamma_1_CL", 
        "gamma_2_CL",
        'Kpl_PE',
        'Kmi_PE',
        'pi_PE',
        "pi0_PTAsym",
        "pi0_etaAsym",
        'gamma_1_PE',
        'gamma_2_PE',
        'gamma_1_PT',
        'gamma_2_PT',
        'pi0_PT']

# transforms the data into a Pandas DataFrame
npyd = df.AsNumpy(columns=cols)
data_df = pd.DataFrame(npyd)

# loads the MC data and defines the Pi0 and Ds0* mass
mdf = ROOT.RDataFrame("D2KKPiTuple/DecayTree", 
                      pathmc+"Ds0DsPi0*.root")

mdf = mdf.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                        - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                        - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                        - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")

mdf = mdf.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")    

mdf = mdf.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       + (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")      

mdf = mdf.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")      
    
mdf = mdf.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       +(Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")  

mdf = mdf.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")           
    
mdf = mdf.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")   
    
mdf = mdf.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
                             
mdf = mdf.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")

mdf = mdf.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

mdf = mdf.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
mdf = mdf.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

mdf = mdf.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 
    
mdf = mdf.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")

mdf = mdf.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
mdf = mdf.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")    

mdf = mdf.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

mdf = mdf.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")   
                               
# selects the columns we want to work with                             
colsmc = ["Ds0stM", 
          "Ds_eta",
          "pi0_eta",
          "Ds_phi",
          "pi0_phi",
          "R_Ds_Pi0",
          "R_Ds_g1",
          "R_Ds_g2",
          "R_g1_g2",
          "pi0_PTAsym",
          "pi0_etaAsym",
          "pi_PT", 
          "Kpl_PT", 
          "Kmi_PT", 
          "gamma_1_CL", 
          "gamma_2_CL",
          'gamma_1_PT',
          'gamma_2_PT',
          'pi0M',
          'pi0_PT',
          'Kpl_PE',
          'Kmi_PE',
          'pi_PE',
          'Ds_MC_MOTHER_ID',
          'gamma_1_PE',
          'gamma_2_PE',
          'gamma_1_TRUEID',
          'gamma_2_TRUEID',
          'gamma_2_MC_MOTHER_ID',
          'gamma_1_MC_MOTHER_ID',
          'gamma_2_MC_GD_MOTHER_ID',
          'gamma_1_MC_GD_MOTHER_ID']                             
    
# transforms the MC data into a Pandas DataFrame
npym = mdf.AsNumpy(columns=colsmc)
mc_df = pd.DataFrame(npym) 

# defines the background and the MC peak signal
bkg_df = data_df.query("~(2200 < Ds0stM < 2400) &\
                       gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       90 < pi0M &\
                       pi0M < 190 &\
                       pi0_PT > 900 ")
                       
data_df = data_df.query("gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       90 < pi0M &\
                       pi0M < 170 &\
                       pi0_PT > 900 ")                       
                       
                       
mc_df = mc_df.query("abs(gamma_1_TRUEID) == 22 &\
                     abs(gamma_2_TRUEID) == 22 &\
                     abs(gamma_1_MC_MOTHER_ID) == 111 &\
                     abs(gamma_2_MC_MOTHER_ID) == 111 &\
                     abs(gamma_1_MC_GD_MOTHER_ID) == 10431 &\
                     abs(gamma_2_MC_GD_MOTHER_ID) == 10431 &\
                     abs(Ds_MC_MOTHER_ID) == 10431 &\
                     gamma_1_CL > 0.7 &\
                     gamma_2_CL > 0.7 &\
                     90 < pi0M &\
                     pi0M < 190 &\
                     pi0_PT > 900 ")

# selects the training columns
training_columns = ["gamma_1_PT",
                    "gamma_2_PT",
                    "gamma_1_CL",
                    "gamma_2_CL",
                    "R_g1_g2",
                    "pi0_PTAsym",
                    "pi0_etaAsym"]

# assigns 0 and 1 to background and signal events
bkg_df = bkg_df.copy()
bkg_df['category'] = 0  # Use 0 for background
mc_df['category'] = 1  # Use 1 for signal
training_data = pd.concat([bkg_df, mc_df], copy=True, ignore_index=True)

# XGBoost
# defines the classifier and performs the training
bdt_xgb = XGBClassifier()
bdt_xgb.fit(training_data[training_columns], training_data['category'])

# makes the predictions and adds the new column to the data
for idf in [mc_df, bkg_df, data_df, training_data]:
    idf['XGB'] = bdt_xgb.predict_proba(idf[training_columns])[:,1]

# AdaBoost
bdt_ada = AdaBoostClassifier()
bdt_ada.fit(training_data[training_columns], training_data['category'])

# makes the predictions and adds the new column to the data
for idf in [mc_df, bkg_df, data_df, training_data]:
    idf['ADA'] = bdt_ada.predict_proba(idf[training_columns])[:,1]

# GBC
bdt_gbc = GradientBoostingClassifier()
bdt_gbc.fit(training_data[training_columns], training_data['category'])

# makes the predictions and adds the new column to the data
for idf in [mc_df, bkg_df, data_df, training_data]:
    idf['GBC'] = bdt_gbc.predict_proba(idf[training_columns])[:,1]

# plots the BDT variable for background and MC

plt.figure()
plot_comparison('GBC', mc_df, bkg_df, 0., 1.)
plt.savefig("comp_gbc.pdf")

plt.figure()
plot_comparison('XGB', mc_df, bkg_df, 0., 1.)
plt.savefig("comp_xgb.pdf")

plt.figure()
plot_comparison('ADA', mc_df, bkg_df, 0.44, 0.52)
plt.savefig("comp_ada.pdf")

# plots the ROC curve
plt.figure()
plot_roc(bdt_gbc, training_data, training_columns, label="ROC curve for GBC")
plot_roc(bdt_xgb, training_data, training_columns, label="ROC curve for XGB")
plot_roc(bdt_ada, training_data, training_columns, label="ROC curve for ADA")
plt.savefig('roc.pdf')

# plots the significance curve
plt.figure()
oc_gbc = plot_significance(bdt_gbc, training_data, training_columns, 0., 1., label="GBC")
plt.savefig('sign_gbc.pdf')

plt.figure()
oc_xgb = plot_significance(bdt_xgb, training_data, training_columns, 0., 1., label="XGB")
plt.savefig('sign_xgb.pdf')

plt.figure()
oc_ada = plot_significance(bdt_ada, training_data, training_columns, 0.4, 0.6, label="ADA")
plt.savefig('sign_ada.pdf')

# Perform the cuts
ocut_gbc = data_df.query(f'GBC > {oc_gbc:.3f}')
ocut_xgb = data_df.query(f'XGB > {oc_xgb:.3f}')
ocut_ada = data_df.query(f'ADA > {oc_ada:.3f}')

plt.figure()
plot_mass(data_df, label='No BDT', density=1)
plot_mass(ocut_gbc, label=f'GBC > {oc_gbc:.3f}', density=1)
plot_mass(ocut_xgb, label=f'XGB > {oc_xgb:.3f}', density=1)
plot_mass(ocut_ada, label=f'ADA > {oc_ada:.3f}', density=1)
plt.legend(loc='upper left')
plt.savefig('mass1.pdf')
