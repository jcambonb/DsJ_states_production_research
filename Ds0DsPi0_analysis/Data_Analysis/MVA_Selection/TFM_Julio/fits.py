#!/usr/bin/env python

from ROOT import *
from math import *
import sys
import pandas as pd
from xgboost import XGBClassifier
ROOT.EnableImplicitMT()

path1 = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/Data/Exclusive/MagUp/2018/'
path2 = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/Data/Exclusive/MagDown/2018/'
pathmc = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/MC/Exclusive/'   
path = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/Data/Exclusive/MagDown/2018/'

# loads the experimental data and defines Pi0 and Dso* mass
df = ROOT.RDataFrame("Ds2KKPiTuple/DecayTree", path+"4068638*.root")
df = df.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                       - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")
    
df = df.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")

df = df.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       +(gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")    

df = df.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")     
        
df = df.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       + (Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")

df = df.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")    
    
df = df.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")  

df = df.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
df = df.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

df = df.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
df = df.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

df = df.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 

df = df.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")
    
df = df.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
df = df.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")
    
df = df.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

df = df.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")  
    
df = df.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
    
# selects the columns we want to work with
cols = ["Ds0stM", 
        "Ds_eta",
        "pi0_eta",
        "R_Ds_Pi0",
        "R_g1_g2",
        "R_Ds_g1",
        "R_Ds_g2",
        "Ds_phi",
        "pi0_phi",
        "pi_PT", 
        "Kpl_PT", 
        "Kmi_PT", 
        'pi0M',
        "gamma_1_CL", 
        "gamma_2_CL",
        'Kpl_PE',
        'Kmi_PE',
        'pi_PE',
        "pi0_PTAsym",
        "pi0_etaAsym",
        'gamma_1_PE',
        'gamma_2_PE',
        'gamma_1_PT',
        'gamma_2_PT',
        'pi0_PT']

# transforms the data into a Pandas DataFrame
npyd = df.AsNumpy(columns=cols)
data_df = pd.DataFrame(npyd)

# loads the MC data and defines the Pi0 and Ds0* mass
mdf = ROOT.RDataFrame("D2KKPiTuple/DecayTree", 
                      pathmc+"Ds0DsPi0*.root")

mdf = mdf.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                        - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                        - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                        - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")

mdf = mdf.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")    

mdf = mdf.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       + (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")      

mdf = mdf.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")      
    
mdf = mdf.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       +(Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")  

mdf = mdf.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")           
    
mdf = mdf.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")   
    
mdf = mdf.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
                             
mdf = mdf.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")

mdf = mdf.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

mdf = mdf.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
mdf = mdf.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

mdf = mdf.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 
    
mdf = mdf.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")

mdf = mdf.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
mdf = mdf.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")    

mdf = mdf.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

mdf = mdf.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")   
                               
# selects the columns we want to work with                             
colsmc = ["Ds0stM", 
          "Ds_eta",
          "pi0_eta",
          "Ds_phi",
          "pi0_phi",
          "R_Ds_Pi0",
          "R_Ds_g1",
          "R_Ds_g2",
          "R_g1_g2",
          "pi0_PTAsym",
          "pi0_etaAsym",
          "pi_PT", 
          "Kpl_PT", 
          "Kmi_PT", 
          "gamma_1_CL", 
          "gamma_2_CL",
          'gamma_1_PT',
          'gamma_2_PT',
          'pi0M',
          'pi0_PT',
          'Kpl_PE',
          'Kmi_PE',
          'pi_PE',
          'Ds_MC_MOTHER_ID',
          'gamma_1_PE',
          'gamma_2_PE',
          'gamma_1_TRUEID',
          'gamma_2_TRUEID',
          'gamma_2_MC_MOTHER_ID',
          'gamma_1_MC_MOTHER_ID',
          'gamma_2_MC_GD_MOTHER_ID',
          'gamma_1_MC_GD_MOTHER_ID']                             
    
# transforms the MC data into a Pandas DataFrame
npym = mdf.AsNumpy(columns=colsmc)
mc_df = pd.DataFrame(npym) 

# defines the background and the MC peak signal
bkg_df = data_df.query("~(2200 < Ds0stM < 2400) &\
                       gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       gamma_1_PT > 400 &\
                       gamma_2_PT > 400 &\
                       90 < pi0M &\
                       pi0M < 190 &\
                       pi0_PT > 900 &\
                       R_Ds_Pi0 < 0.5")
                       
data_df = data_df.query("gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       gamma_1_PT > 400 &\
                       gamma_2_PT > 400 &\
                       90 < pi0M &\
                       pi0M < 170 &\
                       pi0_PT > 900 &\
                       R_Ds_Pi0 < 0.5")                       
                                    
mc_df = mc_df.query("abs(gamma_1_TRUEID) == 22 &\
                     abs(gamma_2_TRUEID) == 22 &\
                     abs(gamma_1_MC_MOTHER_ID) == 111 &\
                     abs(gamma_2_MC_MOTHER_ID) == 111 &\
                     abs(gamma_1_MC_GD_MOTHER_ID) == 10431 &\
                     abs(gamma_2_MC_GD_MOTHER_ID) == 10431 &\
                     abs(Ds_MC_MOTHER_ID) == 10431 &\
                     gamma_1_CL > 0.7 &\
                     gamma_2_CL > 0.7 &\
                     gamma_1_PT > 400 &\
                     gamma_2_PT > 400 &\
                     90 < pi0M &\
                     pi0M < 190 &\
                     pi0_PT > 900 &\
                     R_Ds_Pi0 < 0.5")

# selects the training columns
training_columns = ["gamma_1_PT",
                    "gamma_2_PT",
                    "gamma_1_CL",
                    "gamma_2_CL",
                    "R_g1_g2"]

# assigns 0 and 1 to background and signal events
bkg_df = bkg_df.copy()
bkg_df['category'] = 0  # Use 0 for background
mc_df['category'] = 1  # Use 1 for signal
training_data = pd.concat([bkg_df, mc_df], copy=True, ignore_index=True)

# XGBoost
# defines the classifier and performs the training
bdt_xgb = XGBClassifier()
bdt_xgb.fit(training_data[training_columns], training_data['category'])

tdf1 = ROOT.RDataFrame("Ds2KKPiTuple/DecayTree", path1+"4068649*.root")
tdf2 = ROOT.RDataFrame("Ds2KKPiTuple/DecayTree", path2+"4068639*.root")

tdf1 = tdf1.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                       - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")
    
tdf1 = tdf1.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")

tdf1 = tdf1.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       +(gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")    

tdf1 = tdf1.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")     
        
tdf1 = tdf1.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       + (Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")

tdf1 = tdf1.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")    
    
tdf1 = tdf1.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")  

tdf1 = tdf1.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
tdf1 = tdf1.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

tdf1 = tdf1.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
tdf1 = tdf1.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

tdf1 = tdf1.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 

tdf1 = tdf1.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")
    
tdf1 = tdf1.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
tdf1 = tdf1.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")
    
tdf1 = tdf1.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

tdf1 = tdf1.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")  
    
tdf1 = tdf1.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
                             
tdf2 = tdf2.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                       - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")
    
tdf2 = tdf2.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")

tdf2 = tdf2.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       +(gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")    

tdf2 = tdf2.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")     
        
tdf2 = tdf2.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       + (Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")

tdf2 = tdf2.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")    
    
tdf2 = tdf2.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")  

tdf2 = tdf2.Define("R_Ds_Pi0", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
tdf2 = tdf2.Define("gamma_1_eta", "atanh((gamma_1_PZ)/ \
                       sqrt((gamma_1_PX)*(gamma_1_PX) \
                       + (gamma_1_PY)*(gamma_1_PY) \
                       + (gamma_1_PZ)*(gamma_1_PZ)))")    

tdf2 = tdf2.Define("gamma_2_eta", "atanh((gamma_2_PZ)/ \
                       sqrt((gamma_2_PX)*(gamma_2_PX) \
                       + (gamma_2_PY)*(gamma_2_PY) \
                       + (gamma_2_PZ)*(gamma_2_PZ)))")      
    
tdf2 = tdf2.Define("gamma_1_phi", "acos(gamma_1_PX/gamma_1_PT)")  

tdf2 = tdf2.Define("gamma_2_phi", "acos(gamma_2_PX/gamma_2_PT)") 

tdf2 = tdf2.Define("R_g1_g2", "sqrt((gamma_1_eta - gamma_2_eta)*(gamma_1_eta - gamma_2_eta) + \
               (gamma_1_phi - gamma_2_phi)*(gamma_1_phi - gamma_2_phi))")
    
tdf2 = tdf2.Define("R_Ds_g1", "sqrt((Ds_eta - gamma_1_eta)*(Ds_eta - gamma_1_eta) + \
               (Ds_phi - gamma_1_phi)*(Ds_phi - gamma_1_phi))") 
    
tdf2 = tdf2.Define("R_Ds_g2", "sqrt((Ds_eta - gamma_2_eta)*(Ds_eta - gamma_2_eta) + \
               (Ds_phi - gamma_2_phi)*(Ds_phi - gamma_2_phi))")
    
tdf2 = tdf2.Define("pi0_PTAsym", "(gamma_1_PT - gamma_2_PT)/ \
                               (gamma_1_PT + gamma_2_PT)")   

tdf2 = tdf2.Define("pi0_etaAsym", "(gamma_1_eta - gamma_2_eta)/ \
                               (gamma_1_eta + gamma_2_eta)")  
    
tdf2 = tdf2.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")                             

n1 = tdf1.AsNumpy(columns=cols)
final1 = pd.DataFrame(n1)
final1 = final1.query("gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       gamma_1_PT > 400 &\
                       gamma_2_PT > 400 &\
                       90 < pi0M &\
                       pi0M < 170 &\
                       pi0_PT > 900 &\
                       R_Ds_Pi0 < 0.5") 
final1['XGB'] = bdt_xgb.predict_proba(final1[training_columns])[:,1]

n2 = tdf2.AsNumpy(columns=cols)
final2 = pd.DataFrame(n2)
final2 = final2.query("gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       gamma_1_PT > 400 &\
                       gamma_2_PT > 400 &\
                       90 < pi0M &\
                       pi0M < 170 &\
                       pi0_PT > 900 &\
                       R_Ds_Pi0 < 0.5") 
final2['XGB'] = bdt_xgb.predict_proba(final2[training_columns])[:,1]

import ROOT 

cols.append("XGB")
tdf1_np = {key: final1[key].values for key in cols}
tdf1 = ROOT.RDF.MakeNumpyDataFrame(tdf1_np)

tdf2_np = {key: final2[key].values for key in cols}
tdf2 = ROOT.RDF.MakeNumpyDataFrame(tdf2_np)

histoMassDs0st_cut1 = tdf1.Filter("gamma_1_CL > 0.7 &&\
                                gamma_2_CL > 0.7 &&\
                                gamma_1_PT > 400 &&\
                                gamma_2_PT > 400 &&\
                                90 < pi0M &&\
                                pi0M < 170 &&\
                                R_Ds_Pi0 < 0.5 &&\
                                pi0_PT > 900 &&\
                                XGB > 0.093").Histo1D(ROOT.RDF.TH1DModel("Ds0stM", "", 50, 2250, 2400), "Ds0stM")

histoMassDs0st_cut2 = tdf2.Filter("gamma_1_CL > 0.7 &&\
                                gamma_2_CL > 0.7 &&\
                                gamma_1_PT > 400 &&\
                                gamma_2_PT > 400 &&\
                                90 < pi0M &&\
                                pi0M < 170 &&\
                                R_Ds_Pi0 < 0.5 &&\
                                pi0_PT > 900 &&\
                                XGB > 0.093").Histo1D(ROOT.RDF.TH1DModel("Ds0stM", "", 50, 2250, 2400), "Ds0stM")

histoMassDs0st_cut1.Add(histoMassDs0st_cut2.GetPtr())
nentries = histoMassDs0st_cut1.Integral()

mhigh = 2400
mlow = 2250
meancentral = 2317
meanlow = 2312
meanhigh= 2322

m = RooRealVar("m", "m", mlow, mhigh)
dh = RooDataHist("data", "data", RooArgList(m), histoMassDs0st_cut1.GetPtr())

## create model high mass
mean = RooRealVar("mean", "mean", meancentral, meanlow, meanhigh)
sigma = RooRealVar("sigma", "sigma", 20, 15, 35)
gauss = RooGaussian("gauss", "gauss", m, mean, sigma)

a1 = RooRealVar("a1","a1",0.3,-1,1)
a2 = RooRealVar("a2","a2",-0.1,-1,1)
a3 = RooRealVar("a3","a3",-0.01,-1,1)
expo = RooChebychev("cheb", "cheb", m, RooArgList(a1,a2,a3))

nsig = RooRealVar("nsig", "nsig", nentries*0.05, 0, 1.*nentries)
nbkg = RooRealVar("nbkg", "nbkg", nentries*0.9, 0, 1.*nentries)
nsig.setError(sqrt(nentries))
nbkg.setError(sqrt(nentries))

model = RooAddPdf("model", "model", RooArgList(gauss, expo), 
                                    RooArgList(nsig, nbkg))

gROOT.ProcessLine(".x lhcbStyle.C")

c3 = TCanvas()
fr = m.frame()
r = model.fitTo(dh)
dh.plotOn(fr)
model.plotOn(fr)
model.plotOn(fr,RooFit.Components("gauss"),RooFit.LineColor(kRed))
model.plotOn(fr,RooFit.Components("cheb"),RooFit.LineColor(kGreen+2))
# fr.SetMaximum(20000)
fr.GetXaxis().SetTitle("m(D_{s}^{+} #pi^{0}) (MeV)")
fr.Draw()
c3.SaveAs("dsfit.pdf")

nsig.Print()
nbkg.Print()

