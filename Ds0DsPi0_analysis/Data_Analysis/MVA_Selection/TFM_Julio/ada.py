 # -*- coding: utf-8 -*-
"""

@author: novoa
"""

import root_pandas
from ROOT import *
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from xgboost import XGBClassifier
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
ROOT.EnableImplicitMT()

pd.set_option('display.max_columns', None)

def plot_comparison(var, mc_df, bkg_df):
    fig, ax = plt.subplots()
    _, bins, _ = plt.hist(mc_df[var], bins=100, 
                          histtype='step', label='MC', density=1)
    _, bins, _ = plt.hist(bkg_df[var], bins=bins, 
                          histtype='step', label='Background', density=1)
    plt.xlabel(var)
    plt.xlim(0.40, 0.52)
    plt.legend(loc='best')

    textstr = '\n'.join((
        "Training variables",
        "-------------------------",
        "gamma_1_PT",
        "gamma_2_PT",
        "gamma_1_CL",
        "gamma_2_CL"))
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    ax.text(0.725, 0.4, textstr, transform=ax.transAxes, bbox=props)
    

def plot_mass(df, **kwargs):
    counts, bins, _ = plt.hist(df["Ds0stM"], bins=100, 
                               range=[2000, 2700], histtype='step', **kwargs)
    # You can also use LaTeX in the axis label
    plt.xlabel('$Ds0*$ mass [MeV]')
    plt.xlim(bins[0], bins[-1])
    
def plot_roc(bdt, training_data, training_columns, label=None):
    y_score = bdt.predict_proba(training_data[training_columns])[:,1]
    fpr, tpr, thresholds = roc_curve(training_data['category'], y_score)
    area = auc(fpr, tpr)

    plt.plot([0, 1], [0, 1], color='grey', linestyle='--')
    if label:
        plt.plot(fpr, tpr, label=f'{label} (area = {area:.2f})')
    else:
        plt.plot(fpr, tpr, label=f'ROC curve (area = {area:.2f})')
    plt.xlim(0.0, 1.0)
    plt.ylim(0.0, 1.0)
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='lower right')
    # We can make the plot look nicer by forcing the grid to be square
    plt.gca().set_aspect('equal', adjustable='box')

def plot_significance(bdt, training_data, training_columns, label=None):
    y_score = bdt.predict_proba(training_data[training_columns])[:,1]
    fpr, tpr, thresholds = roc_curve(training_data['category'], y_score)
    n_sig = 1200
    n_bkg = 23000
    S = n_sig*tpr
    B = n_bkg*fpr
    metric = S/np.sqrt(S+B)

    plt.plot(thresholds, metric, label=label)
    plt.xlabel('BDT cut value')
    plt.ylabel('$\\frac{S}{\\sqrt{S+B}}$')
    plt.xlim(0.2, 0.6)

    optimal_cut = thresholds[np.argmax(metric)]
    plt.axvline(optimal_cut, color='black', linestyle='--')

# paths for data and MC    
path = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/Data/Exclusive/MagDown/2018/'
pathmc = '/RXcHadronic_2/ricardo.vazquez.gomez/DsSpectroscopy/MC/Exclusive/'

# loads the experimental data and defines Pi0 and Dso* mass
listvars = {
        "pi_PT", 
        "Kpl_PT", 
        "Kmi_PT", 
        "gamma_1_CL", 
        "gamma_2_CL",
        'Kpl_PE',
        'Kmi_PE',
        'pi_PE',
        'gamma_1_PE',
        'gamma_2_PE',
        'gamma_1_PT',
        'gamma_2_PT'}

df = ROOT.RDataFrame("Ds2KKPiTuple/DecayTree",
                     path+"406863884*.root", listvars)
 
df = df.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                       - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")
    
df = df.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")

df = df.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       +(gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")    

df = df.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")     
        
df = df.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       +(Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")

df = df.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")    
    
df = df.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")  

df = df.Define("R", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
df = df.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
    
# selects the columns we want to work with
cols = ["Ds0stM", 
        "Ds_eta",
        "pi0_eta",
        "R",
        "Ds_phi",
        "pi0_phi",
        "pi_PT", 
        "Kpl_PT", 
        "Kmi_PT", 
        'pi0M',
        "gamma_1_CL", 
        "gamma_2_CL",
        'Kpl_PE',
        'Kmi_PE',
        'pi_PE',
        'gamma_1_PE',
        'gamma_2_PE',
        'gamma_1_PT',
        'gamma_2_PT',
        'pi0_PT']

# transforms the data into a Pandas DataFrame
npyd = df.AsNumpy(columns=cols)
data_df = pd.DataFrame(npyd)

# loads the MC data and defines the Pi0 and Ds0* mass
listvars_mc = {
          "pi_PT", 
          "Kpl_PT", 
          "Kmi_PT", 
          "gamma_1_CL", 
          "gamma_2_CL",
          'gamma_1_PT',
          'gamma_2_PT',
          'pi0_PT',
          'Kpl_PE',
          'Kmi_PE',
          'pi_PE',
          'Ds_MC_MOTHER_ID',
          'gamma_1_PE',
          'gamma_2_PE',
          'gamma_1_TRUEID',
          'gamma_2_TRUEID',
          'gamma_2_MC_MOTHER_ID',
          'gamma_1_MC_MOTHER_ID',
          'gamma_2_MC_GD_MOTHER_ID',
          'gamma_1_MC_GD_MOTHER_ID'}

mdf = ROOT.RDataFrame("D2KKPiTuple/DecayTree", 
                      pathmc+"Ds0DsPi0*.root",
                      listvars_mc)

mdf = mdf.Define("pi0M", "sqrt((gamma_1_PE+gamma_2_PE)*(gamma_1_PE+gamma_2_PE) \
                        - (gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                        - (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                        - (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ))")

mdf = mdf.Define("pi0_PT", "sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY))")    

mdf = mdf.Define("pi0_eta", "atanh((gamma_1_PZ+gamma_2_PZ)/ \
                       sqrt((gamma_1_PX+gamma_2_PX)*(gamma_1_PX+gamma_2_PX) \
                       + (gamma_1_PY+gamma_2_PY)*(gamma_1_PY+gamma_2_PY) \
                       + (gamma_1_PZ+gamma_2_PZ)*(gamma_1_PZ+gamma_2_PZ)))")      

mdf = mdf.Define("pi0_phi", "acos((gamma_1_PX+gamma_2_PX)/pi0_PT)")      
    
mdf = mdf.Define("Ds_eta", "atanh((Kpl_PZ+Kmi_PZ+pi_PZ)/ \
                       sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY) \
                       +(Kpl_PZ+Kmi_PZ+pi_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ)))")  

mdf = mdf.Define("Ds_pt", "sqrt((Kpl_PX+Kmi_PX+pi_PX)*(Kpl_PX+Kmi_PX+pi_PX) \
                       + (Kpl_PY+Kmi_PY+pi_PY)*(Kpl_PY+Kmi_PY+pi_PY))")           
    
mdf = mdf.Define("Ds_phi", "acos((Kpl_PX+Kmi_PX+pi_PX)/Ds_pt)")   
    
mdf = mdf.Define("Ds0stM","sqrt((Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE)*(Kpl_PE+Kmi_PE+pi_PE+gamma_1_PE+gamma_2_PE) \
                             -(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX)*(Kpl_PX+Kmi_PX+pi_PX+gamma_1_PX+gamma_2_PX) \
                             -(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY)*(Kpl_PY+Kmi_PY+pi_PY+gamma_1_PY+gamma_2_PY) \
                             -(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)*(Kpl_PZ+Kmi_PZ+pi_PZ+gamma_1_PZ+gamma_2_PZ)) \
                             - pi0M + 134.9766")
                             
mdf = mdf.Define("R", "sqrt((Ds_eta - pi0_eta)*(Ds_eta - pi0_eta) + \
               (Ds_phi - pi0_phi)*(Ds_phi - pi0_phi))")
    
# selects the columns we want to work with                             
colsmc = ["Ds0stM", 
          "Ds_eta",
          "pi0_eta",
          "Ds_phi",
          "pi0_phi",
          "R",
          "pi_PT", 
          "Kpl_PT", 
          "Kmi_PT", 
          "gamma_1_CL", 
          "gamma_2_CL",
          'gamma_1_PT',
          'gamma_2_PT',
          'pi0M',
          'pi0_PT',
          'Kpl_PE',
          'Kmi_PE',
          'pi_PE',
          'Ds_MC_MOTHER_ID',
          'gamma_1_PE',
          'gamma_2_PE',
          'gamma_1_TRUEID',
          'gamma_2_TRUEID',
          'gamma_2_MC_MOTHER_ID',
          'gamma_1_MC_MOTHER_ID',
          'gamma_2_MC_GD_MOTHER_ID',
          'gamma_1_MC_GD_MOTHER_ID']                             
    
# transforms the MC data into a Pandas DataFrame
npym = mdf.AsNumpy(columns=colsmc)
mc_df = pd.DataFrame(npym) 

# defines the background and the MC peak signal
bkg_df = data_df.query("~(90 < pi0M < 170) &\
                       gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       pi0_PT > 900 ")
                       
data_df = data_df.query("gamma_1_CL > 0.7 &\
                       gamma_2_CL > 0.7 &\
                       90 < pi0M &\
                       pi0M < 170 &\
                       pi0_PT > 900 ")                       
                      
                       
mc_df = mc_df.query("abs(gamma_1_TRUEID) == 22 &\
                     abs(gamma_2_TRUEID) == 22 &\
                     abs(gamma_1_MC_MOTHER_ID) == 111 &\
                     abs(gamma_2_MC_MOTHER_ID) == 111 &\
                     abs(gamma_1_MC_GD_MOTHER_ID) == 10431 &\
                     abs(gamma_2_MC_GD_MOTHER_ID) == 10431 &\
                     abs(Ds_MC_MOTHER_ID) == 10431 &\
                     gamma_1_CL > 0.7 &\
                     gamma_2_CL > 0.7 &\
                     90 < pi0M &\
                     pi0M < 190 &\
                     pi0_PT > 900 ")

# selects the training columns
training_columns = ["gamma_1_PT",
                    "gamma_2_PT",
                    "gamma_1_CL",
                    "gamma_2_CL"]

# assigns 0 and 1 to background and signal events
bkg_df = bkg_df.copy()
bkg_df['category'] = 0  # Use 0 for background
mc_df['category'] = 1  # Use 1 for signal
training_data = pd.concat([bkg_df, mc_df], copy=True, ignore_index=True)

# defines the classifier and performs the training
bdt = AdaBoostClassifier()
bdt.fit(training_data[training_columns], training_data['category'])

# makes the predictions and adds the new column to the data
for idf in [mc_df, bkg_df, data_df, training_data]:
    idf['ADA'] = bdt.predict_proba(idf[training_columns])[:,1]
"""
df.Define('ADA', "data_df['ADA']").Snapshot('DecayTree', 'outData.root')
mdf.Define('ADA', "mc_df['ADA']").Snapshot('DecayTree', 'outMC.root')
"""

"""
df.Snapshot('DecayTree', 'outData.root')
mdf.Snapshot('DecayTree', 'outMC.root')
"""

import ROOT

cols.append('ADA')
data_df_np = {key: data_df[key].values for key in cols}
data_df_root = ROOT.RDF.MakeNumpyDataFrame(data_df_np)
data_df_root.Snapshot('DecayTree', 'outData.root')

# plots the BDT variable for background and MC
plt.figure()
plot_comparison('ADA', mc_df, bkg_df)
plt.savefig("comp.pdf")

# plots the ROC curve
plt.figure()
plot_roc(bdt, training_data, training_columns)
plt.savefig('roc.pdf')

# plots the significance curve
plt.figure()
plot_significance(bdt, training_data, training_columns)
plt.savefig('sign.pdf')

# Perform the cuts
cuts1 = data_df.query('ADA > 0.4')
cuts2 = data_df.query('ADA > 0.47')
cuts3 = data_df.query('ADA > 0.2')

plt.figure()
plot_mass(data_df, label='No BDT', density=1)
#plot_mass(cuts1, label='XGB > 0.05', density=1)
plot_mass(cuts2, label='ADA > 0.47', density=1)
#plot_mass(cuts3, label='XGB > 0.2', density=1)
plt.legend(loc='upper left')
plt.savefig('mass2.pdf')



