# -----------------------------------------------------------------------------------------------------------                                                 
# Compute cos theta and store it in a new tree with M                                                       |
# No cuts applied because RDataFrame are not used                                                           |
# -----------------------------------------------------------------------------------------------------------

from ROOT import * 
#gROOT.ProcessLine(".x src/variables.cxx++")
#gROOT.ProcessLine(".x src/MassProp2.cxx++")
#gROOT.ProcessLine(".x src/Pwave.cxx++")
#gROOT.ProcessLine(".x src/Swave.cxx++")
gInterpreter.AddIncludePath("src")
gSystem.Load("src/variables_cxx.so")
gSystem.Load("src/MassProp2_cxx.so")
gSystem.Load("src/Pwave_cxx.so")
gSystem.Load("src/Swave_cxx.so")
from array import array

#  Compute cos(theta)
gInterpreter.Declare(
"""  
#include "Math/Boost.h"
#include <Math/Vector4D.h>
using namespace ROOT;
double ComputeCosTheta( double px1 , double py1 , double pz1 , double m1 , double px2 , double py2 , double pz2 , double m2 ) 
{
    Math::PxPyPzMVector p1(px1 , py1 , pz1 , m1) ; 
    Math::PxPyPzMVector p2(px2 , py2 , pz2 , m2) ; 

    Math::Boost boost( p1.BoostToCM() ) ; 
    Math::XYZVector boostedp2 = (boost( p2 )).Vect().unit() ;
    Math::XYZVector boostedp1 = p1.Vect().unit();
    return (boostedp1.Dot(boostedp2));
} 
""")

# Input files
files = "/scratch41/belin/juliette/F0_pPb_tight_pp.root"

# Create Tchain to loop over the files
chainPP = TChain("F0TOPP/DecayTree")
chainMM = TChain("F0TOMM/DecayTree")
chainPM = TChain("F0TOOS/DecayTree")


# Create list of directories
import glob
for filename in [files]:
    filelist = glob.glob(filename)
#filelist.remove("/scratch41/belin/gangadir/workspace/samuel.belin/LocalXML/3/27/output/F0_pPb_tight.root")   
print(len(filelist))


# Add directories/trees to Tchain
for chainEl in filelist :
    chainPP.Add(chainEl)
    chainMM.Add(chainEl)
    chainPM.Add(chainEl)


# Create a new file and a new tree to store M and cos(theta)
outfile = TFile("fit2d_output.root","recreate")
treeLS = TTree('treeLS', 'LS tree')     
treePM = TTree('treePM', 'PM tree')     

# Define variables you'll need to fill the new trees
Mvar = array('d', [ 0. ])
costhetavar = array('d', [ 0. ])


# Create branches in the new tree linked to variables previously defined
treeLS.Branch('M', Mvar, 'M/D' )
treeLS.Branch('costheta', costhetavar, 'costheta/D' )
treePM.Branch('M', Mvar, 'M/D' )
treePM.Branch('costheta', costhetavar, 'costheta/D' )

# Get variables in the old trees
originalMvar = array('d', [ 0. ])
f0px = array('d', [ 0. ])
f0py = array('d', [ 0. ])
f0pz = array('d', [ 0. ])
f0m = array('d', [ 0. ])
pipx = array('d', [ 0. ])
pipy = array('d', [ 0. ])
pipz = array('d', [ 0. ])
pim = array('d', [ 0. ])


# Link the variables to the branches in the old tree
chainlist = [chainMM, chainPP, chainPM ]
for chain in chainlist :
    chain.SetBranchAddress('f_0_980_MM', originalMvar)
    chain.SetBranchAddress('f_0_980_M', f0m)
    chain.SetBranchAddress('f_0_980_PX', f0px)
    chain.SetBranchAddress('f_0_980_PY', f0py)
    chain.SetBranchAddress('f_0_980_PZ', f0pz)
    if chain == chainMM or chain == chainPM  :
        chain.SetBranchAddress('piminus_M', pim)
        chain.SetBranchAddress('piminus_PX', pipx)
        chain.SetBranchAddress('piminus_PY', pipy)
        chain.SetBranchAddress('piminus_PZ', pipz)
    if chain == chainPP or chain == chainPM :
        chain.SetBranchAddress('piplus_M', pim)
        chain.SetBranchAddress('piplus_PX', pipx)
        chain.SetBranchAddress('piplus_PY', pipy)
        chain.SetBranchAddress('piplus_PZ', pipz)
    
    # Fill the branches in the new tree
    n = chain.GetEntries()
    ntmp = 1000
    for i in range(ntmp):
        chain.GetEntry(i)
        if i%1000==0 :
            print (i)
        Mvar[0] = originalMvar[0]
        costhetavar[0] = ComputeCosTheta(f0px[0], f0py[0], f0pz[0], f0m[0], pipx[0], pipy[0], pipz[0], pim[0])
        if chain == chainPM :
            treePM.Fill()
        if chain == chainMM :
            treeLS.Fill()
        if chain == chainPP :
            treeLS.Fill()

print("entries=",treePM.GetEntries())
treeLS.Write()
treePM.Write()
outfile.Close()

# Create datasets and pdf
newfile = TFile("fit2d_output.root")
newtreeLS = newfile.Get("treeLS")
newtreePM = newfile.Get("treePM")
M = RooRealVar("M", "MM", 600, 1700.)
costheta = RooRealVar("costheta", "costheta", -0.95, 0.95)
dataset= RooDataSet("dataset", "data", newtreePM, RooArgSet(M, costheta))
bgdataset= RooDataSet("bgdataset", "data", newtreeLS, RooArgSet(M, costheta))

###################################### MASS ###############################################################

# Background LS
filehist = TFile("firsth_juliette_output.root")
hLS_MM = filehist.Get("hLS_MM")
massbackground = RooDataHist("massbackground","background mass",RooArgList(M), hLS_MM)
bgmasspdf  = RooHistPdf("bgmasspdf","background mass pdf",RooArgSet(M),massbackground)

# S wave and P wave from María's code
# central values for M resonances:
mRho    = RooRealVar("mRho","mRho",775.26)
mOm     = RooRealVar("mOm","mOm",782.65)
mf0500  = RooRealVar("mf0500","mf0500",475.)
mf0980  = RooRealVar("mf0980","mf0980",945.)
mf01370 = RooRealVar("mf01370","mf01370",1475.)
# gammas:
GRho  = RooRealVar("GRho","GRho",147.8)
GOm   = RooRealVar("GOm","GOm",8.49)
G500  = RooRealVar("G500","G500",337.)
G1370 = RooRealVar("G1370","G1370",113.)
# Flatte:
gpi    = RooRealVar("gpi","gpi",199.)
RgKgpi = RooRealVar("RgKgpi","RgKgpi",3.)
# Ranges:
rRho = RooRealVar("rRho","rRho",0.0053)
rOm  = RooRealVar("rOm","rOm",0.003)
r500 = RooRealVar("r500","r500",0.)
r1370 = RooRealVar("r1370","r1370",0.)
# Vector amplitudes:
ARho_Re = RooRealVar("ARho_Re","ARho_Re",1.) # overall norm
ARho_Im = RooRealVar("ARho_Im","ARho_Im",0.)
AOm_Re = RooRealVar("AOm_Re","AOm_Re",0.)
AOm_Im = RooRealVar("AOm_Im","AOm_Im",0.)
# Swave amplitudes
A500_Re  = RooRealVar("A500_Re","A500_Re",0.)
A500_Im  = RooRealVar("A500_Im","A500_Im",0.)
A980_Re  = RooRealVar("A980_Re","A980_Re",1.) # norm due to parameter fraction
A980_Im  = RooRealVar("A980_Im","A980_Im",0.)
A1370_Re = RooRealVar("A1370_Re","A1370_Re",0.)
A1370_Im = RooRealVar("A1370_Im","A1370_Im",0.)

myPwave = Pwave("myPwave","P wave (rho0)",mRho,GRho,rRho,mOm,GOm,rOm,ARho_Re,ARho_Im,AOm_Re,AOm_Im,M)
mySwave = Swave("mySwave","S wave (f0)",mf0500,G500,r500,mf01370,G1370,r1370,mf0980,gpi,RgKgpi,A500_Re,A500_Im,A1370_Re,A1370_Im,A980_Re,A980_Im,M)

print("Mass is done.")

############################################ cos(theta) ####################################################

# Background : polynomial fit
a0 = RooRealVar('a0', 'zero order coef', -2.38597e-3 , -5 , 5)
a1 = RooRealVar('a1', 'first order coef', 8.45797e-1 , -5 , 5)
a2 = RooRealVar('a2', 'second order coef', -1.83049e-2 , -5 , 5)
a3 = RooRealVar('a3', 'third order coef', -3.76535 , -5 , 5)
a4 = RooRealVar('a4', 'fourth order coef', 3.43676e-2 , -5 , 5)
a5 = RooRealVar('a5', 'fifth order coef', 1.75178 , -5 , 5)
bgcosthetapdf = RooPolynomial('bgcosthetapdf','background polynomial fit', costheta , RooArgList(a0,a1,a2,a3,a4,a5))
bgcosthetapdf.fitTo(bgdataset)
# Fix polynome parameters 
a0.setConstant(kTRUE)
a1.setConstant(kTRUE)
a2.setConstant(kTRUE)
a3.setConstant(kTRUE)
a4.setConstant(kTRUE)
a5.setConstant(kTRUE)

# S wave
f0costhetapdf = RooPolynomial('f0costhetapdf','f0 polynomial fit', costheta , RooArgList(a0,a1,a2,a3,a4,a5))

# P wave
rhocosthetapdf = RooGenericPdf( 'rhocosthetapdf', "@0*@0*@1" , RooArgSet(costheta,bgcosthetapdf))

print("Theta is done.")

##################################### Combine PDFs #####################################################

# Total 2D pdf : multiply pdfs mass * cos(theta) ... 
bgpdf = RooProdPdf('bgpdf', 'background : mass*cos(theta)', RooArgList(bgmasspdf, bgcosthetapdf))
f0pdf = RooProdPdf('f0pdf', 'f0 : mass*cos(theta)', RooArgList(mySwave, f0costhetapdf))
rho0pdf = RooProdPdf('rho0pdf', 'rho0 : mass*cos^2(theta)', RooArgList(myPwave, rhocosthetapdf))
# ... and add them 
#nbg = RooRealVar('nbg', 'coef background', 0.7*ntmp, 0., ntmp)
nswave = RooRealVar('nswave', 'coef f0' , 0.01 , 1.)
npwave = RooRealVar('npwave', 'coef rho0', 0.3, 0.01 , 1.)
pdftotal = RooAddPdf("pdftotal","pdftotal",RooArgList(f0pdf,rho0pdf,bgpdf),RooArgList(nswave,npwave))
# Fit to data from PM
print("Starting 2D fit...")
r = pdftotal.fitTo(dataset, RooFit.NumCPU(80,0))
print("2D pdf is done.")

################################### Plot everything ###################################################

c0 = TCanvas()
mframe =  M.frame()
dataset.plotOn(mframe, RooFit.Name("Data"))
pdftotal.plotOn(mframe, RooFit.LineColor(kBlack), RooFit.Name("PDFtotal"))
pdftotal.plotOn(mframe, RooFit.Components("bgpdf"),RooFit.LineColor(kBlue),RooFit.Name("Background"))
pdftotal.plotOn(mframe, RooFit.Components("f0pdf"),RooFit.LineColor(kRed+2),RooFit.Name("Swave"))
pdftotal.plotOn(mframe, RooFit.Components("rho0pdf"),RooFit.LineColor(kGreen+2),RooFit.Name("Pwave"))
pdftotal.paramOn(mframe)
mframe.Draw()
legend = TLegend()
legend.SetBorderSize(0)
legend.SetFillStyle(0)
legend.AddEntry(mframe.findObject("Data"),"Data","LEP")
legend.AddEntry(mframe.findObject("PDFtotal"),"PDF total","L")
legend.AddEntry(mframe.findObject("Pwave"),"P wave (rho_0)","L")
legend.AddEntry(mframe.findObject("Swave"),"S wave (f_0)","L")
legend.AddEntry(mframe.findObject("Background"),"Background","L")
legend.Draw("sames")

c1 = TCanvas()
thetaframe =  costheta.frame()
dataset.plotOn(thetaframe, RooFit.Name("Data"))
pdftotal.plotOn(thetaframe, RooFit.LineColor(kBlack), RooFit.Name("PDFtotal"))
pdftotal.plotOn(thetaframe, RooFit.Components("bgpdf"),RooFit.LineColor(kBlue),RooFit.Name("Background"))
pdftotal.plotOn(thetaframe, RooFit.Components("f0pdf"),RooFit.LineColor(kRed+2),RooFit.Name("Swave"))
pdftotal.plotOn(thetaframe, RooFit.Components("rho0pdf"),RooFit.LineColor(kGreen+2),RooFit.Name("Pwave"))
thetaframe.Draw()
legend1 = TLegend()
legend1.SetBorderSize(0)
legend1.SetFillStyle(0)
legend1.AddEntry(thetaframe.findObject("Data"),"Data","LEP")
legend1.AddEntry(thetaframe.findObject("PDFtotal"),"PDF total","L")
legend1.AddEntry(thetaframe.findObject("Pwave"),"P wave (rho_0)","L")
legend1.AddEntry(thetaframe.findObject("Swave"),"S wave (f_0)","L")
legend1.AddEntry(thetaframe.findObject("Background"),"Background","L")
legend1.Draw("sames")

c4 = TCanvas()
twodimhisttotal = pdftotal.createHistogram("M,costheta")
twodimhisttotal.Draw('surf')




















