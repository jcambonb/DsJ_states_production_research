#include <cstdlib>
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <iostream>
#include <fstream>

#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TStopwatch.h"
#include "TLorentzVector.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TMatrixD.h"
#include "./functions.C"
#include "./weight_PID.C"
#include "./weight_Track.C"

using namespace std;

double mpi = 139.57039;
double mpi0 = 134.9768;
double meta = 547.862;
double mDst = 2010.26;
//double mD0 = 1864.83;
double mD0 = 1864.84;
double mK = 493.677;
double mtau = 1776.86;
double mnu = 0.;
double mB0 = 5279.65;
double mBp = 5279.34;

double getRes(double beta, double rnd1, double rnd2, double rnd3) {

  double ret = 0.;

  if (rnd3<=beta) {
    ret = rnd1;
  } else {
    ret = rnd2;
  }

  return ret;

}

int getYear(int run) {

  int year = 0;

  if (run>=205957 && run<=219122) year = 2018;
  if (run>=191782 && run<=202897) year = 2017;
  if (run>=173624 && run<=185611) year = 2016;
  if (run>=162247 && run<=167136) year = 2015;
  if (run>=111183 && run<=133785) year = 2012;
  if (run>=87219 && run<=104414) year = 2011;

  return year;

}


double getP0Tau(double mass, TLorentzVector p3pi, TVector3 taudir) {

  double thetatau = taudir.Angle(p3pi.Vect());
  double ptau_0_calc = (p3pi.M()*p3pi.M()+mass*mass)*p3pi.P()*cos(thetatau)/2./(p3pi.E()*p3pi.E()-p3pi.P()*p3pi.P()*cos(thetatau)*cos(thetatau));

  return ptau_0_calc;
}

double getP0Taumax(double mass, TLorentzVector p3pi) {

  double xmass = 1.e-3*mass;
  TLorentzVector xp3pi = 1.e-3*p3pi;
  double argu = (xmass*xmass-xp3pi.M()*xp3pi.M())/2./xmass/xp3pi.P();
  double thetataumax = asin(argu);
  double ptau_max_calc = (xp3pi.M()*xp3pi.M()+xmass*xmass)*xp3pi.P()*cos(thetataumax)/2./(xp3pi.E()*xp3pi.E()-xp3pi.P()*xp3pi.P()*cos(thetataumax)*cos(thetataumax));

  if (abs(argu)>1.) {
    return -1.E+8;
  } else {
    return 1.e+3*ptau_max_calc;
  }
}

double getDeltaTau(double mass, TLorentzVector p3pi, TVector3 taudir) {

  double xmass = 1.e-3*mass;
  TLorentzVector xp3pi = 1.e-3*p3pi;
  double thetatau = taudir.Angle(xp3pi.Vect());
  double ptau_D_calc = 0.;

  double Delta2 = (xmass*xmass-xp3pi.M()*xp3pi.M())*(xmass*xmass-xp3pi.M()*xp3pi.M())-4.*xmass*xmass*xp3pi.P()*xp3pi.P()*sin(thetatau)*sin(thetatau);

  ptau_D_calc = -1.E+8;
  if (Delta2>0) {
    ptau_D_calc = 1.e+3*xp3pi.E()*sqrt(Delta2)/2./(xp3pi.E()*xp3pi.E()-xp3pi.P()*xp3pi.P()*cos(thetatau)*cos(thetatau));
  }

  return ptau_D_calc;
}

Double_t det(TVector3 X, TVector3 Y, TVector3 Z) {

    return X.X()*(Y.Y()*Z.Z()-Y.Z()*Z.Y()) - X.Y()*(Y.X()*Z.Z()-Y.Z()*Z.X()) + X.Z()*(Y.X()*Z.Y()-Y.Y()*Z.X());
}

TVector3 VProduct(TVector3 Vect1, TVector3 Vect2) {

    TVector3 Product;
    Product[0] = Vect1.Y()*Vect2.Z() - Vect1.Z()*Vect2.Y();
    Product[1] = -Vect1.X()*Vect2.Z() + Vect1.Z()*Vect2.X();
    Product[2] = Vect1.X()*Vect2.Y() - Vect1.Y()*Vect2.X();

    return TVector3(Product[0],Product[1],Product[2]);
}


Int_t main() {

   TRandom3 rnr;
   rnr.SetSeed(1);

   // Get the old tree:
   std::cout << "Starting...." <<  std::endl;

   bool WS = false;
   bool NonPhys = false;
   bool MonteCarlo = true;

   int YearMC = 2016;

   TFile *input;
   TTree *tree;
   TTree *tree_out;

   TFile *f_out;

   if (WS) {
     //f_out  = new TFile("out.root","UPDATE");
     f_out  = new TFile("outWS.root","RECREATE");
   } else if (NonPhys) {
     //f_out  = new TFile("out.root","UPDATE");
     f_out  = new TFile("outNonPhys.root","RECREATE");
   } else {
     f_out  = new TFile("out.root","RECREATE");
   }

   string line;
   TString tline;
   ifstream myfile ("files_list");

   if (myfile.is_open()) {
     getline (myfile,line);
     tline = line;
     input = new TFile(tline); 
   }

   if (WS) {   
     //tree = (TTree*) input->Get("Bd2DsttaunuWSTuple/DecayTree");
     tree = (TTree*) input->Get("Bd2DsttaunuTupleWS/DecayTree");
     //tree = (TTree*) input->Get("DecayTreeWS");
   } else if (NonPhys) {   
     //tree = (TTree*) input->Get("Bd2DsttaunuNonPhysTuple/DecayTree");
     tree = (TTree*) input->Get("Bd2DsttaunuTupleNonPhys/DecayTree");
     //tree = (TTree*) input->Get("DecayTreeNonPhys");
   } else {   
     tree = (TTree*) input->Get("Bd2DsttaunuTuple/DecayTree");
//     tree = (TTree*) input->Get("DecayTree");
     //tree = (TTree*) input->Get("DecayTreeWS");
   }

   std::cout<<"Nentries "<<tree->GetEntries()<<std::endl;
   
   // Variables that will be used:

   double B_M, tau_M, tau_PT;
   int tau_ID;

   double tau_ENDVERTEX_Z, tau_OWNPV_Z, tau_ENDVERTEX_ZERR, tau_OWNPV_ZERR;
   double tau_OWNPV_X, tau_OWNPV_Y;
   ULong64_t totCandidates;
   UInt_t nCandidate;
   double tau_ENDVERTEX_CHI2;
   double B_ENDVERTEX_CHI2;
   double tau_ENDVERTEX_X, tau_ENDVERTEX_Y;
   double B_ENDVERTEX_X, B_ENDVERTEX_Y, B_ENDVERTEX_Z;
   double B_ENDVERTEX_ZERR;
   double B_TRUEENDVERTEX_X, B_TRUEENDVERTEX_Y, B_TRUEENDVERTEX_Z;
   double tau_TRUEENDVERTEX_X, tau_TRUEENDVERTEX_Y, tau_TRUEENDVERTEX_Z;
   double D_ENDVERTEX_X, D_ENDVERTEX_Y, D_ENDVERTEX_Z;
   double B_OWNPV_X, B_OWNPV_Y, B_OWNPV_Z;

   double tau_pion0_ProbNNpi, tau_pion1_ProbNNpi, tau_pion2_ProbNNpi;
   double tau_pion0_ProbNNk, tau_pion1_ProbNNk, tau_pion2_ProbNNk;
   double tau_pion0_PIDK, tau_pion1_PIDK, tau_pion2_PIDK;
   //double tau_pion0_RichAbovePiThres, tau_pion1_RichAbovePiThres, tau_pion2_RichAbovePiThres;

   double D_IPCHI2_OWNPV;
   double D_OWNPV_Z;

   double tau_pion0_IPCHI2_OWNPV, tau_pion1_IPCHI2_OWNPV, tau_pion2_IPCHI2_OWNPV;
   double tau_pion0_PX, tau_pion0_PY, tau_pion0_PZ, tau_pion0_P;
   double tau_pion1_PX, tau_pion1_PY, tau_pion1_PZ, tau_pion1_P;
   double tau_pion2_PX, tau_pion2_PY, tau_pion2_PZ, tau_pion2_P;

   double tau_040_nc_PX, tau_040_nc_PY, tau_040_nc_PZ;
   double tau_040_nc_vPT;
   ULong64_t eventNumber;
   UInt_t runNumber;
   double B_PX, B_PY, B_PZ, B_PE, B_P;
   double D_PX, D_PY, D_PZ, D_PE, D_P, D_M;
   double tau_PX, tau_PY, tau_PZ, tau_P;
   double tau_TRUEP_X, tau_TRUEP_Y, tau_TRUEP_Z, tau_TRUEP_E;
   double B_TRUEP_X, B_TRUEP_Y, B_TRUEP_Z, B_TRUEP_E;
   double tau_pion0_TRUEP_X, tau_pion0_TRUEP_Y, tau_pion0_TRUEP_Z, tau_pion0_TRUEP_E;
   double tau_pion1_TRUEP_X, tau_pion1_TRUEP_Y, tau_pion1_TRUEP_Z, tau_pion1_TRUEP_E;
   double tau_pion2_TRUEP_X, tau_pion2_TRUEP_Y, tau_pion2_TRUEP_Z, tau_pion2_TRUEP_E;
   double D_TRUEP_X, D_TRUEP_Y, D_TRUEP_Z, D_TRUEP_E;
   double D_K_ProbNNk, D_pi_ProbNNpi;

   int tau_isoPlus_nGood;
   float tau_isoPlus_trk_LOKI_IPChi2[100];
   float tau_isoPlus_trk_LOKI_IPChi2MOTHER[100];
   float tau_isoPlus_trk_LOKI_IPChi2PV[100];
   float tau_isoPlus_trk_LOKI_DOCAVX[100];
   float tau_isoPlus_trk_LOKI_DOCAVXMOTHER[100];
   float tau_isoPlus_trk_LOKI_TYPE[100];
   float tau_isoPlus_newP_LOKI_MM[100];
   float tau_isoPlus_trk_LOKI_PX[100], tau_isoPlus_trk_LOKI_PY[100], tau_isoPlus_trk_LOKI_PZ[100];
   float tau_isoPlus_trk_LOKI_ID[100];
   float tau_isoPlus_trk_LOKI_P[100];
   float tau_isoPlus_trk_LOKI_PIDK[100];

   int D_isoPlus_nGood;
   float D_isoPlus_trk_LOKI_IPChi2[100];
   float D_isoPlus_trk_LOKI_IPChi2PV[100];
   float D_isoPlus_trk_LOKI_DOCAVX[100];
   float D_isoPlus_newP_LOKI_MM[100];
   float D_isoPlus_trk_LOKI_PX[100], D_isoPlus_trk_LOKI_PY[100], D_isoPlus_trk_LOKI_PZ[100];
   float D_isoPlus_trk_LOKI_ID[100];
   float D_isoPlus_trk_LOKI_P[100];

   int B_isoPlus_nGood;
   float B_isoPlus_trk_LOKI_IPChi2[100];
   float B_isoPlus_trk_LOKI_IPChi2PV[100];
   float B_isoPlus_trk_LOKI_DOCAVX[100];
   float B_isoPlus_newP_LOKI_MM[100];
   float B_isoPlus_trk_LOKI_PIDK[100];
   float B_isoPlus_trk_LOKI_ID[100];
   float B_isoPlus_trk_LOKI_P[100], B_isoPlus_trk_LOKI_PX[100], B_isoPlus_trk_LOKI_PY[100], B_isoPlus_trk_LOKI_PZ[100];

   double D_K_PX, D_K_PY, D_K_PZ, D_K_P;
   double D_pi_PX, D_pi_PY, D_pi_PZ, D_pi_P;
   int B_Added_D1_ID, B_Added_D2_ID, B_Added_ID;
   Float_t B_Added_D1_daugID[100], B_Added_D2_daugID[100], B_Added_daugID[100];
   Float_t B_Added_daug_TRUEP_X[100], B_Added_daug_TRUEP_Y[100], B_Added_daug_TRUEP_Z[100], B_Added_daug_TRUEP_E[100];
   Float_t B_Added_tau_daugID[100];
   int B_Added_D1_ndaug, B_Added_D2_ndaug, B_Added_B_ndaug;
   int B_Added_tau_ndaug;

   double D_MC_MOTHER_TRUEP_E, D_MC_MOTHER_TRUEP_X, D_MC_MOTHER_TRUEP_Y, D_MC_MOTHER_TRUEP_Z;
   int D_MC_MOTHER_ID, D_TRUEID;
   int D_K_TRUEID, D_pi_TRUEID;
   int D_K_MC_MOTHER_ID, D_pi_MC_MOTHER_ID;
   int D_K_MC_GD_MOTHER_ID, D_pi_MC_GD_MOTHER_ID;
   int D_K_MC_GD_GD_MOTHER_ID, D_pi_MC_GD_GD_MOTHER_ID;
   double D_K_MC_MOTHER_TRUEP_X, D_K_MC_MOTHER_TRUEP_Y, D_K_MC_MOTHER_TRUEP_Z, D_K_MC_MOTHER_TRUEP_E;
   double D_pi_MC_MOTHER_TRUEP_X, D_pi_MC_MOTHER_TRUEP_Y, D_pi_MC_MOTHER_TRUEP_Z, D_pi_MC_MOTHER_TRUEP_E;
   int tau_TRUEID;
   int tau_MC_MOTHER_ID, tau_pion0_MC_MOTHER_ID, tau_pion1_MC_MOTHER_ID, tau_pion2_MC_MOTHER_ID;
   int tau_MC_GD_MOTHER_ID, tau_pion0_MC_GD_MOTHER_ID, tau_pion1_MC_GD_MOTHER_ID, tau_pion2_MC_GD_MOTHER_ID;
   int tau_MC_GD_GD_MOTHER_ID, tau_pion0_MC_GD_GD_MOTHER_ID, tau_pion1_MC_GD_GD_MOTHER_ID, tau_pion2_MC_GD_GD_MOTHER_ID;
   double tau_MC_MOTHER_TRUEP_X, tau_MC_MOTHER_TRUEP_Y, tau_MC_MOTHER_TRUEP_Z, tau_MC_MOTHER_TRUEP_E;
   double tau_MC_GD_MOTHER_TRUEP_X, tau_MC_GD_MOTHER_TRUEP_Y, tau_MC_GD_MOTHER_TRUEP_Z, tau_MC_GD_MOTHER_TRUEP_E;
   double tau_MC_GD_GD_MOTHER_TRUEP_X, tau_MC_GD_GD_MOTHER_TRUEP_Y, tau_MC_GD_GD_MOTHER_TRUEP_Z, tau_MC_GD_GD_MOTHER_TRUEP_E;
   double tau_pion0_MC_MOTHER_TRUEP_X, tau_pion0_MC_MOTHER_TRUEP_Y, tau_pion0_MC_MOTHER_TRUEP_Z, tau_pion0_MC_MOTHER_TRUEP_E;
   double tau_pion1_MC_MOTHER_TRUEP_X, tau_pion1_MC_MOTHER_TRUEP_Y, tau_pion1_MC_MOTHER_TRUEP_Z, tau_pion1_MC_MOTHER_TRUEP_E;
   double tau_pion2_MC_MOTHER_TRUEP_X, tau_pion2_MC_MOTHER_TRUEP_Y, tau_pion2_MC_MOTHER_TRUEP_Z, tau_pion2_MC_MOTHER_TRUEP_E;
   double tau_pion0_MC_GD_MOTHER_TRUEP_X, tau_pion0_MC_GD_MOTHER_TRUEP_Y, tau_pion0_MC_GD_MOTHER_TRUEP_Z, tau_pion0_MC_GD_MOTHER_TRUEP_E;
   double tau_pion1_MC_GD_MOTHER_TRUEP_X, tau_pion1_MC_GD_MOTHER_TRUEP_Y, tau_pion1_MC_GD_MOTHER_TRUEP_Z, tau_pion1_MC_GD_MOTHER_TRUEP_E;
   double tau_pion2_MC_GD_MOTHER_TRUEP_X, tau_pion2_MC_GD_MOTHER_TRUEP_Y, tau_pion2_MC_GD_MOTHER_TRUEP_Z, tau_pion2_MC_GD_MOTHER_TRUEP_E;
   double tau_pion0_MC_GD_GD_MOTHER_TRUEP_X, tau_pion0_MC_GD_GD_MOTHER_TRUEP_Y, tau_pion0_MC_GD_GD_MOTHER_TRUEP_Z, tau_pion0_MC_GD_GD_MOTHER_TRUEP_E;
   double tau_pion1_MC_GD_GD_MOTHER_TRUEP_X, tau_pion1_MC_GD_GD_MOTHER_TRUEP_Y, tau_pion1_MC_GD_GD_MOTHER_TRUEP_Z, tau_pion1_MC_GD_GD_MOTHER_TRUEP_E;
   double tau_pion2_MC_GD_GD_MOTHER_TRUEP_X, tau_pion2_MC_GD_GD_MOTHER_TRUEP_Y, tau_pion2_MC_GD_GD_MOTHER_TRUEP_Z, tau_pion2_MC_GD_GD_MOTHER_TRUEP_E;
   int nSPDHits, nTracks;
   Short_t Polarity;


   int D_ID;

   // Selecting the variables
   
   tree->SetBranchStatus("*",0);

   tree->SetBranchStatus("Polarity",1);
   tree->SetBranchStatus("eventNumber",1);
   tree->SetBranchStatus("runNumber",1);
   tree->SetBranchStatus("totCandidates",1);
   tree->SetBranchStatus("nCandidate",1);
   tree->SetBranchStatus("nSPDHits",1);
   tree->SetBranchStatus("nTracks",1);
   tree->SetBranchStatus("nLongTracks",1);
   tree->SetBranchStatus("B_OWNPV_NDOF",1);

   tree->SetBranchStatus("*BDT*",1);

   tree->SetBranchStatus("B_L0HadronDecision_TOS",1);
   tree->SetBranchStatus("B_L0HadronDecision_TIS",1);
   tree->SetBranchStatus("B_L0Global_TIS",1);
   tree->SetBranchStatus("D0_L0HadronDecision_TOS",1);
   tree->SetBranchStatus("D0_L0HadronDecision_TIS",1);
   tree->SetBranchStatus("B_Hlt1TrackMVADecision_TOS",1);
   tree->SetBranchStatus("B_Hlt1TwoTrackMVADecision_TOS",1);
   tree->SetBranchStatus("D0_Hlt2Topo2BodyDecision_TOS",1);
   tree->SetBranchStatus("D0_Hlt2CharmHadD02KmPipTurboDecision_TOS",1);
   tree->SetBranchStatus("B_Hlt2Topo2BodyDecision_TOS",1);
   tree->SetBranchStatus("B_Hlt2Topo3BodyDecision_TOS",1);
   tree->SetBranchStatus("B_Hlt2Topo4BodyDecision_TOS",1);
   tree->SetBranchStatus("tau_Hlt2Topo2BodyDecision_TOS",1);
   tree->SetBranchStatus("tau_Hlt2Topo3BodyDecision_TOS",1);
   tree->SetBranchStatus("B_Hlt2Xc3PiXForTauD02Kpi*",1);
   tree->SetBranchStatus("B_Hlt2Global_TIS",1);
   tree->SetBranchStatus("B_Hlt2Phys_TIS",1);
   //

   tree->SetBranchStatus("B_DIRA_OWNPV",1);
   tree->SetBranchStatus("D0_DIRA_OWNPV",1);
   tree->SetBranchStatus("tau_DIRA_OWNPV",1);
   tree->SetBranchStatus("D0_pi_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("D0_K_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("D0_FDCHI2_ORIVX",1);
   tree->SetBranchStatus("D0_FDCHI2_OWNPV",1);
   tree->SetBranchStatus("B_FDCHI2_OWNPV",1);
   tree->SetBranchStatus("D0_FD_ORIVX",1);
   tree->SetBranchStatus("D0_FD_OWNPV",1);
   tree->SetBranchStatus("B_FD_OWNPV",1);
   tree->SetBranchStatus("D0_K_ProbNNk",1);
   tree->SetBranchStatus("D0_K_ProbNNpi",1);
   tree->SetBranchStatus("D0_K_ProbNNmu",1);
   tree->SetBranchStatus("D0_K_ProbNNe",1);
   tree->SetBranchStatus("D0_K_ProbNNghost",1);
   tree->SetBranchStatus("D0_K_isMuon",1);
   tree->SetBranchStatus("D0_pi_ProbNNk",1);
   tree->SetBranchStatus("D0_pi_ProbNNpi",1);
   tree->SetBranchStatus("D0_pi_ProbNNmu",1);
   tree->SetBranchStatus("D0_pi_ProbNNe",1);
   tree->SetBranchStatus("D0_pi_ProbNNghost",1);
   tree->SetBranchStatus("D0_pi_isMuon",1);
   tree->SetBranchStatus("D0_pi_PIDK",1);
   tree->SetBranchStatus("D0_K_PIDK",1);
   tree->SetBranchStatus("tau_ID",1);
   tree->SetBranchStatus("D0_ID",1);
   tree->SetBranchStatus("B_ENDVERTEX_ZERR",1);
   tree->SetBranchStatus("tau_ENDVERTEX_ZERR",1);
   tree->SetBranchStatus("B_ENDVERTEX_XERR",1);
   tree->SetBranchStatus("tau_ENDVERTEX_XERR",1);
   tree->SetBranchStatus("B_ENDVERTEX_YERR",1);
   tree->SetBranchStatus("tau_ENDVERTEX_YERR",1);
   tree->SetBranchStatus("tau_OWNPV_X",1);
   tree->SetBranchStatus("tau_OWNPV_Y",1);
   tree->SetBranchStatus("tau_OWNPV_Z",1);


   tree->SetBranchStatus("D0_M",1);
   tree->SetBranchStatus("D0_K_P",1);
   tree->SetBranchStatus("D0_K_PX",1);
   tree->SetBranchStatus("D0_K_PY",1);
   tree->SetBranchStatus("D0_K_PZ",1);
   tree->SetBranchStatus("D0_pi_P",1);
   tree->SetBranchStatus("D0_pi_PX",1);
   tree->SetBranchStatus("D0_pi_PY",1);
   tree->SetBranchStatus("D0_pi_PZ",1);

   tree->SetBranchStatus("*pi*_PZ",1);
   tree->SetBranchStatus("*_3pi_*",1);
   tree->SetBranchStatus("*_01_*",1);
   tree->SetBranchStatus("*_02_*",1);
   tree->SetBranchStatus("*_12_*",1);
   tree->SetBranchStatus("*_pi0_DOCA12",1);
   tree->SetBranchStatus("*_pi1_DOCA02",1);
   tree->SetBranchStatus("*_pi2_DOCA01",1);
   tree->SetBranchStatus("*_pi*_DOCAMOTHER",1);


   tree->SetBranchStatus("D0_K_RichAbovePiThres",1);
   tree->SetBranchStatus("D0_pi_RichAbovePiThres",1);
   tree->SetBranchStatus("D0_K_RichAboveKaThres",1);
   tree->SetBranchStatus("D0_pi_RichAboveKaThres",1);

   tree->SetBranchStatus("D0_ENDVERTEX_X",1);
   tree->SetBranchStatus("D0_ENDVERTEX_Y",1);
   tree->SetBranchStatus("D0_ENDVERTEX_Z",1);
   tree->SetBranchStatus("D0_ENDVERTEX_ZERR",1);
   tree->SetBranchStatus("D0_ENDVERTEX_CHI2",1);

   tree->SetBranchStatus("B_M",1);
   tree->SetBranchStatus("tau_M",1);
   tree->SetBranchStatus("tau_PT",1);
   tree->SetBranchStatus("tau_PX",1);
   tree->SetBranchStatus("tau_PY",1);
   tree->SetBranchStatus("tau_PZ",1);
   tree->SetBranchStatus("tau_P",1);

   tree->SetBranchStatus("tau_pion0_PX",1);
   tree->SetBranchStatus("tau_pion0_PY",1);
   tree->SetBranchStatus("tau_pion0_PZ",1);
   tree->SetBranchStatus("tau_pion0_P",1);
   tree->SetBranchStatus("tau_pion1_PX",1);
   tree->SetBranchStatus("tau_pion1_PY",1);
   tree->SetBranchStatus("tau_pion1_PZ",1);
   tree->SetBranchStatus("tau_pion1_P",1);
   tree->SetBranchStatus("tau_pion2_PX",1);
   tree->SetBranchStatus("tau_pion2_PY",1);
   tree->SetBranchStatus("tau_pion2_PZ",1);
   tree->SetBranchStatus("tau_pion2_P",1);

   tree->SetBranchStatus("B_ENDVERTEX_X",1);
   tree->SetBranchStatus("B_ENDVERTEX_Y",1);
   tree->SetBranchStatus("B_ENDVERTEX_Z",1);
   tree->SetBranchStatus("B_OWNPV_X",1);
   tree->SetBranchStatus("B_OWNPV_Y",1);
   tree->SetBranchStatus("B_OWNPV_Z",1);

   tree->SetBranchStatus("D0_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("D0_OWNPV_Z",1);
   //tree->SetBranchStatus("B_DOCA",1);

   tree->SetBranchStatus("tau_ENDVERTEX_Z",1);
   tree->SetBranchStatus("tau_ENDVERTEX_ZERR",1);
   tree->SetBranchStatus("tau_OWNPV_ZERR",1);

   tree->SetBranchStatus("tau_ENDVERTEX_CHI2",1);
   tree->SetBranchStatus("B_ENDVERTEX_CHI2",1);

   tree->SetBranchStatus("tau_ENDVERTEX_X",1);
   tree->SetBranchStatus("tau_ENDVERTEX_Y",1);

   tree->SetBranchStatus("tau_pion0_ProbNNpi",1);
   tree->SetBranchStatus("tau_pion1_ProbNNpi",1);
   tree->SetBranchStatus("tau_pion2_ProbNNpi",1);
   tree->SetBranchStatus("tau_pion0_ProbNNk",1);
   tree->SetBranchStatus("tau_pion1_ProbNNk",1);
   tree->SetBranchStatus("tau_pion2_ProbNNk",1);
   tree->SetBranchStatus("tau_pion0_ProbNNmu",1);
   tree->SetBranchStatus("tau_pion1_ProbNNmu",1);
   tree->SetBranchStatus("tau_pion2_ProbNNmu",1);
   tree->SetBranchStatus("tau_pion0_ProbNNe",1);
   tree->SetBranchStatus("tau_pion1_ProbNNe",1);
   tree->SetBranchStatus("tau_pion2_ProbNNe",1);
   tree->SetBranchStatus("tau_pion0_ProbNNghost",1);
   tree->SetBranchStatus("tau_pion1_ProbNNghost",1);
   tree->SetBranchStatus("tau_pion2_ProbNNghost",1);
   tree->SetBranchStatus("tau_pion0_isMuon",1);
   tree->SetBranchStatus("tau_pion1_isMuon",1);
   tree->SetBranchStatus("tau_pion2_isMuon",1);
   tree->SetBranchStatus("tau_pion0_PIDK",1);
   tree->SetBranchStatus("tau_pion1_PIDK",1);
   tree->SetBranchStatus("tau_pion2_PIDK",1);
   tree->SetBranchStatus("tau_pion0_RichAbovePiThres",1);
   tree->SetBranchStatus("tau_pion1_RichAbovePiThres",1);
   tree->SetBranchStatus("tau_pion2_RichAbovePiThres",1);
   tree->SetBranchStatus("tau_pion0_RichAboveKaThres",1);
   tree->SetBranchStatus("tau_pion1_RichAboveKaThres",1);
   tree->SetBranchStatus("tau_pion2_RichAboveKaThres",1);

   tree->SetBranchStatus("tau_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("tau_pion0_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("tau_pion1_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("tau_pion2_IPCHI2_OWNPV",1);
   tree->SetBranchStatus("tau_FDCHI2_ORIVX",1);

   tree->SetBranchStatus("B_PX",1);
   tree->SetBranchStatus("B_PY",1);
   tree->SetBranchStatus("B_PZ",1);
   tree->SetBranchStatus("B_P",1);
   tree->SetBranchStatus("B_PE",1);
   tree->SetBranchStatus("D0_PX",1);
   tree->SetBranchStatus("D0_PY",1);
   tree->SetBranchStatus("D0_PZ",1);
   tree->SetBranchStatus("D0_PE",1);
   tree->SetBranchStatus("D0_P",1);

   tree->SetBranchStatus("tau_EW_0.00*_nc_*",1);
   tree->SetBranchStatus("tau_EW_0.00*_pi0_*",1);
   tree->SetBranchStatus("tau_EW_0.00*_eta_*",1);
   tree->SetBranchStatus("tau_EW_0.30*_nc_*",1);
   tree->SetBranchStatus("tau_EW_0.30*_pi0_*",1);
   tree->SetBranchStatus("tau_EW_0.30*_eta_*",1);
   tree->SetBranchStatus("tau_EW_0.40*_nc_*",1);
   tree->SetBranchStatus("tau_EW_0.40*_pi0_*",1);
   tree->SetBranchStatus("tau_EW_0.40*_eta_*",1);
   tree->SetBranchStatus("tau_EW_0.50*_nc_*",1);
   tree->SetBranchStatus("tau_EW_0.50*_pi0_*",1);
   tree->SetBranchStatus("tau_EW_0.50*_eta_*",1);
   tree->SetBranchStatus("D0_EW_0.00*_nc_*",1);
   tree->SetBranchStatus("D0_EW_0.00*_pi0_*",1);
   tree->SetBranchStatus("D0_EW_0.10*_nc_*",1);
   tree->SetBranchStatus("D0_EW_0.10*_pi0_*",1);
   tree->SetBranchStatus("D0_EW_0.20*_nc_*",1);
   tree->SetBranchStatus("D0_EW_0.20*_pi0_*",1);
   tree->SetBranchStatus("D0_EW_0.30*_nc_*",1);
   tree->SetBranchStatus("D0_EW_0.30*_pi0_*",1);
   tree->SetBranchStatus("tau_IsoKS*",1);

   // Iso

   //tree->SetBranchStatus("*_isoVelo*",1);
   tree->SetBranchStatus("D0_isoPlus*",1);
   tree->SetBranchStatus("tau_isoPlus*",1);
   tree->SetBranchStatus("B_isoPlus*",1);

   tree->SetBranchStatus("D0_isoPlus_trk_ProbNN*",0);
   tree->SetBranchStatus("B_isoPlus_trk_LOKI_IPChi2MOTHER",0);
   tree->SetBranchStatus("*_isoPlus_trk_hasMuon",0);
   //tree->SetBranchStatus("*_isoPlus_trk_LOKI_P",0); // Needed for charged isolation

   //tree->SetBranchStatus("*photon*",1);

   tree->SetBranchStatus("nPVs",1);
   //tree->SetBranchStatus("nPV",1);
   tree->SetBranchStatus("PVX",1);
   tree->SetBranchStatus("PVY",1);
   tree->SetBranchStatus("PVZ",1);
   //tree->SetBranchStatus("PVXERR",1);
   //tree->SetBranchStatus("PVYERR",1);
   //tree->SetBranchStatus("PVZERR",1);
   //tree->SetBranchStatus("PVCHI2",1);
   //tree->SetBranchStatus("PVNTRACKS",1);

   tree->SetBranchStatus("*DOCA*",1);

   tree->SetBranchStatus("*isoPlus_newP*",0);
   tree->SetBranchStatus("*maxPt_Deta",0);
   tree->SetBranchStatus("*maxPt_Dphi",0);
   tree->SetBranchStatus("*maxPt_Dtheta",0);
   tree->SetBranchStatus("*g1_PX",0);
   tree->SetBranchStatus("*g1_PY",0);
   tree->SetBranchStatus("*g1_PZ",0);
   tree->SetBranchStatus("*g2_PX",0);
   tree->SetBranchStatus("*g2_PY",0);
   tree->SetBranchStatus("*g2_PZ",0);
   //tree->SetBranchStatus("*secPt_g1_CL",0);
   //tree->SetBranchStatus("*secPt_g2_CL",0);
   //tree->SetBranchStatus("*thiPt_g1_CL",0);
   //tree->SetBranchStatus("*thiPt_g2_CL",0);

   if (MonteCarlo) {

     tree->SetBranchStatus("*TRUEORIGINVERTEX*",1);
     tree->SetBranchStatus("*Added_ID",1);
     tree->SetBranchStatus("*Added_B_ndaug",1);
     tree->SetBranchStatus("*Added_daugID",1);
     tree->SetBranchStatus("B_Added_daug_TRUEP_*",1);
     tree->SetBranchStatus("B_Added_D1_ndaug",1);
     tree->SetBranchStatus("B_Added_D2_ndaug",1);
     tree->SetBranchStatus("B_Added_tau_ndaug",1);
     tree->SetBranchStatus("*Added_D1_ID",1);
     tree->SetBranchStatus("*Added_D2_ID",1);
     tree->SetBranchStatus("*Added_D1_daugID",1);
     tree->SetBranchStatus("*Added_D2_daugID",1);
     tree->SetBranchStatus("*Added_tau_daugID",1);
     tree->SetBranchStatus("*Added_n_pi",1);
     tree->SetBranchStatus("*Added_n_K",1);

     tree->SetBranchStatus("tau_TRUETAU",1);
     tree->SetBranchStatus("B_TRUEENDVERTEX_X",1);
     tree->SetBranchStatus("B_TRUEENDVERTEX_Y",1);
     tree->SetBranchStatus("B_TRUEENDVERTEX_Z",1);
     tree->SetBranchStatus("tau_TRUEENDVERTEX_X",1);
     tree->SetBranchStatus("tau_TRUEENDVERTEX_Y",1);
     tree->SetBranchStatus("tau_TRUEENDVERTEX_Z",1);
     tree->SetBranchStatus("D0_TRUEENDVERTEX_X",1);
     tree->SetBranchStatus("D0_TRUEENDVERTEX_Y",1);
     tree->SetBranchStatus("D0_TRUEENDVERTEX_Z",1);
     //tree->SetBranchStatus("*_MOTHER_ID",1);
     tree->SetBranchStatus("*_MOTHER_*",1);
     tree->SetBranchStatus("*TRUEID*",1);
     tree->SetBranchStatus("D0_TRUEP_X",1);
     tree->SetBranchStatus("D0_TRUEP_Y",1);
     tree->SetBranchStatus("D0_TRUEP_Z",1);
     tree->SetBranchStatus("D0_TRUEP_E",1);
     tree->SetBranchStatus("B_TRUEP_X",1);
     tree->SetBranchStatus("B_TRUEP_Y",1);
     tree->SetBranchStatus("B_TRUEP_Z",1);
     tree->SetBranchStatus("B_TRUEP_E",1);
     tree->SetBranchStatus("tau_pion0_TRUEP_X",1);
     tree->SetBranchStatus("tau_pion0_TRUEP_Y",1);
     tree->SetBranchStatus("tau_pion0_TRUEP_Z",1);
     tree->SetBranchStatus("tau_pion0_TRUEP_E",1);
     tree->SetBranchStatus("tau_pion1_TRUEP_X",1);
     tree->SetBranchStatus("tau_pion1_TRUEP_Y",1);
     tree->SetBranchStatus("tau_pion1_TRUEP_Z",1);
     tree->SetBranchStatus("tau_pion1_TRUEP_E",1);
     tree->SetBranchStatus("tau_pion2_TRUEP_X",1);
     tree->SetBranchStatus("tau_pion2_TRUEP_Y",1);
     tree->SetBranchStatus("tau_pion2_TRUEP_Z",1);
     tree->SetBranchStatus("tau_pion2_TRUEP_E",1);
     tree->SetBranchStatus("tau_TRUEP_X",1);
     tree->SetBranchStatus("tau_TRUEP_Y",1);
     tree->SetBranchStatus("tau_TRUEP_Z",1);
     tree->SetBranchStatus("tau_TRUEP_E",1);
   }

   //tree->SetBranchStatus("*",1);

   //
   //

   // Get variables address

   tree->SetBranchAddress("Polarity", &Polarity);
   tree->SetBranchAddress("nSPDHits", &nSPDHits);
   tree->SetBranchAddress("nTracks", &nTracks);

   tree->SetBranchAddress("D0_pi_P", &D_pi_P);
   tree->SetBranchAddress("D0_K_P", &D_K_P);
   tree->SetBranchAddress("D0_ID", &D_ID); 
   tree->SetBranchAddress("D0_K_ProbNNk", &D_K_ProbNNk); 
   tree->SetBranchAddress("D0_pi_ProbNNpi", &D_pi_ProbNNpi); 

   tree->SetBranchAddress("D0_K_PX", &D_K_PX);
   tree->SetBranchAddress("D0_K_PY", &D_K_PY);
   tree->SetBranchAddress("D0_K_PZ", &D_K_PZ);
   tree->SetBranchAddress("D0_pi_PX", &D_pi_PX);
   tree->SetBranchAddress("D0_pi_PY", &D_pi_PY);
   tree->SetBranchAddress("D0_pi_PZ", &D_pi_PZ);

   tree->SetBranchAddress("eventNumber", &eventNumber);
   tree->SetBranchAddress("runNumber", &runNumber);

   tree->SetBranchAddress("B_M", &B_M);
   tree->SetBranchAddress("tau_M", &tau_M);
   tree->SetBranchAddress("tau_PT", &tau_PT);
   tree->SetBranchAddress("tau_ID", &tau_ID);

   tree->SetBranchAddress("totCandidates", &totCandidates);
//   TBranch* br_totCandidates = tree->GetBranch("totCandidates");
   tree->SetBranchAddress("nCandidate", &nCandidate);

   tree->SetBranchAddress("tau_pion0_PX", &tau_pion0_PX);
   tree->SetBranchAddress("tau_pion0_PY", &tau_pion0_PY);
   tree->SetBranchAddress("tau_pion0_PZ", &tau_pion0_PZ);
   tree->SetBranchAddress("tau_pion0_P", &tau_pion0_P);
   tree->SetBranchAddress("tau_pion1_PX", &tau_pion1_PX);
   tree->SetBranchAddress("tau_pion1_PY", &tau_pion1_PY);
   tree->SetBranchAddress("tau_pion1_PZ", &tau_pion1_PZ);
   tree->SetBranchAddress("tau_pion1_P", &tau_pion1_P);
   tree->SetBranchAddress("tau_pion2_PX", &tau_pion2_PX);
   tree->SetBranchAddress("tau_pion2_PY", &tau_pion2_PY);
   tree->SetBranchAddress("tau_pion2_PZ", &tau_pion2_PZ);
   tree->SetBranchAddress("tau_pion2_P", &tau_pion2_P);

   tree->SetBranchAddress("D0_ENDVERTEX_X", &D_ENDVERTEX_X);
   tree->SetBranchAddress("D0_ENDVERTEX_Y", &D_ENDVERTEX_Y);
   tree->SetBranchAddress("D0_ENDVERTEX_Z", &D_ENDVERTEX_Z);
   tree->SetBranchAddress("B_ENDVERTEX_X", &B_ENDVERTEX_X);
   tree->SetBranchAddress("B_ENDVERTEX_Y", &B_ENDVERTEX_Y);
   tree->SetBranchAddress("B_ENDVERTEX_Z", &B_ENDVERTEX_Z);
   tree->SetBranchAddress("B_ENDVERTEX_ZERR", &B_ENDVERTEX_ZERR);
   tree->SetBranchAddress("B_OWNPV_X", &B_OWNPV_X);
   tree->SetBranchAddress("B_OWNPV_Y", &B_OWNPV_Y);
   tree->SetBranchAddress("B_OWNPV_Z", &B_OWNPV_Z);
   tree->SetBranchAddress("tau_OWNPV_X", &tau_OWNPV_X);
   tree->SetBranchAddress("tau_OWNPV_Y", &tau_OWNPV_Y);
   tree->SetBranchAddress("tau_OWNPV_Z", &tau_OWNPV_Z);

   tree->SetBranchAddress("tau_ENDVERTEX_Z", &tau_ENDVERTEX_Z);
   tree->SetBranchAddress("tau_ENDVERTEX_ZERR", &tau_ENDVERTEX_ZERR);
   tree->SetBranchAddress("tau_OWNPV_ZERR", &tau_OWNPV_ZERR);

   tree->SetBranchAddress("tau_ENDVERTEX_CHI2", &tau_ENDVERTEX_CHI2);
   tree->SetBranchAddress("B_ENDVERTEX_CHI2", &B_ENDVERTEX_CHI2);

   tree->SetBranchAddress("tau_ENDVERTEX_X",&tau_ENDVERTEX_X);
   tree->SetBranchAddress("tau_ENDVERTEX_Y",&tau_ENDVERTEX_Y);

   tree->SetBranchAddress("tau_pion0_ProbNNpi",&tau_pion0_ProbNNpi);
   tree->SetBranchAddress("tau_pion1_ProbNNpi",&tau_pion1_ProbNNpi);
   tree->SetBranchAddress("tau_pion2_ProbNNpi",&tau_pion2_ProbNNpi);
   tree->SetBranchAddress("tau_pion0_ProbNNk",&tau_pion0_ProbNNk);
   tree->SetBranchAddress("tau_pion1_ProbNNk",&tau_pion1_ProbNNk);
   tree->SetBranchAddress("tau_pion2_ProbNNk",&tau_pion2_ProbNNk);
   tree->SetBranchAddress("tau_pion0_PIDK",&tau_pion0_PIDK);
   tree->SetBranchAddress("tau_pion1_PIDK",&tau_pion1_PIDK);
   tree->SetBranchAddress("tau_pion2_PIDK",&tau_pion2_PIDK);


   tree->SetBranchAddress("D0_IPCHI2_OWNPV",&D_IPCHI2_OWNPV);

   tree->SetBranchAddress("D0_OWNPV_Z",&D_OWNPV_Z);

   tree->SetBranchAddress("tau_pion0_IPCHI2_OWNPV",&tau_pion0_IPCHI2_OWNPV);
   tree->SetBranchAddress("tau_pion1_IPCHI2_OWNPV",&tau_pion1_IPCHI2_OWNPV);
   tree->SetBranchAddress("tau_pion2_IPCHI2_OWNPV",&tau_pion2_IPCHI2_OWNPV);

   tree->SetBranchAddress("B_PX",&B_PX);
   tree->SetBranchAddress("B_PY",&B_PY);
   tree->SetBranchAddress("B_PZ",&B_PZ);
   tree->SetBranchAddress("B_P",&B_P);
   tree->SetBranchAddress("B_PE",&B_PE);
   tree->SetBranchAddress("D0_PX",&D_PX);
   tree->SetBranchAddress("D0_PY",&D_PY);
   tree->SetBranchAddress("D0_PZ",&D_PZ);
   tree->SetBranchAddress("D0_PE",&D_PE);
   tree->SetBranchAddress("D0_P",&D_P);
   tree->SetBranchAddress("D0_M",&D_M);
   tree->SetBranchAddress("tau_PX",&tau_PX);
   tree->SetBranchAddress("tau_PY",&tau_PY);
   tree->SetBranchAddress("tau_PZ",&tau_PZ);
   tree->SetBranchAddress("tau_P",&tau_P);

   tree->SetBranchAddress("tau_EW_0.40_nc_PX",&tau_040_nc_PX);
   tree->SetBranchAddress("tau_EW_0.40_nc_PY",&tau_040_nc_PY);
   tree->SetBranchAddress("tau_EW_0.40_nc_PZ",&tau_040_nc_PZ);
   tree->SetBranchAddress("tau_EW_0.40_nc_vPT",&tau_040_nc_vPT);

   double tau_EW_000_nc_maxPt_PX, tau_EW_000_nc_maxPt_PY, tau_EW_000_nc_maxPt_PZ;
   double tau_EW_000_nc_secPt_PX, tau_EW_000_nc_secPt_PY, tau_EW_000_nc_secPt_PZ;
   double tau_EW_000_nc_thiPt_PX, tau_EW_000_nc_thiPt_PY, tau_EW_000_nc_thiPt_PZ;

   tree->SetBranchAddress("tau_EW_0.00_nc_maxPt_PX",&tau_EW_000_nc_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_nc_maxPt_PY",&tau_EW_000_nc_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_nc_maxPt_PZ",&tau_EW_000_nc_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_nc_secPt_PX",&tau_EW_000_nc_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_nc_secPt_PY",&tau_EW_000_nc_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_nc_secPt_PZ",&tau_EW_000_nc_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_nc_thiPt_PX",&tau_EW_000_nc_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_nc_thiPt_PY",&tau_EW_000_nc_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_nc_thiPt_PZ",&tau_EW_000_nc_thiPt_PZ);

   double tau_EW_050_nc_maxPt_PX, tau_EW_050_nc_maxPt_PY, tau_EW_050_nc_maxPt_PZ;
   double tau_EW_050_nc_secPt_PX, tau_EW_050_nc_secPt_PY, tau_EW_050_nc_secPt_PZ;
   double tau_EW_050_nc_thiPt_PX, tau_EW_050_nc_thiPt_PY, tau_EW_050_nc_thiPt_PZ;

   tree->SetBranchAddress("tau_EW_0.50_nc_maxPt_PX",&tau_EW_050_nc_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_nc_maxPt_PY",&tau_EW_050_nc_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_nc_maxPt_PZ",&tau_EW_050_nc_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_nc_secPt_PX",&tau_EW_050_nc_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_nc_secPt_PY",&tau_EW_050_nc_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_nc_secPt_PZ",&tau_EW_050_nc_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_nc_thiPt_PX",&tau_EW_050_nc_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_nc_thiPt_PY",&tau_EW_050_nc_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_nc_thiPt_PZ",&tau_EW_050_nc_thiPt_PZ);

   double tau_EW_000_pi0_maxPt_PX, tau_EW_000_pi0_maxPt_PY, tau_EW_000_pi0_maxPt_PZ, tau_EW_000_pi0_maxPt_MM;
   double tau_EW_000_pi0_secPt_PX, tau_EW_000_pi0_secPt_PY, tau_EW_000_pi0_secPt_PZ, tau_EW_000_pi0_secPt_MM;
   double tau_EW_000_pi0_thiPt_PX, tau_EW_000_pi0_thiPt_PY, tau_EW_000_pi0_thiPt_PZ, tau_EW_000_pi0_thiPt_MM;

   tree->SetBranchAddress("tau_EW_0.00_pi0_maxPt_PX",&tau_EW_000_pi0_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_pi0_maxPt_PY",&tau_EW_000_pi0_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_pi0_maxPt_PZ",&tau_EW_000_pi0_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_pi0_maxPt_MM",&tau_EW_000_pi0_maxPt_MM);
   tree->SetBranchAddress("tau_EW_0.00_pi0_secPt_PX",&tau_EW_000_pi0_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_pi0_secPt_PY",&tau_EW_000_pi0_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_pi0_secPt_PZ",&tau_EW_000_pi0_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_pi0_secPt_MM",&tau_EW_000_pi0_secPt_MM);
   tree->SetBranchAddress("tau_EW_0.00_pi0_thiPt_PX",&tau_EW_000_pi0_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_pi0_thiPt_PY",&tau_EW_000_pi0_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_pi0_thiPt_PZ",&tau_EW_000_pi0_thiPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_pi0_thiPt_MM",&tau_EW_000_pi0_thiPt_MM);

   double tau_EW_050_pi0_maxPt_PX, tau_EW_050_pi0_maxPt_PY, tau_EW_050_pi0_maxPt_PZ, tau_EW_050_pi0_maxPt_MM;
   double tau_EW_050_pi0_secPt_PX, tau_EW_050_pi0_secPt_PY, tau_EW_050_pi0_secPt_PZ, tau_EW_050_pi0_secPt_MM;
   double tau_EW_050_pi0_thiPt_PX, tau_EW_050_pi0_thiPt_PY, tau_EW_050_pi0_thiPt_PZ, tau_EW_050_pi0_thiPt_MM;

   tree->SetBranchAddress("tau_EW_0.50_pi0_maxPt_PX",&tau_EW_050_pi0_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_pi0_maxPt_PY",&tau_EW_050_pi0_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_pi0_maxPt_PZ",&tau_EW_050_pi0_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_pi0_maxPt_MM",&tau_EW_050_pi0_maxPt_MM);
   tree->SetBranchAddress("tau_EW_0.50_pi0_secPt_PX",&tau_EW_050_pi0_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_pi0_secPt_PY",&tau_EW_050_pi0_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_pi0_secPt_PZ",&tau_EW_050_pi0_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_pi0_secPt_MM",&tau_EW_050_pi0_secPt_MM);
   tree->SetBranchAddress("tau_EW_0.50_pi0_thiPt_PX",&tau_EW_050_pi0_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_pi0_thiPt_PY",&tau_EW_050_pi0_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_pi0_thiPt_PZ",&tau_EW_050_pi0_thiPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_pi0_thiPt_MM",&tau_EW_050_pi0_thiPt_MM);

   double tau_EW_000_eta_maxPt_PX, tau_EW_000_eta_maxPt_PY, tau_EW_000_eta_maxPt_PZ, tau_EW_000_eta_maxPt_MM;
   double tau_EW_000_eta_secPt_PX, tau_EW_000_eta_secPt_PY, tau_EW_000_eta_secPt_PZ, tau_EW_000_eta_secPt_MM;
   double tau_EW_000_eta_thiPt_PX, tau_EW_000_eta_thiPt_PY, tau_EW_000_eta_thiPt_PZ, tau_EW_000_eta_thiPt_MM;

   tree->SetBranchAddress("tau_EW_0.00_eta_maxPt_PX",&tau_EW_000_eta_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_eta_maxPt_PY",&tau_EW_000_eta_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_eta_maxPt_PZ",&tau_EW_000_eta_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_eta_maxPt_MM",&tau_EW_000_eta_maxPt_MM);
   tree->SetBranchAddress("tau_EW_0.00_eta_secPt_PX",&tau_EW_000_eta_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_eta_secPt_PY",&tau_EW_000_eta_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_eta_secPt_PZ",&tau_EW_000_eta_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_eta_secPt_MM",&tau_EW_000_eta_secPt_MM);
   tree->SetBranchAddress("tau_EW_0.00_eta_thiPt_PX",&tau_EW_000_eta_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.00_eta_thiPt_PY",&tau_EW_000_eta_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.00_eta_thiPt_PZ",&tau_EW_000_eta_thiPt_PZ);
   tree->SetBranchAddress("tau_EW_0.00_eta_thiPt_MM",&tau_EW_000_eta_thiPt_MM);

   double tau_EW_050_eta_maxPt_PX, tau_EW_050_eta_maxPt_PY, tau_EW_050_eta_maxPt_PZ, tau_EW_050_eta_maxPt_MM;
   double tau_EW_050_eta_secPt_PX, tau_EW_050_eta_secPt_PY, tau_EW_050_eta_secPt_PZ, tau_EW_050_eta_secPt_MM;
   double tau_EW_050_eta_thiPt_PX, tau_EW_050_eta_thiPt_PY, tau_EW_050_eta_thiPt_PZ, tau_EW_050_eta_thiPt_MM;

   tree->SetBranchAddress("tau_EW_0.50_eta_maxPt_PX",&tau_EW_050_eta_maxPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_eta_maxPt_PY",&tau_EW_050_eta_maxPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_eta_maxPt_PZ",&tau_EW_050_eta_maxPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_eta_maxPt_MM",&tau_EW_050_eta_maxPt_MM);
   tree->SetBranchAddress("tau_EW_0.50_eta_secPt_PX",&tau_EW_050_eta_secPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_eta_secPt_PY",&tau_EW_050_eta_secPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_eta_secPt_PZ",&tau_EW_050_eta_secPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_eta_secPt_MM",&tau_EW_050_eta_secPt_MM);
   tree->SetBranchAddress("tau_EW_0.50_eta_thiPt_PX",&tau_EW_050_eta_thiPt_PX);
   tree->SetBranchAddress("tau_EW_0.50_eta_thiPt_PY",&tau_EW_050_eta_thiPt_PY);
   tree->SetBranchAddress("tau_EW_0.50_eta_thiPt_PZ",&tau_EW_050_eta_thiPt_PZ);
   tree->SetBranchAddress("tau_EW_0.50_eta_thiPt_MM",&tau_EW_050_eta_thiPt_MM);

   // Iso

   tree->SetBranchAddress("tau_isoPlus_nGood",&tau_isoPlus_nGood);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_IPChi2",tau_isoPlus_trk_LOKI_IPChi2);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_IPChi2MOTHER",tau_isoPlus_trk_LOKI_IPChi2MOTHER);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_IPChi2PV",tau_isoPlus_trk_LOKI_IPChi2PV);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_DOCAVX",tau_isoPlus_trk_LOKI_DOCAVX);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_DOCAVXMOTHER",tau_isoPlus_trk_LOKI_DOCAVXMOTHER);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_TYPE",tau_isoPlus_trk_LOKI_TYPE);
   tree->SetBranchAddress("tau_isoPlus_newP_LOKI_MM",tau_isoPlus_newP_LOKI_MM);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_PX",tau_isoPlus_trk_LOKI_PX);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_PY",tau_isoPlus_trk_LOKI_PY);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_PZ",tau_isoPlus_trk_LOKI_PZ);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_ID",tau_isoPlus_trk_LOKI_ID);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_P",tau_isoPlus_trk_LOKI_P);
   tree->SetBranchAddress("tau_isoPlus_trk_LOKI_PIDK",tau_isoPlus_trk_LOKI_PIDK);

   tree->SetBranchAddress("D0_isoPlus_nGood",&D_isoPlus_nGood);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_IPChi2",D_isoPlus_trk_LOKI_IPChi2);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_IPChi2PV",D_isoPlus_trk_LOKI_IPChi2PV);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_DOCAVX",D_isoPlus_trk_LOKI_DOCAVX);
   tree->SetBranchAddress("D0_isoPlus_newP_LOKI_MM",D_isoPlus_newP_LOKI_MM);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_PX",D_isoPlus_trk_LOKI_PX);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_PY",D_isoPlus_trk_LOKI_PY);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_PZ",D_isoPlus_trk_LOKI_PZ);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_ID",D_isoPlus_trk_LOKI_ID);
   tree->SetBranchAddress("D0_isoPlus_trk_LOKI_P",D_isoPlus_trk_LOKI_P);

   tree->SetBranchAddress("B_isoPlus_nGood",&B_isoPlus_nGood);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_IPChi2",B_isoPlus_trk_LOKI_IPChi2);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_IPChi2PV",B_isoPlus_trk_LOKI_IPChi2PV);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_DOCAVX",B_isoPlus_trk_LOKI_DOCAVX);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_PIDK",B_isoPlus_trk_LOKI_PIDK);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_ID",B_isoPlus_trk_LOKI_ID);
   tree->SetBranchAddress("B_isoPlus_newP_LOKI_MM",B_isoPlus_newP_LOKI_MM);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_PX",B_isoPlus_trk_LOKI_PX);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_PY",B_isoPlus_trk_LOKI_PY);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_PZ",B_isoPlus_trk_LOKI_PZ);
   tree->SetBranchAddress("B_isoPlus_trk_LOKI_P",B_isoPlus_trk_LOKI_P);

   if (MonteCarlo) {

     tree->SetBranchAddress("B_Added_B_ndaug", &B_Added_B_ndaug);
     tree->SetBranchAddress("B_Added_daugID", B_Added_daugID);
     tree->SetBranchAddress("B_Added_daug_TRUEP_X", B_Added_daug_TRUEP_X);
     tree->SetBranchAddress("B_Added_daug_TRUEP_Y", B_Added_daug_TRUEP_Y);
     tree->SetBranchAddress("B_Added_daug_TRUEP_Z", B_Added_daug_TRUEP_Z);
     tree->SetBranchAddress("B_Added_daug_TRUEP_E", B_Added_daug_TRUEP_E);
     tree->SetBranchAddress("B_Added_D1_ndaug", &B_Added_D1_ndaug);
     tree->SetBranchAddress("B_Added_D2_ndaug", &B_Added_D2_ndaug);
     tree->SetBranchAddress("B_Added_tau_ndaug", &B_Added_tau_ndaug);
     tree->SetBranchAddress("B_Added_ID", &B_Added_ID);
     tree->SetBranchAddress("B_Added_D1_ID", &B_Added_D1_ID);
     tree->SetBranchAddress("B_Added_D2_ID", &B_Added_D2_ID);
     tree->SetBranchAddress("B_Added_D1_daugID", B_Added_D1_daugID);
     tree->SetBranchAddress("B_Added_D2_daugID", B_Added_D2_daugID);
     tree->SetBranchAddress("B_Added_tau_daugID", B_Added_tau_daugID);

     tree->SetBranchAddress("B_TRUEENDVERTEX_X", &B_TRUEENDVERTEX_X);
     tree->SetBranchAddress("B_TRUEENDVERTEX_Y", &B_TRUEENDVERTEX_Y);
     tree->SetBranchAddress("B_TRUEENDVERTEX_Z", &B_TRUEENDVERTEX_Z);
     tree->SetBranchAddress("tau_TRUEENDVERTEX_X", &tau_TRUEENDVERTEX_X);
     tree->SetBranchAddress("tau_TRUEENDVERTEX_Y", &tau_TRUEENDVERTEX_Y);
     tree->SetBranchAddress("tau_TRUEENDVERTEX_Z", &tau_TRUEENDVERTEX_Z);
     tree->SetBranchAddress("D0_TRUEP_X",&D_TRUEP_X);
     tree->SetBranchAddress("D0_TRUEP_Y",&D_TRUEP_Y);
     tree->SetBranchAddress("D0_TRUEP_Z",&D_TRUEP_Z);
     tree->SetBranchAddress("D0_TRUEP_E",&D_TRUEP_E);
     tree->SetBranchAddress("B_TRUEP_X",&B_TRUEP_X);
     tree->SetBranchAddress("B_TRUEP_Y",&B_TRUEP_Y);
     tree->SetBranchAddress("B_TRUEP_Z",&B_TRUEP_Z);
     tree->SetBranchAddress("B_TRUEP_E",&B_TRUEP_E);
     tree->SetBranchAddress("tau_pion0_TRUEP_X",&tau_pion0_TRUEP_X);
     tree->SetBranchAddress("tau_pion0_TRUEP_Y",&tau_pion0_TRUEP_Y);
     tree->SetBranchAddress("tau_pion0_TRUEP_Z",&tau_pion0_TRUEP_Z);
     tree->SetBranchAddress("tau_pion0_TRUEP_E",&tau_pion0_TRUEP_E);
     tree->SetBranchAddress("tau_pion1_TRUEP_X",&tau_pion1_TRUEP_X);
     tree->SetBranchAddress("tau_pion1_TRUEP_Y",&tau_pion1_TRUEP_Y);
     tree->SetBranchAddress("tau_pion1_TRUEP_Z",&tau_pion1_TRUEP_Z);
     tree->SetBranchAddress("tau_pion1_TRUEP_E",&tau_pion1_TRUEP_E);
     tree->SetBranchAddress("tau_pion2_TRUEP_X",&tau_pion2_TRUEP_X);
     tree->SetBranchAddress("tau_pion2_TRUEP_Y",&tau_pion2_TRUEP_Y);
     tree->SetBranchAddress("tau_pion2_TRUEP_Z",&tau_pion2_TRUEP_Z);
     tree->SetBranchAddress("tau_pion2_TRUEP_E",&tau_pion2_TRUEP_E);
     tree->SetBranchAddress("tau_TRUEP_X",&tau_TRUEP_X);
     tree->SetBranchAddress("tau_TRUEP_Y",&tau_TRUEP_Y);
     tree->SetBranchAddress("tau_TRUEP_Z",&tau_TRUEP_Z);
     tree->SetBranchAddress("tau_TRUEP_E",&tau_TRUEP_E);

     tree->SetBranchAddress("D0_MC_MOTHER_TRUEP_E",&D_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("D0_MC_MOTHER_TRUEP_X",&D_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("D0_MC_MOTHER_TRUEP_Y",&D_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("D0_MC_MOTHER_TRUEP_Z",&D_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("D0_MC_MOTHER_ID",&D_MC_MOTHER_ID);
     tree->SetBranchAddress("D0_K_TRUEID",&D_K_TRUEID);
     tree->SetBranchAddress("D0_pi_TRUEID",&D_pi_TRUEID);
     tree->SetBranchAddress("D0_TRUEID",&D_TRUEID);
     tree->SetBranchAddress("D0_K_MC_MOTHER_ID",&D_K_MC_MOTHER_ID);
     tree->SetBranchAddress("D0_K_MC_GD_MOTHER_ID",&D_K_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("D0_K_MC_GD_GD_MOTHER_ID",&D_K_MC_GD_GD_MOTHER_ID);
     tree->SetBranchAddress("D0_pi_MC_MOTHER_ID",&D_pi_MC_MOTHER_ID);
     tree->SetBranchAddress("D0_pi_MC_GD_MOTHER_ID",&D_pi_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("D0_pi_MC_GD_GD_MOTHER_ID",&D_pi_MC_GD_GD_MOTHER_ID);
     tree->SetBranchAddress("D0_K_MC_MOTHER_TRUEP_X",&D_K_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("D0_K_MC_MOTHER_TRUEP_Y",&D_K_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("D0_K_MC_MOTHER_TRUEP_Z",&D_K_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("D0_K_MC_MOTHER_TRUEP_E",&D_K_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("D0_pi_MC_MOTHER_TRUEP_X",&D_pi_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("D0_pi_MC_MOTHER_TRUEP_Y",&D_pi_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("D0_pi_MC_MOTHER_TRUEP_Z",&D_pi_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("D0_pi_MC_MOTHER_TRUEP_E",&D_pi_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_TRUEID",&tau_TRUEID);
     tree->SetBranchAddress("tau_MC_MOTHER_ID",&tau_MC_MOTHER_ID);
     tree->SetBranchAddress("tau_MC_MOTHER_TRUEP_X",&tau_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_MC_MOTHER_TRUEP_Y",&tau_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_MC_MOTHER_TRUEP_Z",&tau_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_MC_MOTHER_TRUEP_E",&tau_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_MC_GD_MOTHER_TRUEP_X",&tau_MC_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_MC_GD_MOTHER_TRUEP_Y",&tau_MC_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_MC_GD_MOTHER_TRUEP_Z",&tau_MC_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_MC_GD_MOTHER_TRUEP_E",&tau_MC_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_MC_GD_GD_MOTHER_TRUEP_X",&tau_MC_GD_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_MC_GD_GD_MOTHER_TRUEP_Y",&tau_MC_GD_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_MC_GD_GD_MOTHER_TRUEP_Z",&tau_MC_GD_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_MC_GD_GD_MOTHER_TRUEP_E",&tau_MC_GD_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion0_MC_MOTHER_ID",&tau_pion0_MC_MOTHER_ID);
     tree->SetBranchAddress("tau_pion1_MC_MOTHER_ID",&tau_pion1_MC_MOTHER_ID);
     tree->SetBranchAddress("tau_pion2_MC_MOTHER_ID",&tau_pion2_MC_MOTHER_ID);
     tree->SetBranchAddress("tau_pion0_MC_MOTHER_TRUEP_X",&tau_pion0_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion0_MC_MOTHER_TRUEP_Y",&tau_pion0_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion0_MC_MOTHER_TRUEP_Z",&tau_pion0_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion0_MC_MOTHER_TRUEP_E",&tau_pion0_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion1_MC_MOTHER_TRUEP_X",&tau_pion1_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion1_MC_MOTHER_TRUEP_Y",&tau_pion1_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion1_MC_MOTHER_TRUEP_Z",&tau_pion1_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion1_MC_MOTHER_TRUEP_E",&tau_pion1_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion2_MC_MOTHER_TRUEP_X",&tau_pion2_MC_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion2_MC_MOTHER_TRUEP_Y",&tau_pion2_MC_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion2_MC_MOTHER_TRUEP_Z",&tau_pion2_MC_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion2_MC_MOTHER_TRUEP_E",&tau_pion2_MC_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion0_MC_GD_MOTHER_TRUEP_X",&tau_pion0_MC_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion0_MC_GD_MOTHER_TRUEP_Y",&tau_pion0_MC_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion0_MC_GD_MOTHER_TRUEP_Z",&tau_pion0_MC_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion0_MC_GD_MOTHER_TRUEP_E",&tau_pion0_MC_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion1_MC_GD_MOTHER_TRUEP_X",&tau_pion1_MC_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion1_MC_GD_MOTHER_TRUEP_Y",&tau_pion1_MC_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion1_MC_GD_MOTHER_TRUEP_Z",&tau_pion1_MC_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion1_MC_GD_MOTHER_TRUEP_E",&tau_pion1_MC_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion2_MC_GD_MOTHER_TRUEP_X",&tau_pion2_MC_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion2_MC_GD_MOTHER_TRUEP_Y",&tau_pion2_MC_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion2_MC_GD_MOTHER_TRUEP_Z",&tau_pion2_MC_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion2_MC_GD_MOTHER_TRUEP_E",&tau_pion2_MC_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion0_MC_GD_GD_MOTHER_TRUEP_X",&tau_pion0_MC_GD_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion0_MC_GD_GD_MOTHER_TRUEP_Y",&tau_pion0_MC_GD_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion0_MC_GD_GD_MOTHER_TRUEP_Z",&tau_pion0_MC_GD_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion0_MC_GD_GD_MOTHER_TRUEP_E",&tau_pion0_MC_GD_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion1_MC_GD_GD_MOTHER_TRUEP_X",&tau_pion1_MC_GD_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion1_MC_GD_GD_MOTHER_TRUEP_Y",&tau_pion1_MC_GD_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion1_MC_GD_GD_MOTHER_TRUEP_Z",&tau_pion1_MC_GD_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion1_MC_GD_GD_MOTHER_TRUEP_E",&tau_pion1_MC_GD_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_pion2_MC_GD_GD_MOTHER_TRUEP_X",&tau_pion2_MC_GD_GD_MOTHER_TRUEP_X);
     tree->SetBranchAddress("tau_pion2_MC_GD_GD_MOTHER_TRUEP_Y",&tau_pion2_MC_GD_GD_MOTHER_TRUEP_Y);
     tree->SetBranchAddress("tau_pion2_MC_GD_GD_MOTHER_TRUEP_Z",&tau_pion2_MC_GD_GD_MOTHER_TRUEP_Z);
     tree->SetBranchAddress("tau_pion2_MC_GD_GD_MOTHER_TRUEP_E",&tau_pion2_MC_GD_GD_MOTHER_TRUEP_E);
     tree->SetBranchAddress("tau_MC_GD_MOTHER_ID",&tau_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion0_MC_GD_MOTHER_ID",&tau_pion0_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion1_MC_GD_MOTHER_ID",&tau_pion1_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion2_MC_GD_MOTHER_ID",&tau_pion2_MC_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_MC_GD_GD_MOTHER_ID",&tau_MC_GD_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion0_MC_GD_GD_MOTHER_ID",&tau_pion0_MC_GD_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion1_MC_GD_GD_MOTHER_ID",&tau_pion1_MC_GD_GD_MOTHER_ID);
     tree->SetBranchAddress("tau_pion2_MC_GD_GD_MOTHER_ID",&tau_pion2_MC_GD_GD_MOTHER_ID);

   }

   f_out->cd();

   tree_out = tree->CloneTree(0);
   if (WS) {
     tree_out->SetName("DecayTreeWS");
   } else if (NonPhys) {
     tree_out->SetName("DecayTreeNonPhys");
   } else {
     tree_out->SetName("DecayTree");
   }

   // new branches

   double cosTheta1, cosTheta1B, cosTheta2B;
   double MW, cosThetaTau, TauCTAU, cosTau3piMC, cosTau3pi2MC;
   double TauCTAUsm2016, TauCTAUsm2017, TauCTAUsm2018, TauCTAUsmAll, TauCTAUsmComb;
   double cosThetaTauDst;
   double TauCTAU1, TauCTAU2;
   double cosThetaTau11, cosThetaTau12, cosThetaTau21, cosThetaTau22;
   double cos01, cos12, q2, q2Dst;
   double q2sm2016, q2sm2017, q2sm2018, q2smAll, q2smComb;
   double q2Dstsm2016, q2Dstsm2017, q2Dstsm2018, q2DstsmAll, q2DstsmComb;
   double cos2pimin, cos2pimax;
   double PB, PTAU, tauPHI, tauPHIMC;
   //double PBDst;
   double PB11, PB12, PB21, PB22;
   double PTAU1, PTAU2;
   double PBsc, PTAUsc, TauCTAUsc;
   double MWsc;
   double D0CTAU;

   double MBcor;
   double MTaucor;
   double pTD0, pTB;
   double pTTau, pTTauB, pTTauB2;
   double pTTauMC, pTTauBMC;
   double pTD0MC;

   double MWMC, MLostMC, cosTau3pi, cosTau3pi2, cosThetaTauMC, cosTheta1BMC;
   double cosThetaTauDstMC;
   double q2MC, q2DstMC;
   double cosTheta1MC, mNu;
   double thetamax, thetatau, thetaB, thetaBmax;
   double thetatauMC;
   double MW00, MW11, MW12, MW21, MW22;
   double M01, M12, M02, MDst1, MDst2, MDst3, MD01, MD02, MD03;
   double MDst12, MDst23, MDst13;
   int nTauIso9sig, nTauIso5sig, nTauIso25sig;
   int nTauIso9sig0, nTauIso5sig0, nTauIso25sig0;
   int nBTauIso25sig0, nBTauIso25sig0_tau, nBTauIso25sig0_B;
   int nBTauIso25sig0_Dst, nBTauIso25sig0_Dst_WS;
   int nTauIso25sig0_pi, nTauIso25sig0_e, nTauIso25sig0_mu;
   int nTauIso5sig0_Dst, nTauIso9sig0_Dst, nTauIso25sig0_Dst;
   int nTauIso5sig0_Dst_WS, nTauIso9sig0_Dst_WS, nTauIso25sig0_Dst_WS;
   int nTauIso9sig1, nTauIso5sig1, nTauIso25sig1;
   int nDIso9sig, nDIso5sig, nDIso25sig;
   int nDIso9sig0, nDIso5sig0, nDIso25sig0;
   int nDIso9sig1, nDIso5sig1, nDIso25sig1;
   double minChi2TauIsosig, minChi2TauIsosig0, minChi2TauIsosig1;
   double minDOCAVXTauIsosig, minDOCAVXTauIsosig0, minDOCAVXTauIsosig1;
   double minDOCAVXTauIsoAll, minDOCAVXTauIsoAll0, minDOCAVXTauIsoAll1;
   int nB0Iso9sig, nB0Iso5sig, nB0Iso25sig;
   int nB0Iso25sig_pi, nB0Iso25sig_e, nB0Iso25sig_mu;
   int nB0Iso5sig_Dst, nB0Iso9sig_Dst, nB0Iso25sig_Dst;
   int nB0Iso5sig_Dst_WS, nB0Iso9sig_Dst_WS, nB0Iso25sig_Dst_WS;
   double minChi2BIsosig;
   double minDOCAVXBIsosig;
   double minDOCAVXBIsoAll;
   double minChi2DIsosig, minChi2DIsosig0, minChi2DIsosig1;
   double minDOCAVXDIsosig, minDOCAVXDIsosig0, minDOCAVXDIsosig1;
   double M2piK, M2piKmax, M2Kpi;
   double BM2piK;

   double Dst_M, Dst_M_WS, MD0pi1, MD0pi2, MD0pi0;
   double Dst_pion_PX, Dst_pion_PY, Dst_pion_PZ;
   double Dst_PX, Dst_PY, Dst_PZ;
   double Dst_pionWS_PX, Dst_pionWS_PY, Dst_pionWS_PZ;
   double Dst_pion_IPCHI2_OWNPV;
   double Dst_pionWS_IPCHI2_OWNPV;
   int nDst, nDst_WS;
   int nCandDst;
   int nCandSel;
   double DRD0tau;

   double tau_pion0_eta, tau_pion1_eta, tau_pion2_eta, D0_K_eta, D0_pi_eta, tau_eta, D0_eta, B_eta;
   int Year, YearSim;

//   TBranch* br_nCandDst;
//   TBranch* br_nCandSel;
//   TBranch* br_Dst_M;

   double Tau_sc_zerr_2016, Tau_sc_zerr_2017, Tau_sc_zerr_2018, Tau_sc_zerr_2016_data, Tau_sc_zerr;
   double b_sc_zerr_2016, b_sc_zerr_2017, b_sc_zerr_2018, b_sc_zerr_2016_data, b_sc_zerr;
   double Reso_DzS_2016, Reso_DzS_2017, Reso_DzS_2018, Reso_DzS_2016_data, Reso_DzS;
   double Reso_Dz_2016, Reso_Dz_2017, Reso_Dz_2018, Reso_Dz_2016_data, Reso_Dz;
   double MC_MD1D2, q2MCFF;

//   double tau_ENDVERTEX_ZERR_cor_2016, tau_ENDVERTEX_ZERR_cor_2017, tau_ENDVERTEX_ZERR_cor_2018, tau_ENDVERTEX_ZERR_cor_Comb;

//   br_nCandDst = tree_out->Branch("nCandDst",&nCandDst,"nCandDst/I");
//   br_nCandSel = tree_out->Branch("nCandSel",&nCandSel,"nCandSel/I");
//
   tree_out->Branch("MC_MD1D2",&MC_MD1D2,"MC_MD1D2/D");
   tree_out->Branch("q2MCFF",&q2MCFF,"q2MCFF/D");

   tree_out->Branch("Year",&Year,"Year/I");
   tree_out->Branch("YearSim",&YearSim,"YearSim/I");
   tree_out->Branch("Tau_sc_zerr_2016",&Tau_sc_zerr_2016,"Tau_sc_zerr_2016/D");
   tree_out->Branch("Tau_sc_zerr_2017",&Tau_sc_zerr_2017,"Tau_sc_zerr_2017/D");
   tree_out->Branch("Tau_sc_zerr_2018",&Tau_sc_zerr_2018,"Tau_sc_zerr_2018/D");
   tree_out->Branch("Tau_sc_zerr_2016_data",&Tau_sc_zerr_2016_data,"Tau_sc_zerr_2016_data/D");
   tree_out->Branch("Tau_sc_zerr",&Tau_sc_zerr,"Tau_sc_zerr/D");
   tree_out->Branch("b_sc_zerr_2016",&b_sc_zerr_2016,"b_sc_zerr_2016/D");
   tree_out->Branch("b_sc_zerr_2017",&b_sc_zerr_2017,"b_sc_zerr_2017/D");
   tree_out->Branch("b_sc_zerr_2018",&b_sc_zerr_2018,"b_sc_zerr_2018/D");
   tree_out->Branch("b_sc_zerr_2016_data",&b_sc_zerr_2016_data,"b_sc_zerr_2016_data/D");
   tree_out->Branch("b_sc_zerr",&b_sc_zerr,"b_sc_zerr/D");
   tree_out->Branch("Reso_DzS_2016",&Reso_DzS_2016,"Reso_DzS_2016/D");
   tree_out->Branch("Reso_DzS_2017",&Reso_DzS_2017,"Reso_DzS_2017/D");
   tree_out->Branch("Reso_DzS_2018",&Reso_DzS_2018,"Reso_DzS_2018/D");
   tree_out->Branch("Reso_DzS_2016_data",&Reso_DzS_2016_data,"Reso_DzS_2016_data/D");
   tree_out->Branch("Reso_DzS",&Reso_DzS,"Reso_DzS/D");
   tree_out->Branch("Reso_Dz_2016",&Reso_Dz_2016,"Reso_Dz_2016/D");
   tree_out->Branch("Reso_Dz_2017",&Reso_Dz_2017,"Reso_Dz_2017/D");
   tree_out->Branch("Reso_Dz_2018",&Reso_Dz_2018,"Reso_Dz_2018/D");
   tree_out->Branch("Reso_Dz_2016_data",&Reso_Dz_2016_data,"Reso_Dz_2016_data/D");
   tree_out->Branch("Reso_Dz",&Reso_Dz,"Reso_Dz/D");

//   tree_out->Branch("tau_ENDVERTEX_ZERR_cor_2016",&tau_ENDVERTEX_ZERR_cor_2016,"tau_ENDVERTEX_ZERR_2016/D");
//   tree_out->Branch("tau_ENDVERTEX_ZERR_cor_2017",&tau_ENDVERTEX_ZERR_cor_2017,"tau_ENDVERTEX_ZERR_cor_2017/D");
//   tree_out->Branch("tau_ENDVERTEX_ZERR_cor_2018",&tau_ENDVERTEX_ZERR_cor_2018,"tau_ENDVERTEX_ZERR_cor_2018/D");
//   tree_out->Branch("tau_ENDVERTEX_ZERR_cor_Comb",&tau_ENDVERTEX_ZERR_cor_Comb,"tau_ENDVERTEX_ZERR_cor_Comb/D");

   tree_out->Branch("tau_pion0_eta",&tau_pion0_eta,"tau_pion0_eta/D");
   tree_out->Branch("tau_pion1_eta",&tau_pion1_eta,"tau_pion1_eta/D");
   tree_out->Branch("tau_pion2_eta",&tau_pion2_eta,"tau_pion2_eta/D");
   tree_out->Branch("D0_K_eta",&D0_K_eta,"D0_K_eta/D");
   tree_out->Branch("D0_pi_eta",&D0_pi_eta,"D0_pi_eta/D");
   tree_out->Branch("D0_eta",&D0_eta,"D0_eta/D");
   tree_out->Branch("tau_eta",&tau_eta,"tau_eta/D");
   tree_out->Branch("B_eta",&B_eta,"B_eta/D");

   tree_out->Branch("nDst",&nDst,"nDst/I");
   tree_out->Branch("nDst_WS",&nDst_WS,"nDst_WS/I");
   //br_Dst_M = tree_out->Branch("Dst_M",&Dst_M,"Dst_M/D");
   tree_out->Branch("Dst_M",&Dst_M,"Dst_M/D");
   tree_out->Branch("Dst_M_WS",&Dst_M_WS,"Dst_M_WS/D");
   tree_out->Branch("MD0pi0",&MD0pi0,"MD0pi0/D");
   tree_out->Branch("MD0pi1",&MD0pi1,"MD0pi1/D");
   tree_out->Branch("MD0pi2",&MD0pi2,"MD0pi2/D");
   tree_out->Branch("Dst_pion_PX",&Dst_pion_PX,"Dst_pion_PX/D");
   tree_out->Branch("Dst_pion_PY",&Dst_pion_PY,"Dst_pion_PY/D");
   tree_out->Branch("Dst_pion_PZ",&Dst_pion_PZ,"Dst_pion_PZ/D");
   tree_out->Branch("Dst_pionWS_PX",&Dst_pionWS_PX,"Dst_pionWS_PX/D");
   tree_out->Branch("Dst_pionWS_PY",&Dst_pionWS_PY,"Dst_pionWS_PY/D");
   tree_out->Branch("Dst_pionWS_PZ",&Dst_pionWS_PZ,"Dst_pionWS_PZ/D");
   tree_out->Branch("Dst_pion_IPCHI2_OWNPV",&Dst_pion_IPCHI2_OWNPV,"Dst_pion_IPCHI2_OWNPV/D");
   tree_out->Branch("Dst_pionWS_IPCHI2_OWNPV",&Dst_pionWS_IPCHI2_OWNPV,"Dst_pionWS_IPCHI2_OWNPV/D");
//   tree_out->Branch("Dst_PX",&Dst_PX,"Dst_PX/D");
//   tree_out->Branch("Dst_PY",&Dst_PY,"Dst_PY/D");
 //  tree_out->Branch("Dst_PZ",&Dst_PZ,"Dst_PZ/D");
   tree_out->Branch("D0CTAU",&D0CTAU,"D0CTAU/D");

   tree_out->Branch("cosTheta1",&cosTheta1,"cosTheta1/D");
   tree_out->Branch("cosTheta1B",&cosTheta1B,"cosTheta1B/D");
   tree_out->Branch("cosTheta2B",&cosTheta2B,"cosTheta2B/D");
   tree_out->Branch("tauPHI",&tauPHI,"tauPHI/D");
   tree_out->Branch("MW",&MW,"MW/D");
   tree_out->Branch("q2",&q2,"q2/D");
   tree_out->Branch("q2sm2016",&q2sm2016,"q2sm2016/D");
   tree_out->Branch("q2sm2017",&q2sm2017,"q2sm2017/D");
   tree_out->Branch("q2sm2018",&q2sm2018,"q2sm2018/D");
   tree_out->Branch("q2smAll",&q2smAll,"q2smAll/D");
   tree_out->Branch("q2smComb",&q2smComb,"q2smComb/D");
   tree_out->Branch("q2Dst",&q2Dst,"q2Dst/D");
   tree_out->Branch("q2Dstsm2016",&q2Dstsm2016,"q2Dstsm2016/D");
   tree_out->Branch("q2Dstsm2017",&q2Dstsm2017,"q2Dstsm2017/D");
   tree_out->Branch("q2Dstsm2018",&q2Dstsm2018,"q2Dstsm2018/D");
   tree_out->Branch("q2DstsmAll",&q2DstsmAll,"q2DstsmAll/D");
   tree_out->Branch("q2DstsmComb",&q2DstsmComb,"q2DstsmComb/D");
   tree_out->Branch("mNu",&mNu,"mNu/D");
   tree_out->Branch("PB",&PB,"PB/D");
   tree_out->Branch("PB11",&PB11,"PB11/D");
   tree_out->Branch("PB12",&PB12,"PB12/D");
   tree_out->Branch("PB21",&PB21,"PB21/D");
   tree_out->Branch("PB22",&PB22,"PB22/D");
   tree_out->Branch("PBsc",&PBsc,"PBsc/D");
   tree_out->Branch("PTAU",&PTAU,"PTAU/D");
   tree_out->Branch("PTAU1",&PTAU1,"PTAU1/D");
   tree_out->Branch("PTAU2",&PTAU2,"PTAU2/D");
   tree_out->Branch("PTAUsc",&PTAUsc,"PTAUsc/D");
   tree_out->Branch("TauCTAUsc",&TauCTAUsc,"TauCTAUsc/D");
   tree_out->Branch("MWsc",&MWsc,"MWsc/D");
   tree_out->Branch("MBcor",&MBcor,"MBcor/D");
   tree_out->Branch("MTaucor",&MTaucor,"MTaucor/D");
   tree_out->Branch("pTB",&pTB,"pTB/D");
   tree_out->Branch("pTD0",&pTD0,"pTD0/D");
   tree_out->Branch("pTTau",&pTTau,"pTTau/D");
   tree_out->Branch("pTTauB",&pTTauB,"pTTauB/D");
   tree_out->Branch("pTTauB2",&pTTauB2,"pTTauB2/D");
   tree_out->Branch("MW00",&MW00,"MW00/D");
   tree_out->Branch("MW11",&MW11,"MW11/D");
   tree_out->Branch("MW12",&MW12,"MW12/D");
   tree_out->Branch("MW21",&MW21,"MW21/D");
   tree_out->Branch("MW22",&MW22,"MW22/D");
   tree_out->Branch("MDst1",&MDst1,"MDst1/D");
   tree_out->Branch("MDst2",&MDst2,"MDst2/D");
   tree_out->Branch("MDst3",&MDst3,"MDst3/D");
   tree_out->Branch("MDst12",&MDst12,"MDst12/D");
   tree_out->Branch("MDst13",&MDst13,"MDst13/D");
   tree_out->Branch("MDst23",&MDst23,"MDst23/D");
   tree_out->Branch("MD01",&MD01,"MD01/D");
   tree_out->Branch("MD02",&MD02,"MD02/D");
   tree_out->Branch("MD03",&MD03,"MD03/D");
   tree_out->Branch("cosThetaTau",&cosThetaTau,"cosThetaTau/D");
   tree_out->Branch("cosThetaTauDst",&cosThetaTauDst,"cosThetaTauDst/D");
   tree_out->Branch("cosThetaTau11",&cosThetaTau11,"cosThetaTau11/D");
   tree_out->Branch("cosThetaTau12",&cosThetaTau12,"cosThetaTau12/D");
   tree_out->Branch("cosThetaTau21",&cosThetaTau21,"cosThetaTau21/D");
   tree_out->Branch("cosThetaTau22",&cosThetaTau22,"cosThetaTau22/D");
   tree_out->Branch("TauCTAU",&TauCTAU,"TauCTAU/D");
   tree_out->Branch("TauCTAUsm2016",&TauCTAUsm2016,"TauCTAUsm2016/D");
   tree_out->Branch("TauCTAUsm2017",&TauCTAUsm2017,"TauCTAUsm2017/D");
   tree_out->Branch("TauCTAUsm2018",&TauCTAUsm2018,"TauCTAUsm2018/D");
   tree_out->Branch("TauCTAUsmAll",&TauCTAUsmAll,"TauCTAUsmAll/D");
   tree_out->Branch("TauCTAUsmComb",&TauCTAUsmComb,"TauCTAUsmComb/D");
   tree_out->Branch("TauCTAU1",&TauCTAU1,"TauCTAU1/D");
   tree_out->Branch("TauCTAU2",&TauCTAU2,"TauCTAU2/D");
   tree_out->Branch("cosTau3pi",&cosTau3pi,"cosTau3pi/D");
   tree_out->Branch("cosTau3pi2",&cosTau3pi2,"cosTau3pi2/D");
   tree_out->Branch("cos01",&cos01,"cos01/D");
   tree_out->Branch("cos12",&cos12,"cos12/D");
   tree_out->Branch("cos2pimin",&cos2pimin,"cos2pimin/D");
   tree_out->Branch("cos2pimax",&cos2pimax,"cos2pimax/D");
   tree_out->Branch("thetamax",&thetamax,"thetamax/D");
   tree_out->Branch("thetatau",&thetatau,"thetatau/D");
   tree_out->Branch("thetaB",&thetaB,"thetaB/D");
   tree_out->Branch("thetaBmax",&thetaBmax,"thetaBmax/D");
   tree_out->Branch("m01",&M01,"m01/D");
   tree_out->Branch("m12",&M12,"m12/D");
   tree_out->Branch("m02",&M02,"m02/D");
   //tree_out->Branch("minChi2TauIsosig",&minChi2TauIsosig,"minChi2TauIsosig/D");
   tree_out->Branch("minChi2TauIsosig0",&minChi2TauIsosig0,"minChi2TauIsosig0/D");
   tree_out->Branch("minChi2TauIsosig1",&minChi2TauIsosig1,"minChi2TauIsosig1/D");
   //tree_out->Branch("minDOCAVXTauIsosig",&minDOCAVXTauIsosig,"minDOCAVXTauIsosig/D");
   tree_out->Branch("minDOCAVXTauIsosig0",&minDOCAVXTauIsosig0,"minDOCAVXTauIsosig0/D");
   tree_out->Branch("minDOCAVXTauIsosig1",&minDOCAVXTauIsosig1,"minDOCAVXTauIsosig1/D");
   tree_out->Branch("minDOCAVXTauIsoAll0",&minDOCAVXTauIsoAll0,"minDOCAVXTauIsoAll0/D");
   tree_out->Branch("minDOCAVXTauIsoAll1",&minDOCAVXTauIsoAll1,"minDOCAVXTauIsoAll1/D");
   //tree_out->Branch("nTauIso9sig",&nTauIso9sig,"nTauIso9sig/I");
   //tree_out->Branch("nTauIso5sig",&nTauIso5sig,"nTauIso5sig/I");
   //tree_out->Branch("nTauIso25sig",&nTauIso25sig,"nTauIso25sig/I");
   tree_out->Branch("nTauIso5sig0",&nTauIso5sig0,"nTauIso5sig0/I");
   tree_out->Branch("nTauIso9sig0",&nTauIso9sig0,"nTauIso9sig0/I");
   tree_out->Branch("nBTauIso25sig0",&nBTauIso25sig0,"nBTauIso25sig0/I");
   tree_out->Branch("nBTauIso25sig0_tau",&nBTauIso25sig0_tau,"nBTauIso25sig0_tau/I");
   tree_out->Branch("nBTauIso25sig0_B",&nBTauIso25sig0_B,"nBTauIso25sig0_B/I");
   tree_out->Branch("nBTauIso25sig0_Dst",&nBTauIso25sig0_Dst,"nBTauIso25sig0_Dst/I");
   tree_out->Branch("nBTauIso25sig0_Dst_WS",&nBTauIso25sig0_Dst_WS,"nBTauIso25sig0_Dst_WS/I");
   tree_out->Branch("nTauIso25sig0",&nTauIso25sig0,"nTauIso25sig0/I");
   tree_out->Branch("nTauIso25sig0_pi",&nTauIso25sig0_pi,"nTauIso25sig0_pi/I");
   tree_out->Branch("nTauIso25sig0_e",&nTauIso25sig0_e,"nTauIso25sig0_e/I");
   tree_out->Branch("nTauIso25sig0_mu",&nTauIso25sig0_mu,"nTauIso25sig0_mu/I");
   tree_out->Branch("nTauIso5sig0_Dst",&nTauIso5sig0_Dst,"nTauIso5sig0_Dst/I");
   tree_out->Branch("nTauIso9sig0_Dst",&nTauIso9sig0_Dst,"nTauIso9sig0_Dst/I");
   tree_out->Branch("nTauIso25sig0_Dst",&nTauIso25sig0_Dst,"nTauIso25sig0_Dst/I");
   tree_out->Branch("nTauIso5sig0_Dst_WS",&nTauIso5sig0_Dst_WS,"nTauIso5sig0_Dst_WS/I");
   tree_out->Branch("nTauIso9sig0_Dst_WS",&nTauIso9sig0_Dst_WS,"nTauIso9sig0_Dst_WS/I");
   tree_out->Branch("nTauIso25sig0_Dst_WS",&nTauIso25sig0_Dst_WS,"nTauIso25sig0_Dst_WS/I");
   tree_out->Branch("nTauIso5sig1",&nTauIso5sig1,"nTauIso5sig1/I");
   tree_out->Branch("nTauIso9sig1",&nTauIso9sig1,"nTauIso9sig1/I");
   tree_out->Branch("nTauIso25sig1",&nTauIso25sig1,"nTauIso25sig1/I");
   //tree_out->Branch("nDIso9sig",&nDIso9sig,"nDIso9sig/I");
   //tree_out->Branch("nDIso5sig",&nDIso5sig,"nDIso5sig/I");
   //tree_out->Branch("nDIso25sig",&nDIso25sig,"nDIso25sig/I");
   //tree_out->Branch("minChi2DIsosig",&minChi2DIsosig,"minChi2DIsosig/D");
   tree_out->Branch("minChi2DIsosig0",&minChi2DIsosig0,"minChi2DIsosig0/D");
   tree_out->Branch("minChi2DIsosig1",&minChi2DIsosig1,"minChi2DIsosig1/D");
   //tree_out->Branch("minDOCAVXDIsosig",&minDOCAVXDIsosig,"minDOCAVXDIsosig/D");
   tree_out->Branch("minDOCAVXDIsosig0",&minDOCAVXDIsosig0,"minDOCAVXDIsosig0/D");
   tree_out->Branch("minDOCAVXDIsosig1",&minDOCAVXDIsosig1,"minDOCAVXDIsosig1/D");
   tree_out->Branch("nDIso5sig0",&nDIso5sig0,"nDIso5sig0/I");
   tree_out->Branch("nDIso9sig0",&nDIso9sig0,"nDIso9sig0/I");
   tree_out->Branch("nDIso25sig0",&nDIso25sig0,"nDIso25sig0/I");
   tree_out->Branch("nDIso5sig1",&nDIso5sig1,"nDIso5sig1/I");
   tree_out->Branch("nDIso9sig1",&nDIso9sig1,"nDIso9sig1/I");
   tree_out->Branch("nDIso25sig1",&nDIso25sig1,"nDIso25sig1/I");
   tree_out->Branch("minChi2BIsosig",&minChi2BIsosig,"minChi2BIsosig/D");
   tree_out->Branch("minDOCAVXBIsosig",&minDOCAVXBIsosig,"minDOCAVXBIsosig/D");
   tree_out->Branch("minDOCAVXBIsoAll",&minDOCAVXBIsoAll,"minDOCAVXBIsoAll/D");
   tree_out->Branch("nB0Iso5sig",&nB0Iso5sig,"nB0Iso5sig/I");
   tree_out->Branch("nB0Iso9sig",&nB0Iso9sig,"nB0Iso9sig/I");
   tree_out->Branch("nB0Iso25sig",&nB0Iso25sig,"nB0Iso25sig/I");
   tree_out->Branch("nB0Iso25sig_pi",&nB0Iso25sig_pi,"nB0Iso25sig_pi/I");
   tree_out->Branch("nB0Iso25sig_e",&nB0Iso25sig_e,"nB0Iso25sig_e/I");
   tree_out->Branch("nB0Iso25sig_mu",&nB0Iso25sig_mu,"nB0Iso25sig_mu/I");
   tree_out->Branch("nB0Iso5sig_Dst",&nB0Iso5sig_Dst,"nB0Iso5sig_Dst/I");
   tree_out->Branch("nB0Iso9sig_Dst",&nB0Iso9sig_Dst,"nB0Iso9sig_Dst/I");
   tree_out->Branch("nB0Iso25sig_Dst",&nB0Iso25sig_Dst,"nB0Iso25sig_Dst/I");
   tree_out->Branch("nB0Iso5sig_Dst_WS",&nB0Iso5sig_Dst_WS,"nB0Iso5sig_Dst_WS/I");
   tree_out->Branch("nB0Iso9sig_Dst_WS",&nB0Iso9sig_Dst_WS,"nB0Iso9sig_Dst_WS/I");
   tree_out->Branch("nB0Iso25sig_Dst_WS",&nB0Iso25sig_Dst_WS,"nB0Iso25sig_Dst_WS/I");
   tree_out->Branch("M2piK",&M2piK,"M2piK/D");
   tree_out->Branch("BM2piK",&BM2piK,"BM2piK/D");
   tree_out->Branch("M2piKmax",&M2piKmax,"M2piKmax/D");
   tree_out->Branch("M2Kpi",&M2Kpi,"M2Kpi/D");
   tree_out->Branch("DRD0tau",&DRD0tau,"DRD0tau/D");

   // Control variables
   double m_4pi, m_5pi, m_7pi, m_3piK, m_3pi2K, m_2K;
   tree_out->Branch("m_4pi",&m_4pi,"m_4pi/D");
   tree_out->Branch("m_5pi",&m_5pi,"m_5pi/D");
   tree_out->Branch("m_7pi",&m_7pi,"m_7pi/D");
   tree_out->Branch("m_3piK",&m_3piK,"m_3piK/D");
   tree_out->Branch("m_3pi2K",&m_3pi2K,"m_3pi2K/D");
   tree_out->Branch("m_2K",&m_2K,"m_2K/D");

   double m_3pi_gammaMax00, m_3pi_gammaSec00, m_3pi_gammaThi00;
   double m_B_gammaMax00, m_B_gammaSec00, m_B_gammaThi00;
   double m_01_gammaMax00, m_12_gammaMax00, m_02_gammaMax00;
   double m_01_gammaSec00, m_12_gammaSec00, m_02_gammaSec00;
   double m_01_gammaThi00, m_12_gammaThi00, m_02_gammaThi00;
   double m_2g_00, m_B_2g_00, m_3pi_2g_00, m_01_2g_00, m_12_2g_00;
   tree_out->Branch("m_B_gammaMax00",&m_B_gammaMax00,"m_B_gammaMax00/D");
   tree_out->Branch("m_B_gammaSec00",&m_B_gammaSec00,"m_B_gammaSec00/D");
   tree_out->Branch("m_B_gammaThi00",&m_B_gammaThi00,"m_B_gammaThi00/D");
   tree_out->Branch("m_3pi_gammaMax00",&m_3pi_gammaMax00,"m_3pi_gammaMax00/D");
   tree_out->Branch("m_3pi_gammaSec00",&m_3pi_gammaSec00,"m_3pi_gammaSec00/D");
   tree_out->Branch("m_3pi_gammaThi00",&m_3pi_gammaThi00,"m_3pi_gammaThi00/D");
   tree_out->Branch("m_01_gammaMax00",&m_01_gammaMax00,"m_01_gammaMax00/D");
   tree_out->Branch("m_01_gammaSec00",&m_01_gammaSec00,"m_01_gammaSec00/D");
   tree_out->Branch("m_01_gammaThi00",&m_01_gammaThi00,"m_01_gammaThi00/D");
   tree_out->Branch("m_12_gammaMax00",&m_12_gammaMax00,"m_12_gammaMax00/D");
   tree_out->Branch("m_12_gammaSec00",&m_12_gammaSec00,"m_12_gammaSec00/D");
   tree_out->Branch("m_12_gammaThi00",&m_12_gammaThi00,"m_12_gammaThi00/D");
   tree_out->Branch("m_02_gammaMax00",&m_02_gammaMax00,"m_02_gammaMax00/D");
   tree_out->Branch("m_02_gammaSec00",&m_02_gammaSec00,"m_02_gammaSec00/D");
   tree_out->Branch("m_02_gammaThi00",&m_02_gammaThi00,"m_02_gammaThi00/D");
   tree_out->Branch("m_2g_00",&m_2g_00,"m_2g_00/D");
   tree_out->Branch("m_B_2g_00",&m_B_2g_00,"m_B_2g_00/D");
   tree_out->Branch("m_3pi_2g_00",&m_3pi_2g_00,"m_3pi_2g_00/D");
   tree_out->Branch("m_01_2g_00",&m_01_2g_00,"m_01_2g_00/D");
   tree_out->Branch("m_12_2g_00",&m_12_2g_00,"m_12_2g_00/D");

   double m_3pi_gammaMax05, m_3pi_gammaSec05, m_3pi_gammaThi05;
   double m_B_gammaMax05, m_B_gammaSec05, m_B_gammaThi05;
   double m_01_gammaMax05, m_12_gammaMax05, m_02_gammaMax05;
   double m_01_gammaSec05, m_12_gammaSec05, m_02_gammaSec05;
   double m_01_gammaThi05, m_12_gammaThi05, m_02_gammaThi05;
   double m_2g_05, m_B_2g_05, m_3pi_2g_05, m_01_2g_05, m_12_2g_05;
   tree_out->Branch("m_B_gammaMax05",&m_B_gammaMax05,"m_B_gammaMax05/D");
   tree_out->Branch("m_B_gammaSec05",&m_B_gammaSec05,"m_B_gammaSec05/D");
   tree_out->Branch("m_B_gammaThi05",&m_B_gammaThi05,"m_B_gammaThi05/D");
   tree_out->Branch("m_3pi_gammaMax05",&m_3pi_gammaMax05,"m_3pi_gammaMax05/D");
   tree_out->Branch("m_3pi_gammaSec05",&m_3pi_gammaSec05,"m_3pi_gammaSec05/D");
   tree_out->Branch("m_3pi_gammaThi05",&m_3pi_gammaThi05,"m_3pi_gammaThi05/D");
   tree_out->Branch("m_01_gammaMax05",&m_01_gammaMax05,"m_01_gammaMax05/D");
   tree_out->Branch("m_01_gammaSec05",&m_01_gammaSec05,"m_01_gammaSec05/D");
   tree_out->Branch("m_01_gammaThi05",&m_01_gammaThi05,"m_01_gammaThi05/D");
   tree_out->Branch("m_12_gammaMax05",&m_12_gammaMax05,"m_12_gammaMax05/D");
   tree_out->Branch("m_12_gammaSec05",&m_12_gammaSec05,"m_12_gammaSec05/D");
   tree_out->Branch("m_12_gammaThi05",&m_12_gammaThi05,"m_12_gammaThi05/D");
   tree_out->Branch("m_02_gammaMax05",&m_02_gammaMax05,"m_02_gammaMax05/D");
   tree_out->Branch("m_02_gammaSec05",&m_02_gammaSec05,"m_02_gammaSec05/D");
   tree_out->Branch("m_02_gammaThi05",&m_02_gammaThi05,"m_02_gammaThi05/D");
   tree_out->Branch("m_2g_05",&m_2g_05,"m_2g_05/D");
   tree_out->Branch("m_B_2g_05",&m_B_2g_05,"m_B_2g_05/D");
   tree_out->Branch("m_3pi_2g_05",&m_3pi_2g_05,"m_3pi_2g_05/D");
   tree_out->Branch("m_01_2g_05",&m_01_2g_05,"m_01_2g_05/D");
   tree_out->Branch("m_12_2g_05",&m_12_2g_05,"m_12_2g_05/D");


   double m_B_pi0Max00, m_B_pi0Sec00, m_B_pi0Thi00;
   double m_3pi_pi0Max00, m_3pi_pi0Sec00, m_3pi_pi0Thi00;
   double m_01_pi0Max00, m_12_pi0Max00, m_02_pi0Max00;
   double m_01_pi0Sec00, m_12_pi0Sec00, m_02_pi0Sec00;
   double m_01_pi0Thi00, m_12_pi0Thi00, m_02_pi0Thi00;
   double m_3pi_2pi0_00;
   double m_2pi0_00, m_B_2pi0_00;
   tree_out->Branch("m_B_pi0Max00",&m_B_pi0Max00,"m_B_pi0Max00/D");
   tree_out->Branch("m_B_pi0Sec00",&m_B_pi0Sec00,"m_B_pi0Sec00/D");
   tree_out->Branch("m_B_pi0Thi00",&m_B_pi0Thi00,"m_B_pi0Thi00/D");
   tree_out->Branch("m_3pi_pi0Max00",&m_3pi_pi0Max00,"m_3pi_pi0Max00/D");
   tree_out->Branch("m_3pi_pi0Sec00",&m_3pi_pi0Sec00,"m_3pi_pi0Sec00/D");
   tree_out->Branch("m_3pi_pi0Thi00",&m_3pi_pi0Thi00,"m_3pi_pi0Thi00/D");
   tree_out->Branch("m_01_pi0Max00",&m_01_pi0Max00,"m_01_pi0Max00/D");
   tree_out->Branch("m_01_pi0Sec00",&m_01_pi0Sec00,"m_01_pi0Sec00/D");
   tree_out->Branch("m_01_pi0Thi00",&m_01_pi0Thi00,"m_01_pi0Thi00/D");
   tree_out->Branch("m_12_pi0Max00",&m_12_pi0Max00,"m_12_pi0Max00/D");
   tree_out->Branch("m_12_pi0Sec00",&m_12_pi0Sec00,"m_12_pi0Sec00/D");
   tree_out->Branch("m_12_pi0Thi00",&m_12_pi0Thi00,"m_12_pi0Thi00/D");
   tree_out->Branch("m_02_pi0Max00",&m_02_pi0Max00,"m_02_pi0Max00/D");
   tree_out->Branch("m_02_pi0Sec00",&m_02_pi0Sec00,"m_02_pi0Sec00/D");
   tree_out->Branch("m_02_pi0Thi00",&m_02_pi0Thi00,"m_02_pi0Thi00/D");
   tree_out->Branch("m_B_2pi0_00",&m_B_2pi0_00,"m_B_2pi0_00/D");
   tree_out->Branch("m_3pi_2pi0_00",&m_3pi_2pi0_00,"m_3pi_2pi0_00/D");
   tree_out->Branch("m_2pi0_00",&m_2pi0_00,"m_2pi0_00/D");

   double m_B_pi0Max05, m_B_pi0Sec05, m_B_pi0Thi05;
   double m_3pi_pi0Max05, m_3pi_pi0Sec05, m_3pi_pi0Thi05;
   double m_01_pi0Max05, m_12_pi0Max05, m_02_pi0Max05;
   double m_01_pi0Sec05, m_12_pi0Sec05, m_02_pi0Sec05;
   double m_01_pi0Thi05, m_12_pi0Thi05, m_02_pi0Thi05;
   double m_3pi_2pi0_05;
   double m_2pi0_05, m_B_2pi0_05;
   tree_out->Branch("m_B_pi0Max05",&m_B_pi0Max05,"m_B_pi0Max05/D");
   tree_out->Branch("m_B_pi0Sec05",&m_B_pi0Sec05,"m_B_pi0Sec05/D");
   tree_out->Branch("m_B_pi0Thi05",&m_B_pi0Thi05,"m_B_pi0Thi05/D");
   tree_out->Branch("m_3pi_pi0Max05",&m_3pi_pi0Max05,"m_3pi_pi0Max05/D");
   tree_out->Branch("m_3pi_pi0Sec05",&m_3pi_pi0Sec05,"m_3pi_pi0Sec05/D");
   tree_out->Branch("m_3pi_pi0Thi05",&m_3pi_pi0Thi05,"m_3pi_pi0Thi05/D");
   tree_out->Branch("m_01_pi0Max05",&m_01_pi0Max05,"m_01_pi0Max05/D");
   tree_out->Branch("m_01_pi0Sec05",&m_01_pi0Sec05,"m_01_pi0Sec05/D");
   tree_out->Branch("m_01_pi0Thi05",&m_01_pi0Thi05,"m_01_pi0Thi05/D");
   tree_out->Branch("m_12_pi0Max05",&m_12_pi0Max05,"m_12_pi0Max05/D");
   tree_out->Branch("m_12_pi0Sec05",&m_12_pi0Sec05,"m_12_pi0Sec05/D");
   tree_out->Branch("m_12_pi0Thi05",&m_12_pi0Thi05,"m_12_pi0Thi05/D");
   tree_out->Branch("m_02_pi0Max05",&m_02_pi0Max05,"m_02_pi0Max05/D");
   tree_out->Branch("m_02_pi0Sec05",&m_02_pi0Sec05,"m_02_pi0Sec05/D");
   tree_out->Branch("m_02_pi0Thi05",&m_02_pi0Thi05,"m_02_pi0Thi05/D");
   tree_out->Branch("m_B_2pi0_05",&m_B_2pi0_05,"m_B_2pi0_05/D");
   tree_out->Branch("m_3pi_2pi0_05",&m_3pi_2pi0_05,"m_3pi_2pi0_05/D");
   tree_out->Branch("m_2pi0_05",&m_2pi0_05,"m_2pi0_05/D");

   double m_B_etaMax00, m_B_etaSec00, m_B_etaThi00;
   double m_3pi_etaMax00, m_3pi_etaSec00, m_3pi_etaThi00;
   double m_01_etaMax00, m_12_etaMax00, m_02_etaMax00;
   double m_01_etaSec00, m_12_etaSec00, m_02_etaSec00;
   double m_01_etaThi00, m_12_etaThi00, m_02_etaThi00;
   tree_out->Branch("m_B_etaMax00",&m_B_etaMax00,"m_B_etaMax00/D");
   tree_out->Branch("m_B_etaSec00",&m_B_etaSec00,"m_B_etaSec00/D");
   tree_out->Branch("m_B_etaThi00",&m_B_etaThi00,"m_B_etaThi00/D");
   tree_out->Branch("m_3pi_etaMax00",&m_3pi_etaMax00,"m_3pi_etaMax00/D");
   tree_out->Branch("m_3pi_etaSec00",&m_3pi_etaSec00,"m_3pi_etaSec00/D");
   tree_out->Branch("m_3pi_etaThi00",&m_3pi_etaThi00,"m_3pi_etaThi00/D");
   tree_out->Branch("m_01_etaMax00",&m_01_etaMax00,"m_01_etaMax00/D");
   tree_out->Branch("m_01_etaSec00",&m_01_etaSec00,"m_01_etaSec00/D");
   tree_out->Branch("m_01_etaThi00",&m_01_etaThi00,"m_01_etaThi00/D");
   tree_out->Branch("m_12_etaMax00",&m_12_etaMax00,"m_12_etaMax00/D");
   tree_out->Branch("m_12_etaSec00",&m_12_etaSec00,"m_12_etaSec00/D");
   tree_out->Branch("m_12_etaThi00",&m_12_etaThi00,"m_12_etaThi00/D");
   tree_out->Branch("m_02_etaMax00",&m_02_etaMax00,"m_02_etaMax00/D");
   tree_out->Branch("m_02_etaSec00",&m_02_etaSec00,"m_02_etaSec00/D");
   tree_out->Branch("m_02_etaThi00",&m_02_etaThi00,"m_02_etaThi00/D");

   double m_B_etaMax05, m_B_etaSec05, m_B_etaThi05;
   double m_3pi_etaMax05, m_3pi_etaSec05, m_3pi_etaThi05;
   double m_01_etaMax05, m_12_etaMax05, m_02_etaMax05;
   double m_01_etaSec05, m_12_etaSec05, m_02_etaSec05;
   double m_01_etaThi05, m_12_etaThi05, m_02_etaThi05;
   tree_out->Branch("m_B_etaMax05",&m_B_etaMax05,"m_B_etaMax05/D");
   tree_out->Branch("m_B_etaSec05",&m_B_etaSec05,"m_B_etaSec05/D");
   tree_out->Branch("m_B_etaThi05",&m_B_etaThi05,"m_B_etaThi05/D");
   tree_out->Branch("m_3pi_etaMax05",&m_3pi_etaMax05,"m_3pi_etaMax05/D");
   tree_out->Branch("m_3pi_etaSec05",&m_3pi_etaSec05,"m_3pi_etaSec05/D");
   tree_out->Branch("m_3pi_etaThi05",&m_3pi_etaThi05,"m_3pi_etaThi05/D");
   tree_out->Branch("m_01_etaMax05",&m_01_etaMax05,"m_01_etaMax05/D");
   tree_out->Branch("m_01_etaSec05",&m_01_etaSec05,"m_01_etaSec05/D");
   tree_out->Branch("m_01_etaThi05",&m_01_etaThi05,"m_01_etaThi05/D");
   tree_out->Branch("m_12_etaMax05",&m_12_etaMax05,"m_12_etaMax05/D");
   tree_out->Branch("m_12_etaSec05",&m_12_etaSec05,"m_12_etaSec05/D");
   tree_out->Branch("m_12_etaThi05",&m_12_etaThi05,"m_12_etaThi05/D");
   tree_out->Branch("m_02_etaMax05",&m_02_etaMax05,"m_02_etaMax05/D");
   tree_out->Branch("m_02_etaSec05",&m_02_etaSec05,"m_02_etaSec05/D");
   tree_out->Branch("m_02_etaThi05",&m_02_etaThi05,"m_02_etaThi05/D");

   double zSmear2016, zSmear2017, zSmear2018, zSmearAll, zSmearComb;
   double DzSmear2016, DzSmear2017, DzSmear2018, DzSmearAll, DzSmearComb;
   double tauErrZRot, BErrZRot;
   tree_out->Branch("zSmear2016",&zSmear2016,"zSmear2016/D");
   tree_out->Branch("zSmear2017",&zSmear2017,"zSmear2017/D");
   tree_out->Branch("zSmear2018",&zSmear2018,"zSmear2018/D");
   tree_out->Branch("zSmearAll",&zSmearAll,"zSmearAll/D");
   tree_out->Branch("zSmearComb",&zSmearComb,"zSmearComb/D");
   tree_out->Branch("DzSmear2016",&DzSmear2016,"DzSmear2016/D");
   tree_out->Branch("DzSmear2017",&DzSmear2017,"DzSmear2017/D");
   tree_out->Branch("DzSmear2018",&DzSmear2018,"DzSmear2018/D");
   tree_out->Branch("DzSmearAll",&DzSmearAll,"DzSmearAll/D");
   tree_out->Branch("DzSmearComb",&DzSmearComb,"DzSmearComb/D");
   tree_out->Branch("tauErrZRot",&tauErrZRot,"tauErrZRot/D");
   tree_out->Branch("BErrZRot",&BErrZRot,"BErrZRot/D");

   std::string DsDecay;
   int DsDecayCode;
   int D0DecayCode;
   int DpDecayCode;
   int BpDecayCode;
   int BdDecayCode;
   int BsDecayCode;
   int B2DsDecayCode;
   int B2D0DecayCode;
   int B2DpDecayCode;
   int B2tauDecayCode;
   int tauDecayCode;
   int nK0fromB;
   int nKSfromB;
   int nKLfromB;
   double wgDstau;
   double wgDs2eta3pi_shape;
   double wg_D0_K_PID, wg_D0_pi_PID, wg_tau_pion0_PID, wg_tau_pion1_PID, wg_tau_pion2_PID, wg_PID;
   double wg_D0_K_Track, wg_D0_pi_Track, wg_tau_pion0_Track, wg_tau_pion1_Track, wg_tau_pion2_Track, wg_Track;

   if (MonteCarlo) {
   
     //tree_out->Branch("DsDecay",&DsDecay,"DsDecay/C");
     tree_out->Branch("tauDecayCode",&tauDecayCode,"tauDecayCode/I");
     tree_out->Branch("wgDstau",&wgDstau,"wgDstau/D");
     tree_out->Branch("wgDs2eta3pi_shape",&wgDs2eta3pi_shape,"wgDs2eta3pi_shape/D");
     tree_out->Branch("wg_D0_K_PID",&wg_D0_K_PID,"wg_D0_K_PID/D");
     tree_out->Branch("wg_D0_pi_PID",&wg_D0_pi_PID,"wg_D0_pi_PID/D");
     tree_out->Branch("wg_tau_pion0_PID",&wg_tau_pion0_PID,"wg_tau_pion0_PID/D");
     tree_out->Branch("wg_tau_pion1_PID",&wg_tau_pion1_PID,"wg_tau_pion1_PID/D");
     tree_out->Branch("wg_tau_pion2_PID",&wg_tau_pion2_PID,"wg_tau_pion2_PID/D");
     tree_out->Branch("wg_PID",&wg_PID,"wg_PID/D");
     tree_out->Branch("wg_D0_K_Track",&wg_D0_K_Track,"wg_D0_K_Track/D");
     tree_out->Branch("wg_D0_pi_Track",&wg_D0_pi_Track,"wg_D0_pi_Track/D");
     tree_out->Branch("wg_tau_pion0_Track",&wg_tau_pion0_Track,"wg_tau_pion0_Track/D");
     tree_out->Branch("wg_tau_pion1_Track",&wg_tau_pion1_Track,"wg_tau_pion1_Track/D");
     tree_out->Branch("wg_tau_pion2_Track",&wg_tau_pion2_Track,"wg_tau_pion2_Track/D");
     tree_out->Branch("wg_Track",&wg_Track,"wg_Track/D");
     tree_out->Branch("BpDecayCode",&BpDecayCode,"BpDecayCode/I");
     tree_out->Branch("BdDecayCode",&BdDecayCode,"BdDecayCode/I");
     tree_out->Branch("BsDecayCode",&BsDecayCode,"BsDecayCode/I");
     tree_out->Branch("B2DsDecayCode",&B2DsDecayCode,"B2DsDecayCode/I");
     tree_out->Branch("B2D0DecayCode",&B2D0DecayCode,"B2D0DecayCode/I");
     tree_out->Branch("B2DpDecayCode",&B2DpDecayCode,"B2DpDecayCode/I");
     tree_out->Branch("B2tauDecayCode",&B2tauDecayCode,"B2tauDecayCode/I");
     tree_out->Branch("DsDecayCode",&DsDecayCode,"DsDecayCode/I");
     tree_out->Branch("D0DecayCode",&D0DecayCode,"D0DecayCode/I");
     tree_out->Branch("DpDecayCode",&DpDecayCode,"DpDecayCode/I");
     tree_out->Branch("nK0fromB",&nK0fromB,"nK0fromB/I");
     tree_out->Branch("nKSfromB",&nKSfromB,"nKSfromB/I");
     tree_out->Branch("nKLfromB",&nKLfromB,"nKLfromB/I");
     tree_out->Branch("thetatauMC",&thetatauMC,"thetatauMC/D");
     tree_out->Branch("cosTheta1MC",&cosTheta1MC,"cosTheta1MC/D");
     tree_out->Branch("cosTheta1BMC",&cosTheta1BMC,"cosTheta1BMC/D");
     tree_out->Branch("MWMC",&MWMC,"MWMC/D");
     tree_out->Branch("q2MC",&q2MC,"q2MC/D");
     tree_out->Branch("q2DstMC",&q2DstMC,"q2DstMC/D");
     tree_out->Branch("MLostMC",&MLostMC,"MLostMC/D");
     tree_out->Branch("cosTau3piMC",&cosTau3piMC,"cosTau3piMC/D");
     tree_out->Branch("cosTau3pi2MC",&cosTau3pi2MC,"cosTau3pi2MC/D");
     tree_out->Branch("cosThetaTauMC",&cosThetaTauMC,"cosThetaTauMC/D");
     tree_out->Branch("cosThetaTauDstMC",&cosThetaTauDstMC,"cosThetaTauDstMC/D");
     tree_out->Branch("tauPHIMC",&tauPHIMC,"tauPHIMC/D");
     tree_out->Branch("pTTauMC",&pTTauMC,"pTTauMC/D");
     tree_out->Branch("pTTauBMC",&pTTauBMC,"pTTauBMC/D");
     tree_out->Branch("pTD0MC",&pTD0MC,"pTD0MC/D");
   }


   TLorentzVector p3pi2;
   TLorentzVector pBcmsDst;
   TLorentzVector pBcmsW;
   TLorentzVector ptaucmsW;
   TLorentzVector ptaucmsDst;
   TLorentzVector p_D_K;
   TLorentzVector p_D_pi;

   nCandDst = -1;
   nCandSel = -1;

//   for (Long64_t ievt=0; ievt<min(10000,(int) tree->GetEntries()); ievt++) {
   for (Long64_t ievt=0; ievt<tree->GetEntries(); ievt++) {

      if (ievt%10000 == 0) std::cout << "--- ... Processing event: " << ievt << std::endl;

      tree->GetEntry(ievt);

      if (nCandidate==0) {
        nCandDst = -1;
        nCandSel = -1;
      }

      //double xDz1 = (tau_ENDVERTEX_Z-tau_OWNPV_Z)/sqrt(tau_ENDVERTEX_ZERR*tau_ENDVERTEX_ZERR+tau_OWNPV_ZERR*tau_OWNPV_ZERR);
      //if (xDz1<10) continue;
      //if (totCandidates != 1) continue;
//      if (nCandidate != 0) continue;
//      if (tau_ENDVERTEX_CHI2>10) continue;

      //double Dz2 = (tau_ENDVERTEX_Z-B_ENDVERTEX_Z)/sqrt(B_ENDVERTEX_ZERR*B_ENDVERTEX_ZERR+tau_ENDVERTEX_ZERR*tau_ENDVERTEX_ZERR);
      //if(Dz2<4) continue;

      double BRD = sqrt( (B_ENDVERTEX_X-B_OWNPV_X)*(B_ENDVERTEX_X-B_OWNPV_X)+(B_ENDVERTEX_Y-B_OWNPV_Y)*(B_ENDVERTEX_Y-B_OWNPV_Y));
      double tauRD = sqrt( (tau_ENDVERTEX_X-tau_OWNPV_X)*(tau_ENDVERTEX_X-tau_OWNPV_X)+(tau_ENDVERTEX_Y-tau_OWNPV_Y)*(tau_ENDVERTEX_Y-tau_OWNPV_Y));

      if (tauRD<0.2) continue;
      if (BRD<0.6) continue;

      if (tau_pion0_ProbNNpi<0.2) continue;
      if (tau_pion1_ProbNNpi<0.2) continue;
      if (tau_pion2_ProbNNpi<0.2) continue;

      //if (D_IPCHI2_OWNPV<10.) continue;

      //if (D_OWNPV_Z != tau_OWNPV_Z) continue;
      if (abs(D_OWNPV_Z-tau_OWNPV_Z)>0.0001) continue;

      double minIP = min(tau_pion0_IPCHI2_OWNPV,min(tau_pion1_IPCHI2_OWNPV,tau_pion2_IPCHI2_OWNPV));

      if (minIP<15.) continue;

      if(D_K_ProbNNk<0.4) continue;
      if(D_pi_ProbNNpi<0.4) continue;

      if(nSPDHits>450) continue;
      //if(nTracks>=500) continue;

      nCandSel ++;

      // Setting up 4-momenta

      TLorentzVector pD0_D_M; pD0_D_M.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      TLorentzVector pD0; pD0.SetXYZM(D_PX,D_PY,D_PZ,mD0);
      //TLorentzVector pB; pB.SetXYZM(B_PX,B_PY,B_PZ,B_M);
      TLorentzVector pB; pB.SetXYZM(B_PX,B_PY,B_PZ,B_M-D_M+mD0);
      TLorentzVector ptau; ptau.SetXYZM(tau_PX,tau_PY,tau_PZ,tau_M);
      TLorentzVector ptau_pion0; ptau_pion0.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mpi);
      TLorentzVector ptau_pion1; ptau_pion1.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mpi);
      TLorentzVector ptau_pion2; ptau_pion2.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mpi);

      //if (ptau_pion0.P()>100000.) continue;
      //if (ptau_pion1.P()>100000.) continue;
      //if (ptau_pion2.P()>100000.) continue;
      if (tau_pion0_P<=2000. || tau_pion0_P>=200000.) continue;
      if (tau_pion1_P<=2000. || tau_pion1_P>=200000.) continue;
      if (tau_pion2_P<=2000. || tau_pion2_P>=200000.) continue;
      if (D_K_P<=2000. || D_K_P>=200000.) continue;
      if (D_pi_P<=2000. || D_pi_P>=200000.) continue;

      if (MonteCarlo) {
        Year = YearMC;
      } else {
        Year = getYear(runNumber);
      }

      tau_pion0_eta = TMath::ATanH(tau_pion0_PZ/tau_pion0_P);
      tau_pion1_eta = TMath::ATanH(tau_pion1_PZ/tau_pion1_P);
      tau_pion2_eta = TMath::ATanH(tau_pion2_PZ/tau_pion2_P);
      D0_K_eta = TMath::ATanH(D_K_PZ/D_K_P);
      D0_pi_eta = TMath::ATanH(D_pi_PZ/D_pi_P);
      D0_eta = TMath::ATanH(D_PZ/D_P);
      tau_eta = TMath::ATanH(tau_PZ/tau_P);
      B_eta = TMath::ATanH(B_PZ/B_P);

      if (D0_K_eta<=1.5 || D0_K_eta>=5) continue;
      if (D0_pi_eta<=1.5 || D0_pi_eta>=5) continue;
      if (tau_pion0_eta<=1.5 || tau_pion0_eta>=5) continue;
      if (tau_pion1_eta<=1.5 || tau_pion1_eta>=5) continue;
      if (tau_pion2_eta<=1.5 || tau_pion2_eta>=5) continue;

      // Invariant mass M(D0 pi+)
      
      MD0pi0 = (pD0+ptau_pion0).M();
      MD0pi1 = (pD0+ptau_pion1).M();
      MD0pi2 = (pD0+ptau_pion2).M();

      // D0 lifetime:

      double D0betasc = pD0.P()/pD0.E();
      double D0gammasc = 1./sqrt(1.-D0betasc*D0betasc);
      double D0Lsc = sqrt((D_ENDVERTEX_X-B_ENDVERTEX_X)*(D_ENDVERTEX_X-B_ENDVERTEX_X)+(D_ENDVERTEX_Y-B_ENDVERTEX_Y)*(D_ENDVERTEX_Y-B_ENDVERTEX_Y)+(D_ENDVERTEX_Z-B_ENDVERTEX_Z)*(D_ENDVERTEX_Z-B_ENDVERTEX_Z));
      double D0_dirX = D_ENDVERTEX_X-B_ENDVERTEX_X;
      double D0_dirY = D_ENDVERTEX_Y-B_ENDVERTEX_Y;
      double D0_dirZ = D_ENDVERTEX_Z-B_ENDVERTEX_Z;

      TVector3 dirD0(D0_dirX, D0_dirY, D0_dirZ);
      double thetaBD = dirD0.Angle(pD0.Vect());

      if (thetaBD>TMath::Pi()/2.) { D0Lsc = -1.*D0Lsc; }

      D0CTAU = D0Lsc/D0betasc/D0gammasc/299.792458 * 1000.;

      // Distance D0 line to tau vertex

      TVector3 uD0 = pD0.Vect();uD0 = uD0.Unit();
      TVector3 vD0tau(tau_ENDVERTEX_X-D_ENDVERTEX_X,tau_ENDVERTEX_Y-D_ENDVERTEX_Y,tau_ENDVERTEX_Z-D_ENDVERTEX_Z);
      TVector3 crossD0tau = uD0.Cross(vD0tau);
      //double thetaDtau = uD0.Angle(vD0tau);
      DRD0tau = crossD0tau.Mag();

      // Creating D* invariant mass
      
      Dst_M = -1000.;
      Dst_M_WS = -1000.;
      nDst = 0;
      nDst_WS = 0;
      Dst_pion_PX = -1.e6;
      Dst_pion_PY = -1.e6;
      Dst_pion_PZ = -1.e6;
      Dst_pionWS_PX = -1.e6;
      Dst_pionWS_PY = -1.e6;
      Dst_pionWS_PZ = -1.e6;
      Dst_pion_IPCHI2_OWNPV = -1.e6;
      Dst_pionWS_IPCHI2_OWNPV = -1.e6;
      Dst_PX = -1.e6;
      Dst_PY = -1.e6;
      Dst_PZ = -1.e6;

      for (int j=0;j<B_isoPlus_nGood;j++) {

        if (abs(B_isoPlus_trk_LOKI_ID[j])!=211) continue;

        TLorentzVector pDst_pion_tmp; pDst_pion_tmp.SetXYZM(B_isoPlus_trk_LOKI_PX[j],B_isoPlus_trk_LOKI_PY[j],B_isoPlus_trk_LOKI_PZ[j],mpi);
        //TLorentzVector pDst_tmp = pD0+pDst_pion_tmp;
        TLorentzVector pDst_tmp = pD0_D_M+pDst_pion_tmp;

        if (B_isoPlus_trk_LOKI_PIDK[j]<999 && pDst_tmp.M()-D_M<160) {
          if (B_isoPlus_trk_LOKI_ID[j]*D_ID>0) {
            nDst++;
            if (nDst==1) { 
              Dst_M = pDst_tmp.M(); 
              Dst_pion_PX = B_isoPlus_trk_LOKI_PX[j]; 
              Dst_pion_PY = B_isoPlus_trk_LOKI_PY[j]; 
              Dst_pion_PZ = B_isoPlus_trk_LOKI_PZ[j];
              Dst_pion_IPCHI2_OWNPV = B_isoPlus_trk_LOKI_IPChi2PV[j];
              Dst_PX = D_PX+Dst_pion_PX; 
              Dst_PY = D_PY+Dst_pion_PY; 
              Dst_PZ = D_PZ+Dst_pion_PZ; 
            }
          } else {
            nDst_WS++;
            if (nDst_WS==1) { 
              Dst_M_WS = pDst_tmp.M();
              Dst_pionWS_PX = B_isoPlus_trk_LOKI_PX[j]; 
              Dst_pionWS_PY = B_isoPlus_trk_LOKI_PY[j]; 
              Dst_pionWS_PZ = B_isoPlus_trk_LOKI_PZ[j]; 
              Dst_pionWS_IPCHI2_OWNPV = B_isoPlus_trk_LOKI_IPChi2PV[j];
            }
          }
        }

      }

      if (nDst>0) nCandDst ++;

      double mDstcor = Dst_M-D_M+mD0;
      double Dst_P = TMath::Sqrt(Dst_PX*Dst_PX+Dst_PY*Dst_PY+Dst_PZ*Dst_PZ);
      const TLorentzVector pDst(Dst_PX,Dst_PY,Dst_PZ,TMath::Sqrt( mDstcor*mDstcor+Dst_P*Dst_P) );
      //TLorentzVector pDst = pD0;

      TLorentzVector pDst_pion; pDst_pion.SetXYZM(Dst_pion_PX,Dst_pion_PY,Dst_pion_PZ,mpi);
      //TLorentzVector pDst; pDst.SetXYZM(D_PX,D_PY,D_PZ, D_M);
      TLorentzVector pDst5 = (pD0+pDst_pion);
      TLorentzVector pBandPion = (pB+pDst_pion);

      // B direction

      TVector3 Bdir(B_ENDVERTEX_X-B_OWNPV_X,B_ENDVERTEX_Y-B_OWNPV_Y,B_ENDVERTEX_Z-B_OWNPV_Z);
      //TVector3 Bdir(B_TRUEENDVERTEX_X-B_OWNPV_X,B_TRUEENDVERTEX_Y-B_OWNPV_Y,B_TRUEENDVERTEX_Z-B_OWNPV_Z);
      Bdir = Bdir.Unit();

      // tau_ENDVERTEX_ZERR and B_ENDVERTEX_ZER in diagonal space

      //     (           )   (                       )   (           )
      //     ( 1.16122 -0 )   ( -0.691914 -0.72198  )  ( x1  - -0.722599 ) 
      // z = (           ) x (                       ) x (           )
      //     ( -0 2.23477 )   ( -0.72198 0.691914  )  ( x2  - -0.785392 ) 
      //     (           )   (                       )   (           )
      // TMatrixD z = sqrtLambdaInv*matrixEigenVectorsInv*(x-x0);

      TMatrixD sqrtLambdaInv(2,2);
      sqrtLambdaInv[0][0] = 1.16122;
      sqrtLambdaInv[0][1] = 0.;
      sqrtLambdaInv[1][0] = 0.;
      sqrtLambdaInv[1][1] = 2.23477;
      TMatrixD RotInv(2,2);
      RotInv[0][0] = -0.691914;
      RotInv[0][1] = -0.72198;
      RotInv[1][0] = -0.72198;
      RotInv[1][1] = 0.691914;
      TMatrixD xv(2,1);
      xv[0][0] = TMath::Log(tau_ENDVERTEX_ZERR);
      xv[1][0] = TMath::Log(B_ENDVERTEX_ZERR);
      TMatrixD xv0(2,1);
      xv0[0][0] = -0.722599;
      xv0[1][0] = -0.785392;

      TMatrixD z = sqrtLambdaInv*RotInv*(xv-xv0);
      tauErrZRot = z[0][0];
      BErrZRot = z[1][0];

      // Smearing
            
      Tau_sc_zerr_2016 = 1.;
      Tau_sc_zerr_2017 = 1.;
      Tau_sc_zerr_2018 = 1.;
      Tau_sc_zerr_2016_data = 1.;
      Tau_sc_zerr = 1.;

      b_sc_zerr_2016 = 1.;
      b_sc_zerr_2017 = 1.;
      b_sc_zerr_2018 = 1.;
      b_sc_zerr_2016_data = 1.;
      b_sc_zerr = 1.;

      Reso_DzS_2016 = 0.;
      Reso_DzS_2017 = 0.;
      Reso_DzS_2018 = 0.;
      Reso_DzS_2016_data = 0.;
      Reso_DzS = 0.;

      Reso_Dz_2016 = 0.;
      Reso_Dz_2017 = 0.;
      Reso_Dz_2018 = 0.;
      Reso_Dz_2016_data = 0.;
      Reso_Dz = 0.;

      if (MonteCarlo) {

        double rnd4 = 1.e6*(D_pi_P-int(D_pi_P))-int(1.e6*(D_pi_P-int(D_pi_P)));

        //Tau_sc_zerr_2016 = (0.978538+3.82304e-05*tau_M)*1.0011*(1.03543-3.43372e-05*tau_M);
        //Tau_sc_zerr_2017 = (1.01829-2.83098e-05*tau_M);
        //Tau_sc_zerr_2018 = (1.01549-3.1229e-05*tau_M);
        Tau_sc_zerr_2016 = (0.977538+3.15771e-05*tau_M)*0.99225073;
        Tau_sc_zerr_2017 = (0.977538+3.15771e-05*tau_M)*0.99225073/((1.0029+3.84443e-05*tau_M)*0.99156107);
        Tau_sc_zerr_2018 = (0.977538+3.15771e-05*tau_M)*0.99225073/((1.02003+2.88693e-05*tau_M)*0.99288267);
        Tau_sc_zerr_2016_data = Tau_sc_zerr_2016;

        //b_sc_zerr_2016 = (1.02823+2.94131e-06*B_M)*(1.02279-4.69351e-06*B_M)*0.989495;
        //b_sc_zerr_2017 = (1.09048-3.45365e-05*B_M)*(0.926841+1.58361e-05*B_M);
        //b_sc_zerr_2018 = (1.11425-4.2393e-05*B_M)*(0.915522+1.91995e-05*B_M);
        b_sc_zerr_2016 = (1.00032+3.58414e-06*B_M);
        b_sc_zerr_2017 = (1.00032+3.58414e-06*B_M)/(1.00395+2.16318e-05*B_M);
        b_sc_zerr_2018 = (1.00032+3.58414e-06*B_M)/(1.00472+2.2367e-05*B_M);
        b_sc_zerr_2016_data = b_sc_zerr_2016;

        double rnd1_0 = 1.e6*(tau_pion1_P-int(tau_pion1_P))-int(1.e6*(tau_pion1_P-int(tau_pion1_P)));
        //double rnd1 = (TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd1_0-0.5)))*sqrt((Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016*B_ENDVERTEX_ZERR));
        double rnd1 = TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd1_0-0.5));
        double rnd2_0 = 1.e6*(tau_pion2_P-int(tau_pion2_P))-int(1.e6*(tau_pion2_P-int(tau_pion2_P)));
        //double rnd2 = (TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd2_0-0.5)))*sqrt((Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016*B_ENDVERTEX_ZERR));
        double rnd2 = TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd2_0-0.5));
        double rnd3 = 1.e6*(D_K_P-int(D_K_P))-int(1.e6*(D_K_P-int(D_K_P)));

        //double beta_2016 = 8.90226e-01;
        //double rnd1_2016 = 2.65682e-01*rnd1;
        //double rnd2_2016 = 1.50286e+00*rnd2;
        double beta_2016 = 8.95834e-01;
        double rnd1_2016 = 2.49764e-01*rnd1;
        double rnd2_2016 = 1.51309e+00*rnd2;
        double rnd3_2016 = rnd3;

        Reso_DzS_2016 = getRes(beta_2016,rnd1_2016,rnd2_2016,rnd3_2016);
        Reso_DzS_2017 = 0.;
        Reso_DzS_2018 = 0.;
        Reso_DzS_2016_data = Reso_DzS_2016;

        Reso_Dz_2016 = Reso_DzS_2016*sqrt((Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016*B_ENDVERTEX_ZERR));
        Reso_Dz_2017 = Reso_DzS_2017*sqrt((Tau_sc_zerr_2017*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2017*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2017*B_ENDVERTEX_ZERR)*(b_sc_zerr_2017*B_ENDVERTEX_ZERR));
        Reso_Dz_2018 = Reso_DzS_2018*sqrt((Tau_sc_zerr_2018*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2018*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2018*B_ENDVERTEX_ZERR)*(b_sc_zerr_2018*B_ENDVERTEX_ZERR));
        Reso_Dz_2016_data = Reso_Dz_2016;

        if (rnd4<0.31) { // 2016 data
          YearSim = 2016;
          Tau_sc_zerr = Tau_sc_zerr_2016 ;
          b_sc_zerr = b_sc_zerr_2016 ;
          Reso_DzS = Reso_DzS_2016;
          Reso_Dz = Reso_Dz_2016;
        } else if (rnd4<0.63) { // 2017 data
          YearSim = 2017;
          Tau_sc_zerr = Tau_sc_zerr_2017 ;
          b_sc_zerr = b_sc_zerr_2017 ;
          Reso_DzS = Reso_DzS_2017;
          Reso_Dz = Reso_Dz_2017;
        } else { // 2018 data
          YearSim = 2018;
          Tau_sc_zerr = Tau_sc_zerr_2018 ;
          b_sc_zerr = b_sc_zerr_2018 ;
          Reso_DzS = Reso_DzS_2018;
          Reso_Dz = Reso_Dz_2018;
        }

      } else {

        YearSim = Year;

        Tau_sc_zerr_2016 = 1.;
        Tau_sc_zerr_2017 = 1.;
        Tau_sc_zerr_2018 = 1.;
        Tau_sc_zerr = 1.;
        //if (Year==2016) Tau_sc_zerr_2016_data = 1.;
        //if (Year==2017) Tau_sc_zerr_2016_data = (1.01511+3.12541e-05*tau_M)*(1.02148-1.60105e-05*tau_M)*0.99024626*(0.982666+1.44887e-05*tau_M)*0.995888*0.998612*0.999;
        //if (Year==2018) Tau_sc_zerr_2016_data = (1.02308+3.06372e-05*tau_M)*(1.02758-1.75036e-05*tau_M)*0.98876712*(0.979658+1.60738e-05*tau_M)*0.99602*0.998331*0.998;
        if (Year==2016) Tau_sc_zerr_2016_data = 1.;
        if (Year==2017) Tau_sc_zerr_2016_data = (1.0029+3.84443e-05*tau_M)*0.99156107;
        if (Year==2018) Tau_sc_zerr_2016_data = (1.02003+2.88693e-05*tau_M)*0.99288267;

        b_sc_zerr_2016 = 1.;
        b_sc_zerr_2017 = 1.;
        b_sc_zerr_2018 = 1.;
        b_sc_zerr = 1.;
        //if (Year==2016) b_sc_zerr_2016_data = 1.;
        //if (Year==2017) b_sc_zerr_2016_data = (0.977577+2.86537e-05*B_M)*(1.03778-7.65038e-06*B_M)*0.997394*0.998833;
        //if (Year==2018) b_sc_zerr_2016_data = (0.978649+2.96442e-05*B_M)*(1.04024-8.20028e-06*B_M)*0.998942*0.999517;
        if (Year==2016) b_sc_zerr_2016_data = 1.;
        if (Year==2017) b_sc_zerr_2016_data = (1.00395+2.16318e-05*B_M);
        if (Year==2018) b_sc_zerr_2016_data = (1.00472+2.2367e-05*B_M);

        double rnd1_0 = 1.e6*(tau_pion1_P-int(tau_pion1_P))-int(1.e6*(tau_pion1_P-int(tau_pion1_P)));
        //double rnd1 = (TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd1_0-0.5)))*sqrt((Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016*B_ENDVERTEX_ZERR));
        double rnd1 = TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd1_0-0.5));
        double rnd2_0 = 1.e6*(tau_pion2_P-int(tau_pion2_P))-int(1.e6*(tau_pion2_P-int(tau_pion2_P)));
        //double rnd2 = (TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd2_0-0.5)))*sqrt((Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016*B_ENDVERTEX_ZERR));
        double rnd2 = TMath::Sqrt(2.)*TMath::ErfInverse(2.*(rnd2_0-0.5));
        double rnd3 = 1.e6*(D_K_P-int(D_K_P))-int(1.e6*(D_K_P-int(D_K_P)));

        //double beta_2017 = 9.02163e-01;
        //double rnd1_2017 = 2.65826e-01*rnd1;
        //double rnd2_2017 = 1.55983e+00*rnd2;
        double beta_2017 = 8.95834e-01;
        double rnd1_2017 = 2.49764e-01*rnd1;
        double rnd2_2017 = 1.51309e+00*rnd2;
        double rnd3_2017 = rnd3;

        //double beta_2018 = 9.03984e-01;
        //double rnd1_2018 = 2.94076e-01*rnd1;
        //double rnd2_2018 = 1.57753e+00*rnd2;
        double beta_2018 = 8.95834e-01;
        double rnd1_2018 = 2.49764e-01*rnd1;
        double rnd2_2018 = 1.51309e+00*rnd2;
        double rnd3_2018 = rnd3;

        Reso_DzS_2016 = 0.;
        Reso_DzS_2017 = 0.;
        Reso_DzS_2018 = 0.;
        Reso_DzS = 0.;
        if (Year==2016) Reso_DzS_2016_data = 0.;
        if (Year==2017) Reso_DzS_2016_data = getRes(beta_2017,rnd1_2017,rnd2_2017,rnd3_2017);
        if (Year==2018) Reso_DzS_2016_data = getRes(beta_2018,rnd1_2018,rnd2_2018,rnd3_2018);

        Reso_Dz_2016 = 0.;
        Reso_Dz_2017 = 0.;
        Reso_Dz_2018 = 0.;
        Reso_Dz_2016_data = Reso_DzS_2016_data*sqrt((Tau_sc_zerr_2016_data*tau_ENDVERTEX_ZERR)*(Tau_sc_zerr_2016_data*tau_ENDVERTEX_ZERR)+(b_sc_zerr_2016_data*B_ENDVERTEX_ZERR)*(b_sc_zerr_2016_data*B_ENDVERTEX_ZERR));
        Reso_Dz = 0.;


      }

//      double z_smear2016 = 0.2282; // +- 0.0070
//      double z_smear2017 = 0.110; // +- 0.011
//      double z_smear2018 = 0.100; // +- 0.011 
//      double z_smearAll = 0.1418; // +- 0.0065

      zSmear2016 = 0.;
      zSmear2017 = 0.;
      zSmear2018 = 0.;
      //zSmearAll = 0.;
      zSmearAll = Reso_Dz_2016_data;
      zSmearComb = 0.;

      DzSmear2016 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      DzSmear2017 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      DzSmear2018 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      //DzSmearAll = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      DzSmearAll = tau_ENDVERTEX_Z-B_ENDVERTEX_Z+Reso_Dz_2016_data;
      DzSmearComb = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;

      if (MonteCarlo) {


        //zSmear2016 = rnr.Gaus(0.,z_smear2016);
        //zSmear2017 = rnr.Gaus(0.,z_smear2017);
        //zSmear2018 = rnr.Gaus(0.,z_smear2018);
        //zSmearAll = rnr.Gaus(0.,z_smearAll);
        
        zSmear2016 = Reso_Dz_2016; 
        zSmear2017 = Reso_Dz_2017;
        zSmear2018 = Reso_Dz_2018;
        zSmearAll = Reso_Dz_2016_data;
        zSmearComb = Reso_Dz;

        //double rnr_data_fraction = rnr.Uniform();

        // Check fraction in data !!!

        ////if (rnr_data_fraction<0.31) { // 2016 data
        //if (YearSim == 2016) {
        //  zSmearComb = zSmear2016;
        ////} else if (rnr_data_fraction<0.63) { // 2017 data
        //} else if (YearSim == 2017) {
        //  zSmearComb = zSmear2017;
        //} else { // 2018 data
        //  zSmearComb = zSmear2018;
        //}

        DzSmear2016 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2016;
        DzSmear2017 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2017;
        DzSmear2018 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2018;
        DzSmearAll = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmearAll;
        DzSmearComb = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmearComb;

      }

      // tau direction
      TLorentzVector p3pi;
      p3pi.SetXYZM(tau_PX,tau_PY,tau_PZ,tau_M);

      double x_dir = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      double z_dir = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;

      if (p3pi.Px()*x_dir<0) x_dir = -x_dir;
      if (p3pi.Py()*y_dir<0) y_dir = -y_dir;
      if (p3pi.Pz()*z_dir<0) z_dir = -z_dir;

      // 2016
      double x_dir_sm2016 = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir_sm2016 = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      //double z_dir_sm2016 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      // X and Y are not smeared
      double z_dir_sm2016 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2016;

      if (p3pi.Px()*x_dir_sm2016<0) x_dir_sm2016 = -x_dir_sm2016;
      if (p3pi.Py()*y_dir_sm2016<0) y_dir_sm2016 = -y_dir_sm2016;
      if (p3pi.Pz()*z_dir_sm2016<0) z_dir_sm2016 = -z_dir_sm2016;

      // 2017
      double x_dir_sm2017 = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir_sm2017 = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      //double z_dir_sm2017 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      // X and Y are not smeared
      double z_dir_sm2017 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2017;

      if (p3pi.Px()*x_dir_sm2017<0) x_dir_sm2017 = -x_dir_sm2017;
      if (p3pi.Py()*y_dir_sm2017<0) y_dir_sm2017 = -y_dir_sm2017;
      if (p3pi.Pz()*z_dir_sm2017<0) z_dir_sm2017 = -z_dir_sm2017;

      // 2018
      double x_dir_sm2018 = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir_sm2018 = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      //double z_dir_sm2018 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      // X and Y are not smeared
      double z_dir_sm2018 = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmear2018;

      if (p3pi.Px()*x_dir_sm2018<0) x_dir_sm2018 = -x_dir_sm2018;
      if (p3pi.Py()*y_dir_sm2018<0) y_dir_sm2018 = -y_dir_sm2018;
      if (p3pi.Pz()*z_dir_sm2018<0) z_dir_sm2018 = -z_dir_sm2018;

      // All
      double x_dir_smAll = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir_smAll = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      //double z_dir_smAll = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      // X and Y are not smeared
      double z_dir_smAll = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmearAll;

      if (p3pi.Px()*x_dir_smAll<0) x_dir_smAll = -x_dir_smAll;
      if (p3pi.Py()*y_dir_smAll<0) y_dir_smAll = -y_dir_smAll;
      if (p3pi.Pz()*z_dir_smAll<0) z_dir_smAll = -z_dir_smAll;

      // Comb
      double x_dir_smComb = tau_ENDVERTEX_X-B_ENDVERTEX_X;
      double y_dir_smComb = tau_ENDVERTEX_Y-B_ENDVERTEX_Y;
      //double z_dir_smComb = tau_ENDVERTEX_Z-B_ENDVERTEX_Z;
      // X and Y are not smeared
      double z_dir_smComb = tau_ENDVERTEX_Z-B_ENDVERTEX_Z + zSmearComb;

      if (p3pi.Px()*x_dir_smComb<0) x_dir_smComb = -x_dir_smComb;
      if (p3pi.Py()*y_dir_smComb<0) y_dir_smComb = -y_dir_smComb;
      if (p3pi.Pz()*z_dir_smComb<0) z_dir_smComb = -z_dir_smComb;


      TVector3 taudir(x_dir,y_dir,z_dir);
      taudir = taudir.Unit();

      TVector3 taudir_sm2016(x_dir_sm2016,y_dir_sm2016,z_dir_sm2016);
      taudir_sm2016 = taudir_sm2016.Unit();
      TVector3 taudir_sm2017(x_dir_sm2017,y_dir_sm2017,z_dir_sm2017);
      taudir_sm2017 = taudir_sm2017.Unit();
      TVector3 taudir_sm2018(x_dir_sm2018,y_dir_sm2018,z_dir_sm2018);
      taudir_sm2018 = taudir_sm2018.Unit();
      TVector3 taudir_smAll(x_dir_smAll,y_dir_smAll,z_dir_smAll);
      taudir_smAll = taudir_smAll.Unit();
      TVector3 taudir_smComb(x_dir_smComb,y_dir_smComb,z_dir_smComb);
      taudir_smComb = taudir_smComb.Unit();

      double m3pi = p3pi.M();

      thetatau = taudir.Angle(p3pi.Vect());

      double thetataumax = asin((mtau*mtau-m3pi*m3pi)/2./mtau/p3pi.P());
      //thetatau = thetataumax;

      double ptau_max_calc = getP0Taumax(mtau, p3pi);
      double ptau_0_calc = getP0Tau(mtau, p3pi, taudir);
      double ptau_D_calc = getDeltaTau(mtau, p3pi, taudir);

      double ptau_solplus = -1.E8;
      double ptau_solminus = -1.E8;
      if (ptau_D_calc>-1) {
        ptau_solplus = ptau_0_calc+ptau_D_calc;
        ptau_solminus = ptau_0_calc-ptau_D_calc;
      }

      TLorentzVector pTaurec;
      pTaurec.SetXYZM(taudir.X()*ptau_max_calc,taudir.Y()*ptau_max_calc,taudir.Z()*ptau_max_calc,mtau);

      TLorentzVector pTaurec_sm2016;
      pTaurec_sm2016.SetXYZM(taudir_sm2016.X()*ptau_max_calc,taudir_sm2016.Y()*ptau_max_calc,taudir_sm2016.Z()*ptau_max_calc,mtau);
      TLorentzVector pTaurec_sm2017;
      pTaurec_sm2017.SetXYZM(taudir_sm2017.X()*ptau_max_calc,taudir_sm2017.Y()*ptau_max_calc,taudir_sm2017.Z()*ptau_max_calc,mtau);
      TLorentzVector pTaurec_sm2018;
      pTaurec_sm2018.SetXYZM(taudir_sm2018.X()*ptau_max_calc,taudir_sm2018.Y()*ptau_max_calc,taudir_sm2018.Z()*ptau_max_calc,mtau);
      TLorentzVector pTaurec_smAll;
      pTaurec_smAll.SetXYZM(taudir_smAll.X()*ptau_max_calc,taudir_smAll.Y()*ptau_max_calc,taudir_smAll.Z()*ptau_max_calc,mtau);
      TLorentzVector pTaurec_smComb;
      pTaurec_smComb.SetXYZM(taudir_smComb.X()*ptau_max_calc,taudir_smComb.Y()*ptau_max_calc,taudir_smComb.Z()*ptau_max_calc,mtau);

      TLorentzVector pTaurecplus;
      pTaurecplus.SetXYZM(taudir.X()*ptau_solplus,taudir.Y()*ptau_solplus,taudir.Z()*ptau_solplus,mtau);

      TLorentzVector pTaurecminus;
      pTaurecminus.SetXYZM(taudir.X()*ptau_solminus,taudir.Y()*ptau_solminus,taudir.Z()*ptau_solminus,mtau);

      TLorentzVector p1 = pD0+pTaurec;

      TLorentzVector pD0Taurec_sm2016 = pD0+pTaurec_sm2016;
      TLorentzVector pD0Taurec_sm2017 = pD0+pTaurec_sm2017;
      TLorentzVector pD0Taurec_sm2018 = pD0+pTaurec_sm2018;
      TLorentzVector pD0Taurec_smAll = pD0+pTaurec_smAll;
      TLorentzVector pD0Taurec_smComb = pD0+pTaurec_smComb;

      TLorentzVector p1Dst = pDst5+pTaurec;

      TLorentzVector pDstTaurec_sm2016 = pDst5+pTaurec_sm2016;
      TLorentzVector pDstTaurec_sm2017 = pDst5+pTaurec_sm2017;
      TLorentzVector pDstTaurec_sm2018 = pDst5+pTaurec_sm2018;
      TLorentzVector pDstTaurec_smAll = pDst5+pTaurec_smAll;
      TLorentzVector pDstTaurec_smComb = pDst5+pTaurec_smComb;

      TLorentzVector p1plus = pD0+pTaurecplus;
      TLorentzVector p1minus = pD0+pTaurecminus;

      thetaB = Bdir.Angle(p1.Vect());

      thetaBmax = -1.E8;
      if (abs((mBp*mBp-p1.M()*p1.M())/(2.*mBp*p1.P()))<=1) {
        thetaBmax = asin((mBp*mBp-p1.M()*p1.M())/(2.*mBp*p1.P()));
      }
      //thetaB = thetaBmax;

      TVector3 pTBv = Bdir.Cross(p1.Vect());
      pTBv = Bdir.Cross(pTBv);
      pTB = pTBv.Mag();

      MBcor = sqrt(B_M*B_M+pTB*pTB)+pTB;

      TVector3 pTTauv = taudir.Cross(ptau.Vect());
      pTTauv = taudir.Cross(pTTauv);
      pTTau = pTTauv.Mag();

      TVector3 pTTauBv = Bdir.Cross(ptau.Vect());
      pTTauBv = Bdir.Cross(pTTauBv);
      pTTauB = pTTauBv.Mag();

      MTaucor = sqrt(tau_M*tau_M+pTTau*pTTau)+pTTau;

      TVector3 pTDv = Bdir.Cross(pD0.Vect());
      pTDv = Bdir.Cross(pTDv);
      pTD0 = pTDv.Mag();

      // --- //

      //double pB_0 = getP0Tau(mBp, p1, Bdir);
      double pB_max = getP0Taumax(mBp, p1);

      double pB_max_sm2016 = getP0Taumax(mBp, pD0Taurec_sm2016);
      double pB_max_sm2017 = getP0Taumax(mBp, pD0Taurec_sm2017);
      double pB_max_sm2018 = getP0Taumax(mBp, pD0Taurec_sm2018);
      double pB_max_smAll = getP0Taumax(mBp, pD0Taurec_smAll);
      double pB_max_smComb = getP0Taumax(mBp, pD0Taurec_smComb);

      double pB_maxDst = getP0Taumax(mBp, p1Dst);

      double pB_maxDst_sm2016 = getP0Taumax(mBp, pDstTaurec_sm2016);
      double pB_maxDst_sm2017 = getP0Taumax(mBp, pDstTaurec_sm2017);
      double pB_maxDst_sm2018 = getP0Taumax(mBp, pDstTaurec_sm2018);
      double pB_maxDst_smAll = getP0Taumax(mBp, pDstTaurec_smAll);
      double pB_maxDst_smComb = getP0Taumax(mBp, pDstTaurec_smComb);

      //double pB_D = getDeltaTau(mBp, p1, Bdir);
      double pB1_0 = getP0Tau(mBp, p1minus, Bdir);
      double pB1_D = getDeltaTau(mBp, p1minus, Bdir);
      double pB2_0 = getP0Tau(mBp, p1plus, Bdir);
      double pB2_D = getDeltaTau(mBp, p1plus, Bdir);

      //double pB_plus = -1.E8;
      //double pB_minus = -1.E8;
      //if (pB_D>-1) {
      //  pB_plus = pB_0+pB_D;
      //  pB_minus = pB_0-pB_D;
      //}
      double pB1_plus = -1.E8;
      double pB1_minus = -1.E8;
      if (pB1_D>-1) {
        pB1_plus = pB1_0+pB1_D;
        pB1_minus = pB1_0-pB1_D;
      }
      double pB2_plus = -1.E8;
      double pB2_minus = -1.E8;
      if (pB2_D>-1) {
        pB2_plus = pB2_0+pB2_D;
        pB2_minus = pB2_0-pB2_D;
      }

      TVector3 pBv2(B_PX,B_PY,B_PZ);
      TVector3 pBv3 = pBv2+pTBv;

      TLorentzVector pBmax;
      pBmax.SetXYZM(pB_max*Bdir.X(),pB_max*Bdir.Y(),pB_max*Bdir.Z(),mBp);

      TLorentzVector pBmax_sm2016;
      pBmax_sm2016.SetXYZM(pB_max_sm2016*Bdir.X(),pB_max_sm2016*Bdir.Y(),pB_max_sm2016*Bdir.Z(),mBp);
      TLorentzVector pBmax_sm2017;
      pBmax_sm2017.SetXYZM(pB_max_sm2017*Bdir.X(),pB_max_sm2017*Bdir.Y(),pB_max_sm2017*Bdir.Z(),mBp);
      TLorentzVector pBmax_sm2018;
      pBmax_sm2018.SetXYZM(pB_max_sm2018*Bdir.X(),pB_max_sm2018*Bdir.Y(),pB_max_sm2018*Bdir.Z(),mBp);
      TLorentzVector pBmax_smAll;
      pBmax_smAll.SetXYZM(pB_max_smAll*Bdir.X(),pB_max_smAll*Bdir.Y(),pB_max_smAll*Bdir.Z(),mBp);
      TLorentzVector pBmax_smComb;
      pBmax_smComb.SetXYZM(pB_max_smComb*Bdir.X(),pB_max_smComb*Bdir.Y(),pB_max_smComb*Bdir.Z(),mBp);

      TLorentzVector pBmaxDst;
      pBmaxDst.SetXYZM(pB_maxDst*Bdir.X(),pB_maxDst*Bdir.Y(),pB_maxDst*Bdir.Z(),mBp);

      TLorentzVector pBmaxDst_sm2016;
      pBmaxDst_sm2016.SetXYZM(pB_maxDst_sm2016*Bdir.X(),pB_maxDst_sm2016*Bdir.Y(),pB_maxDst_sm2016*Bdir.Z(),mBp);
      TLorentzVector pBmaxDst_sm2017;
      pBmaxDst_sm2017.SetXYZM(pB_maxDst_sm2017*Bdir.X(),pB_maxDst_sm2017*Bdir.Y(),pB_maxDst_sm2017*Bdir.Z(),mBp);
      TLorentzVector pBmaxDst_sm2018;
      pBmaxDst_sm2018.SetXYZM(pB_maxDst_sm2018*Bdir.X(),pB_maxDst_sm2018*Bdir.Y(),pB_maxDst_sm2018*Bdir.Z(),mBp);
      TLorentzVector pBmaxDst_smAll;
      pBmaxDst_smAll.SetXYZM(pB_maxDst_smAll*Bdir.X(),pB_maxDst_smAll*Bdir.Y(),pB_maxDst_smAll*Bdir.Z(),mBp);
      TLorentzVector pBmaxDst_smComb;
      pBmaxDst_smComb.SetXYZM(pB_maxDst_smComb*Bdir.X(),pB_maxDst_smComb*Bdir.Y(),pB_maxDst_smComb*Bdir.Z(),mBp);

      //pBmax.SetXYZM(pB1_plus*Bdir.X(),pB1_plus*Bdir.Y(),pB1_plus*Bdir.Z(),mBp);
      //pBmax.SetXYZM(pB1_minus*Bdir.X(),pB1_minus*Bdir.Y(),pB1_minus*Bdir.Z(),mBp);
      //pBmax.SetXYZM(pB2_plus*Bdir.X(),pB2_plus*Bdir.Y(),pB2_plus*Bdir.Z(),mBp);
      //double xpb = TMath::Sqrt(B_TRUEP_X*B_TRUEP_X+B_TRUEP_Y*B_TRUEP_Y+B_TRUEP_Z*B_TRUEP_Z);
      //double xpb = TMath::Sqrt(B_PX*B_PX+B_PY*B_PY+B_PZ*B_PZ)*mBp/B_M;
      //pBmax.SetXYZM(xpb*Bdir.X(),xpb*Bdir.Y(),xpb*Bdir.Z(),mBp);
      //pBmax.SetXYZM(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,mBp);
      TLorentzVector pB00;
      pB00.SetXYZM(pB_max*Bdir.X(),pB_max*Bdir.Y(),pB_max*Bdir.Z(),mBp);
      TLorentzVector pB11;
      pB11.SetXYZM(pB1_minus*Bdir.X(),pB1_minus*Bdir.Y(),pB1_minus*Bdir.Z(),mBp);
      TLorentzVector pB12;
      pB12.SetXYZM(pB1_plus*Bdir.X(),pB1_plus*Bdir.Y(),pB1_plus*Bdir.Z(),mBp);
      TLorentzVector pB21;
      pB21.SetXYZM(pB2_minus*Bdir.X(),pB2_minus*Bdir.Y(),pB2_minus*Bdir.Z(),mBp);
      TLorentzVector pB22;
      pB22.SetXYZM(pB2_plus*Bdir.X(),pB2_plus*Bdir.Y(),pB2_plus*Bdir.Z(),mBp);

      TLorentzVector pTaurec0 = pTaurec;

      PB = -1.E+8;
      //PBDst = -1.E+8;
      PB11 = -1.E+8;
      PB12 = -1.E+8;
      PB21 = -1.E+8;
      PB22 = -1.E+8;

//      if(pB_maxDst>0) PBDst = pBmaxDst.P();

      if(pB_max>0) PB = pBmax.P();
      if(pB1_minus>0) PB11 = pB11.P();
      if(pB1_plus>0) PB12 = pB12.P();
      if(pB2_minus>0) PB21 = pB21.P();
      if(pB2_plus>0) PB22 = pB22.P();

      PTAU = pTaurec.P();

      PTAU1 = -1.E+8;
      PTAU2 = -1.E+8;
      if(ptau_solminus>0) PTAU1 = pTaurecminus.P();
      if(ptau_solplus>0) PTAU2 = pTaurecplus.P();

      // Variables as in D*tau-mu muonic paper

      PBsc = mBp/B_M*sqrt(B_PX*B_PX+B_PY*B_PY+B_PZ*B_PZ);
      PTAUsc = mtau/tau_M*sqrt(tau_PX*tau_PX+tau_PY*tau_PY+tau_PZ*tau_PZ);

      TLorentzVector pBsc;
      pBsc.SetXYZM(PBsc*Bdir.X(),PBsc*Bdir.Y(),PBsc*Bdir.Z(),mBp);
      TLorentzVector pDst3pisc;
      pDst3pisc.SetXYZM(B_PX,B_PY,B_PZ,B_M);
      TLorentzVector pTAUsc;
      pTAUsc.SetXYZM(PTAUsc*taudir.X(),PTAUsc*taudir.Y(),PTAUsc*taudir.Z(),mtau);
      TLorentzVector p3pisc;
      p3pisc.SetXYZM(tau_PX,tau_PY,tau_PZ,tau_M);

      MWsc = (pBsc-pD0).M();

      double betasc = pTAUsc.P()/pTAUsc.E();
      double gammasc = 1./sqrt(1.-betasc*betasc);
      double Lsc = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+(tau_ENDVERTEX_Z-B_ENDVERTEX_Z)*(tau_ENDVERTEX_Z-B_ENDVERTEX_Z));

      TauCTAUsc = Lsc/betasc/gammasc/299.792458 * 1000.;

      // --- //

      mNu = (pBmax-pD0-pTaurec0).M();

      TLorentzVector pWDst = pBmaxDst-pDst5;

      TLorentzVector pWDst_sm2016 = pBmaxDst_sm2016-pDst5;
      TLorentzVector pWDst_sm2017 = pBmaxDst_sm2017-pDst5;
      TLorentzVector pWDst_sm2018 = pBmaxDst_sm2018-pDst5;
      TLorentzVector pWDst_smAll = pBmaxDst_smAll-pDst5;
      TLorentzVector pWDst_smComb = pBmaxDst_smComb-pDst5;

      TLorentzVector pW = pBmax-pD0;

      TLorentzVector pW_sm2016 = pBmax_sm2016-pD0;
      TLorentzVector pW_sm2017 = pBmax_sm2017-pD0;
      TLorentzVector pW_sm2018 = pBmax_sm2018-pD0;
      TLorentzVector pW_smAll = pBmax_smAll-pD0;
      TLorentzVector pW_smComb = pBmax_smComb-pD0;

      TLorentzVector pW00 = pB00-pD0;
      TLorentzVector pW11 = pB11-pD0;
      TLorentzVector pW12 = pB12-pD0;
      TLorentzVector pW21 = pB21-pD0;
      TLorentzVector pW22 = pB22-pD0;


      double MWDst = -1.E8;
      MW = -1.E8;
      q2 = -1.E8;
      q2sm2016 = -1.E8;
      q2sm2017 = -1.E8;
      q2sm2018 = -1.E8;
      q2smAll = -1.E8;
      q2smComb = -1.E8;
      q2Dst = -1.E8;
      q2Dstsm2016 = -1.E8;
      q2Dstsm2017 = -1.E8;
      q2Dstsm2018 = -1.E8;
      q2DstsmAll = -1.E8;
      q2DstsmComb = -1.E8;
      MW00 = -1.E8;
      MW11 = -1.E8;
      MW12 = -1.E8;
      MW21 = -1.E8;
      MW22 = -1.E8;

      if (pB_maxDst>-1 && ptau_max_calc>-1) {
        MWDst = pWDst.M(); 
        if (MWDst>0) {
          q2Dst = 1.e-6*MWDst*MWDst;
        } else {
          q2Dst = -1.e-6*MWDst*MWDst;
        }
      }

      if (pB_max>-1 && ptau_max_calc>-1) {
        MW = pW.M(); 
        if (MW>0) {
          q2 = 1.e-6*MW*MW;
        } else {
          q2 = -1.e-6*MW*MW;
        }
      }

      if (pB_max_sm2016>-1 && ptau_max_calc>-1) {
        double MW_sm2016 = pW_sm2016.M(); 
        if (MW_sm2016>0) { q2sm2016 = 1.e-6*MW_sm2016*MW_sm2016;
        } else { q2sm2016 = -1.e-6*MW_sm2016*MW_sm2016; }
      }
      if (pB_max_sm2017>-1 && ptau_max_calc>-1) {
        double MW_sm2017 = pW_sm2017.M(); 
        if (MW_sm2017>0) { q2sm2017 = 1.e-6*MW_sm2017*MW_sm2017;
        } else { q2sm2017 = -1.e-6*MW_sm2017*MW_sm2017; }
      }
      if (pB_max_sm2018>-1 && ptau_max_calc>-1) {
        double MW_sm2018 = pW_sm2018.M(); 
        if (MW_sm2018>0) { q2sm2018 = 1.e-6*MW_sm2018*MW_sm2018;
        } else { q2sm2018 = -1.e-6*MW_sm2018*MW_sm2018; }
      }
      if (pB_max_smAll>-1 && ptau_max_calc>-1) {
        double MW_smAll = pW_smAll.M(); 
        if (MW_smAll>0) { q2smAll = 1.e-6*MW_smAll*MW_smAll;
        } else { q2smAll = -1.e-6*MW_smAll*MW_smAll; }
      }
      if (pB_max_smComb>-1 && ptau_max_calc>-1) {
        double MW_smComb = pW_smComb.M(); 
        if (MW_smComb>0) { q2smComb = 1.e-6*MW_smComb*MW_smComb;
        } else { q2smComb = -1.e-6*MW_smComb*MW_smComb; }
      }


      if (pB_max>-1 && ptau_max_calc>-1) { MW00 = pW00.M(); }
      if (pB1_minus>-1 && ptau_solminus>-1) { MW11 = pW11.M(); }
      if (pB1_plus>-1 && ptau_solminus>-1) { MW12 = pW12.M(); }
      if (pB2_minus>-1 && ptau_solplus>-1) { MW21 = pW21.M(); }
      if (pB2_plus>-1 && ptau_solplus>-1) { MW22 = pW22.M(); }

      //TLorentzVector pDst2; pDst2.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      TLorentzVector pDst2 = pDst;
      TLorentzVector pD02;
      //pD02.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      pD02.SetXYZM(D_PX,D_PY,D_PZ,mD0);
      p3pi2.SetXYZM(tau_PX,tau_PY,tau_PZ,tau_M);
      TLorentzVector pTau2 = pTaurec;
      TLorentzVector pBmax2 = pBmax;
      TLorentzVector pBmax3 = pBmax;
      TLorentzVector pW2 = pW;

      tauPHI = -1.E8;

      if (MW>0) {
        //pBmax2.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        pD02.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        p3pi2.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        pTau2.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        pDst2.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        pW2.Boost(-pBmax3.Px()/pBmax3.E(),-pBmax3.Py()/pBmax3.E(),-pBmax3.Pz()/pBmax3.E());
        TVector3 nv1 = pDst2.Vect().Unit().Cross(pD02.Vect().Unit());
        //TVector3 nv2 = pTau2.Vect().Unit().Cross(p3pi2.Vect().Unit());
        TVector3 nv2 = pW2.Vect().Unit().Cross(pTau2.Vect().Unit());
        TVector3 Vx = nv1.Unit();
        TVector3 Vz = pDst2.Vect().Unit(); 
        TVector3 Vy = (Vz.Cross(Vx)).Unit();
        double cosv = nv2.Dot(Vx);
        double sinv = nv2.Dot(Vy);
        tauPHI = atan2(sinv,cosv);
      }

      // Tau Helicity

      pBmax2.Boost(-pW.Px()/pW.E(),-pW.Py()/pW.E(),-pW.Pz()/pW.E());
      pTaurec0.Boost(-pW.Px()/pW.E(),-pW.Py()/pW.E(),-pW.Pz()/pW.E());
      double angleTau = pBmax2.Vect().Angle(pTaurec0.Vect());

      cosThetaTau = cos(angleTau);

      // Tau Helicity in B0 --> D*-

      TLorentzVector pB_Dst_WCMS = pBmax;
      pB_Dst_WCMS.Boost(-pWDst.Px()/pWDst.E(),-pWDst.Py()/pWDst.E(),-pWDst.Pz()/pWDst.E());
      TLorentzVector pTau_Dst_WCMS = pTaurec;
      pTau_Dst_WCMS.Boost(-pWDst.Px()/pWDst.E(),-pWDst.Py()/pWDst.E(),-pWDst.Pz()/pWDst.E());
      double angleTauDst = pB_Dst_WCMS.Vect().Angle(pTau_Dst_WCMS.Vect());

      cosThetaTauDst = cos(angleTauDst);


      // 11
      pB11.Boost(-pW11.Px()/pW11.E(),-pW11.Py()/pW11.E(),-pW11.Pz()/pW11.E());
      TLorentzVector pTaurec11 = pTaurecminus;
      pTaurec11.Boost(-pW11.Px()/pW11.E(),-pW11.Py()/pW11.E(),-pW11.Pz()/pW11.E());
      double angleTau11 = pB11.Vect().Angle(pTaurec11.Vect());
      cosThetaTau11 = cos(angleTau11);

      // 12
      pB12.Boost(-pW12.Px()/pW12.E(),-pW12.Py()/pW12.E(),-pW12.Pz()/pW12.E());
      TLorentzVector pTaurec12 = pTaurecminus;
      pTaurec12.Boost(-pW12.Px()/pW12.E(),-pW12.Py()/pW12.E(),-pW12.Pz()/pW12.E());
      double angleTau12 = pB12.Vect().Angle(pTaurec12.Vect());
      cosThetaTau12 = cos(angleTau12);

      // 21
      pB21.Boost(-pW21.Px()/pW21.E(),-pW21.Py()/pW21.E(),-pW21.Pz()/pW21.E());
      TLorentzVector pTaurec21 = pTaurecplus;
      pTaurec21.Boost(-pW21.Px()/pW21.E(),-pW21.Py()/pW21.E(),-pW21.Pz()/pW21.E());
      double angleTau21 = pB21.Vect().Angle(pTaurec21.Vect());
      cosThetaTau21 = cos(angleTau21);

      // 22
      pB22.Boost(-pW22.Px()/pW22.E(),-pW22.Py()/pW22.E(),-pW22.Pz()/pW22.E());
      TLorentzVector pTaurec22 = pTaurecplus;
      pTaurec22.Boost(-pW22.Px()/pW22.E(),-pW22.Py()/pW22.E(),-pW22.Pz()/pW22.E());
      double angleTau22 = pB22.Vect().Angle(pTaurec22.Vect());
      cosThetaTau22 = cos(angleTau22);

       
      //

      // cos(theta_Dst_hel) in Dst3pi cms


      TLorentzVector pD0cmsDst = pD0;
      TLorentzVector ptaucmsDst = ptau;
      ptaucmsDst.Boost(-pDst5.Px()/pDst5.E(),-pDst5.Py()/pDst5.E(),-pDst5.Pz()/pDst5.E());
      pD0cmsDst.Boost(-pDst5.Px()/pDst5.E(),-pDst5.Py()/pDst5.E(),-pDst5.Pz()/pDst5.E());

      cosTheta1 = cos(pD0cmsDst.Vect().Angle(ptaucmsDst.Vect()));
      //

      TLorentzVector pBmax4;
      pBmax4.SetXYZM(pB_max*Bdir.X(),pB_max*Bdir.Y(),pB_max*Bdir.Z(),mBp);
      const TLorentzVector pDst4(Dst_PX,Dst_PY,Dst_PZ,TMath::Sqrt( mDstcor*mDstcor+Dst_P*Dst_P) );
      //pDst4.SetXYZM(Dst_PX,Dst_PY,Dst_PZ,Dst_M);
      //pDst4.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      TLorentzVector pD04;
      //pD04.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      pD04.SetXYZM(D_PX,D_PY,D_PZ,mD0);

      pBmax4.Boost(-pDst4.Px()/pDst4.E(),-pDst4.Py()/pDst4.E(),-pDst4.Pz()/pDst4.E());
      pD04.Boost(-pDst4.Px()/pDst4.E(),-pDst4.Py()/pDst4.E(),-pDst4.Pz()/pDst4.E());

      cosTheta1B = -9.;
      if (Dst_M>0) cosTheta1B = cos(pBmax4.Vect().Angle(pD04.Vect()));

      double beta = pTaurec.P()/pTaurec.E();
      double gamma = 1./sqrt(1.-beta*beta);
      double L = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+(tau_ENDVERTEX_Z-B_ENDVERTEX_Z)*(tau_ENDVERTEX_Z-B_ENDVERTEX_Z));

      TauCTAU = L/beta/gamma/299.792458 * 1000.;
      //if (cos(thetatau)<0.) TauCTAU = -TauCTAU;
      if ((tau_ENDVERTEX_Z-B_ENDVERTEX_Z)*tau_PZ<0) TauCTAU = -TauCTAU;


      TauCTAUsm2016 = TauCTAU;
//      if (MonteCarlo) {
        double beta_sm2016 = pTaurec_sm2016.P()/pTaurec_sm2016.E();
        double gamma_sm2016 = 1./sqrt(1.-beta_sm2016*beta_sm2016);
        double L_sm2016 = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+DzSmear2016*DzSmear2016);
        TauCTAUsm2016 = L_sm2016/beta_sm2016/gamma_sm2016/299.792458 * 1000.;
        if (DzSmear2016*tau_PZ<0) TauCTAUsm2016 = -TauCTAUsm2016;
//      }
      TauCTAUsm2017 = TauCTAU;
//      if (MonteCarlo) {
        double beta_sm2017 = pTaurec_sm2017.P()/pTaurec_sm2017.E();
        double gamma_sm2017 = 1./sqrt(1.-beta_sm2017*beta_sm2017);
        double L_sm2017 = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+DzSmear2017*DzSmear2017);
        TauCTAUsm2017 = L_sm2017/beta_sm2017/gamma_sm2017/299.792458 * 1000.;
        if (DzSmear2017*tau_PZ<0) TauCTAUsm2017 = -TauCTAUsm2017;
//      }
      TauCTAUsm2018 = TauCTAU;
//      if (MonteCarlo) {
        double beta_sm2018 = pTaurec_sm2018.P()/pTaurec_sm2018.E();
        double gamma_sm2018 = 1./sqrt(1.-beta_sm2018*beta_sm2018);
        double L_sm2018 = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+DzSmear2018*DzSmear2018);
        TauCTAUsm2018 = L_sm2018/beta_sm2018/gamma_sm2018/299.792458 * 1000.;
        if (DzSmear2018*tau_PZ<0) TauCTAUsm2018 = -TauCTAUsm2018;
//      }
      TauCTAUsmAll = TauCTAU;
//      if (MonteCarlo) {
        double beta_smAll = pTaurec_smAll.P()/pTaurec_smAll.E();
        double gamma_smAll = 1./sqrt(1.-beta_smAll*beta_smAll);
        double L_smAll = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+DzSmearAll*DzSmearAll);
        TauCTAUsmAll = L_smAll/beta_smAll/gamma_smAll/299.792458 * 1000.;
        if (DzSmearAll*tau_PZ<0) TauCTAUsmAll = -TauCTAUsmAll;
//      }
      TauCTAUsmComb = TauCTAU;
//      if (MonteCarlo) {
        double beta_smComb = pTaurec_smComb.P()/pTaurec_smComb.E();
        double gamma_smComb = 1./sqrt(1.-beta_smComb*beta_smComb);
        double L_smComb = sqrt((tau_ENDVERTEX_X-B_ENDVERTEX_X)*(tau_ENDVERTEX_X-B_ENDVERTEX_X)+(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)*(tau_ENDVERTEX_Y-B_ENDVERTEX_Y)+DzSmearComb*DzSmearComb);
        TauCTAUsmComb = L_smComb/beta_smComb/gamma_smComb/299.792458 * 1000.;
        if (DzSmearComb*tau_PZ<0) TauCTAUsmComb = -TauCTAUsmComb;
//      }



      double beta1 = pTaurecminus.P()/pTaurecminus.E();
      double gamma1 = 1./sqrt(1.-beta1*beta1);

      TauCTAU1 = L/beta1/gamma1/299.792458 * 1000.;
      if (cos(thetatau)<0.) TauCTAU1 = -TauCTAU1;

      double beta2 = pTaurecplus.P()/pTaurecplus.E();
      double gamma2 = 1./sqrt(1.-beta2*beta2);

      TauCTAU2 = L/beta2/gamma2/299.792458 * 1000.;
      if (cos(thetatau)<0.) TauCTAU2 = -TauCTAU2;


      TVector3 taudir1(tau_ENDVERTEX_X-B_ENDVERTEX_X,tau_ENDVERTEX_Y-B_ENDVERTEX_Y,tau_ENDVERTEX_Z-B_ENDVERTEX_Z);
      taudir1 = taudir1.Unit();
      TLorentzVector pTaurec1;
      pTaurec1.SetXYZM(taudir1.X()*ptau_max_calc,taudir1.Y()*ptau_max_calc,taudir1.Z()*ptau_max_calc,mtau);

      TLorentzVector pDst0;
      //pDst0.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      pDst0.SetXYZM(D_PX,D_PY,D_PZ,mD0);
      TLorentzVector pD00;
      //pD00.SetXYZM(D_PX,D_PY,D_PZ,D_M);
      pD00.SetXYZM(D_PX,D_PY,D_PZ,mD0);
      TLorentzVector ppion00;
      TLorentzVector ppion10;
      TLorentzVector ppion20;
      ppion00.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mpi);
      ppion10.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mpi);
      ppion20.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mpi);
      TLorentzVector p3pi1 = (ppion00+ppion10+ppion20);

      TLorentzVector plost = pTaurec1-p3pi1;

      TLorentzVector ptaunc050;
      ptaunc050.SetXYZM(tau_040_nc_PX,tau_040_nc_PY,tau_040_nc_PZ,mpi);

      M01 = (ppion00+ppion10).M();
      M12 = (ppion10+ppion20).M();
      M02 = (ppion00+ppion20).M();

      MDst1 = (pDst0+ppion00).M();
      MDst2 = (pDst0+ppion10).M();
      MDst3 = (pDst0+ppion20).M();

      MDst12 = (pDst0+ppion00+ppion10).M();
      MDst13 = (pDst0+ppion00+ppion20).M();
      MDst23 = (pDst0+ppion10+ppion20).M();
    
      MD01 = (pD00+ppion00).M();
      MD02 = (pD00+ppion10).M();
      MD03 = (pD00+ppion20).M();
    
      ppion00.Boost(-p3pi1.Px()/p3pi1.E(),-p3pi1.Py()/p3pi1.E(),-p3pi1.Pz()/p3pi1.E());
      ppion10.Boost(-p3pi1.Px()/p3pi1.E(),-p3pi1.Py()/p3pi1.E(),-p3pi1.Pz()/p3pi1.E());
      ppion20.Boost(-p3pi1.Px()/p3pi1.E(),-p3pi1.Py()/p3pi1.E(),-p3pi1.Pz()/p3pi1.E());
      pTaurec1.Boost(-p3pi1.Px()/p3pi1.E(),-p3pi1.Py()/p3pi1.E(),-p3pi1.Pz()/p3pi1.E());
      pDst0.Boost(-p3pi1.Px()/p3pi1.E(),-p3pi1.Py()/p3pi1.E(),-p3pi1.Pz()/p3pi1.E());

      //TVector3 n3pi = ppion00.Vect().Cross(ppion10.Vect());
      TVector3 n3pi = ppion00.Vect().Cross(ppion20.Vect());

      thetamax = thetataumax;

      double angleTau3pi = n3pi.Angle(pTaurec1.Vect());
      double angleTau3pi2 = n3pi.Angle(pDst0.Vect());

      cosTau3pi = cos(angleTau3pi);
      cosTau3pi2 = cos(angleTau3pi2);

      TLorentzVector ppion01;
      TLorentzVector ppion11;
      TLorentzVector ppion21;
      ppion01.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mpi);
      ppion11.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mpi);
      ppion21.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mpi);
      TLorentzVector ppion02;
      TLorentzVector ppion12;
      TLorentzVector ppion22;
      ppion02.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mpi);
      ppion12.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mpi);
      ppion22.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mpi);

      TLorentzVector pions01 = (ppion01+ppion11);
      TLorentzVector pions12 = (ppion11+ppion21);

      ppion01.Boost(-pions01.Px()/pions01.E(),-pions01.Py()/pions01.E(),-pions01.Pz()/pions01.E());
      ppion11.Boost(-pions01.Px()/pions01.E(),-pions01.Py()/pions01.E(),-pions01.Pz()/pions01.E());
      ppion21.Boost(-pions01.Px()/pions01.E(),-pions01.Py()/pions01.E(),-pions01.Pz()/pions01.E());

      ppion02.Boost(-pions12.Px()/pions12.E(),-pions12.Py()/pions12.E(),-pions12.Pz()/pions12.E());
      ppion12.Boost(-pions12.Px()/pions12.E(),-pions12.Py()/pions12.E(),-pions12.Pz()/pions12.E());
      ppion22.Boost(-pions12.Px()/pions12.E(),-pions12.Py()/pions12.E(),-pions12.Pz()/pions12.E());

      double ang01 =  ppion01.Vect().Angle(ppion21.Vect());
      double ang12 =  ppion22.Vect().Angle(ppion02.Vect());

      cos01 = cos(ang01);
      cos12 = cos(ang12);
      
      if (M01<M12) {
        cos2pimin  = cos01;
        cos2pimax  = cos12;
      } else {
        cos2pimin  = cos12;
        cos2pimax  = cos01;
      }

      q2MCFF = -1.;

      if (MonteCarlo) {

        // B and D decays

        int B_n_D0 = 0;
        int B_n_Ds = 0;
        int B_n_Dp = 0;
        int B_n_Dsst = 0;
        int B_n_Dsst0 = 0;
        int B_n_Ds1 = 0;
        int B_n_Dst0 = 0;
        int B_n_Dst = 0;
        int B_n_D_10 = 0;
        int B_n_D_1p = 0;
        int B_n_Dp_10 = 0;
        int B_n_Dp_1p = 0;
        int B_n_D_2st0 = 0;
        int B_n_D_2stp = 0;
        int B_n_Ds_2stp = 0;
        int B_n_pi = 0;
        int B_n_K = 0;
        int B_n_a1 = 0;
        int B_n_a10 = 0;
        int B_n_pi0 = 0;
        int B_n_rho0 = 0;
        int B_n_f2 = 0;
        int B_n_omega = 0;
        int B_n_rhop = 0;
        int B_n_K_2stp = 0;
        int B_n_etap = 0;
        int B_n_Kst0 = 0;
        int B_n_Dp_s1 = 0;
        int B_n_D0st0 = 0;
        int B_n_D0stp = 0;

        nK0fromB = 0;
        nKSfromB = 0;
        nKLfromB = 0;

        int n_tau = 0;
        int n_nutau = 0;
        TLorentzVector p_tau, p_nu;

        BpDecayCode = 0;
        BdDecayCode = 0;
        BsDecayCode = 0;
        B2DsDecayCode = 0;
        B2D0DecayCode = 0;
        B2DpDecayCode = 0;
        B2tauDecayCode = 0;

        for (int id=0; id<B_Added_B_ndaug;id++) {

          if (abs(B_Added_daugID[id])==311 || abs(B_Added_daugID[id])==310 || abs(B_Added_daugID[id])==130) { nK0fromB ++; }
          if (abs(B_Added_daugID[id])==310) { nKSfromB ++; }
          if (abs(B_Added_daugID[id])==130) { nKLfromB ++; }

          if (abs(B_Added_daugID[id])==313) { B_n_Kst0 ++; }

          if (abs(B_Added_daugID[id]) == 431) { B_n_Ds ++; }
          if (abs(B_Added_daugID[id]) == 433) { B_n_Dsst ++; }
          if (abs(B_Added_daugID[id]) == 10431) { B_n_Dsst0 ++; }
          if (abs(B_Added_daugID[id]) == 20433) { B_n_Ds1 ++; }

          if (abs(B_Added_daugID[id]) == 10421) { B_n_D0st0 ++; }
          if (abs(B_Added_daugID[id]) == 10411) { B_n_D0stp ++; }

          if (abs(B_Added_daugID[id]) == 10433) { B_n_Dp_s1 ++; }

          if (abs(B_Added_daugID[id]) == 411) { B_n_Dp ++; }
          if (abs(B_Added_daugID[id]) == 421) { B_n_D0 ++; }
          if (abs(B_Added_daugID[id]) == 423) { B_n_Dst0 ++; }
          if (abs(B_Added_daugID[id]) == 10423) { B_n_D_10 ++; }
          if (abs(B_Added_daugID[id]) == 10413) { B_n_D_1p ++; }
          if (abs(B_Added_daugID[id]) == 20423) { B_n_Dp_10 ++; }
          if (abs(B_Added_daugID[id]) == 20413) { B_n_Dp_1p ++; }
          if (abs(B_Added_daugID[id]) == 425) { B_n_D_2st0 ++; }
          if (abs(B_Added_daugID[id]) == 415) { B_n_D_2stp ++; }
          if (abs(B_Added_daugID[id]) == 435) { B_n_Ds_2stp ++; }
          if (abs(B_Added_daugID[id]) == 211) { B_n_pi ++; }
          if (abs(B_Added_daugID[id]) == 321) { B_n_K ++; }
          if (abs(B_Added_daugID[id]) == 20213) { B_n_a1 ++; }
          if (abs(B_Added_daugID[id]) == 20113) { B_n_a10 ++; }
          if (abs(B_Added_daugID[id]) == 111) { B_n_pi0 ++; }
          if (abs(B_Added_daugID[id]) == 113) { B_n_rho0 ++; }
          if (abs(B_Added_daugID[id]) == 225) { B_n_f2 ++; }
          if (abs(B_Added_daugID[id]) == 413) { B_n_Dst ++; }
          if (abs(B_Added_daugID[id]) == 223) { B_n_omega ++; }
          if (abs(B_Added_daugID[id]) == 213) { B_n_rhop ++; }
          if (abs(B_Added_daugID[id]) == 325) { B_n_K_2stp ++; }
          if (abs(B_Added_daugID[id]) == 331) { B_n_etap ++; }

          if (abs(B_Added_daugID[id])==15) { // tau
            n_tau++;
            p_tau.SetPxPyPzE(B_Added_daug_TRUEP_X[id],B_Added_daug_TRUEP_Y[id],B_Added_daug_TRUEP_Z[id],B_Added_daug_TRUEP_E[id]);
          }
          if (abs(B_Added_daugID[id])==16) { // nu_tau
            n_nutau++;
            p_nu.SetPxPyPzE(B_Added_daug_TRUEP_X[id],B_Added_daug_TRUEP_Y[id],B_Added_daug_TRUEP_Z[id],B_Added_daug_TRUEP_E[id]);
          }

        }

        if (n_tau>0 && n_nutau>0) {
          q2MCFF = (p_tau+p_nu).M2()*1.e-6;
        }

        int B_n_light = B_n_pi+B_n_K+B_n_a1+B_n_a10+B_n_pi0+B_n_rho0+B_n_f2+B_n_omega+B_n_rhop+B_n_K_2stp+B_n_etap+nK0fromB+B_n_Kst0;

        // B --> D0 tau X
        
        if (n_tau>0) {
          if (abs(B_Added_ID)==521) { // B+
            if (B_n_D0==1) {
              B2tauDecayCode = 2101;
            } else if (B_n_Dst0==1) {
              B2tauDecayCode = 2201;
            } else {
              if (B_n_D0st0==1) {
                B2tauDecayCode = 2301;
              } else if (B_n_D_10==1) {
                B2tauDecayCode = 2401;
              } else if (B_n_Dp_10==1) {
                B2tauDecayCode = 2501;
              } else if (B_n_D_2st0==1) {
                B2tauDecayCode = 2601;
              } else {
                B2tauDecayCode = 2099;
              }
            }
          } else if (abs(B_Added_ID)==511) { // B0
            if (B_n_Dp==1) {
              B2tauDecayCode = 1101;
            } else if (B_n_Dst==1) {
              B2tauDecayCode = 1201;
            } else {
              if (B_n_D0stp==1) {
                B2tauDecayCode = 1301;
              } else if (B_n_D_1p==1) {
                B2tauDecayCode = 1401;
              } else if (B_n_Dp_1p==1) {
                B2tauDecayCode = 1501;
              } else if (B_n_D_2stp==1) {
                B2tauDecayCode = 1601;
              } else {
                B2tauDecayCode = 1099;
              }
            }
          } else if (abs(B_Added_ID)==531) { // B_s0
            if (B_n_Dp_s1==1) {
              B2tauDecayCode = 3301;
            } else if (B_n_Ds_2stp==1) {
              B2tauDecayCode = 3302;
            }
          } else if (abs(B_Added_ID)==5122) { // Lambda_b0
            B2tauDecayCode = 4001;
          }
        }
        
        // B --> D0 Ds X

        if (B_n_Ds+B_n_Dsst+B_n_Dsst0+B_n_Ds1>0) {
          if (abs(B_Added_ID)==521) { // B+
            if (B_n_D0==1) {
              //if (B_n_Ds==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2101;
              //} else if (B_n_Dsst==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2102;
              //} else if (B_n_Dsst0==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2103;
              //} else if (B_n_Ds1==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2104;
              //} else { B2DsDecayCode = 2199;}
              if (B_n_Ds==1 && B_n_light==0) { B2DsDecayCode = 2101;
              } else if (B_n_Dsst==1 && B_n_light==0) { B2DsDecayCode = 2102;
              } else if (B_n_Dsst0==1 && B_n_light==0) { B2DsDecayCode = 2103;
              } else if (B_n_Ds1==1 && B_n_light==0) { B2DsDecayCode = 2104;
              } else { B2DsDecayCode = 2199;}
            } else if (B_n_Dst0==1) {
              //if (B_n_Ds==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2201;
              //} else if (B_n_Dsst==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2202;
              //} else if (B_n_Dsst0==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2203;
              //} else if (B_n_Ds1==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 2204;
              //} else { B2DsDecayCode = 2299;}
              if (B_n_Ds==1 && B_n_light==0) { B2DsDecayCode = 2201;
              } else if (B_n_Dsst==1 && B_n_light==0) { B2DsDecayCode = 2202;
              } else if (B_n_Dsst0==1 && B_n_light==0) { B2DsDecayCode = 2203;
              } else if (B_n_Ds1==1 && B_n_light==0) { B2DsDecayCode = 2204;
              } else { B2DsDecayCode = 2299;}
            } else {
              B2DsDecayCode = 2099;
            }
          } else if (abs(B_Added_ID)==511) { // B0
            if (B_n_Dst==1) {
              //if (B_n_Ds==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 1201;
              //} else if (B_n_Dsst==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 1202;
              //} else if (B_n_Dsst0==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 1203;
              //} else if (B_n_Ds1==1 && B_n_pi==0 && B_n_K==0 && B_n_pi0==0) { B2DsDecayCode = 1204;
              //} else { B2DsDecayCode = 1299;}
              if (B_n_Ds==1 && B_n_light==0) { B2DsDecayCode = 1201;
              } else if (B_n_Dsst==1 && B_n_light==0) { B2DsDecayCode = 1202;
              } else if (B_n_Dsst0==1 && B_n_light==0) { B2DsDecayCode = 1203;
              } else if (B_n_Ds1==1 && B_n_light==0) { B2DsDecayCode = 1204;
              } else { B2DsDecayCode = 1299;}
            } else {
              B2DsDecayCode = 1099;
            }
          } else if (abs(B_Added_ID)==531) { // B_s0
              B2DsDecayCode = 3099;
          }
        }

        // B --> D0 D0b X
        
        if (TMath::Abs(B_Added_D1_ID)==421 && TMath::Abs(B_Added_D2_ID)==421) {

          if (abs(B_Added_ID)==521) { // B+
            if (B_n_D0==1) {
              if (B_n_Dst==1 && B_n_light==0) {
                B2D0DecayCode = 2101;
              } else if (B_n_Dp_s1==1 && B_n_light==0) {
                B2D0DecayCode = 2102;
              } else {
                B2D0DecayCode = 2199;
              }
            } else if (B_n_Dst0==1) {
              if (B_n_Dst==1 && B_n_light==0) {
                B2D0DecayCode = 2201;
              } else if (B_n_Dp_s1==1 && B_n_light==0) {
                B2D0DecayCode = 2202;
              } else {
                B2D0DecayCode = 2299;
              }
            } else {
              B2D0DecayCode = 2099;
            }
          } else if (abs(B_Added_ID)==511) { // B0

            if (B_n_Dst==2 && B_n_light==0) {
              B2D0DecayCode = 1101;
            } else if (B_n_Dst==1 && B_n_Dp_s1==1 && B_n_light==0) {
              B2D0DecayCode = 1102;
            } else {
              B2D0DecayCode = 1099;
            }

          } else if (abs(B_Added_ID)==531) { // B_s0

            B2D0DecayCode = 3099;

          }

        }

        // B --> D0 D- X
        
        if ( (TMath::Abs(B_Added_D1_ID)==421 && TMath::Abs(B_Added_D2_ID)==411) || (TMath::Abs(B_Added_D1_ID)==411 && TMath::Abs(B_Added_D2_ID)==421)) {

          if (abs(B_Added_ID)==521) { // B+
            if (B_n_D0==1) {
              if (B_n_Dp==1 && B_n_light==0) {
                B2DpDecayCode = 2101;
              } else if (B_n_Dst==1 && B_n_light==0) {
                B2DpDecayCode = 2102;
              } else if (B_n_Dp_s1==1 && B_n_light==0) {
                B2DpDecayCode = 2103;
              } else {
                B2DpDecayCode = 2199;
              }
            } else if (B_n_Dst0==1) {
              if (B_n_Dp==1 && B_n_light==0) {
                B2DpDecayCode = 2201;
              } else if (B_n_Dst==1 && B_n_light==0) {
                B2DpDecayCode = 2202;
              } else if (B_n_Dp_s1==1 && B_n_light==0) {
                B2DpDecayCode = 2203;
              } else {
                B2DpDecayCode = 2299;
              }
            } else {
              B2DpDecayCode = 2099;
            }
          } else if (abs(B_Added_ID)==511) { // B0
            if (B_n_Dst==1 && B_n_Dp==1 && B_n_light==0) {
              B2DpDecayCode = 1201;
            } else if (B_n_Dst==2 && B_n_light==0) {
              B2DpDecayCode = 1202;
            } else if (B_n_Dst==1 && B_n_Dp_s1==1 && B_n_light==0) {
              B2DpDecayCode = 1203;
            } else if (B_n_Dp==1 && B_n_Dp_s1==1 && B_n_light==0) {
              B2DpDecayCode = 1103;
            } else {
              B2DpDecayCode = 1099;
            }
          } else if (abs(B_Added_ID)==531) { // B_s0
            B2DpDecayCode = 3099;
          }

        }


        // B+ --> D0 3pi
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21130000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_a10==0 && B_n_omega==0 && B_n_rho0==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21130001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_rho0==1 && B_n_pi==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21130002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_f2==1 && B_n_pi==1 && B_n_omega==0 && B_n_rho0==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21130003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21230000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_rho0==0 && B_n_omega==0 && B_n_a10==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21230001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_rho0==1 && B_n_pi==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21230002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_f2==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21230003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21330000;
        if (B_n_D_10==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21430001;
        if (B_n_Dp_10==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21430002;
        if (B_n_D_2st0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21430003;

        // B+ --> D0 5pi 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21150000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21150001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21150002;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21250000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21250001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21250002;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21350000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21350001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21350002;

        // B+ --> D0 7pi 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21170000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21170001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21170002;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21270000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21270001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21270002;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21370000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21370001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21370002;

        // B+ --> D0(*-,*0,0) 2K 3pi 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21130200;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21130201;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21130202;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21230200;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21230201;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21230202;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21330200;

        // B+ --> D0 3pi pi0's
        if (B_n_D0==1 && B_n_a1==0 && B_n_omega==1 && B_n_rho0==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21131000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_omega==1 && B_n_rho0==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21231000;
        if (B_n_Dp_10==1 && B_n_a1==0 && B_n_rhop==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21431000;
        if (B_n_D_2st0==1 && B_n_a1==0 && B_n_rhop==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21431001;
        if (B_n_D_10==1 && B_n_a1==0 && B_n_rhop==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==0 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21431002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_rhop==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21331000;

        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21131001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21131002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21131003;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21231001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21231002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21231003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21331001;

        //if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21132000;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21132001;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21132002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21132003;
        //if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21232000;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21232001;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21232002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21232003;
        //if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21332000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21133000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21233000;

        // B+ --> D0 5pi pi0 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21151000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21151001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21151002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21151003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21151004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21151005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21251000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21251001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21251002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21251003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21251004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21251005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21351000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21351001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21351002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21351003;

        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21152000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21152001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21152002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21152003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21152004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21152005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21252000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21252001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21252002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21252003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21252004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21252005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21352000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21352001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21352002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21352003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21153000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21253000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21353000;

        // B+ --> D0 7pi pi0's
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21171000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21171001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21171002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21171003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21171004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21171005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21271000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21271001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21271002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21271003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21271004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21271005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21371000;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21371001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21371002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21371003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21371004;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BpDecayCode = 21371005;

        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21172000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21172001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21172002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21172003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21172004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21172005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21272000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21272001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21272002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21272003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21272004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21272005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21372000;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21372001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21372002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21372003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BpDecayCode = 21372004;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BpDecayCode = 21372005;

        // B+ --> D0 2K 3pi pi0's
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21131200;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21131201;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21131202;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21131203;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21231200;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21231201;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21231202;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==2) BpDecayCode = 21231203;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==2) BpDecayCode = 21331200;

        // B0 

        // B0 --> D0 4pi
        if (B_n_Dst==1 && B_n_a1==1 && B_n_rho0==0 && B_n_omega==0 && B_n_a10==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11340000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_f2==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11340001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_rho0==1 && B_n_pi==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11340002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11340003;
        if (B_n_D_10==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11440000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11140000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11140001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11140002;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11240000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11240001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11240002;

        // B0 --> D0 6pi 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11160000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11160001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11160002;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11260000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11260001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11260002;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11360000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11360001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11360002;

        // B0 --> D0 8pi 
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11180000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11180001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==8 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11180002;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11280000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11280001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==8 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11280002;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11380000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11380001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11380002;

        // B0 --> D0 3pi K K0
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11130200;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11130201;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11130202;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11230200;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11230201;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11230202;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==1) BdDecayCode = 11330200;

        // B0 --> D0 4pi pi0's
        if (B_n_Dst==1 && B_n_a1==0 && B_n_omega==1 && B_n_rho0==0 && B_n_pi==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11341000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_etap==1 && B_n_pi==0 && B_n_omega==0 && B_n_rho0==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11100002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_etap==1 && B_n_pi==0 && B_n_omega==0 && B_n_rho0==0 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11200002;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11141000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11141001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11141002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11141003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11141004;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11241000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11241001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11241002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11241003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11241004;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11341001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11341002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11341003;

        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11142000;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11142001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11142002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11142003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11142004;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11242000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11242001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11242002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11242003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11242004;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11342000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11342001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11342002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11342003;

        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11143000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==0 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11243000;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11343000;

        // B0 --> D0 6pi pi0's
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11161000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11161001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11161002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11161003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11161004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11161005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11261000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11261001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11261002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11261003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11261004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11261005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11361000;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11361001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11361002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11361003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11361004;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11361005;

        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11162000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11162001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11162002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11162003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11162004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11162005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11262000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11262001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11262002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11262003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11262004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11262005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11362000;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11362001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11362002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11362003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==0) BdDecayCode = 11362004;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11362005;

        // B0 --> D0 8pi pi0's
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11181000;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11181001;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11181002;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11181003;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==8 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11181004;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11181005;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11281000;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11281001;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11281002;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11281003;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==8 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11281004;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==6 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11281005;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11381000;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11381001;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11381002;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11381003;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==7 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==0) BdDecayCode = 11381004;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==0) BdDecayCode = 11381005;


        // B_s0

        // B_s0 --> D0 eta' 
        if (B_n_D0==1 && B_n_a1==0 && B_n_etap==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BsDecayCode = 31100000;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_etap==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==0) BsDecayCode = 31200000;

        // B_s0 --> D0 3pi K
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31130100;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31130101;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31130102;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31230100;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31230101;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31230102;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31330100;

        // B_s0 --> D0 5pi K
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31150100;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31150101;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31150102;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31250100;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31250101;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31250102;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31350100;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31350101;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==0 && B_n_K==1 && nK0fromB==0) BsDecayCode = 31350102;

        // B_s0 --> D0 3pi K pi0's
        //if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31131100;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31131101;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31131102;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31131103;
        //if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31231100;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31231101;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31231102;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31231103;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31331100;

        //if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31132100;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31132101;
        //if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31132102;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31132103;
        //if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31232100;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31232101;
        //if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31232102;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31232103;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31332100;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31133100;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==2 && B_n_K==1) BsDecayCode = 31233100;

        // B_s0 --> D0 5pi K pi0's
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31151100;
        if (B_n_D0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31151101;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31151102;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31151103;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31151104;
        if (B_n_D0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31151105;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31251100;
        if (B_n_Dst0==1 && B_n_a1==1 && B_n_pi==0 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31251101;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31251102;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==1 && B_n_rho0==1 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31251103;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==5 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31251104;
        if (B_n_Dst0==1 && B_n_a1==0 && B_n_pi==3 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31251105;
        if (B_n_Dst==1 && B_n_a1==1 && B_n_pi==1 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31351100;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==1 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31351101;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==4 && B_n_rho0==0 && B_n_omega==0 && B_n_pi0==1 && B_n_K==1) BsDecayCode = 31351102;
        if (B_n_Dst==1 && B_n_a1==0 && B_n_pi==2 && B_n_rho0==0 && B_n_omega==1 && B_n_pi0==0 && B_n_K==1) BsDecayCode = 31351103;


        if ( false && BsDecayCode==0 && B_Added_B_ndaug>1) {
          //cout<<B_n_D0<<" "<<B_n_pi<<" "<<B_n_pi0<<" "<<B_n_K<<endl;
        cout<<" "<<endl;
        cout<<"B_Added_B_ndaug "<<B_Added_B_ndaug<<endl;
        for (int id=0; id<B_Added_B_ndaug;id++) {
          cout<<B_Added_daugID[id]<<endl;
        }
        cout<<"B_n_D0 "<<B_n_D0<<endl;
        cout<<"B_n_Dst0 "<<B_n_Dst0<<endl;
        cout<<"B_n_Dst "<<B_n_Dst<<endl;
        cout<<"B_n_D_10 "<<B_n_D_10<<endl;
        cout<<"B_n_Dp_10 "<<B_n_Dp_10<<endl;
        cout<<"B_n_D_2st0 "<<B_n_D_2st0<<endl;
        cout<<"B_n_pi "<<B_n_pi<<endl;
        cout<<"B_n_K "<<B_n_K<<endl;
        cout<<"B_n_a1 "<<B_n_a1<<endl;
        cout<<"B_n_a10 "<<B_n_a10<<endl;
        cout<<"B_n_pi0 "<<B_n_pi0<<endl;
        cout<<"B_n_rho0 "<<B_n_rho0<<endl;
        cout<<"B_n_f2 "<<B_n_f2<<endl;
        cout<<"B_n_omega "<<B_n_omega<<endl;
        cout<<"B_n_rhop "<<B_n_rhop<<endl;
        cout<<"B_n_K_2stp "<<B_n_K_2stp<<endl;
        cout<<"B_n_etap "<<B_n_etap<<endl;

        }


        DsDecay = "null";
        DsDecayCode = 0;
        D0DecayCode = 0;
        DpDecayCode = 0;

        int neta = 0;
        int netap = 0;
        int nomega = 0;
        int nphi = 0;
        int npi = 0;
        int npi0 = 0;
        int nrho0 = 0;
        int nrhop = 0;
        int ntau = 0;
        int nK0 = 0;
        int nK = 0;
        int na1 = 0;
        int nKp10 = 0;
        int nKst0 = 0;
        int nlep = 0;

        bool thereisDs = false;
        bool thereisDp = false;

        if (abs(B_Added_D1_ID)==431 || abs(B_Added_D1_ID)==411) {

          if (abs(B_Added_D1_ID)==431) thereisDs = true;
          if (abs(B_Added_D1_ID)==411) thereisDp = true;

          for (int id=0; id<B_Added_D1_ndaug;id++) {

            if (abs(B_Added_D1_daugID[id])==331) { // etap
              netap ++;
            } else if (abs(B_Added_D1_daugID[id])==221) { // eta
              neta ++;
            } else if (abs(B_Added_D1_daugID[id])==223) { // omega
              nomega ++;
            } else if (abs(B_Added_D1_daugID[id])==333) { // phi
              nphi ++;
            } else if (abs(B_Added_D1_daugID[id])==211) { // pi
              npi ++;
            } else if (abs(B_Added_D1_daugID[id])==321) { // K
              nK ++;
            } else if (abs(B_Added_D1_daugID[id])==111) { // pi0
              npi0 ++;
            } else if (abs(B_Added_D1_daugID[id])==113) { // rho0
              nrho0 ++;
            } else if (abs(B_Added_D1_daugID[id])==213) { // rho+
              nrhop ++;
            } else if (abs(B_Added_D1_daugID[id])==20313) { // K'_10
              nKp10 ++;
            } else if (abs(B_Added_D1_daugID[id])==313) { // K*0
              nKst0 ++;
            } else if (abs(B_Added_D1_daugID[id])==15) { // tau
              ntau ++;
            } else if (abs(B_Added_D1_daugID[id])==20213) { // a_1+
              na1 ++;
            //} else if (abs(B_Added_D1_daugID[id])==310 || abs(B_Added_D1_daugID[id])==130) { // K_S0 || K_L0
            } else if (abs(B_Added_D1_daugID[id])==310 || abs(B_Added_D1_daugID[id])==130 || abs(B_Added_D1_daugID[id])==311) { // K_S0 || K_L0 || K0
              nK0 ++;
            } else if (abs(B_Added_D1_daugID[id])==11 || abs(B_Added_D1_daugID[id])==13) { // e || mu
              nlep ++;
            }

          }

        }

        if (abs(B_Added_D2_ID)==431 || abs(B_Added_D2_ID)==411) {

          if (abs(B_Added_D2_ID)==431) thereisDs = true;
          if (abs(B_Added_D2_ID)==411) thereisDp = true;

          for (int id=0; id<B_Added_D2_ndaug;id++) {

            if (abs(B_Added_D2_daugID[id])==331) { // etap
              netap ++;
            } else if (abs(B_Added_D2_daugID[id])==221) { // eta
              neta ++;
            } else if (abs(B_Added_D2_daugID[id])==223) { // omega
              nomega ++;
            } else if (abs(B_Added_D2_daugID[id])==333) { // phi
              nphi ++;
            } else if (abs(B_Added_D2_daugID[id])==211) { // pi
              npi ++;
            } else if (abs(B_Added_D2_daugID[id])==321) { // K
              nK ++;
            } else if (abs(B_Added_D2_daugID[id])==111) { // pi0
              npi0 ++;
            } else if (abs(B_Added_D2_daugID[id])==113) { // rho0
              nrho0 ++;
            } else if (abs(B_Added_D2_daugID[id])==213) { // rho+
              nrhop ++;
            } else if (abs(B_Added_D2_daugID[id])==20313) { // K'_10
              nKp10 ++;
            } else if (abs(B_Added_D2_daugID[id])==313) { // K*0
              nKst0 ++;
            } else if (abs(B_Added_D2_daugID[id])==15) { // tau
              ntau ++;
            } else if (abs(B_Added_D2_daugID[id])==20213) { // a_1+
              na1 ++;
            } else if (abs(B_Added_D2_daugID[id])==310 || abs(B_Added_D2_daugID[id])==130 || abs(B_Added_D2_daugID[id])==311) { // K_S0 || K_L0
              nK0 ++;
            } else if (abs(B_Added_D2_daugID[id])==11 || abs(B_Added_D2_daugID[id])==13) { // e || mu
              nlep ++;
            }

          }

        }

        int n_tau_pi = 0;
        int n_tau_pi0 = 0;
        tauDecayCode = 0;

        for (int it=0;it<B_Added_tau_ndaug;it++) {
          if(TMath::Abs(B_Added_tau_daugID[it])==211) n_tau_pi ++;
          if(TMath::Abs(B_Added_tau_daugID[it])==111) n_tau_pi0 ++;
        }

        if (n_tau_pi==3 && n_tau_pi0==0) tauDecayCode = 1;
        if (n_tau_pi==3 && n_tau_pi0==1) tauDecayCode = 2;


        if (ntau>0) { 
          if (thereisDs) {
            DsDecay = "Ds2tau"; DsDecayCode = 1; 
          } 
          if(thereisDp) DpDecayCode = 1; 
        }
        if (npi==5 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds25pi"; DsDecayCode = 2; 
          } 
          if(thereisDp) DpDecayCode = 2; 
        }
        if (npi==3 && npi0==0 && netap==0 && neta==0 && nphi==0 && nomega==0 ) { 
          if (thereisDs) {
            DsDecay = "Ds23pi"; DsDecayCode = 3; 
          } 
          if(thereisDp) DpDecayCode = 3; 
        }
        if (npi==3 && npi0==1 && netap==0 && neta==0 && nphi==0 && nomega==0 ) { 
          if (thereisDs) {
            DsDecay = "Ds23pipi0"; DsDecayCode = 4; 
          } 
          if(thereisDp) DpDecayCode = 4; 
        }
        //if (nK0>0 && (npi==3 || na1==1 || (npi==1 && nrho0==1)) && npi0==0) { 
        if ( (nK0==1 && npi0==0 && (npi==3 || na1==1 || (npi==1 && nrho0==1)) ) || ( nKp10==1 && npi==1) ) { 
          if (thereisDs) {
            DsDecay = "Ds23piK0"; DsDecayCode = 5; 
          } 
          if(thereisDp) DpDecayCode = 5; 
        }
        if (nK==1 && npi==2 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2K2pi"; DsDecayCode = 6; 
          } 
          if(thereisDp) DpDecayCode = 6; 
        }
        if (nlep>0) { 
          if (thereisDs) {
            DsDecay = "Ds2lep"; DsDecayCode = 7; 
          } 
          if(thereisDp) DpDecayCode = 7; 
        }
        if (netap==1 && npi==1 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2etappi"; DsDecayCode = 8; 
          } 
          if(thereisDp) DpDecayCode = 8; 
        }
        if (neta==1 && npi==1 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2etapi"; DsDecayCode = 9; 
          } 
          if(thereisDp) DpDecayCode = 9; 
        }
        if (nomega==1 && npi==1 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2omegapi"; DsDecayCode = 10; 
          } 
          if(thereisDp) DpDecayCode = 10; 
        }
        if (nphi==1 && npi==1 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2phipi"; DsDecayCode = 11; 
          } 
          if(thereisDp) DpDecayCode = 11; 
        }

        //if (netap==1 && (nrhop==1 || ( npi==1 && npi0==1)) ) { 
        if (netap==1 && (nrhop==1 || ( npi==1 && npi0>0)) ) { 
          if (thereisDs) {
            DsDecay = "Ds2etappipi0"; DsDecayCode = 12; 
          } 
          if(thereisDp) DpDecayCode = 12; 
        }
        //if (neta==1 && (nrhop==1 || ( npi==1 && npi0==1)) ) { 
        if (neta==1 && (nrhop==1 || ( npi==1 && npi0>0)) ) { 
          if (thereisDs) {
            DsDecay = "Ds2etapipi0"; DsDecayCode = 13; 
          } 
          if(thereisDp) DpDecayCode = 13; 
        }
        //if (nomega==1 && (nrhop==1 || ( npi==1 && npi0==1)) ) { 
        if (nomega==1 && (nrhop==1 || ( npi==1 && npi0>0)) ) { 
          if (thereisDs) {
            DsDecay = "Ds2omegapipi0"; DsDecayCode = 14; 
          } 
          if(thereisDp) DpDecayCode = 14; 
        }
        //if (nphi==1 && (nrhop==1 || ( npi==1 && npi0==1)) ) { 
        if (nphi==1 && (nrhop==1 || ( npi==1 && npi0>0)) ) { 
          if (thereisDs) {
            DsDecay = "Ds2phipipi0"; DsDecayCode = 15; 
          } 
          if(thereisDp) DpDecayCode = 15; 
        }

        if (netap==1 && npi==3 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2etap3pi"; DsDecayCode = 16; 
          } 
          if(thereisDp) DpDecayCode = 16; 
        }
        if (neta==1 && npi==3 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2eta3pi"; DsDecayCode = 17;
          } 
          if(thereisDp) DpDecayCode = 17;
        }
        if (nomega==1 && npi==3 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2omega3pi"; DsDecayCode = 18; 
          } 
          if(thereisDp) DpDecayCode = 18; 
        }
        if (nphi==1 && npi==3 && npi0==0) { 
          if (thereisDs) {
            DsDecay = "Ds2phi3pi"; DsDecayCode = 19; 
          } 
          if(thereisDp) DpDecayCode = 19; 
        }
        if (nKst0==1 && (npi==3 || na1==1 || (nrho0==1 && npi==1) ) ) { // K*03pi 
          if (thereisDs) {
            DsDecay = "Ds2Kst03pi"; DsDecayCode = 20; 
          } 
          if(thereisDp) DpDecayCode = 20; 
        }
        if (nK==1 && (npi==4 || (nrho0==1 && npi==2) ) ) { // K4pi
          if (thereisDs) {
            DsDecay = "Ds2K4pi"; DsDecayCode = 21; 
          } 
          if(thereisDp) DpDecayCode = 21; 
        }
        if (nK==2 && npi==3 ) { // 2K3pi
          if (thereisDs) {
            DsDecay = "Ds22K3pi"; DsDecayCode = 22; 
          } 
          if(thereisDp) DpDecayCode = 22; 
        }
        if (nK0==1 && ( (npi==3 && npi0==1) || (nomega==1 && npi==1) )  ) { 
          if (thereisDs) {
            DsDecay = "Ds23piK0pi0"; DsDecayCode = 23; 
          } 
          if(thereisDp) DpDecayCode = 23; 
        }
        if (nKst0==1 && ( (npi==3 && npi0==1) || (nomega==1 && npi==1) )  ) { 
          if (thereisDs) {
            DsDecay = "Ds2Kst3pipi0"; DsDecayCode = 24;
          } 
          if(thereisDp) DpDecayCode = 24; 
        }

        wgDstau = 1.;

        if (DsDecayCode==1) { // Ds->taunu
          if (tauDecayCode==1) { // tau->3pinu
            wgDstau *= 2.;
          } 
        }

        //wgDs2eta3pi_shape = functions::weightDs(DsDecayCode,tau_M,M01,M12);
        wgDs2eta3pi_shape = functions::weightDs(DsDecayCode,tauDecayCode,tau_M,M01,M12);

        // D0->3piX decays
        if (TMath::Abs(B_Added_D1_ID)==421 && TMath::Abs(B_Added_D2_ID)==421) {

          npi = 0;
          nK = 0;
          npi0 = 0;
          na1 = 0;
          nrho0 = 0;
          nKst0 = 0;
          int nK_1p = 0;
          int nK_10 = 0;
          nomega = 0;
          netap = 0;
          neta = 0;
          nK0 = 0;
          nphi = 0;
          int nKstp = 0;
          nrhop = 0;

          if (B_Added_D1_ID == -D_ID) {

          for (int id=0; id<B_Added_D1_ndaug;id++) {

            if (abs(B_Added_D1_daugID[id])==211) { // pi
              npi ++;
            } else if (abs(B_Added_D1_daugID[id])==321) { // K
              nK ++;
            } else if (abs(B_Added_D1_daugID[id])==111) { // pi0
              npi0 ++;
            } else if (abs(B_Added_D1_daugID[id])==20213) { // a_1+
              na1 ++;
            } else if (abs(B_Added_D1_daugID[id])==113) { // rho0
              nrho0 ++;
            } else if (abs(B_Added_D1_daugID[id])==313) { // K*0
              nKst0 ++;
            } else if (abs(B_Added_D1_daugID[id])==323) { // K*+
              nKstp ++;
            } else if (abs(B_Added_D1_daugID[id])==10323) { // K_1+
              nK_1p ++;
            } else if (abs(B_Added_D1_daugID[id])==10313) { // K_10
              nK_10 ++;
            } else if (abs(B_Added_D1_daugID[id])==223) { // omega
              nomega ++;
            } else if (abs(B_Added_D1_daugID[id])==331) { // etap
              netap ++;
            } else if (abs(B_Added_D1_daugID[id])==221) { // eta
              neta ++;
            } else if (abs(B_Added_D1_daugID[id])==310 || abs(B_Added_D1_daugID[id])==130 || abs(B_Added_D1_daugID[id])==311) { // K_S0 || K_L0 || K0
              nK0 ++;
            } else if (abs(B_Added_D1_daugID[id])==333) { // phi
              nphi ++;
            } else if (abs(B_Added_D1_daugID[id])==213) { // rho+
              nrhop ++;
            }
          }

          }

          if (B_Added_D2_ID == -D_ID) {

          for (int id=0; id<B_Added_D2_ndaug;id++) {

            if (abs(B_Added_D2_daugID[id])==211) { // pi
              npi ++;
            } else if (abs(B_Added_D2_daugID[id])==321) { // K
              nK ++;
            } else if (abs(B_Added_D2_daugID[id])==111) { // pi0
              npi0 ++;
            } else if (abs(B_Added_D2_daugID[id])==20213) { // a_1+
              na1 ++;
            } else if (abs(B_Added_D2_daugID[id])==113) { // rho0
              nrho0 ++;
            } else if (abs(B_Added_D2_daugID[id])==313) { // K*0
              nKst0 ++;
            } else if (abs(B_Added_D2_daugID[id])==323) { // K*+
              nKstp ++;
            } else if (abs(B_Added_D2_daugID[id])==10323) { // K_1+
              nK_1p ++;
            } else if (abs(B_Added_D2_daugID[id])==10313) { // K_10
              nK_10 ++;
            } else if (abs(B_Added_D2_daugID[id])==223) { // omega
              nomega ++;
            } else if (abs(B_Added_D2_daugID[id])==331) { // etap
              netap ++;
            } else if (abs(B_Added_D2_daugID[id])==221) { // eta
              neta ++;
            } else if (abs(B_Added_D2_daugID[id])==310 || abs(B_Added_D2_daugID[id])==130 || abs(B_Added_D2_daugID[id])==311) { // K_S0 || K_L0 || K0
              nK0 ++;
            } else if (abs(B_Added_D2_daugID[id])==333) { // phi
              nphi ++;
            } else if (abs(B_Added_D2_daugID[id])==213) { // rho+
              nrhop ++;
            }
          }

          }

          D0DecayCode = 1;
          if (nK==1 && npi==3 && npi0==0 && nK0==0 ) { 
            D0DecayCode = 401;
          } else if (nK==1 && na1==1 && npi0==0 && nK0==0 ) { 
            D0DecayCode = 402;
          } else if (nK==1 && npi==1 && npi0==0 && nrho0==1 && nK0==0 ) { 
            D0DecayCode = 403;
          } else if (nKst0==1 && npi==2 && npi0==0 && nK0==0 ) { 
            D0DecayCode = 404;
          } else if (nKst0==1 && nrho0==1 && npi==0 && npi0==0 && nK0==0 ) { 
            D0DecayCode = 405;
          } else if (nK_1p==1 && npi==1 && npi0==0 && nK0==0 ) { 
            D0DecayCode = 406;
          } else if (nKst0==1 && npi==2 &&  npi0==1 && nK0==0 ) { 
            D0DecayCode = 407;
          } else if (nK==1 && npi==1 &&  nomega==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 408;
          } else if (nKst0==1 && nomega==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 409;
          } else if (nK==1 && npi==1 && neta==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 410;
          } else if (nK==1 && npi==1 && netap==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 411;
          } else if (nK==0 && npi==4 && npi0==0 && nK0==0) { 
            D0DecayCode = 412;
          } else if (na1==1 && npi==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 413;
          } else if (nrho0==2 && npi==0 && npi0==0 && nK0==0) { 
            D0DecayCode = 414;
          } else if (npi==4 && npi0==0 && nK0==1) { 
            D0DecayCode = 415;
          } else if (npi==4 && npi0==1 && nK0==0) { 
            D0DecayCode = 416;
          } else if (npi==2 && neta==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 417;
          } else if (npi==2 && nomega==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 418;
          } else if (npi==6 && npi0==0 && nK0==0) { 
            D0DecayCode = 419;
          } else if (npi==2 && nphi==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 420;
          } else if (npi==2 && netap==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 421;
          } else if (nphi==1 && neta==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 422;
          } else if (nKst0==1 && neta==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 423;
          } else if (nKst0==1 && netap==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 424;
          } else if (neta==2 && npi0==0 && nK0==0) { 
            D0DecayCode = 425;
          } else if (neta==1 && netap==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 426;
          } else if (neta==0 && netap==0 && npi==5 && nK==1 && npi0==0 && nK0==0) { 
            D0DecayCode = 427;

          } else if (npi==2 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0 && nK0==0) { 
            D0DecayCode = 201;
          } else if (npi==2 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==1 && nK0==0) { 
            D0DecayCode = 202;
          } else if (nK0==1 && npi==2 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0) { 
            D0DecayCode = 203;
          } else if (nK0==1 && npi==2 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==1) { 
            D0DecayCode = 204;
          } else if (nK0==2 && npi==2 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0) { 
            D0DecayCode = 205;
          } else if (nK0==0 && npi==0 && nrhop==1 && nKstp==1 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0) { 
            D0DecayCode = 206;
          } else if (nK0==1 && nomega==1 && npi==0 && nrhop==0 && nKstp==0 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0) { 
            D0DecayCode = 207;
          } else if (nK0==0 && npi==1 && nrhop==0 && nKstp==1 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==1) { 
            D0DecayCode = 208;
          } else if (nK0==0 && npi==1 && nrhop==0 && nKstp==1 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==0) { 
            D0DecayCode = 210;
          } else if (nK0==0 && npi==0 && nrhop==0 && nK_10==1 && neta==0 && netap==0 && nK_1p==0 && nKst0==0 && npi0==1) { 
            D0DecayCode = 211;
          } else if (nKst0==2 && npi==0 && nrhop==0 && nK_10==0 && neta==0 && netap==0 && nK_1p==0 && npi0==0) { 
            D0DecayCode = 212;
          } else if (nKst0==0 && npi==1 && nK==1 && nrhop==0 && nK_10==0 && neta==0 && netap==0 && nK_1p==0 && npi0==0) { 
            D0DecayCode = 213;
          }
        }

        wg_D0_K_PID = ReweightingPID::weight_D0_K(Polarity,D_K_P,D0_K_eta,nTracks);
        wg_D0_pi_PID = ReweightingPID::weight_D0_pi(Polarity,D_pi_P,D0_pi_eta,nTracks);
        wg_tau_pion0_PID = ReweightingPID::weight_tau_pion0(Polarity,tau_pion0_P,tau_pion0_eta,nTracks);
        wg_tau_pion1_PID = ReweightingPID::weight_tau_pion1(Polarity,tau_pion1_P,tau_pion1_eta,nTracks);
        wg_tau_pion2_PID = ReweightingPID::weight_tau_pion2(Polarity,tau_pion2_P,tau_pion2_eta,nTracks);
        wg_PID = wg_D0_K_PID*wg_D0_pi_PID*wg_tau_pion0_PID*wg_tau_pion1_PID*wg_tau_pion2_PID;

        wg_D0_K_Track = ReweightingTrack::weight(D_K_P,D0_K_eta);
        wg_D0_pi_Track = ReweightingTrack::weight(D_pi_P,D0_pi_eta);
        wg_tau_pion0_Track = ReweightingTrack::weight(tau_pion0_P,tau_pion0_eta);
        wg_tau_pion1_Track = ReweightingTrack::weight(tau_pion1_P,tau_pion1_eta);
        wg_tau_pion2_Track = ReweightingTrack::weight(tau_pion2_P,tau_pion2_eta);
        wg_Track = wg_D0_K_Track*wg_D0_pi_Track*wg_tau_pion0_Track*wg_tau_pion1_Track*wg_tau_pion2_Track;


        // -----------------------------------------------------------------------------------------------------------
       
//        if (MonteCarlo) { 


          TLorentzVector p_D0;
          TLorentzVector p_3pi;

          MC_MD1D2 = -100.;

          if (TMath::Abs(D_K_MC_MOTHER_ID) == 421) p_D0.SetPxPyPzE(D_K_MC_MOTHER_TRUEP_X,D_K_MC_MOTHER_TRUEP_Y,D_K_MC_MOTHER_TRUEP_Z,D_K_MC_MOTHER_TRUEP_E);
          if (TMath::Abs(D_pi_MC_MOTHER_ID) == 421) p_D0.SetPxPyPzE(D_pi_MC_MOTHER_TRUEP_X,D_pi_MC_MOTHER_TRUEP_Y,D_pi_MC_MOTHER_TRUEP_Z,D_pi_MC_MOTHER_TRUEP_E);


          if (TMath::Abs(tau_TRUEID) == 411 || TMath::Abs(tau_TRUEID) == 421 || TMath::Abs(tau_TRUEID) == 431 || TMath::Abs(tau_TRUEID) == 4122 ) p_3pi.SetPxPyPzE(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);
          if (TMath::Abs(tau_pion0_MC_MOTHER_ID) == 411 || TMath::Abs(tau_pion0_MC_MOTHER_ID) == 421 || TMath::Abs(tau_pion0_MC_MOTHER_ID) == 431 || TMath::Abs(tau_pion0_MC_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion0_MC_MOTHER_TRUEP_X,tau_pion0_MC_MOTHER_TRUEP_Y,tau_pion0_MC_MOTHER_TRUEP_Z,tau_pion0_MC_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion1_MC_MOTHER_ID) == 411 || TMath::Abs(tau_pion1_MC_MOTHER_ID) == 421 || TMath::Abs(tau_pion1_MC_MOTHER_ID) == 431 || TMath::Abs(tau_pion1_MC_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion1_MC_MOTHER_TRUEP_X,tau_pion1_MC_MOTHER_TRUEP_Y,tau_pion1_MC_MOTHER_TRUEP_Z,tau_pion1_MC_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion2_MC_MOTHER_ID) == 411 || TMath::Abs(tau_pion2_MC_MOTHER_ID) == 421 || TMath::Abs(tau_pion2_MC_MOTHER_ID) == 431 || TMath::Abs(tau_pion2_MC_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion2_MC_MOTHER_TRUEP_X,tau_pion2_MC_MOTHER_TRUEP_Y,tau_pion2_MC_MOTHER_TRUEP_Z,tau_pion2_MC_MOTHER_TRUEP_E);

          if (TMath::Abs(tau_MC_MOTHER_ID) == 411 || TMath::Abs(tau_MC_MOTHER_ID) == 421 || TMath::Abs(tau_MC_MOTHER_ID) == 431 || TMath::Abs(tau_MC_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_MC_MOTHER_TRUEP_X,tau_MC_MOTHER_TRUEP_Y,tau_MC_MOTHER_TRUEP_Z,tau_MC_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion0_MC_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion0_MC_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion0_MC_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion0_MC_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion0_MC_GD_MOTHER_TRUEP_X,tau_pion0_MC_GD_MOTHER_TRUEP_Y,tau_pion0_MC_GD_MOTHER_TRUEP_Z,tau_pion0_MC_GD_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion1_MC_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion1_MC_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion1_MC_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion1_MC_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion1_MC_GD_MOTHER_TRUEP_X,tau_pion1_MC_GD_MOTHER_TRUEP_Y,tau_pion1_MC_GD_MOTHER_TRUEP_Z,tau_pion1_MC_GD_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion2_MC_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion2_MC_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion2_MC_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion2_MC_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion2_MC_GD_MOTHER_TRUEP_X,tau_pion2_MC_GD_MOTHER_TRUEP_Y,tau_pion2_MC_GD_MOTHER_TRUEP_Z,tau_pion2_MC_GD_MOTHER_TRUEP_E);

          if (TMath::Abs(tau_MC_GD_MOTHER_ID) == 411 || TMath::Abs(tau_MC_GD_MOTHER_ID) == 421 || TMath::Abs(tau_MC_GD_MOTHER_ID) == 431 || TMath::Abs(tau_MC_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_MC_GD_MOTHER_TRUEP_X,tau_MC_GD_MOTHER_TRUEP_Y,tau_MC_GD_MOTHER_TRUEP_Z,tau_MC_GD_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion0_MC_GD_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion0_MC_GD_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion0_MC_GD_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion0_MC_GD_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion0_MC_GD_GD_MOTHER_TRUEP_X,tau_pion0_MC_GD_GD_MOTHER_TRUEP_Y,tau_pion0_MC_GD_GD_MOTHER_TRUEP_Z,tau_pion0_MC_GD_GD_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion1_MC_GD_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion1_MC_GD_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion1_MC_GD_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion1_MC_GD_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion1_MC_GD_GD_MOTHER_TRUEP_X,tau_pion1_MC_GD_GD_MOTHER_TRUEP_Y,tau_pion1_MC_GD_GD_MOTHER_TRUEP_Z,tau_pion1_MC_GD_GD_MOTHER_TRUEP_E);
          if (TMath::Abs(tau_pion2_MC_GD_GD_MOTHER_ID) == 411 || TMath::Abs(tau_pion2_MC_GD_GD_MOTHER_ID) == 421 || TMath::Abs(tau_pion2_MC_GD_GD_MOTHER_ID) == 431 || TMath::Abs(tau_pion2_MC_GD_GD_MOTHER_ID) == 4122 ) p_3pi.SetPxPyPzE(tau_pion2_MC_GD_GD_MOTHER_TRUEP_X,tau_pion2_MC_GD_GD_MOTHER_TRUEP_Y,tau_pion2_MC_GD_GD_MOTHER_TRUEP_Z,tau_pion2_MC_GD_GD_MOTHER_TRUEP_E);

          MC_MD1D2 = (p_D0+p_3pi).M();

//        }

        // -----------------------------------------------------------------------------------------------------------

        //
        // Kinematic variables
        //

        const TLorentzVector pBMC(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        const TLorentzVector pD0MC(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);
        const TLorentzVector pDstMC(D_MC_MOTHER_TRUEP_X,D_MC_MOTHER_TRUEP_Y,D_MC_MOTHER_TRUEP_Z,D_MC_MOTHER_TRUEP_E);
        const TLorentzVector p_pion0_MC(tau_pion0_TRUEP_X,tau_pion0_TRUEP_Y,tau_pion0_TRUEP_Z,tau_pion0_TRUEP_E);
        const TLorentzVector p_pion1_MC(tau_pion1_TRUEP_X,tau_pion1_TRUEP_Y,tau_pion1_TRUEP_Z,tau_pion1_TRUEP_E);
        const TLorentzVector p_pion2_MC(tau_pion2_TRUEP_X,tau_pion2_TRUEP_Y,tau_pion2_TRUEP_Z,tau_pion2_TRUEP_E);
        const TLorentzVector p3piMC = (p_pion0_MC+p_pion1_MC+p_pion2_MC);
        const TLorentzVector pTauMC(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);

        // Angle between tau and 3pi in lab system

        thetatauMC = pTauMC.Vect().Angle(p3piMC.Vect());

        // Missing mass, W* mass and q2 

        const TLorentzVector pLostMC = pBMC-pD0MC-p3piMC;
        const TLorentzVector pWMC = pBMC-pD0MC;

        MLostMC = pLostMC.M();
        MWMC = pWMC.M();
        q2MC = 1.e-6*MWMC*MWMC;
        if (MWMC<0) q2MC = -1.e-6*MWMC*MWMC;

        // Missing mass, W* mass and q2 in B-->D* decays

        const TLorentzVector pLostDstMC = pBMC-pDstMC-p3piMC;
        const TLorentzVector pWDstMC = pBMC-pDstMC;

        double MWDstMC = pWDstMC.M();
        q2DstMC = 1.e-6*MWDstMC*MWDstMC;
        if (MWDstMC<0) q2DstMC = -1.e-6*MWDstMC*MWDstMC;

        // Helicity of tau lepton (supposing B^- --> D0 tau nu decays)

        TLorentzVector pBMC_WCMS(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        TLorentzVector pTauMC_WCMS(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);

        pBMC_WCMS.Boost(-pWMC.Px()/pWMC.E(),-pWMC.Py()/pWMC.E(),-pWMC.Pz()/pWMC.E());
        pTauMC_WCMS.Boost(-pWMC.Px()/pWMC.E(),-pWMC.Py()/pWMC.E(),-pWMC.Pz()/pWMC.E());

        double angleTauMC = pBMC_WCMS.Vect().Angle(pTauMC_WCMS.Vect());
        cosThetaTauMC = cos(angleTauMC);

        // Helicity of tau lepton (supposing B^0 --> D*- tau nu decays)

        TLorentzVector pBMC_Dst_WCMS(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        TLorentzVector pTauMC_Dst_WCMS(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);

        pBMC_Dst_WCMS.Boost(-pWDstMC.Px()/pWDstMC.E(),-pWDstMC.Py()/pWDstMC.E(),-pWDstMC.Pz()/pWDstMC.E());
        pTauMC_Dst_WCMS.Boost(-pWDstMC.Px()/pWDstMC.E(),-pWDstMC.Py()/pWDstMC.E(),-pWDstMC.Pz()/pWDstMC.E());

        double angleTauDstMC = pBMC_Dst_WCMS.Vect().Angle(pTauMC_Dst_WCMS.Vect());
        cosThetaTauDstMC = cos(angleTauDstMC);


        // --- ??? ----

        TLorentzVector ppion0MC0(tau_pion0_TRUEP_X,tau_pion0_TRUEP_Y,tau_pion0_TRUEP_Z,tau_pion0_TRUEP_E);
        TLorentzVector ppion1MC0(tau_pion1_TRUEP_X,tau_pion1_TRUEP_Y,tau_pion1_TRUEP_Z,tau_pion1_TRUEP_E);
        TLorentzVector ppion2MC0(tau_pion2_TRUEP_X,tau_pion2_TRUEP_Y,tau_pion2_TRUEP_Z,tau_pion2_TRUEP_E);
        TLorentzVector p3piMC1 = (ppion0MC0+ppion1MC0+ppion2MC0);
        TLorentzVector p3piMC2 = (ppion0MC0+ppion1MC0+ppion2MC0);
        TLorentzVector pTauMC1(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);

        //TLorentzVector pDstMC3(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);
        TLorentzVector pDstMC3(D_MC_MOTHER_TRUEP_X,D_MC_MOTHER_TRUEP_Y,D_MC_MOTHER_TRUEP_Z,D_MC_MOTHER_TRUEP_E);
        TLorentzVector pD0MC3(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);
        TLorentzVector p3piMC3 = p3piMC1;
        TLorentzVector pTauMC3(tau_TRUEP_X,tau_TRUEP_Y,tau_TRUEP_Z,tau_TRUEP_E);
        TLorentzVector pBMC3(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        TLorentzVector pBMC32(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        TLorentzVector pWMC3 = pWMC;

        TVector3 pTD0vMC = pBMC.Vect().Unit().Cross(pD0MC.Vect());
        pTD0vMC = pBMC.Vect().Unit().Cross(pTD0vMC);
        pTD0MC = pTD0vMC.Mag();

        TVector3 pTTauvMC = pTauMC.Vect().Unit().Cross(p3piMC.Vect());
        pTTauvMC = pTauMC.Vect().Unit().Cross(pTTauvMC);
        pTTauMC = pTTauvMC.Mag();

        TVector3 pTTauBvMC = pBMC.Vect().Unit().Cross(p3piMC.Vect());
        pTTauBvMC = pBMC.Vect().Unit().Cross(pTTauBvMC);
        pTTauBMC = pTTauBvMC.Mag();

        // --- //

        tauPHIMC = -1.E8;

        //if (MW>0) {
          pBMC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          pD0MC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          p3piMC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          pTauMC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          pDstMC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          pWMC3.Boost(-pBMC32.Px()/pBMC32.E(),-pBMC32.Py()/pBMC32.E(),-pBMC32.Pz()/pBMC32.E());
          TVector3 nv1mc = pDstMC3.Vect().Unit().Cross(pD0MC3.Vect().Unit());
          //TVector3 nv2mc = pTauMC3.Vect().Unit().Cross(p3piMC3.Vect().Unit());
          TVector3 nv2mc = pWMC3.Vect().Unit().Cross(pTauMC3.Vect().Unit());
          TVector3 Vxmc = nv1mc.Unit();
          TVector3 Vzmc = pDstMC3.Vect().Unit(); 
          TVector3 Vymc = (Vzmc.Cross(Vxmc)).Unit();
          double cosvmc = nv2mc.Dot(Vxmc);
          double sinvmc = nv2mc.Dot(Vymc);
          tauPHIMC = atan2(sinvmc,cosvmc);
        //}

        TLorentzVector pDstMC0(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);
        ppion0MC0.Boost(-p3piMC1.Px()/p3piMC1.E(),-p3piMC1.Py()/p3piMC1.E(),-p3piMC1.Pz()/p3piMC1.E());
        ppion1MC0.Boost(-p3piMC1.Px()/p3piMC1.E(),-p3piMC1.Py()/p3piMC1.E(),-p3piMC1.Pz()/p3piMC1.E());
        ppion2MC0.Boost(-p3piMC1.Px()/p3piMC1.E(),-p3piMC1.Py()/p3piMC1.E(),-p3piMC1.Pz()/p3piMC1.E());
        pTauMC1.Boost(-p3piMC1.Px()/p3piMC1.E(),-p3piMC1.Py()/p3piMC1.E(),-p3piMC1.Pz()/p3piMC1.E());
        pDstMC0.Boost(-p3piMC1.Px()/p3piMC1.E(),-p3piMC1.Py()/p3piMC1.E(),-p3piMC1.Pz()/p3piMC1.E());

        //TVector3 n3piMC = ppion0MC0.Vect().Cross(ppion1MC0.Vect());
        TVector3 n3piMC = ppion0MC0.Vect().Cross(ppion2MC0.Vect());

        double angleTau3piMC = n3piMC.Angle(pTauMC1.Vect());
        double angleTau3pi2MC = n3piMC.Angle(pDstMC0.Vect());

        cosTau3piMC = cos(angleTau3piMC);
        cosTau3pi2MC = cos(angleTau3pi2MC);

        TLorentzVector pBMC_DstCMS(B_TRUEP_X,B_TRUEP_Y,B_TRUEP_Z,B_TRUEP_E);
        TLorentzVector pD0MC_DstCMS(D_TRUEP_X,D_TRUEP_Y,D_TRUEP_Z,D_TRUEP_E);

        pBMC_DstCMS.Boost(-pDstMC.Px()/pDstMC.E(),-pDstMC.Py()/pDstMC.E(),-pDstMC.Pz()/pDstMC.E());
        pD0MC_DstCMS.Boost(-pDstMC.Px()/pDstMC.E(),-pDstMC.Py()/pDstMC.E(),-pDstMC.Pz()/pDstMC.E());
        p3piMC2.Boost(-pDstMC.Px()/pDstMC.E(),-pDstMC.Py()/pDstMC.E(),-pDstMC.Pz()/pDstMC.E());

        double angle1MC = pD0MC_DstCMS.Vect().Angle(p3piMC2.Vect());
        cosTheta1MC = cos(angle1MC);

        double angle1BMC = pD0MC_DstCMS.Vect().Angle(pBMC_DstCMS.Vect());
        cosTheta1BMC = cos(angle1BMC);


      }

      // Charged Isolation

      minChi2TauIsosig = 999.;
      minChi2TauIsosig0 = 999.;
      minChi2TauIsosig1 = 999.;
      minDOCAVXTauIsosig = 999.;
      minDOCAVXTauIsosig0 = 999.;
      minDOCAVXTauIsosig1 = 999.;
      minDOCAVXTauIsoAll = 999.;
      minDOCAVXTauIsoAll0 = 999.;
      minDOCAVXTauIsoAll1 = 999.;

      nTauIso5sig = 0;
      nTauIso5sig0 = 0;
      nTauIso5sig1 = 0;
      nTauIso9sig = 0;
      nTauIso9sig0 = 0;
      nTauIso9sig1 = 0;
      nTauIso25sig = 0;
      nTauIso25sig0 = 0;
      nTauIso25sig1 = 0;

      nTauIso25sig0_pi = 0;
      nTauIso25sig0_e = 0;
      nTauIso25sig0_mu = 0;

      nBTauIso25sig0 = 0;
      nBTauIso25sig0_tau = 0;
      nBTauIso25sig0_B = 0;
      nBTauIso25sig0_Dst = 0;
      nBTauIso25sig0_Dst_WS = 0;

      nTauIso5sig0_Dst = 0;
      nTauIso9sig0_Dst = 0;
      nTauIso25sig0_Dst = 0;
      nTauIso5sig0_Dst_WS = 0;
      nTauIso9sig0_Dst_WS = 0;
      nTauIso25sig0_Dst_WS = 0;

      bool alreadyInDecay = false;
      bool trackalreadyInDecay = false;
      int nPitau = 0;
      int nKtau = 0;
      bool thereisPi3 = false;
      bool thereisPi4 = false;
      bool thereisPi5 = false;
      bool thereisPi6 = false;
      bool thereisK3 = false;
      bool thereisK4 = false;
      TLorentzVector ppion3;
      TLorentzVector ppion4;
      TLorentzVector ppion5;
      TLorentzVector ppion6;
      TLorentzVector pK3;
      TLorentzVector pK4;

      for (int j=0;j<tau_isoPlus_nGood;j++) {

        if (tau_isoPlus_trk_LOKI_TYPE[j]==1) continue; 
        //if (tau_isoPlus_trk_LOKI_TYPE[j]!=3) continue; 

        double diffChi2 = tau_isoPlus_trk_LOKI_IPChi2[j];
        double diffChi2_B = tau_isoPlus_trk_LOKI_IPChi2MOTHER[j];
        double DOCAVX = tau_isoPlus_trk_LOKI_DOCAVX[j];
        //double DOCAVX_B = tau_isoPlus_trk_LOKI_DOCAVXMOTHER[j];

        bool PassCuts = true;

        if (tau_isoPlus_trk_LOKI_IPChi2PV[j]<4 || 
            sqrt(tau_isoPlus_trk_LOKI_PX[j]*tau_isoPlus_trk_LOKI_PX[j] + tau_isoPlus_trk_LOKI_PY[j]*tau_isoPlus_trk_LOKI_PY[j])<250) PassCuts = false;

        alreadyInDecay = false; 
        trackalreadyInDecay = false; 

        if (abs(tau_isoPlus_trk_LOKI_P[j]-D_pi_P)<0.1) alreadyInDecay = true; 
        if (abs(tau_isoPlus_trk_LOKI_P[j]-D_K_P)<0.1)  alreadyInDecay = true; 

        if( abs(tau_isoPlus_trk_LOKI_PX[j]/tau_isoPlus_trk_LOKI_PZ[j]-D_pi_PX/D_pi_PZ)<0.001 
         && abs(tau_isoPlus_trk_LOKI_PY[j]/tau_isoPlus_trk_LOKI_PZ[j]-D_pi_PY/D_pi_PZ)<0.001) trackalreadyInDecay = true;

        if( abs(tau_isoPlus_trk_LOKI_PX[j]/tau_isoPlus_trk_LOKI_PZ[j]-D_K_PX/D_K_PZ)<0.001 
         && abs(tau_isoPlus_trk_LOKI_PY[j]/tau_isoPlus_trk_LOKI_PZ[j]-D_K_PY/D_K_PZ)<0.001) trackalreadyInDecay = true;

        bool ifound1 = false;

        for (int k=0;k<j;k++) { if (abs(tau_isoPlus_trk_LOKI_P[j]-tau_isoPlus_trk_LOKI_P[k])<0.1) ifound1 = true; }

        if (ifound1) continue;

        // Remove tracks already in decay:

        if( abs(tau_isoPlus_trk_LOKI_PX[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PX/tau_pion0_PZ)<0.001
         && abs(tau_isoPlus_trk_LOKI_PY[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PY/tau_pion0_PZ)<0.001) continue;

        if( abs(tau_isoPlus_trk_LOKI_PX[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PX/tau_pion1_PZ)<0.001 
         && abs(tau_isoPlus_trk_LOKI_PY[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PY/tau_pion1_PZ)<0.001) continue;

        if( abs(tau_isoPlus_trk_LOKI_PX[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PX/tau_pion2_PZ)<0.001 
         && abs(tau_isoPlus_trk_LOKI_PY[j]/tau_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PY/tau_pion2_PZ)<0.001) continue;

        // Fill info:

        if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsoAll) { minDOCAVXTauIsoAll = tau_isoPlus_trk_LOKI_DOCAVX[j]; }

        if (alreadyInDecay) {
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsoAll1) { minDOCAVXTauIsoAll1 = tau_isoPlus_trk_LOKI_DOCAVX[j]; }
        } else if (!trackalreadyInDecay) {
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsoAll0) { minDOCAVXTauIsoAll0 = tau_isoPlus_trk_LOKI_DOCAVX[j]; }
        }

        if (!PassCuts) continue;

        if (diffChi2>-0.1 && diffChi2<minChi2TauIsosig) { minChi2TauIsosig = tau_isoPlus_trk_LOKI_IPChi2[j]; }
        if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsosig) { minDOCAVXTauIsosig = tau_isoPlus_trk_LOKI_DOCAVX[j]; }

        if (alreadyInDecay) {

          if (diffChi2>-0.1 && diffChi2<minChi2TauIsosig1) { minChi2TauIsosig1 = tau_isoPlus_trk_LOKI_IPChi2[j]; }
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsosig1) { minDOCAVXTauIsosig1 = tau_isoPlus_trk_LOKI_DOCAVX[j]; }

          if (diffChi2>-0.1 && diffChi2<5.) { nTauIso5sig += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nTauIso9sig += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nTauIso25sig += 1; }

          if (diffChi2>-0.1 && diffChi2<5.) { nTauIso5sig1 += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nTauIso9sig1 += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nTauIso25sig1 += 1; }

        } else if (!trackalreadyInDecay) {

          if (diffChi2>-0.1 && diffChi2<minChi2TauIsosig0) { minChi2TauIsosig0 = tau_isoPlus_trk_LOKI_IPChi2[j]; }
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXTauIsosig0) { minDOCAVXTauIsosig0 = tau_isoPlus_trk_LOKI_DOCAVX[j]; }

          if (diffChi2>-0.1 && diffChi2<5.) { nTauIso5sig += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nTauIso9sig += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nTauIso25sig += 1; }

          if (diffChi2>-0.1 && diffChi2<5.) {
            nTauIso5sig0 += 1;

            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1) nTauIso5sig0_Dst += 1;
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1) nTauIso5sig0_Dst_WS += 1;

            // Alessandra: M3piK, M5pi, M3pi2K
            TLorentzVector ptau_extraPi;
            TLorentzVector ptau_extraK;

            if(tau_isoPlus_trk_LOKI_PIDK[j]<1){

              ptau_extraPi.SetXYZM(tau_isoPlus_trk_LOKI_PX[j],tau_isoPlus_trk_LOKI_PY[j],tau_isoPlus_trk_LOKI_PZ[j],mpi);
              ++nPitau;

              if(tau_ID*tau_isoPlus_trk_LOKI_ID[j]>0){
                if(!thereisPi3){
                  ppion3 = ptau_extraPi;
                  thereisPi3 = true;
                }
                else if(!thereisPi5){
                  ppion5 = ptau_extraPi;
                  thereisPi5 = true;
                }
              } else if(!thereisPi4){
                ppion4 = ptau_extraPi;
                thereisPi4 = true;
              } else if(!thereisPi6){
                ppion6 = ptau_extraPi;
                thereisPi6 = true;
              }

            } else {

              ptau_extraK.SetXYZM(tau_isoPlus_trk_LOKI_PX[j],tau_isoPlus_trk_LOKI_PY[j],tau_isoPlus_trk_LOKI_PZ[j],mK);
              ++nKtau;
              if (!thereisK3 && tau_ID*tau_isoPlus_trk_LOKI_ID[j]>0){
                //pi+pi-pi+K-pi+ (or cc)
                thereisK3 = true;
                pK3 = ptau_extraK;
      
              }
              if (!thereisK4 && tau_ID*tau_isoPlus_trk_LOKI_ID[j]<0){
                //pi+pi-pi+pi-K+ (or cc)
                thereisK4 = true;
                pK4 = ptau_extraK;
              }
            }
          }
          if (diffChi2>-0.1 && diffChi2<9.) {
            nTauIso9sig0 += 1;
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1) {nTauIso9sig0_Dst += 1;}
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1) {nTauIso9sig0_Dst_WS += 1;}
          }
          if (diffChi2>-0.1 && diffChi2<25.) {
            nTauIso25sig0 += 1;
            if (abs(tau_isoPlus_trk_LOKI_ID[j]) == 211) { nTauIso25sig0_pi += 1; }
            if (abs(tau_isoPlus_trk_LOKI_ID[j]) == 11) { nTauIso25sig0_e += 1; }
            if (abs(tau_isoPlus_trk_LOKI_ID[j]) == 13) { nTauIso25sig0_mu += 1; }
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1) { nTauIso25sig0_Dst += 1; }
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1) { nTauIso25sig0_Dst_WS += 1; }
          }
          if ( (diffChi2>-0.1 && diffChi2<25.) || (diffChi2_B>-0.1 && diffChi2_B<25.) ) {
            nBTauIso25sig0 += 1;
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1) {nBTauIso25sig0_Dst += 1;}
            if (abs(tau_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1) {nBTauIso25sig0_Dst_WS += 1;}
            if (diffChi2<diffChi2_B) {
              nBTauIso25sig0_tau += 1;
            } else {
              nBTauIso25sig0_B += 1;
            }
          }
        }

        if(nTauIso25sig-nTauIso25sig0-nTauIso25sig1 != 0) {
          cout<<"Problem with Tau charged Isolation ? "<<endl;
          cout<<"nTauIso25sig-nTauIso25sig0-nTauIso25sig1 "<<nTauIso25sig-nTauIso25sig0-nTauIso25sig1<<endl;
        }

        if(nTauIso25sig-nTauIso25sig0>2) {
          cout<<"tau_isoPlus_nGood "<<tau_isoPlus_nGood<<endl;
          cout<<"ix tau_isoPlus_trk_LOKI_P[ix] tau_isoPlus_trk_LOKI_ID[ix] tau_isoPlus_trk_LOKI_IPChi2[ix]  D_pi_P D_K_P"<<endl;
          for (int ix=0;ix<tau_isoPlus_nGood;ix++) {
            cout<<ix<<" "<<tau_isoPlus_trk_LOKI_P[ix]<<" "<<tau_isoPlus_trk_LOKI_ID[ix]<<" "<<tau_isoPlus_trk_LOKI_IPChi2[ix]<<" "<<D_pi_P<<" "<<D_K_P<<endl;
          }
          for (int ix=0;ix<nTauIso25sig;ix++) {
            cout<<ix<<" Chi2TauIso + tau_ENDVERTEX_CHI2 "<<tau_ENDVERTEX_CHI2<<endl;
          }

          cout<<"nTauIso25sig "<<nTauIso25sig<<endl;
          cout<<"nTauIso25sig0 "<<nTauIso25sig0<<endl;
          cout<<"nTauIso25sig1 "<<nTauIso25sig1<<endl;
        }

      }

      m_4pi = -10.;
      m_5pi = -10.;
      m_7pi = -10.;
      m_3piK = -10.;
      m_3pi2K = -10.;
      m_2K = -10.;

      if (thereisPi3) {
        m_4pi =  (ptau_pion0 + ptau_pion1 + ptau_pion2 + ppion3).M();
      }
      if (thereisPi3 && thereisPi4) {
        m_5pi =  (ptau_pion0 + ptau_pion1 + ptau_pion2 + ppion3 + ppion4).M();
        if (thereisPi5 && thereisPi6) {
          m_7pi =  (ptau_pion0 + ptau_pion1 + ptau_pion2 + ppion3 + ppion4 + ppion5 + ppion6 ).M();
        }
      }

      if (thereisK3) {
        m_3piK = (ptau_pion0 + ptau_pion1 + ptau_pion2 + pK3).M();
      }
      if (thereisK3 && thereisK4) {
        m_3pi2K = (ptau_pion0 + ptau_pion1 + ptau_pion2 + pK3 + pK4).M();
        m_2K = (pK3 + pK4).M();
      }

      // Extra photons

      TLorentzVector p_gamma_Max_tau00; p_gamma_Max_tau00.SetXYZM(tau_EW_000_nc_maxPt_PX,tau_EW_000_nc_maxPt_PY,tau_EW_000_nc_maxPt_PZ,0.);
      TLorentzVector p_gamma_Sec_tau00; p_gamma_Sec_tau00.SetXYZM(tau_EW_000_nc_secPt_PX,tau_EW_000_nc_secPt_PY,tau_EW_000_nc_secPt_PZ,0.);
      TLorentzVector p_gamma_Thi_tau00; p_gamma_Thi_tau00.SetXYZM(tau_EW_000_nc_thiPt_PX,tau_EW_000_nc_thiPt_PY,tau_EW_000_nc_thiPt_PZ,0.);
      TLorentzVector p_gamma_Max_tau05; p_gamma_Max_tau05.SetXYZM(tau_EW_050_nc_maxPt_PX,tau_EW_050_nc_maxPt_PY,tau_EW_050_nc_maxPt_PZ,0.);
      TLorentzVector p_gamma_Sec_tau05; p_gamma_Sec_tau05.SetXYZM(tau_EW_050_nc_secPt_PX,tau_EW_050_nc_secPt_PY,tau_EW_050_nc_secPt_PZ,0.);
      TLorentzVector p_gamma_Thi_tau05; p_gamma_Thi_tau05.SetXYZM(tau_EW_050_nc_thiPt_PX,tau_EW_050_nc_thiPt_PY,tau_EW_050_nc_thiPt_PZ,0.);

      // Cone 0.00 (no cut)
      m_2g_00 = (p_gamma_Max_tau00+p_gamma_Sec_tau00).M();

      m_B_gammaMax00 = (ptau+pD0+p_gamma_Max_tau00).M();
      m_B_gammaSec00 = (ptau+pD0+p_gamma_Sec_tau00).M();
      m_B_gammaThi00 = (ptau+pD0+p_gamma_Thi_tau00).M();
      m_B_2g_00 = (ptau+pD0+p_gamma_Max_tau00+p_gamma_Sec_tau00).M();

      m_3pi_gammaMax00 = (ptau+p_gamma_Max_tau00).M();
      m_3pi_gammaSec00 = (ptau+p_gamma_Sec_tau00).M();
      m_3pi_gammaThi00 = (ptau+p_gamma_Thi_tau00).M();
      m_3pi_2g_00 = (ptau+p_gamma_Max_tau00+p_gamma_Sec_tau00).M();

      m_01_gammaMax00 = (ptau_pion0+ptau_pion1+p_gamma_Max_tau00).M();
      m_01_gammaSec00 = (ptau_pion0+ptau_pion1+p_gamma_Sec_tau00).M();
      m_01_gammaThi00 = (ptau_pion0+ptau_pion1+p_gamma_Thi_tau00).M();
      m_01_2g_00 = (ptau_pion0+ptau_pion1+p_gamma_Max_tau00+p_gamma_Sec_tau00).M();

      m_12_gammaMax00 = (ptau_pion1+ptau_pion2+p_gamma_Max_tau00).M();
      m_12_gammaSec00 = (ptau_pion1+ptau_pion2+p_gamma_Sec_tau00).M();
      m_12_gammaThi00 = (ptau_pion1+ptau_pion2+p_gamma_Thi_tau00).M();
      m_12_2g_00 = (ptau_pion1+ptau_pion2+p_gamma_Max_tau00+p_gamma_Sec_tau00).M();

      m_02_gammaMax00 = (ptau_pion0+ptau_pion2+p_gamma_Max_tau00).M();
      m_02_gammaSec00 = (ptau_pion0+ptau_pion2+p_gamma_Sec_tau00).M();
      m_02_gammaThi00 = (ptau_pion0+ptau_pion2+p_gamma_Thi_tau00).M();

      // Cone 0.50
      m_2g_05 = (p_gamma_Max_tau05+p_gamma_Sec_tau05).M();

      m_B_gammaMax05 = (ptau+pD0+p_gamma_Max_tau05).M();
      m_B_gammaSec05 = (ptau+pD0+p_gamma_Sec_tau05).M();
      m_B_gammaThi05 = (ptau+pD0+p_gamma_Thi_tau05).M();
      m_B_2g_05 = (ptau+pD0+p_gamma_Max_tau05+p_gamma_Sec_tau05).M();

      m_3pi_gammaMax05 = (ptau+p_gamma_Max_tau05).M();
      m_3pi_gammaSec05 = (ptau+p_gamma_Sec_tau05).M();
      m_3pi_gammaThi05 = (ptau+p_gamma_Thi_tau05).M();
      m_3pi_2g_05 = (ptau+p_gamma_Max_tau05+p_gamma_Sec_tau05).M();

      m_01_gammaMax05 = (ptau_pion0+ptau_pion1+p_gamma_Max_tau05).M();
      m_01_gammaSec05 = (ptau_pion0+ptau_pion1+p_gamma_Sec_tau05).M();
      m_01_gammaThi05 = (ptau_pion0+ptau_pion1+p_gamma_Thi_tau05).M();
      m_01_2g_05 = (ptau_pion0+ptau_pion1+p_gamma_Max_tau05+p_gamma_Sec_tau05).M();

      m_12_gammaMax05 = (ptau_pion1+ptau_pion2+p_gamma_Max_tau05).M();
      m_12_gammaSec05 = (ptau_pion1+ptau_pion2+p_gamma_Sec_tau05).M();
      m_12_gammaThi05 = (ptau_pion1+ptau_pion2+p_gamma_Thi_tau05).M();
      m_12_2g_05 = (ptau_pion1+ptau_pion2+p_gamma_Max_tau05+p_gamma_Sec_tau05).M();

      m_02_gammaMax05 = (ptau_pion0+ptau_pion2+p_gamma_Max_tau05).M();
      m_02_gammaSec05 = (ptau_pion0+ptau_pion2+p_gamma_Sec_tau05).M();
      m_02_gammaThi05 = (ptau_pion0+ptau_pion2+p_gamma_Thi_tau05).M();

      // Extra pi0's

      TLorentzVector p_pi0_Max_tau00; p_pi0_Max_tau00.SetXYZM(tau_EW_000_pi0_maxPt_PX,tau_EW_000_pi0_maxPt_PY,tau_EW_000_pi0_maxPt_PZ,mpi0);
      TLorentzVector p_pi0_Sec_tau00; p_pi0_Sec_tau00.SetXYZM(tau_EW_000_pi0_secPt_PX,tau_EW_000_pi0_secPt_PY,tau_EW_000_pi0_secPt_PZ,mpi0);
      TLorentzVector p_pi0_Thi_tau00; p_pi0_Thi_tau00.SetXYZM(tau_EW_000_pi0_thiPt_PX,tau_EW_000_pi0_thiPt_PY,tau_EW_000_pi0_thiPt_PZ,mpi0);
      TLorentzVector p_pi0_Max_tau05; p_pi0_Max_tau05.SetXYZM(tau_EW_050_pi0_maxPt_PX,tau_EW_050_pi0_maxPt_PY,tau_EW_050_pi0_maxPt_PZ,mpi0);
      TLorentzVector p_pi0_Sec_tau05; p_pi0_Sec_tau05.SetXYZM(tau_EW_050_pi0_secPt_PX,tau_EW_050_pi0_secPt_PY,tau_EW_050_pi0_secPt_PZ,mpi0);
      TLorentzVector p_pi0_Thi_tau05; p_pi0_Thi_tau05.SetXYZM(tau_EW_050_pi0_thiPt_PX,tau_EW_050_pi0_thiPt_PY,tau_EW_050_pi0_thiPt_PZ,mpi0);

      // Cone 0.00 (no cut)
      m_2pi0_00 = (p_pi0_Max_tau00+p_pi0_Sec_tau00).M();

      m_B_pi0Max00 = (ptau+pD0+p_pi0_Max_tau00).M();
      m_B_pi0Sec00 = (ptau+pD0+p_pi0_Sec_tau00).M();
      m_B_pi0Thi00 = (ptau+pD0+p_pi0_Thi_tau00).M();
      m_B_2pi0_00 = (ptau+pD0+p_pi0_Max_tau00+p_pi0_Sec_tau00).M();

      m_3pi_pi0Max00 = (ptau+p_pi0_Max_tau00).M();
      m_3pi_pi0Sec00 = (ptau+p_pi0_Sec_tau00).M();
      m_3pi_pi0Thi00 = (ptau+p_pi0_Thi_tau00).M();
      m_3pi_2pi0_00 = (ptau+p_pi0_Max_tau00+p_pi0_Sec_tau00).M();

      m_01_pi0Max00 = (ptau_pion0+ptau_pion1+p_pi0_Max_tau00).M();
      m_01_pi0Sec00 = (ptau_pion0+ptau_pion1+p_pi0_Sec_tau00).M();
      m_01_pi0Thi00 = (ptau_pion0+ptau_pion1+p_pi0_Thi_tau00).M();

      m_12_pi0Max00 = (ptau_pion1+ptau_pion2+p_pi0_Max_tau00).M();
      m_12_pi0Sec00 = (ptau_pion1+ptau_pion2+p_pi0_Sec_tau00).M();
      m_12_pi0Thi00 = (ptau_pion1+ptau_pion2+p_pi0_Thi_tau00).M();

      m_02_pi0Max00 = (ptau_pion0+ptau_pion2+p_pi0_Max_tau00).M();
      m_02_pi0Sec00 = (ptau_pion0+ptau_pion2+p_pi0_Sec_tau00).M();
      m_02_pi0Thi00 = (ptau_pion0+ptau_pion2+p_pi0_Thi_tau00).M();

      // Cone 0.50
      m_2pi0_05 = (p_pi0_Max_tau05+p_pi0_Sec_tau05).M();

      m_B_pi0Max05 = (ptau+pD0+p_pi0_Max_tau05).M();
      m_B_pi0Sec05 = (ptau+pD0+p_pi0_Sec_tau05).M();
      m_B_pi0Thi05 = (ptau+pD0+p_pi0_Thi_tau05).M();
      m_B_2pi0_05 = (ptau+pD0+p_pi0_Max_tau05+p_pi0_Sec_tau05).M();

      m_3pi_pi0Max05 = (ptau+p_pi0_Max_tau05).M();
      m_3pi_pi0Sec05 = (ptau+p_pi0_Sec_tau05).M();
      m_3pi_pi0Thi05 = (ptau+p_pi0_Thi_tau05).M();
      m_3pi_2pi0_05 = (ptau+p_pi0_Max_tau05+p_pi0_Sec_tau05).M();

      m_01_pi0Max05 = (ptau_pion0+ptau_pion1+p_pi0_Max_tau05).M();
      m_01_pi0Sec05 = (ptau_pion0+ptau_pion1+p_pi0_Sec_tau05).M();
      m_01_pi0Thi05 = (ptau_pion0+ptau_pion1+p_pi0_Thi_tau05).M();

      m_12_pi0Max05 = (ptau_pion1+ptau_pion2+p_pi0_Max_tau05).M();
      m_12_pi0Sec05 = (ptau_pion1+ptau_pion2+p_pi0_Sec_tau05).M();
      m_12_pi0Thi05 = (ptau_pion1+ptau_pion2+p_pi0_Thi_tau05).M();

      m_02_pi0Max05 = (ptau_pion0+ptau_pion2+p_pi0_Max_tau05).M();
      m_02_pi0Sec05 = (ptau_pion0+ptau_pion2+p_pi0_Sec_tau05).M();
      m_02_pi0Thi05 = (ptau_pion0+ptau_pion2+p_pi0_Thi_tau05).M();

      // Extra eta's

      TLorentzVector p_eta_Max_tau00; p_eta_Max_tau00.SetXYZM(tau_EW_000_eta_maxPt_PX,tau_EW_000_eta_maxPt_PY,tau_EW_000_eta_maxPt_PZ,meta);
      TLorentzVector p_eta_Sec_tau00; p_eta_Sec_tau00.SetXYZM(tau_EW_000_eta_secPt_PX,tau_EW_000_eta_secPt_PY,tau_EW_000_eta_secPt_PZ,meta);
      TLorentzVector p_eta_Thi_tau00; p_eta_Thi_tau00.SetXYZM(tau_EW_000_eta_thiPt_PX,tau_EW_000_eta_thiPt_PY,tau_EW_000_eta_thiPt_PZ,meta);
      TLorentzVector p_eta_Max_tau05; p_eta_Max_tau05.SetXYZM(tau_EW_050_eta_maxPt_PX,tau_EW_050_eta_maxPt_PY,tau_EW_050_eta_maxPt_PZ,meta);
      TLorentzVector p_eta_Sec_tau05; p_eta_Sec_tau05.SetXYZM(tau_EW_050_eta_secPt_PX,tau_EW_050_eta_secPt_PY,tau_EW_050_eta_secPt_PZ,meta);
      TLorentzVector p_eta_Thi_tau05; p_eta_Thi_tau05.SetXYZM(tau_EW_050_eta_thiPt_PX,tau_EW_050_eta_thiPt_PY,tau_EW_050_eta_thiPt_PZ,meta);

      // Cone 0.00 (no cut)
      m_B_etaMax00 = (ptau+pD0+p_eta_Max_tau00).M();
      m_B_etaSec00 = (ptau+pD0+p_eta_Sec_tau00).M();
      m_B_etaThi00 = (ptau+pD0+p_eta_Thi_tau00).M();

      m_3pi_etaMax00 = (ptau+p_eta_Max_tau00).M();
      m_3pi_etaSec00 = (ptau+p_eta_Sec_tau00).M();
      m_3pi_etaThi00 = (ptau+p_eta_Thi_tau00).M();

      m_01_etaMax00 = (ptau_pion0+ptau_pion1+p_eta_Max_tau00).M();
      m_01_etaSec00 = (ptau_pion0+ptau_pion1+p_eta_Sec_tau00).M();
      m_01_etaThi00 = (ptau_pion0+ptau_pion1+p_eta_Thi_tau00).M();

      m_12_etaMax00 = (ptau_pion1+ptau_pion2+p_eta_Max_tau00).M();
      m_12_etaSec00 = (ptau_pion1+ptau_pion2+p_eta_Sec_tau00).M();
      m_12_etaThi00 = (ptau_pion1+ptau_pion2+p_eta_Thi_tau00).M();

      m_02_etaMax00 = (ptau_pion0+ptau_pion2+p_eta_Max_tau00).M();
      m_02_etaSec00 = (ptau_pion0+ptau_pion2+p_eta_Sec_tau00).M();
      m_02_etaThi00 = (ptau_pion0+ptau_pion2+p_eta_Thi_tau00).M();

      // Cone 0.50
      m_B_etaMax05 = (ptau+pD0+p_eta_Max_tau05).M();
      m_B_etaSec05 = (ptau+pD0+p_eta_Sec_tau05).M();
      m_B_etaThi05 = (ptau+pD0+p_eta_Thi_tau05).M();

      m_3pi_etaMax05 = (ptau+p_eta_Max_tau05).M();
      m_3pi_etaSec05 = (ptau+p_eta_Sec_tau05).M();
      m_3pi_etaThi05 = (ptau+p_eta_Thi_tau05).M();

      m_01_etaMax05 = (ptau_pion0+ptau_pion1+p_eta_Max_tau05).M();
      m_01_etaSec05 = (ptau_pion0+ptau_pion1+p_eta_Sec_tau05).M();
      m_01_etaThi05 = (ptau_pion0+ptau_pion1+p_eta_Thi_tau05).M();

      m_12_etaMax05 = (ptau_pion1+ptau_pion2+p_eta_Max_tau05).M();
      m_12_etaSec05 = (ptau_pion1+ptau_pion2+p_eta_Sec_tau05).M();
      m_12_etaThi05 = (ptau_pion1+ptau_pion2+p_eta_Thi_tau05).M();

      m_02_etaMax05 = (ptau_pion0+ptau_pion2+p_eta_Max_tau05).M();
      m_02_etaSec05 = (ptau_pion0+ptau_pion2+p_eta_Sec_tau05).M();
      m_02_etaThi05 = (ptau_pion0+ptau_pion2+p_eta_Thi_tau05).M();


      // D Iso

      minChi2DIsosig = 999.;
      minChi2DIsosig0 = 999.;
      minChi2DIsosig1 = 999.;
      minDOCAVXDIsosig = 999.;
      minDOCAVXDIsosig0 = 999.;
      minDOCAVXDIsosig1 = 999.;

      nDIso5sig = 0;
      nDIso5sig0 = 0;
      nDIso5sig1 = 0;
      nDIso9sig = 0;
      nDIso9sig0 = 0;
      nDIso9sig1 = 0;
      nDIso25sig = 0;
      nDIso25sig0 = 0;
      nDIso25sig1 = 0;

      for (int j=0;j<D_isoPlus_nGood;j++) {

        if (D_isoPlus_trk_LOKI_IPChi2PV[j]<4 || 
            sqrt(D_isoPlus_trk_LOKI_PX[j]*D_isoPlus_trk_LOKI_PX[j] + D_isoPlus_trk_LOKI_PY[j]*D_isoPlus_trk_LOKI_PY[j])<250) continue;

        double diffChi2 = D_isoPlus_trk_LOKI_IPChi2[j];
        double DOCAVX = D_isoPlus_trk_LOKI_DOCAVX[j];

        bool ifound1 = 0;

        for (int k=0;k<j;k++) { if (abs(D_isoPlus_trk_LOKI_P[j]-D_isoPlus_trk_LOKI_P[k])<0.1) ifound1 = 1; }

        if (ifound1) continue;

        alreadyInDecay = false;
        trackalreadyInDecay = false;

        if (abs(D_isoPlus_trk_LOKI_P[j]-tau_pion0_P)<0.1) alreadyInDecay = true; 
        if (abs(D_isoPlus_trk_LOKI_P[j]-tau_pion1_P)<0.1) alreadyInDecay = true; 
        if (abs(D_isoPlus_trk_LOKI_P[j]-tau_pion2_P)<0.1) alreadyInDecay = true; 

        if( abs(D_isoPlus_trk_LOKI_PX[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PX/tau_pion0_PZ)<0.001
         && abs(D_isoPlus_trk_LOKI_PY[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PY/tau_pion0_PZ)<0.001) trackalreadyInDecay = true;

        if( abs(D_isoPlus_trk_LOKI_PX[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PX/tau_pion1_PZ)<0.001 
         && abs(D_isoPlus_trk_LOKI_PY[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PY/tau_pion1_PZ)<0.001) trackalreadyInDecay = true;

        if( abs(D_isoPlus_trk_LOKI_PX[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PX/tau_pion2_PZ)<0.001 
         && abs(D_isoPlus_trk_LOKI_PY[j]/D_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PY/tau_pion2_PZ)<0.001) trackalreadyInDecay = true;

        // Remove tracks already in decay:

        if( abs(D_isoPlus_trk_LOKI_PX[j]/D_isoPlus_trk_LOKI_PZ[j]-D_pi_PX/D_pi_PZ)<0.001 
         && abs(D_isoPlus_trk_LOKI_PY[j]/D_isoPlus_trk_LOKI_PZ[j]-D_pi_PY/D_pi_PZ)<0.001) continue;

        if( abs(D_isoPlus_trk_LOKI_PX[j]/D_isoPlus_trk_LOKI_PZ[j]-D_K_PX/D_K_PZ)<0.001 
         && abs(D_isoPlus_trk_LOKI_PY[j]/D_isoPlus_trk_LOKI_PZ[j]-D_K_PY/D_K_PZ)<0.001) continue;

        // Fill info:

        if (diffChi2>-0.1 && diffChi2<minChi2DIsosig) { minChi2DIsosig = D_isoPlus_trk_LOKI_IPChi2[j]; }
        if (DOCAVX>-0.1 && DOCAVX<minDOCAVXDIsosig) { minDOCAVXDIsosig = D_isoPlus_trk_LOKI_DOCAVX[j]; }

        if (alreadyInDecay) {

          if (diffChi2>-0.1 && diffChi2<minChi2DIsosig1) { minChi2DIsosig1 = D_isoPlus_trk_LOKI_IPChi2[j]; }
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXDIsosig1) { minDOCAVXDIsosig1 = D_isoPlus_trk_LOKI_DOCAVX[j]; }

          if (diffChi2>-0.1 && diffChi2<5.) { nDIso5sig += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nDIso9sig += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nDIso25sig += 1; }

          if (diffChi2>-0.1 && diffChi2<5.) { nDIso5sig1 += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nDIso9sig1 += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nDIso25sig1 += 1; }

        } else if(!trackalreadyInDecay) {

          if (diffChi2<minChi2DIsosig0) { minChi2DIsosig0 = D_isoPlus_trk_LOKI_IPChi2[j]; }
          if (DOCAVX>-0.1 && DOCAVX<minDOCAVXDIsosig0) { minDOCAVXDIsosig0 = D_isoPlus_trk_LOKI_DOCAVX[j]; }

          if (diffChi2>-0.1 && diffChi2<5.) { nDIso5sig += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nDIso9sig += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nDIso25sig += 1; }

          if (diffChi2>-0.1 && diffChi2<5.) { nDIso5sig0 += 1; }
          if (diffChi2>-0.1 && diffChi2<9.) { nDIso9sig0 += 1; }
          if (diffChi2>-0.1 && diffChi2<25.) { nDIso25sig0 += 1; }

        }

        if(nDIso25sig-nDIso25sig0-nDIso25sig1 != 0) {
          cout<<"Problem with D0 charged track Isolation ? "<<endl;
          cout<<nDIso25sig-nDIso25sig0-nDIso25sig1<<endl;
        }
        if(nDIso25sig-nDIso25sig0>3) {
          cout<<"D_isoPlus_nGood "<<D_isoPlus_nGood<<endl;
          cout<<"ix D_isoPlus_trk_LOKI_P[ix] D_isoPlus_trk_LOKI_ID[ix] D_isoPlus_trk_LOKI_IPChi2[ix]  tau_pion0_P tau_pion1_P tau_pion2_P"<<endl;
          for (int ix=0;ix<D_isoPlus_nGood;ix++) {
            cout<<ix<<" "<<D_isoPlus_trk_LOKI_P[ix]<<" "<<D_isoPlus_trk_LOKI_ID[ix]<<" "<<D_isoPlus_trk_LOKI_IPChi2[ix]<<" "<<tau_pion0_P<<" "<<tau_pion1_P<<" "<<tau_pion2_P<<endl;
          }

          cout<<"nDIso25sig "<<nDIso25sig<<endl;
          cout<<"nDIso25sig0 "<<nDIso25sig0<<endl;
        }

      }

      // B0 Iso

      minChi2BIsosig = 999.;
      minDOCAVXBIsosig = 999.;
      minDOCAVXBIsoAll = 999.;

      nB0Iso5sig = 0.;
      nB0Iso9sig = 0.;
      nB0Iso25sig = 0.;
      nB0Iso25sig_pi = 0.;
      nB0Iso25sig_e = 0.;
      nB0Iso25sig_mu = 0.;

      nB0Iso5sig_Dst = 0.;
      nB0Iso9sig_Dst = 0.;
      nB0Iso25sig_Dst = 0.;
      nB0Iso5sig_Dst_WS = 0.;
      nB0Iso9sig_Dst_WS = 0.;
      nB0Iso25sig_Dst_WS = 0.;

      for (int j=0;j<B_isoPlus_nGood;j++) {

        double diffChi2 = B_isoPlus_trk_LOKI_IPChi2[j];
        double DOCAVX = B_isoPlus_trk_LOKI_DOCAVX[j];

        bool PassCuts = true;

        if (B_isoPlus_trk_LOKI_IPChi2PV[j]<4 || 
            sqrt(B_isoPlus_trk_LOKI_PX[j]*B_isoPlus_trk_LOKI_PX[j] + B_isoPlus_trk_LOKI_PY[j]*B_isoPlus_trk_LOKI_PY[j])<250) PassCuts = false;

        bool ifound1 = false;

        for (int k=0;k<j;k++) {
          if (abs(B_isoPlus_trk_LOKI_P[j]-B_isoPlus_trk_LOKI_P[k])<0.1) ifound1 = true; 
        }

        if (ifound1) continue;

        // Remove tracks already in decay:

        if( abs(B_isoPlus_trk_LOKI_PX[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PX/tau_pion0_PZ)<0.001
         && abs(B_isoPlus_trk_LOKI_PY[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion0_PY/tau_pion0_PZ)<0.001) continue;

        if( abs(B_isoPlus_trk_LOKI_PX[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PX/tau_pion1_PZ)<0.001 
         && abs(B_isoPlus_trk_LOKI_PY[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion1_PY/tau_pion1_PZ)<0.001) continue;

        if( abs(B_isoPlus_trk_LOKI_PX[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PX/tau_pion2_PZ)<0.001 
         && abs(B_isoPlus_trk_LOKI_PY[j]/B_isoPlus_trk_LOKI_PZ[j]-tau_pion2_PY/tau_pion2_PZ)<0.001) continue;

        if( abs(B_isoPlus_trk_LOKI_PX[j]/B_isoPlus_trk_LOKI_PZ[j]-D_K_PX/D_K_PZ)<0.001 
         && abs(B_isoPlus_trk_LOKI_PY[j]/B_isoPlus_trk_LOKI_PZ[j]-D_K_PY/D_K_PZ)<0.001) continue;

        if( abs(B_isoPlus_trk_LOKI_PX[j]/B_isoPlus_trk_LOKI_PZ[j]-D_pi_PX/D_pi_PZ)<0.001 
         && abs(B_isoPlus_trk_LOKI_PY[j]/B_isoPlus_trk_LOKI_PZ[j]-D_pi_PY/D_pi_PZ)<0.001) continue;

        if (DOCAVX>-0.1 && DOCAVX<minDOCAVXBIsoAll) { minDOCAVXBIsoAll = B_isoPlus_trk_LOKI_DOCAVX[j]; }

        if (!PassCuts) continue;

        if (diffChi2>-0.1 && diffChi2<minChi2BIsosig) { minChi2BIsosig = B_isoPlus_trk_LOKI_IPChi2[j]; }
        if (DOCAVX>-0.1 && DOCAVX<minDOCAVXBIsosig) { minDOCAVXBIsosig = B_isoPlus_trk_LOKI_DOCAVX[j]; }

        if (diffChi2>-0.1 && diffChi2<5.) {
          nB0Iso5sig += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1 ) nB0Iso5sig_Dst += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1 ) nB0Iso5sig_Dst_WS += 1;
        }
        if (diffChi2>-0.1 && diffChi2<9.) {
          nB0Iso9sig += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1 ) nB0Iso9sig_Dst += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1 ) nB0Iso9sig_Dst_WS += 1;
        }
        if (diffChi2>-0.1 && diffChi2<25.) {
          nB0Iso25sig += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pion_PZ)>0.1 ) nB0Iso25sig_Dst += 1;
          if( abs(B_isoPlus_trk_LOKI_PZ[j]-Dst_pionWS_PZ)>0.1 ) nB0Iso25sig_Dst_WS += 1;
          if(abs(B_isoPlus_trk_LOKI_ID[j])==211) { nB0Iso25sig_pi += 1; }
          if(abs(B_isoPlus_trk_LOKI_ID[j])==11) { nB0Iso25sig_e += 1; }
          if(abs(B_isoPlus_trk_LOKI_ID[j])==13) { nB0Iso25sig_mu += 1; }

          //bool ifound2 = false;

          //for (int l=0;l<tau_isoPlus_nGood;l++) {
          //  if (abs(B_isoPlus_trk_LOKI_P[j]-tau_isoPlus_trk_LOKI_P[l])<0.1 &&
          //      tau_isoPlus_trk_LOKI_IPChi2[l]>-0.1 && tau_isoPlus_trk_LOKI_IPChi2[l]<25.) ifound2 = true; 
          //}
          //if(!ifound2) { nBTauIso25sig0 += 1; }
        }

      }

      // MissID

      TLorentzVector pi0pi; pi0pi.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mpi);
      TLorentzVector pi1pi; pi1pi.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mpi);
      TLorentzVector pi2pi; pi2pi.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mpi);
      TLorentzVector pi0K; pi0K.SetXYZM(tau_pion0_PX,tau_pion0_PY,tau_pion0_PZ,mK);
      TLorentzVector pi1K; pi1K.SetXYZM(tau_pion1_PX,tau_pion1_PY,tau_pion1_PZ,mK);
      TLorentzVector pi2K; pi2K.SetXYZM(tau_pion2_PX,tau_pion2_PY,tau_pion2_PZ,mK);

      M2piK = (pi0pi+pi1K+pi2pi).M();
      BM2piK = (pDst+pi0pi+pi1K+pi2pi).M();

      if (tau_pion0_PIDK>tau_pion2_PIDK) {
        M2piKmax = (pi0K+pi1pi+pi2pi).M();
      } else {
        M2piKmax = (pi0pi+pi1pi+pi2K).M();
      }

      if (tau_pion0_PIDK>tau_pion2_PIDK) {
        M2Kpi = (pi0K+pi1K+pi2pi).M();
      } else {
        M2Kpi = (pi0pi+pi1K+pi2K).M();
      }

      tree_out->Fill();

      if(ievt%100000 == 0) {
	tree_out->AutoSave();
      }

   } // End For Loop

   std::cout << "Initial tree entries:   " << tree->GetEntries()<< std::endl;
   std::cout << "Final tree entries:   " << tree_out->GetEntries()<< std::endl;

/*
   int totCandDst;
   int totCandSel;
   TBranch* br_totCandDst = tree_out->Branch("totCandDst",&totCandDst,"totCandDst/I");
   TBranch* br_totCandSel = tree_out->Branch("totCandSel",&totCandSel,"totCandSel/I");

   totCandSel = 1;
   totCandDst = 0;

   for (int i=0;i<tree_out->GetEntries();i++) {

//     if (i>0 && br_nCandDst->GetEntry(i-1>br_nCandDst->GetEntry(i))
     br_nCandDst->GetEntry(i);
     br_nCandSel->GetEntry(i);
     br_totCandidates->GetEntry(i);
     br_Dst_M->GetEntry(i);

     int nCandSeltmp = 0;

     if (nCandSel==0) {
       totCandSel = 1;
       totCandDst = 0;
       if(Dst_M>0) totCandDst = 1;
       if (i<tree_out->GetEntries()-1) {
         int j = 1;
         bool check = true;

         while(check){
           br_nCandSel->GetEntry(i+j);
           br_nCandDst->GetEntry(i+j);
           br_Dst_M->GetEntry(i+j);
           if (nCandSel>nCandSeltmp) {
             totCandSel ++;
             nCandSeltmp = nCandSel;
             if (Dst_M>0) totCandDst ++;
           } else {
             check = false;
           }
           j++;
         } 
       }
     }
     br_nCandSel->GetEntry(i);
     br_nCandDst->GetEntry(i);
     br_Dst_M->GetEntry(i);

     br_totCandSel->Fill();
     br_totCandDst->Fill();

   }
*/

   std::cout << "Autosaving tree_out." << std::endl;

   tree_out->AutoSave();

//   delete input;
//   delete f_out;

   std::cout << "Done!" << std::endl;

   return 0;

}
