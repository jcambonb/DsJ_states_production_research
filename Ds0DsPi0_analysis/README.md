# Study of the $D_{s0}^*(2317)^+ \to D_s^+ \pi^0$ production at LHC$b$ in $pp$ collisions at $\sqrt{s}=13$ TeV

## Introduction

In this folder we have the production analysis of the $D_{s0}^*(2317)^+$ hadron through $D_{s0}^*(2317)^+ \to D_s^+ \pi^0$ decay. Historically these was the most heated hadron of the $D_{sJ}$ spectrum due to the fact that the only well measured decay is to the $D_s^+ \pi^0$, so most of the studies were focused in it. Due to that, the $D_{sJ}$ study started with it, but is hard to work with due to the $D_{s1}(2460)^+ \to D_s^{*+} \pi^0$ peaking background which is completely overlapped with the signal due to the bad resolution. 

## Analysis characteristics

The following trigger decisions are used:

- L0:   *Ds_L0Global_TIS | Ds_L0HadronDecision_TOS*
- HLT1: *Ds_Hlt1TrackMVADecision_TOS | Ds_Hlt1TwoTrackMVADecision_TOS*
- HLT2: *Ds_Hlt2CharmHadDspToKmKpPipDecision_TOS*

And the stripping line is the following:

 - __DsstGammaForExcitedDsSpectroscopyLine__ for $D_{s1}(2536)^+ \to (D_{s}^{*+}\to (D_s^+ \to K^+ K^- \pi^+)\gamma)\gamma$  decay descriptor
 
This stripping line does not combine the two photons into a $\pi^0$, and that lead to a candidate duplication issue. To correct that, we use the particles of that stripping line to create a new algorithm with the DaVinci Selection Framework which has the correct decay descriptor. It is called __DsPi02ggTuple__ (More details in the **Ganga and DaVinci Scripts** repository)

## Folder Structure

For each term of the ratio, we will have one subfolder. The current ones are the following

1. Ds0 Yields
2. Normalization Yield
3. Selection Efficiencies

Moreover, for each term we will have different subfolders in order to organize even more our analysis. Tipically this subfolders are *MC_Analysis* and *Data_Analysis* where we distinguish the work for MonteCarlo data sets and for the real data measured by the experiment. Finally, at the deepest layer we have little folders for each statistical work that we do: fitting, variable cuts, BDT, ... Here we have the scripts that runs the code. Practically all of them are in python and they don't have too much lines of code.

## Current status

* A MC Request has been presented to get more simulated samples for both signals

* A BDT implemention was used to separate signal and background, but a greater signal sample is needed.

* Currently the analysis is focused on designing a fit model for the $D_s^+\pi^0$ invariant mass. For that we are using MonteCarlo samples for defining the signal and background PDFs and performing a constrained fit. The problem lies in trying trying to include and understand the $D_{s1}(2460)^+\to D_{s}^{*+}\pi^0$ background which is difficult to deal with since we loss the information of the $D_{s}^{*+}\to D_s^+ \gamma$ photon. Here we show an example of a constrained fit performed in the 2018 data:

![plot](Ds0_Yields/Data_Analysis/Fit_models/Graphs/Exp_MagD18_Ds0_mass_fit_CB_nfr_nfm.pdf "plot")

