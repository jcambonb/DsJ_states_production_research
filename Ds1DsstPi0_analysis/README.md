# $`D_{s1}(2460)^+ \to D_s^{*+} \pi^0`$ study at LHC$`b`$ in $pp$ collisions at $\sqrt{s}=13$ TeV

## Introduction

For the multiplicity analysis of both hadrons, $`D_{s0}^*(2317)^+`$ and $`D_{s1}(2460)^+`$, we have to take into account the $`D_{s1}(2460)^+ \to D_s^{*+} \pi^0`$ decay. That produces a peaking background in both analysis due to missing photons, but the worst one is in the $`D_{s0}^*(2317)^+ \to D_s^+ \pi^0`$ since it peaks in the same mass as the $`D_{s0}^*(2317)^+`$ due to Dalitz effects of $`D_s^+\gamma \pi^0$ final state. Combined with bad resolution due to photon measurement, the analysis gets quite challenger. So, a special treatment of this decay should be done

## Analysis characteristics

For $`D_{s1}(2460)^+ \to D_s^{*+} \pi^0`$ there is no stripping line. However, with the Selection Framework of DaVinci we can create an algorithm with 3 photon combinations. So, using the $`D_{s}^{*+}`$ from the __DsstGammaForExcitedDsSpectroscopyLine__ and combining two __StdLooseAllPhotons__ into a $\pi^0$, we create the  __DsstPi02ggTuple__, where we can reach the decay.

## Folder Structure

Since we are not doing an proper multiplicity analysis of this decay (at the moment), we will have only folders with codes that compares MC and Data histograms. However, we will follow the usual structure of subfolders that we included in the other decays treatment

## Current status

* We create a Data and MC sample with the __DsstPi02ggTuple__ algorithm. We have few signal in MC and we reach a peak in the $`D_{s1}(2460)^+`$ mass region in data. But we should be aware since the $`D_{s0}^*(2317)^+ \to D_s^+ \pi^0`$ is also a peaking background here.


